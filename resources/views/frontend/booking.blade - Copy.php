@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')

<div class="container">
 
  <div class="row">
    <div class="col-md-3 text-center">
      <h2 class="car_detail_name">{{$vehicles->name}}</h2>
      <h1 class="car_sub_detail">{{$vehicles->car_make->name}} {{$vehicles->car_model->name}}</h1>
      <img src="{{ asset('storage/app/'.$vehicles->image) }}" class="img-responsive" alt="" />
      <p>For {{$months ?? request()->after_month}} Months</p>
      <p>For {{$miles ?? request()->mile}} Miles Limit</p>
     <p>Price <span style="font-size: 21px;">£{{ $price ?? request()->price}}</span>  </p>
      <a href="#" onclick="history.back(1)">Update</a>
    </div>
    <div class="col-md-9">
      <div class="row form-group">
        <div class="col-xs-12"> 
          <ul class="nav nav-pills nav-justified thumbnail setup-panel">
            <li class="active"><a href="#step-1">
              <h4 class="list-group-item-heading"> Step 1</h4>
              
              <p class="list-group-item-text">Signup & Login</p>
            </a></li>
            <li class="disable"><a href="#step-2">
              <h4 class="list-group-item-heading">Step 2</h4>
              <p class="list-group-item-text">Documents & Information</p>
            </a></li>
       @if(request()->step && request()->step==3)
            <li class="active"><a href="#step-3">
       @else
            <li class="disable"><a href="#step-3">
       @endif       
              <h4 class="list-group-item-heading">Step 3</h4>
              <p class="list-group-item-text">Payment</p>
            </a></li>
          </ul>
        </div>
      </div>
      <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
          <div class="col-md-12 well text-center login">
            <h4 class="text-center">Login</h4>
            <div class="row">
              <div class="col-md-offset-3"></div>
              <div class="col-md-6">
                <div class="align-items-center text-center" id="signup">
                  <form action="" id="">   {{ csrf_field() }}
                    <div class="form-group">
                      <input type="email" required="required" class="form-control" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                      <input type="password" required="required"  class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                      <input id="activate-step-2" value="Login" type="submit" class="form-control btn-primary"  >
                    </div>
                    New User? <a href="#" onclick="showSignup()">Signup</a>
                  </form>
                </div>
              </div>
              <div class="col-md-offset-3"></div>
            </div> 
          </div>




          <div class="col-md-12 well text-center signup">
            <h4 class="text-center">New Signup</h4>
            <div class="row">
              <div class="col-md-offset-3"></div>
              <div class="col-md-6">
                <div class="align-items-center text-center" id="signup">
                  <form action="" id="">   {{ csrf_field() }}

                     <div class="form-group">
                      <input type="text" required="required" class="form-control" maxlength="20" placeholder="Enter Name">
                    </div>


                    <div class="form-group">
                      <input type="email" required="required" class="form-control" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                      <input type="password" required="required"  class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                      <input type="password" required="required"  class="form-control" placeholder="Enter Confirm Password">
                    </div>

                    <div class="form-group">
                      <input type="tel" required="required"  class="form-control" placeholder="Enter Phone">
                    </div>
                    <div class="form-group">
                      <input id="activate-step-2" value="Register" type="submit" class="form-control btn-primary"  >
                    </div>
                    Already Account? <a href="#" onclick="showLogin()">Login</a>
                  </form>
                </div>
              </div>
              <div class="col-md-offset-3"></div>
            </div>
            
          </div>



        </div>
      </div>
      <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
          <div class="col-md-12 well">
            <h4 class="text-center">Documents & Information</h4>
            <div class="row">
              
              <div class="col-md-12">
                <form action="{{route('upload_information') }}" enctype="multipart/form-data" id="upload_information" method="post">   {{ csrf_field() }}
                <input type="hidden" value="{{app('request')->input('id')}}" name="id" />
                <input type="hidden" value="{{$months ?? 4 }}" name="duration" />
                <input type="hidden" value="{{$miles ?? 1000}}" name="mile" />
                <input type="hidden" value="{{$price ?? '' }}" name="price" />

                <div class="row">
                  <div class="col-md-12">
                    
                    <div class="row">
                      <div class="col-md-6"><div class="form-group">DVLA Licence(Front)<input name="file_dvla" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="DVLA" required=""></div></div>
                      <div class="col-md-6"><div class="form-group">DVLA Licence(Back)<input name="file_cnic" id="file_cnic" class="form-control form-control-lg" type="file" placeholder="Passport" required=""></div></div> 
                    </div>  
                    
                     <div class="row">
                      <div class="col-md-6"><div class="form-group">Social Security<input name="file_utility" class="form-control form-control-lg" type="file" placeholder="Utitlity Bill" required=""></div></div>
                      
                    </div>   
                    
                  </div>
                  <div class="col-md-12">
                   
                    
                    <div class="row">
                      <div class="col-md-6"><div class="form-group"><input id="phone" name="phone" type="text" class="form-control" value="" placeholder="Phone"  required="required"  /></div></div>
                      <div class="col-md-6"><div class="form-group"><input id="address" name="address" type="text" class="form-control" value="" placeholder="Address"  required="required"  /></div></div>
                    </div>
                    
                    
                    <div class="row">
                      <div class="col-md-6"><div class="form-group"> <input type="text"  required="required"  class="form-control" value="" placeholder="DOB" name="dob" id="dob" /></div></div>
                      <div class="col-md-6"><input type="text" name="dvla" id="dvla" class="form-control" value="" placeholder="DVLA License Number" /></div></div>
                      
                      <div class="row">
                        <div class="col-md-6"><div class="form-group"> <input type="text"  required="required"  class="form-control" value="" placeholder="National Insurance Number" name="national_insu_numb" id="national_insu_numb" /></div></div>
                        <div class="col-md-6"><input type="text" name="pco_licence_no" id="pco_licence_no" class="form-control" value="" placeholder="PCO License Number" /></div></div>
                        <button type="submit"  class="btn btn-primary btn-lg text-center">Upload & Next >></button>
                        
                      </div>
                    </form></div>
                  </div>
                </div>
                
              </div>
              
            </div>
          </div>
          <div class="row setup-content" id="step-3">
            <div class="col-md-12 well">
              
              <h4 class="text-center">Payment</h4>
               <form 
                            role="form" 
                            action="{{ route('stripe.post') }}" 
                            method="post" 
                            class="require-validation"
                            data-cc-on-file="false"
                            data-stripe-publishable-key="{{ env('STRIPE_KEY') }}" 
                            id="payment-form">
                        @csrf
                        <input type="hidden" name="amount" value="{{$price}}" />
                        <input type="hidden" name="customer_name" value="ali khan" />
                        <input type="hidden" name="customer_email" value="alikhan@gmail.com" />
                        <input type="hidden" name="expire_month_after" value="4" />
                        <input type="hidden" name="v_id" value="{{app('request')->input('id')}}" />
                        <input type="hidden" name="booking_id" value="{{app('request')->input('booking_id') ?? ''}}" /> 
 
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> <input
                                    class='form-control' placeholder='Master'  size='4' type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 form-group  required'>
                                <label class='control-label'>Card Number</label> <input
                                    autocomplete='off' class='form-control card-number' placeholder='35-1111-11-40' size='20'
                                    type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-xs-12 col-md-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Month</label> <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
                            </div>
                            <div class='col-xs-12 col-md-4 form-group expiration required'>
                                <label class='control-label'>Expiration Year</label> <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
                            </div>
                        </div>
  
                        <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div> 
                        <div class='form-row row'>
                            <div class="col-xs-12 form-group text-center">
                                 
                                    <button class="btn btn-primary" type="submit" style="color:#fff" >PAY NOW ({{$price}})</button>
                                
                            </div>    
                        </div>
                    </form>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<script>


  function showSignup(){
  $('.signup').show();
  $('.login').hide();

}
function showLogin(){
  $('.signup').hide();
  $('.login').show();
}




$(document).ready(function() {




 // $('#btn_upload_info').click(function(event){  
    $( "#btn_upload_info" ).click(function() {  
                  event.preventDefault(); 
                var formData = new FormData($('#upload_information')[0]);
                formData.append('#file_dvla', $('input[type=file]')[0].file_dvla[0]);    
                //alert(formData);
                 // formData.append('#file_cnic', $('input[type=file]')[0].file_cnic[0]); 
                 //  $('#f_name').val(    $('input[type=file]').val()     );

                  $.ajax({
                  // url:"http://ip.jsontest.com/",
                  url: '{{ url("booking/upload_information") }}', 
                   method:"POST",
                  data:formData,
                   //dataType:'JSON',
                   contentType: false,
                   cache: false,
                   processData: false,
                   success:function(data)
                   {
                       alert(data);
                   },
                   error: function(data)
                        {
                            //console.log(data);
                              alert('error');
                        }
                  })
 });



var navListItems = $('ul.setup-panel li a'),
allWells = $('.setup-content');
allWells.hide();
navListItems.click(function(e)
{
e.preventDefault();
var $target = $($(this).attr('href')),
$item = $(this).closest('li');

if (!$item.hasClass('disabled')) {
navListItems.closest('li').removeClass('active');
$item.addClass('active');
allWells.hide();
$target.show();
}
});

$('ul.setup-panel li.active a').trigger('click');

// DEMO ONLY //
$('#activate-step-2').on('click', function(e) {
$('ul.setup-panel li:eq(1)').removeClass('disabled');
// $('ul.setup-panel li a[href="#step-1').addClass('done');
$('ul.setup-panel li a[href="#step-2"]').trigger('click');
$(this).remove();
})
//list-group-item-heading
//   <span class="glyphicon glyphicon-ok"></span>
$('#activate-step-3').on('click', function(e) {
$('ul.setup-panel li:eq(2)').removeClass('disabled');
$('ul.setup-panel li a[href="#step-3"]').trigger('click');
$(this).remove();
}) ;
@if(app('request')->input('step')==3)
$('ul.setup-panel li:eq(3)').removeClass('disabled');
@endif
});
</script>

 

 
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
   
    var $form         = $(".require-validation");
   
    $('form.require-validation').bind('submit', function(e) {
        var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
  
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
          var $input = $(el);
          if ($input.val() === '') {
            $input.parent().addClass('has-error');
            $errorMessage.removeClass('hide');
            e.preventDefault();
          }
        });
   
        if (!$form.data('cc-on-file')) {
          e.preventDefault();
          Stripe.setPublishableKey($form.data('stripe-publishable-key'));
          Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
          }, stripeResponseHandler);
        }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            /* token contains id, last4, and card type */
            var token = response['id'];
               
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
   
});
</script>
