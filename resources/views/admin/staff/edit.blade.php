@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
 	<div class="content-page"
    	<div class="content">
        	<div class="container-fluid">
            	<div class="row">
                	<div class="col-md-12">
						<div class="card-box"> 
  							    <form role="form" action="{{ route('updatestaff') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="row">
                                    	 <input type="hidden" name="id" value="{{ $staff->id }}">
                                        
                                        <div class="form-group col-md-6">
                                            <label for="name">Name<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"  id="name" placeholder="Enter staff Name"
                                            value="{{ $staff->name}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Email<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="email"  id="name" placeholder="Enter staff Email" value="{{ $staff->email}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Phone<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="phone"  id="name" placeholder="Enter staff phone" value="{{ $staff->phone}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Address<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="address"  id="name" placeholder="Enter staff Address" value="{{ $staff->address}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Job Description<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="description"  id="name" placeholder="Enter staff Description" 
                                            value="{{ $staff->description}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">N.I.D(Front)<span class="text-danger required">*</span></label>
                                             <input name="nid_front" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="DVLA"  >
                                             <p> @if($staff->nid_front) 
                                             <a target="_blank" href="{{url('/storage/app/').'/'.$staff->nid_front}}"> {{$staff->nid_front}}</a>@endif</p>
                                            
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">N.I.D(Back)<span class="text-danger required">*</span></label>
                                             <input name="nid_back" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="DVLA" >
                                             <p> @if($staff->nid_back) 
                                             <a target="_blank" href="{{url('/storage/app/').'/'.$staff->nid_back}}"> {{$staff->nid_back}}</a>@endif</p>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Profile Img<span class="text-danger required">*</span></label>
                                             <input name="profile_img" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="DVLA" >
                                             <p> @if($staff->profile_img) 
                                             <a target="_blank" href="{{url('/storage/app/').'/'.$staff->profile_img}}"> {{$staff->profile_img}}</a>@endif</p>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Add</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>
                                  </div>

                              </form>
                      
                    	</div>
					</div>
            	</div>
        	</div>
     	</div>
	</div>










@endsection
@section('scripts')
@endsection