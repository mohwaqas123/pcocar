<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
	protected $table = 'expense';
     protected $fillable = [
        'name','price','images','detail','attachment','part','vat_number','dob','int_status','vehicle_id','booking_id','reasons','user_id',
      ];
       public function user()
    {
        return $this->belongsTo('App\models\User','user_id','id');
    }
     public function booking()
    {
        return $this->belongsTo('App\models\Booking','booking_id','id');
    }
    public function vehicle()
    {
        return $this->belongsTo('App\models\Vehicle','vehicle_id','id');
    }
    
     
}
