@extends('layouts.customer')
@section('styles')
@endsection
@section('content')



@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif 
<div class="content-page">
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
            <!-- <ul class="list-unstyled topbar-right-menu float-right mb-0">
                <li class="hide-phone app-search">
      


                    <form>
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
             
            </ul> -->
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    
                </li>
            </ul>
        </nav>
    </div>
    <!-- Top Bar End -->
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-box">
                       <h3>Customer Details</h3>
                       <p>Name: {{$booking->user->name}}</p>
                       <p> Email: {{$booking->user->email}}</p>
                       <p>Phone: {{$booking->user->phone}}</p>
                       <p>DVLA License Number: {{$booking->dvla}}</p>
                       <p>DOB:{{date('d-m-Y', strtotime($booking->dob))}}</p>
                       <p>Address: {{$booking->address}}</p>
                       <p> Insurance Number: {{$booking->national_insu_numb}}</p>
                       <p> @if($booking->pco_licence_no) PCO License Number: {{$booking->pco_licence_no}}  @endif</p>
                       
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="card-box">
                        <h3>Vehicle Details</h3>
                     
                        
                       <p>Make: {{$vehicle->car_make->name}} </p>
                       <p>Model: {{$vehicle->car_model->name}}</p>
                       <p>Colour: {{$vehicle->car_model->colour}}</p>
                       <p>Engine Capacity: {{$vehicle->engine_Capacity}}</p>
                       <p>Year: {{$vehicle->year }}</p>
                       <p>Registration Number: {{$vehicle->licence_plate_number }}</p>
                       <p>Postal pickup dropoff: {{$vehicle->postal_pickup_dropoff }}</p>
                        
                        <!-- end row -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-box">
                        @php
         $discount_amount = $vehicle->price * ($booking->discount / 100);
        $total = $vehicle->price - $booking->discount;  
       if($booking->reference_booking > 0)
        $grand_tota = $booking->price + env('INSURANCE_CODE'); 
       else
        $grand_tota = $booking->price + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');
          //   $total + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');
 if($payment_mode=1) $p = "Stripe"; else $p = "Bank Deposit option";
if($booking->duration==1)
   $m = '4 weeks';
else
  $m = $booking->duration.' weeks';
@endphp
                      <h3>Booking Details</h3>
         
  <p> Booking Started Date: {{date('d-m-Y',strtotime($booking->booking_start_date))}}</p>
   <p>Expire Date: {{date('d-m-Y', strtotime($booking->booking_drop_off_date))}}</p>
                       <p> Price per week: £{{$booking->price}}</p>
                       <p> Duration: {{$booking->duration}} </p>
                       <p> @if ($booking->discount == 0)
                             
                            @else
                            Discount: {{$booking->discount}}%  
                           @endif</p>
                         <p>
                        @if ($booking->insurance_type == 0)
                        Insurance: £{{env('INSURANCE_CODE')}}                            @else
                        @endif
                        </p>
                       @if($booking->reference_booking == 0)
                      <p> Security Deposit(First time only): £ {{env('DEPOSIT_SECURITY')}}</p>
                       @else
                       @endif

                        @if ($booking->insurance_type == 1)
                        @php
                        $grand_tota_without_insurance  =  $booking->price + env('DEPOSIT_SECURITY');
                        @endphp
                        <p>Total:£{{$grand_tota_without_insurance}}</p>
                      Payment Mode:  {{$p}} 

                       @else
                     <p>Total:£{{$grand_tota}}</p>
                      Payment Mode:  {{$p}} 
                        @endif
 
                     <!-- end row -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="card-box">
                          <table class="table table-striped table-bordered"  style="width:100%">
          <thead>
            <tr>
                <th>Date</th>
                 
                <th>Amount</th>
              
                <th>Status</th>
            </tr>
            <tr>
                <td>

                  {{date('d-m-Y H:i:s', strtotime($booking->created_at))}}
                   
                     
                </td>    
                <td>
                    £{{$grand_tota}}
                  
                </td>
                <td>
                    Paid
                </td>
            </tr>
            </thead>
  <?php
  $months = $booking->duration;
  if($months==1) $months=4;
$total_week = $months - 1;

$price =  $booking->price+ env('INSURANCE_CODE') ;
for($i=1;$i<=$total_week;$i++){
 
  $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 

  $date = strtotime( $booking->booking_start_date );

  $date = strtotime($days_add, $date);
  $show_date = date('d/m/Y', $date);
  echo "
  <tr>
  <td>$show_date</td>

          <td>$price</td>
          </tr>
  ";
     
}
?>   


      <tbody>
  <!--<tr> 
          <td>  
           <br>
           <a class="btn btn-warning btn-sm" onclick='return confirm("Do you want to clear this payment?")' href="{{ route('clearpayment', $booking->id) }}" class="mr-5">
            
          clearpayment </a> </td>
        </tr>-->
      </tbody>
 

     

        </table>

                    </div>
                </div>
                 <div class="col-md-3">
                    <div class="card-box">
                         <h3>Attachment</h3>
                        <p>   

                          @if($booking->doc_cnic == Null)
                            @else
                             <a href="{{url('/')}}/storage/app/{{$booking->doc_cnic}}">DVLA Licence(Back)</a> 
                               @endif
                          </p>
                         <p>  <a href="{{url('/')}}/storage/app/{{$booking->doc_cnic}}">DVLA Licence(Back)</a> </p>
                        @if($vehicle->vehicle_category==1) 
                       <p> <a href="{{url('/')}}/storage/app/{{$booking->doc_other}}">PCO Driver Licence Front</a> </p>
                      <p> <a href="{{url('/')}}/storage/app/{{$booking->pco_driver_back}}">PCO Driver Licence Back</a>  </p>
                        <p><a href="{{url('/')}}/storage/app/{{$booking->pco_paper_licence}}">PCO Paper Licence</a> </p>
                        @endif
                      <p>  <a href="{{url('/')}}/generate_pdf_email/{{$booking->id}}">Customer Agreement Document</a>  </p>
                      @if($vehicle->doc_logback == Null)
                      @else
                      <p><a href="{{url('/')}}/storage/app/{{$vehicle->doc_logback}}">Vehicle LogBook</a> </p>
                      @endif
                      @if($vehicle->doc_mot == Null)
                      @else
                       <p><a href="{{url('/')}}/storage/app/{{$vehicle->doc_mot}}">Vehicle Mot</a> </p>
                      @endif
                      @if($vehicle->doc_phv == Null)
                      @else
                       <p><a href="{{url('/')}}/storage/app/{{$vehicle->doc_phv}}">Vehicle PHV</a> </p>
                      @endif
                        </div>
                </div>
                
            </div>
 


 
                
           
         
                
            </div> 
            <!-- end row -->
            </div> <!-- container -->
            </div> <!-- content -->
            
        </div>
@endsection
@section('scripts')
@endsection
 