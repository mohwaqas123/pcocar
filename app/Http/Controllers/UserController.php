<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\models\User;
use App\models\Booking;
use App\models\Notification;
use App\models\Vehicle;
use App\models\Make;
use App\models\Expense_history;
use App\models\Accedental_records;
use App\models\Accidental_records_comments;
use Illuminate\Support\Facades\Storage;
use App\models\CarModel;
use App\models\Expense;
use App\models\Expense_image;
 use App\Helper\Helper;
use Illuminate\Support\Facades\Mail;
use App\models\PaymentHistory;
use App\models\Setting;
use Session;
use Illuminate\Http\File;

class UserController extends Controller
{
   public function stafflogin(Request $request)
    {
        request()->validate([ 
            'staff_email' => 'required',   
            'staff_email' => 'required', 
            ]);    
        if ($request->staff_email =='staff@pcocar.com'&& $request->staff_password=='staff123') 
        {

           session(['staff_id' => 1, 'staff_email' => $request->email]);
          // return redirect('booking')->back()->with('success','Login Successfully');
          // return redirect();
           return 1;
        }
        else
        {
           //return redirect()->back()->with('message','User or Password is not correct');
            return 2;
        }
    }

 public function stafflogout(){

    session_unset(); 

    Session::flush();

    //return Redirect::to('/customerlogin');

    return redirect('/');

    }











    public function signup(Request $request){
      request()->validate([ 
            'email' => 'required|unique:users',   
            'password' => 'required',
            'name' => 'required|max:50',
 ]);

         $data = [

        'name' => $request->name,

        'email' => $request->email,

        'password' => Hash::make($request->password),

        'phone' =>$request->phone,

        

    ];

  

     User::create($data);

     

	if(session()->get('staff_id') ){

		 //Mail::to( 'waqas.ger@mail.com')->subject('New Customer Signup')->send(new \App\Mail\BookingRegister($request->name, $request->email,$request->password, 43 ));

		 

Mail::send('mail.bookingregister', ['name' =>$request->name , 'v_id'=> $request->session()->get('v_id'),'email'=>$request->email,'password'=>$request->password], function($message) {
    $message->to('waqas.ger@gmail.com')->subject('New Customer Signup'); 

});

		  

	}



/*

Mail::send('welcome', [], function($message) {

    $message->to('waqas.ger@gmail.com')->subject('Testing mails'); 

});*/

     return redirect()->back()->with('message','User Registered Successfully');





    }

   

 

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function customerlogin(Request $request)

    {

        request()->validate([ 

            'email' => 'required',   

            'password' => 'required',

            //'name' => 'required|max:20',

             

            ]);  

        //$user = User::where('password',Hash::make($request->password))->where('email',$request->email)->first();

        $user = User::where('email',$request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {

           session(['user_id' => $user->id, 'email' => $request->email]);

          // return redirect('booking')->back()->with('success','Login Successfully');

           return redirect()->route('customerdashboard', ['user_detail' => $user, 'step2'=>1]);            

        }

        else

        {

           return redirect()->back()->with('message','Email or Password is not correct');

        }

 



    }
public function customerLogout(){

    session_unset(); 

    Session::flush();

    //return Redirect::to('/customerlogin');

    return redirect('/login');

    }



    public function customerdashboard()
    { 

    $duration = Booking::where('booking_status', '1')->where('user_id',session()->get('user_id'))->get();  
    $booking_history = PaymentHistory::where('user_id',session()->get('user_id'))->get();
    $booking_pending = Booking::where('payment_status', null)->where('user_id',session()->get('user_id'))->get();
        $url = "customerdashboard";
        $title = 'Car Make list';
        $data = [
              'duration' => $duration,
               'title' =>$title,
                'booking_history' =>$booking_history,
                'breadcrum' => '<a style="color:black; href="'. url( $url ).'" > Dashboard</a> ',
                'booking_pending' => $booking_pending,
           //     'vehicles'=> $vehicles,
                ];  
            return view('customer.dashboard')->with($data);  

}





public function bookingView(Request $request)
{  
       $booking = Booking::where('id', $request->id)->with('vehicle')->first();   
       $vehicles = Vehicle::where('id',  $booking->vehicle_id)->first();     
        $url = "customerdashboard";
       $title = 'Car Make list';
         $data = [
              'vehicle' => $vehicles, 
        'booking' => $booking,
        'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'">Dashboard</a>>'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer Booking Detail',

        ]; 
          return view('customer.booking_detail')->with($data);







}







    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function login(Request $request)

    {
    
        request()->validate([ 

            'email' => 'required',   

            'password' => 'required',

            //'name' => 'required|max:20',

             

            ]);



        //$user = User::where('password',Hash::make($request->password))->where('email',$request->email)->first();

        $user = User::where('email',$request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {

           session(['user_id' => $user->id, 'email' => $request->email]);

          // return redirect('booking')->back()->with('success','Login Successfully');

           return redirect()->route('booking2', ['id' => $request->id, 'step2'=>1]);            

        }

        else

        {

           return redirect()->back()->with('error','User or Password is not correct');

        }

 



    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }



     public function customer_booking_list()

    {

        $booking = Booking::where('user_id', session()->get('user_id') )->where('payment_status',1)->orderby('id', 'Desc')->get();
         $url = "customerdashboard";
          $data = [
           'booking' => $booking,
        'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'">Dashboard</a>>'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Boolking list',
             ];  
        
        return view('customer.customer_booking_list')->with($data);

    }
    public function pco_car(Request $request)
    {
    $Vehicle = Vehicle::where('vehicle_category', '=' , '1')->get();
    $make = Make::all();
    $model = CarModel::all();
    if($request->duration) $duration = $request->duration; else $duration = '';
    return view('frontend.pco', [
                'Vehicle' => $Vehicle , 
                'car_make'=> $make,
                'car_model' => $model,
                'duration' => $duration,
                ]);
    }
     public function private_car(Request $request)
    {
        $Vehicle = Vehicle::where('vehicle_category', '=' , '2')->get();
        $make = Make::all();
        $model = CarModel::all();
        if($request->duration) $duration = $request->duration; else $duration = '';
         return view('frontend.pco', [
                'Vehicle' => $Vehicle , 
                'car_make'=> $make,
                'car_model' => $model,
                'duration' => $duration,
            ]);
    }
        public function customer_account(Request $request)

    {   
       
        $user = User::where('id',session()->get('user_id'))->first();
    
   
        $url = "customerdashboard";
        $title = 'Customer List';
        $data =[
                'user'=> $user,
                 'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer Edit',
            ];  
           
        return view('customer.profile.edit')->with($data); 
      
    }
     public function user_update(Request $request)

    {   
        $user = User::where('id',session()->get('user_id'))->first();
        $data =[
                 
                'name' => $request->name,
                'phone' => $request->phone,
                'dob' => $request->dob,
                'description' => $request->description,
            ];  
            
        $user->update($data);

    
            return redirect('customerdashboard')->with('message','Customer Profile are update');
      
      
    }
     public function customer_account_password(Request $request)

    {   
        $user = User::where('id',session()->get('user_id'))->first();

      $url = "customerdashboard";
        $title = 'Customer List';
        $data =[
                'user'=> $user,
                 'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer Password',
            ]; 

        return view('customer.password.password')->with($data); 
    }
      public function user_update_password(Request $request)

    {   
        
        $user = User::where('id',session()->get('user_id'))->first();
       
        $password = null;

        if(Hash::check($request->password, $user->current_password))
        {
        
        $data= [
            'error'=>"The specified password does not match the database password.", 
             'password' => 1,
             'user' => $user,
        ];
           return view('customer.password.password')->with($data);          
        } 
        else 
        {
       
          request()->validate([ 
            'password' => 'required|string|min:6|confirmed',
            ]);
            $user->password = Hash::make($request->password);  
            $user->save(); 
            $password = 1;
        }
        $data= [
            'message'=>"Your password updated successfully.", 
             'password' => 1,
             'user' => $user,    
        ];
        return view('customer.password.password')->with($data);
        }


  public function customer_accidental_list()

    {
     
          $booking = Accedental_records::where('user_id', session()->get('user_id') )->orderby('id', 'Desc')->get();
     
         $url = "customerdashboard";
          $data = [
           'booking' => $booking,
        'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'">Dashboard</a>>'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Damages list',
             ];  
        
        return view('customer.customer_accidental_list')->with($data);

    }
    public function customerview(Request $request)
    { 

          
      $booking = Accedental_records::where('id',$request->id)->with('vehicle')->with('user')->with('booking')->first();

        $comments = Accidental_records_comments::where('accidental_id',$request->id)->orderBy('created_at', 'desc')->get();
      $url = "customerdashboard";
        
          $data = [
               'booking'=> $booking, 'comments'=>$comments,
             
                
               'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Accidental Recorded',
            ];  
  
        return view('customer.accidental_view')->with($data);

    }




 public function customer_accidental_comment(Request $request)
    {   
        
        $booking = Accedental_records::where('id',$request->id)->with('vehicle')->with('user')->with('booking')->first();
      
        $doc_logback='';
       

        //documents upload process
         if ($request->hasFile('doc_logback') ) 
         {
            $vehicleDocFolder = 'accidentalImages';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
             } 

             $data = [
               'accidental_id' =>$booking->id,
                'user_id' =>$booking->user->id,
               'comments' => $request->detail,
               'attachment' => $doc_logback,
            ];       
             $Accidental_records_comments = Accidental_records_comments::create($data);
            $vehicleData = [ 
             'accidental_id' =>$booking->id,
             'user_id' =>$booking->user->id,$booking->detail,
             'vehicle_id' =>$booking->vehicle->id,
             'detail' =>$booking->detail,
             'int_status' =>2,
              ];
             
                $booking->update($vehicleData); 
                $booking->save();
 
    return redirect()->back()->with('message','Comments Posted Successfully');

    }
     

    public function edit_comments(Request $request)
    {   
 
     $makes = Accidental_records_comments::findOrFail($request->id);
  
    $data = [
    'comments' => $makes->comments, 
    'accidental_id' => $makes->accidental_id,
    'user_id' => $makes->user_id,
    'attachment' => $makes->attachment,
     ];

      // return redirect('admin.accidental.comments')->with($data);
    return response()->json($data); 
    
    }
     public function customer_expense()

    {
        $booking = Booking::where('user_id', session()->get('user_id') )->with('user')->with('vehicle')->first();
     
         $url = "customerdashboard";
          $data = [
           'booking' => $booking,
        'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'">Dashboard</a>>'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Expense',
             ];

      return view('customer.customer_expense')->with($data);

    } 
    public function customer_expense_add(Request $request)

    {
        $booking = Booking::where('user_id', session()->get('user_id') )->with('user')->with('vehicle')->first();
        
         $doc_logback='';
       

        //documents upload process
         if ($request->hasFile('doc_logback') ) 
         {
            $vehicleDocFolder = 'expensedoc';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
             } 

        $expenseData = [
            'price' =>$request->price,
            'detail' => $request->detail,
            'name' => $request->name,
            'dob' =>$request->dob,
            'vat_number' => $request->vat_number,
            'part' => $request->part,
            'attachment' => $doc_logback,
            'int_status' => 1,
            'vehicle_id' => $booking->vehicle->id,
            'booking_id' => $booking->id,
            'user_id' => $booking->user_id,

        ];
       
          $Expense = Expense::create($expenseData);

          if ($request->hasFile('thumbnail')) {
            $vehicleImagesFolder = 'expensedoc';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            $imageUrl = Storage::putFile($vehicleImagesFolder, new File($request->thumbnail));
            $accedental->update(['image'=> $imageUrl]);
        }

        if ($request->hasFile('images')) {
            $vehicleImagesFolder = 'vehicleImages';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            foreach($request->images as $image)
            {

              $imageUrl = Storage::putFile($vehicleImagesFolder, new File($image));
                Expense_image::create(
                    [
                        'expence_id'=> $Expense->id,
                        'images' => $imageUrl 
                    ]
                );
            } 
         }

         
          
        $vehicle_name =  $booking->vehicle->name;
        $vehicle_year =  $booking->vehicle->licence_plate_number;
        $user_name =     $booking->user->name;
        $user_Email =    $booking->user->email;
        $user_Phone =    $booking->user->phone;
        $Expense_price = $request->price;
        $detail        = $request->dob;
             $shop_name =     $request->vat_number;
        $Expense =     $Expense;
         Mail::to('finance@pcocar.com')->send(new \App\Mail\Customer_Expense(
          $vehicle_name,$vehicle_year,$user_name,$user_Email,$user_Phone,$Expense_price,$detail,$shop_name,$Expense)); 




    return redirect()->back()->with('message','Expense submitted Successfully.');
    }
     public function renewbooking_customer(Request $request)

    {
        $booking = Booking::find($request->id);
        $vehicles = Vehicle::where('id', $booking->vehicle_id)->first();
         session(['permotion_discount' => '',]);
         session()->forget('permotion_discount');
        $url = "customerdashboard";
        $data = [
        'vehicles' => $vehicles, 
        'booking' => $booking,
        'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'">Dashboard</a>>'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer Renew Booking',

                'duration'=> session()->get('duration'),
                'discount'=>session()->get('discount'),
                'disc_percent'=> Helper::get_discount_percent_func( session()->get('duration')),
                'promotion' => Helper::check_discount(),
                'promotion_type'=>Helper::get_flat_discount_amount()[0]->promotion_type,
                'flat_discount_rate'=>Helper::get_flat_discount_amount()[0]->flat_discount_rate,
                'setting'=>Helper::get_flat_discount_amount()[0],
                'get_discount_duration_price' =>   Helper::get_discount_duration_price(session()->get('duration'),Helper::get_price_with_check_discount($vehicles->price)),
                'price' => Helper::get_price_with_check_discount($vehicles->price),
        ];
     
         
        return view('customer.renew_booking')->with($data);



    }
     public function customer_expense_list(Request $request)

    {
         
          
        

        $expence_submitted = Expense::where('user_id', session()->get('user_id') )->with('vehicle')->with('booking')->get();
        $expence_in_progress= Expense::where('user_id', session()->get('user_id') )->where('int_status', 2)->with('vehicle')->with('booking')->get();
        $recorded_further_evidence = Expense::where('user_id', session()->get('user_id') )->where('int_status', 6)->with('vehicle')->with('booking')->get();
        $recorded_approved = Expense::where('user_id', session()->get('user_id') )->where('int_status', 4)->with('vehicle')->with('booking')->get();
        $recorded_rejected = Expense::where('user_id', session()->get('user_id') )->where('int_status', 5)->with('vehicle')->with('booking')->get();
        $recorded_on_hold = Expense::where('user_id', session()->get('user_id') )->where('int_status', 6)->with('vehicle')->with('booking')->get();

        $url = "customerdashboard";
        $title = 'Damages Records list';
        $data = [
               'expence_submitted'=> $expence_submitted,
               'expence_in_progress'=> $expence_in_progress,
               'recorded_further_evidence'=> $recorded_further_evidence,
               'recorded_approved'=> $recorded_approved,
               'recorded_rejected'=> $recorded_rejected,
               'recorded_on_hold'=> $recorded_on_hold,
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'customer Expense List',
        ]; 
       
        return view('customer.expense_list')->with($data);
    }
     public function expensesview(Request $request)

    {
          $expense_view = Expense::where('id',$request->id)->with('vehicle')->with('booking')->first();
        $expense_history = Expense_history::where('expence_id',$request->id)->get();
       
         $url = "customerdashboard";
             $data = [
               'expense_view'=> $expense_view,    
               'expense_history'=> $expense_history,               

                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer Expense view',
            ];  
        return view('customer.expence_view')->with($data);
    }


    public function customerexpensesfurtherevidence(Request $request)

    {
        // dd('customerexpensesfurtherevidence');
        // $expense = Expense::find($id);
        $expense = Expense::where('id', $request->id)->first();
         
        $expensehistory = [
          
            'expence_id' => $expense->id,
            'details' => $request->detail,
            'int_status' => 1,


        ];
        $expensehistory = Expense_history::create($expensehistory);
      return redirect()->back()->with('message','Further evidence are Provided.');
       
    }












    public function expensesedit($id)

    {
         
        $expense = Expense::find($id);
        $booking = Booking::where('user_id', session()->get('user_id') )->with('user')->with('vehicle')->first();
         $url = "customerdashboard";
             $data = [
               'expense'=> $expense,  
               'booking'=> $booking,  
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer Expense Edit',
            ];  
        return view('customer.expence_Edit')->with($data);
    }
    public function customer_expense_update(Request $request)

    {
        $expense = Expense::where('id', $request->id)->first();
        $booking = Booking::where('user_id', session()->get('user_id') )->with('user')->with('vehicle')->first();
        
         $doc_logback='';
       

        //documents upload process
         if ($request->hasFile('doc_logback') ) 
         {
            $vehicleDocFolder = 'expensedoc';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
             } 

        $expenseData = [
            'price' =>$request->price,
            'detail' => $request->detail,
            'name' => $request->name,
            'dob' =>$request->dob,
            'vat_number' => $request->vat_number,
            'part' => $request->part,
            'attachment' => $doc_logback,
            'int_status' => 3,
            'vehicle_id' => $booking->vehicle->id,
            'booking_id' => $booking->id,
            'user_id' => $booking->user_id,
            'booking' => $booking,


        ];
      
        $expense->update($expenseData); 
        $expense->save(); 
           

          if ($request->hasFile('thumbnail')) {
            $vehicleImagesFolder = 'expensedoc';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            $imageUrl = Storage::putFile($vehicleImagesFolder, new File($request->thumbnail));
            $accedental->update(['image'=> $imageUrl]);
        }

        if ($request->hasFile('images')) {
            $vehicleImagesFolder = 'vehicleImages';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            foreach($request->images as $image)
            {

              $imageUrl = Storage::putFile($vehicleImagesFolder, new File($image));
                Expense_image::create(
                    [
                        'expence_id'=> $Expense->id,
                        'images' => $imageUrl 
                    ]
                );
            } 
         }
     return redirect()->back()->with('message','Further evidence  is Submitted.');

 
    }


      public function customer_paymenthistory_list( )

    {

     $payment_history = PaymentHistory::where('user_id', session()->get('user_id'))->get();
         $url = "customerdashboard";
             $data = [
               'payment_history'=> $payment_history,  
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer Payment History',
            ];  
        return view('customer.payment_history')->with($data);
    }


public function customer_swape_req( )
    {
        $booking = Booking::where('user_id', session()->get('user_id') )->with('user')->with('vehicle')->first();

        $url = "customerdashboard";
            $data = [
               'booking'=> $booking,  
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer swap Req',
            ];  

        return view('customer.swape_req')->with($data);
    }
public function customer_swap_req_send(Request $request)
    {
      
        $booking = Booking::where('user_id', session()->get('user_id') )->with('user')->with('vehicle')->first();
         $data = [
            'user_id' => $booking->user_id,
            'vehicle_id' => $booking->vehicle->id,
            'booking_id' => $booking->id,
            'details' => $request->detail,
            'date' => $request->dob,
            'int_status' => 1,

        ];
    
        $notification = Notification::create($data);

     return redirect('customer_swape_show')->with('message','Swapping request submitted successfully.');
      
      // return view('customer.swape_req')->with($data);
    }

    public function customer_swap_req_show(Request $request)
    {

        $notification = Notification::where('user_id', session()->get('user_id') )->where('int_status' , 1)->with('user')->with('vehicle')->with('booking')->get();
        $url = "customerdashboard";
         $data = [
            'notification' => $notification,
             'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Customer swape List',
        ];
           
      return view('customer.swape_list')->with($data);
    }
    public function forgot_password(Request $request)
{
       
         request()->validate([ 
            'email' => 'required', 
           
            ]);
        $count_email=User::where('email',$request->post('email'))->count();
        $user_info = User::where('email',$request->post('email'))->first(); 

        if($count_email==0){
       
            //error message
            return redirect('password/reset')->with('error', 'Email does not exit. Please sign up first.');
        } 
        else
        {
      
        $token = md5(rand());
         $to_name =  $user_info->name;
          $to_email = $request->post('email');
      }
       $url = url("/"). '/customer/verify/?v='.$token;



    $data = array('name'=>$user_info->name, 'url' => $url);


     $r = Mail::send('mail.ForgetPassword', $data, function($message) use 
        ($to_name, $to_email) 
        {
            $message->to($to_email, $to_name)->subject('PCOCAR Forgot Password');
            $message->from('cybexo.team@gmail.com','PCOCAR Forgot Password');
          });

         return redirect('/login');
    }
     public function customer_signup_verify(Request $request)
    { 
     dd('customer_signup_verify');
     $user = User::where('remember_token', $request->post('v') )->first();
     $user_id = $user->id; 
      //Users
     $user = User::findOrFail($user_id); 
     $user->password = Hash::make($request->post('password'));  
     $user->save();
     $customer->token = '';
     $customer->save();
     return redirect('login')->with('message', 'You are setup password please login');
      
    }
    public function set_forgot(Request $request){ 
    
     $customer = User::where('remember_token', $request->id )->first();
     
     $user_id = $customer->id; 
      //Users
     $user = User::findOrFail($user_id); 
     
     $user->password = Hash::make($request->post('password'));  
     $user->save();
     
   
     return redirect('login')->with('message', 'You are setup password please login');
      
}

public function customer_verification(Request $request){


     $count_c=User::where('remember_token',$request->get('v'))->count();
    if($count_c==1){ 
    print ' <div class="alert alert-danger msg_password_mismatch" role="alert">
              Sorry!!! link is expired. 
        </div>';
        dd();
            }


    return view('customer.customer_new_signup_verification', ['title' => 'Please Enter your New Passwrod' ]);
}










}

