<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->id();
            $table->boolean('stripe_mode')->comment('0=test,1=live')->nullable();
            $table->longText('stripe_live_published_key')->nullable();
            $table->longText('stripe_live_secret_key')->nullable();
            $table->longText('stripe_live_prod_key')->nullable();
            $table->longText('stripe_test_published_key')->nullable();
            $table->longText('stripe_test_secret_key')->nullable();
            $table->longText('stripe_test_prod_key')->nullable();
            $table->integer('promotion_type')->comment('0=disable,1=enable')->nullable();
            $table->integer('flat_discount_rate')->comment('what will be charge when we enable flate rate')->nullable();
            $table->boolean('flat_discount_type')->comment('0=off,1=on')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
