@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                   


 
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

                                <thead>
                                <tr>
                                <th style="width: 10px">Sr.#</th>
                                <th>Name</th>
                                <th>Email</th>                                
                                <th>Status</th>
                                <th style="width:90px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                    
                                @foreach($driver as $dr)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $dr->name }}</td>
                                    <td>{{ @$dr->email }}</td>
                                    <td style="width:100px;">
                                        @if ($dr->driver_status == 0)
                                            Active
                                        @else
                                            In-active 
                                        @endif
                                    </td>
                                    <td>
                                    <a class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip" 
                                    data-toggle="tooltip" data-placement="Top" data-original-title="Edit" href="{{ route('driverupdate', $dr->id) }}" class="mr-5"><i class="fa fa-pencil"></i> </a>



                                     
                                    <a   class="btn btn-warning btn-sm  metismenu with-tooltip" 
                                    data-toggle="tooltip" data-placement="Top" data-original-title="Delete" href="{{ route('driverdestory', $dr->id) }}"  onclick="return confirm('You want to remove your selected Driver?')" class="mr-5"><i class="fa fa-trash"></i> </a>



                                </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                            {{$driver->links()}}
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
