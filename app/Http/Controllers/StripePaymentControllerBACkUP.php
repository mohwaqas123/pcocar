<?php

namespace App\Http\Controllers;
//use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Hash;
use Session;
use Stripe;
use App\User;
//use App\Merchant;
use App\models\PaymentHistory;
use App\models\Vehicle;
use App\models\Booking;
//use App\Packages;
//use App\History_log;
use Auth;
//use App\Packageoptions;
use Mail;
 

 
  
class StripePaymentController extends Controller
{

     public function __construct()
    {
         //$this->middleware('auth');
    }
 
 

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe(Request $request)
    {
        $package = Packages::where('id', $request->get('p_id') )->first(); 
         $data = [
            'price' => $request->price, 
            'customer_name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'package' => $package,
        ];
        return view('stripe')->with($data);
    }


/*public function stripe_getting_history(Request $request)
    {
          $stripe = new \Stripe\StripeClient(
          'sk_test_QMX38t3rVMu1SxAaM1FS4OjJ00wLlUhQd8' 
          );
        return  $stripe->issuing->transactions->retrieve(
          'ch_1GuEteLP8fXawno0l2m3N7s4',
          []
          );
    }*/

  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    { 
 
        $booking_detail = Booking::where('id', $request->session()->get('booking_id') )->first();
        //Global Parameters
        $product_id = "prod_IO76QwwM1ZkCI0";
        $currency = 'GBP';  
        $amount = $request->amount;
        $nickname = $booking_detail->vehicle->licence_plate_number.'-'.$booking_detail->id;
        $subscription_amount_charge_every_week = $amount - 10000;
        $booking_duration =  $booking_detail->duration;
 


        \Stripe\Stripe::setApiKey( env('STRIPE_SECRET') );
        
      // dd($request->stripeToken);



 //=============================================================
 //===================== Reoccuring Payment Process ============
 //=============================================================
      //  if($users_strip_account_exist==1){ dd( $users_strip_account_exist);
               

            //Step1: Create Price for specific Product
                      $price = Stripe\Price::create([
                'product' => $product_id,
                'unit_amount' => $subscription_amount_charge_every_week,
                'currency' => $currency,
                'nickname'=>$nickname,
                'recurring' => [
                  'interval' => 'week',
                ],
              ]);
$price_id = $price->id;//'price_1HnIDqLP8fXawno07A0HZ6E0';
 
               //Step2: Create Customer for Reoccuring billing
               $create_customer = Stripe\Customer::create([
                    'description' => 'PCOCAR Customer',
                    'email' => $request->customer_email,
                    'name' => $request->customer_name,
                    "source" => $request->stripeToken,
                      "source" => $request->stripeToken,

                ]);

 /*
             //Step3: Create Invoice
                $create_invoice = \Stripe\InvoiceItem::create([
                    'customer' => $create_customer->id,
                    'amount' => $amount,
                    'currency' => 'usd',
                    'description' => 'PCOCAR Booking PL_LBDC0092',
                ]);
                $transaction_id = $create_invoice->id;

 */



$return = Stripe\Charge::create ([
                "amount" => $amount,
                "currency" =>  $currency, 
                "description" => "PCOCAR Booking Vehicle ".$nickname,
                'customer' =>  $create_customer->id,
        ]);


                //Step4: Assign Subsribed Plan
                $startDate = time();
                $timespn =  strtotime('+1 week', $startDate) ;
                
                $booking_duration = "+".  $booking_duration ." week";
                $timespn_cancel =  strtotime( $booking_duration, $startDate) ;
                              
            // $stripe->subscriptionSchedules->create([
              $subscription = Stripe\SubscriptionSchedule::create([
                    'customer' =>    $create_customer->id,
                    'start_date' =>  $timespn,
                    'end_behavior' => 'release',
                    'phases' => [
                      [
                          'end_date'=>$timespn_cancel,
                          'items' => [
                                                    ['price' => $price_id ],
                                                    ['price_data'=>[
                                                        'currency'=>$currency,
                                                        'product'=>$product_id,
                                                       //'unit_amount'=>$subscription_amount_charge_every_week,
                                                        'unit_amount_decimal' =>$subscription_amount_charge_every_week,
                                                         'recurring'=> ['interval'=>'week' , 'interval_count'=>1 ]     ]  ],  
                                                  ],   

                       // 'iterations' => 12,
                      ],
                    ],
                  ]);

             /*   $subscription = Stripe\Subscription::create([
                 //  "source" => $request->stripeToken,
                    'customer' =>  'cus_IO4g4YRVx8e2Oy',// $create_customer->id,
                    "collection_method"=> "charge_automatically",
                    'billing_cycle_anchor'=>$timespn,
                    'backdate_start_date' => $timespn,
                    'cancel_at' => $timespn_cancel,
                  'items' => [
                                  ['price' => $price_id ],
                                  ['price_data'=>[
                                      'currency'=>$currency,
                                      'product'=>$product_id,
                                     'unit_amount'=>4000,
                                      //'unit_amount_decimal' =>'100',
                                       'recurring'=> ['interval'=>'week' , 'interval_count'=>1 ]     ]  ],  
                                ],   

                ]);*/

/*     $create_invoice = \Stripe\InvoiceItem::create([
                    'customer' => 'cus_IO4g4YRVx8e2Oy',
                    'amount' => 2000,
                    'currency' => 'usd',
                    'description' => 'PCOCAR Booking PL_LBDC0092',
                ]);
                $transaction_id = $create_invoice->id;*/


  /*            //Step5: First Charge of Booking
                $return = Stripe\Charge::create ([
                "amount" => $request->amount,
                "currency" => "usd",
             //  "source" => $request->stripeToken,
                "description" => "PCOCAR Booking Fist Payment" 
        ]);
*/

 
 
 


 //=============================================================
 //===================== Reoccuring Payment Process ============
 //=============================================================
/*        if($users_strip_account_exist==1){
                //Step1: Create Customer for Reoccuring billing
               $create_customer = Stripe\Customer::create([
                    'description' => 'My First Test Customer (created for API docs)',
                    'email' => $request->customer_email,
                    'name' => $request->customer_name,
                    "source" => $request->stripeToken,
                ]);
                //Step2: Create Invoice
                $create_invoice = \Stripe\InvoiceItem::create([
                    'customer' => $create_customer->id,
                    'amount' => $request->amount * 100,
                    'currency' => 'usd',
                    'description' => 'Subscription Fee',
                ]);
                $transaction_id = $create_invoice->id;
                //Step3: Assign Subsribed Plan
                $subscription = Stripe\Subscription::create([
                    'customer' =>   $create_customer->id,
                    'items' => [['plan' => 'plan_HEWOBWE0XaLWEZ' ]],
                ]); 


        }//end if
 

 if($users_strip_account_exist==1){ 
    }
    else
    {
        $return = Stripe\Charge::create ([
                "amount" => $request->amount * 100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Test payment from TMS user become Premium" 
        ]);
        $transaction_id = $return->id;
    }*/

                // dd($complate_name);

/* $create_customer = Stripe\Customer::create([
                    'description' => 'Customer created first time when submit booking',
                    'email' => $request->customer_email,
             //      'name' => $request->customer_name,
                    "source" => $request->stripeToken,
                ]);


  $return = Stripe\Charge::create ([
                "amount" =>  $request->amount,
                "currency" => "GBP",
                "source" => $request->stripeToken,
                "description" => "New Booking payment PCOCAR." ,
             //    'customer' => $customer->id,
                
        ]);
 */

        $transaction_id = $return->id;
        $date = date('Y-m-d');
        $package_days =30;// '+ '.$request->package_days. ' days'; 
        $expire_date=date('Y-m-d', strtotime($date.  $package_days));


$date_time = strtotime($date);
$added_months = "+".$request->expire_month_after." month";
$final_date = date("Y-m-d", strtotime($added_months, $date_time));



        $data = [
            'user_id' => $request->session()->get('user_id'),
            'amount' => $request->amount ,
            'transaction_id' =>$return->id,
            'expire_date' => $final_date,
            'month_duration' => $request->expire_month_after,
            'per_month_amount'=>  $request->amount,
            'vehicle_id' =>  $request->session()->get('v_id'),  
            'booking_id' => $request->booking_id,
        ];

        $user = PaymentHistory::create($data);     
   

      $name =  $request->customer_name;
      $title = "Booking Status"; 
      $email_to =  'payments@pcocar.com';//$user->email;//'waqas.ger@gmail.com';
        $booking = Booking::where('id', $request->session()->get('booking_id'))->with('vehicle')->with('user')->first();
        $vehicle = Vehicle::where('id', $booking->vehicle_id)->with('car_make')->with('car_model')->first();
        $ses_values = $request->session();            

$payment_mode=1;
   Mail::to($email_to)->cc($request->session()->get('email'))->send(new \App\Mail\Booking($payment_mode,$name, $title, $booking , $vehicle , $request->duration,  $request->session()->get('discount'))); 

   //$veh = Vehicle::findOrFail($request->id);     
        $vehicle->bookingStatus =1; 
        $vehicle->save();

        //return view('stripe_after_payment')->with($data_post);

        //print "<pre>";    
        //print_r($return); 
 
       // Session::flash('success', 'Payment successful!');
          
        //return back();

 return redirect()->route('done',['booking_id'=>$request->booking_id])->with('success','Payment Done Successfully');


    }


 

 public function update_payment_status(Request $request){
      $input = @file_get_contents('php://input');
      $event_json = json_decode($input);
      //print_r($input);
      if $event_json->type == 'charge.succeeded' {
         ///echo 'You made some money! Hooray!';
         $vehicle = Vehicle::where('id', 22)->with('car_make')->with('car_model')->first();
          $vehicle->partner_id =1; 
        $vehicle->save();
}
 


 }


}