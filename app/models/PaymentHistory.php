<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    //
    protected $table = 'payment_histories';


    protected $fillable = [  
          'user_id' , 'amount' , 'transaction_id','expire_date',
          'vehicle_id',
          'month_duration',
          'per_month_amount',
          'booking_id', 'payment_status','payment_mode','description','payment_type',
    ]; 



    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
         public function booking()
    {
        return $this->belongsTo('App\models\Booking','booking_id','id');
    }

 
}
