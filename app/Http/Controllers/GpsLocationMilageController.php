<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\GpsLocationMilage;
use App\models\Vehicle;
use DB;
use Session;
use PDF; 

class GpsLocationMilageController extends Controller
{

    public function customer_vehicle_mileage_login(Request $request){
        $param = '';
        return view('admin.gps.login')->with( $param);
    }


 public function gps_post_login(Request $request){
        $user= 'pcocar1133';
        $pass = 'gps$19922';
        if($request->username!=$user || $request->password!=$pass){
            return redirect()->back();
        }
        else {
            session(['login' =>'gps$19922']);  
            return redirect('get_gps_customer_records');
            }     
    } 
    public function index(Request $request){
			
			$url = "https://gps-login.co.uk/api/api.php?api=user&ver=1.0&key=FE7B7F2682F88898C36DBB03AD3474B4&cmd=USER_GET_OBJECTS";
			$ch = curl_init( $url );
			# Setup request to send json via POST.
			$data = '';
			$payload = json_encode( array( "customer"=> $data ) );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
			curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			# Return response instead of printing.
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			# Send request.
			$result = curl_exec($ch);
			curl_close($ch);
			# Print response.


			$json_data = json_decode($result)  ;
			//echo "<pre>";
			//print_r(  $json_data );

			foreach($json_data as $rec){
				 $data = [ 
			        'name' => $rec->name, 
			        'imei' => $rec->imei, 
			        'device' => $rec->device, 
			        'odometer' =>$rec->odometer,  
			        'c_date' =>date('Y-m-d'),
			    ];
                GpsLocationMilage::create($data);
			}

    }

    public function get_gps_location(Request $request){
    	$date=date('Y-m-d');
    	$get_gps_location_rec = GpsLocationMilage::where('c_date', $date)->get();

$get_vehicles_partner = Vehicle::where('partner_id','!=',0)->with('car_make')->with('car_model')->get();
    
    	$param = [
               'get_gps_location_rec'=> $get_gps_location_rec,
               'title' =>'', 
               'factor' => 0.6214, 
                'date_to'=> $request->date_to,
               'date_from' => date('Y-m-d'),
               'get_vehicles_partner'=>$get_vehicles_partner,
        ];  
    	 
    	return view('admin.gps.gps')->with( $param);
    }


 public function search_customer_records(Request $request){  

        
        if($request->date_from <= $request->date_to)
        {

         return redirect()->back()->with('message','Please Choose Correct Dates.');
        }
        else
        {

        $data='';
        $get_gps_location_rec = GpsLocationMilage::where('c_date', $request->date_to)->get();
       $get_vehicles_partner = Vehicle::where('partner_id','!=',0)->with('car_make')->with('car_model')->get();
        $param = [
               'get_gps_location_rec'=> $get_gps_location_rec,
               'title' =>'', 
               'factor' => 0.6214,
               'date_to'=> $request->date_to,
               'date_from' => $request->date_from,
               'get_vehicles_partner'=>$get_vehicles_partner,
        ];  

        // $pdf = PDF::loadView('frontend.gps', $param);  
        // $filename = rand().'.pdf';
        // return $pdf->download($filename); 
      

        return view('admin.gps.gps')->with( $param);
        }



        
    }




 public function logout(){

    session_unset(); 
    Session::flush();
    //return Redirect::to('/customerlogin');
    return redirect('/');

    }

public function generate_report_pcn(Request $request)
    {
 
      $data='';
        $get_gps_location_rec = GpsLocationMilage::where('c_date', $request->date_to)->get();

        $get_vehicles_partner = Vehicle::where('partner_id','!=',0)->with('car_make')->with('car_model')->get();
        
        $data = [
                'get_gps_location_rec'=> $get_gps_location_rec,
               'title' =>'', 
               'factor' => 0.6214,
               'date_to'=> $request->date_to,
               'date_from' => $request->date_from,
               'get_vehicles_partner'=>$get_vehicles_partner,

            ];
    
        $pdf = PDF::loadView('frontend.gps', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);     


     }
    

    
}
