@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')

 

<script >
  function change_date(){
    //$('#insurance_end_date').val($('#insurance_started_date').val());
    document.getElementById("insurance_end_date").min = $('#insurance_started_date').val();
}
 
 
  var _validFileExtensions = [".jpg", ".jpeg",".pdf",".png"];    
function Validate(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return false;
                }
            }
        }
    }
  
    return true;
}
</script>








 
<div class="container">
  
 

  <div class="row">
    <div class="col-md-3 text-center">
        @include ('frontend.bookingleft') 
    </div>
    <div class="col-md-9">
        <ul class="progress">
    <li class="completed">
        <span>
            <span class="order">1 </span>Step 1 <span class="inner hidden-sm">(Login)</span>
        </span>
        <div class="diagonal"></div>
    </li>
    <li class="completed">
        <span>
            <span class="order">2 </span>Step 2 <span class="inner hidden-sm">(Information)</span>
        </span>
        <div class="diagonal"></div>
    </li>
    <li>
        <span>
            <span class="order">3 </span>Step 3 <span class="inner hidden-sm">(Agreement)</span>
        </span>
    </li>

     <li>
        <span>
            <span class="order">4 </span>Step 4 <span class="inner hidden-sm">(Payment)</span>
        </span>
    </li>
</ul>
      
   

@if(session()->has('message'))
    <div class="alert alert-danger">
        {{ session()->get('message') }}
    </div>
@endif

    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
          <div class="col-md-12 well">
            <h4 class="text-center">Documents & Information</h4>
            <div class="row">
              
              <div class="col-md-12">
                <form action="{{route('upload_information') }}" enctype="multipart/form-data" id="upload_information" method="post" onsubmit="return Validate(this);"> 
                  {{ csrf_field() }}
                <input type="hidden" value="{{app('request')->input('id')}}" name="id" />
                <input type="hidden" value="{{$months ?? 4 }}" name="duration" />
                <input type="hidden" value="{{$miles ?? 1000}}" name="mile" />
                <input type="hidden" value="{{$price ?? '' }}" name="price" />

                <div class="row">
                  <div class="col-md-12">
                    
                 <div class="row">
             <div class="col-md-6">
              <div class="form-group">DVLA License(Front)
                <input name="file_dvla" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="DVLA" required="">
              </div>
            </div>
                      <div class="col-md-6"><div class="form-group">DVLA License(Back)<input name="file_cnic" id="file_cnic" class="form-control form-control-lg" type="file" placeholder="Passport" required=""></div></div> 
                    </div>    
                 
                    
                  </div>
                  <div class="col-md-12">
                   
                    
                  <div class="row">
                      <div class="col-md-6"><div class="form-group"><input id="phone" name="phone" type="text" class="form-control" value="{{@session()->get('phone')}}" placeholder="Phone"  required="required" maxlength="18" minlength="6" onkeypress="return isNumber(event)"  /></div></div>
                      <div class="col-md-6"><div class="form-group"><input id="autocomplete" name="address" type="text" class="form-control" value="{{@session()->get('address')}}" placeholder="Address"  required="required"  /></div></div>
                    </div>  
                    
                    
                    <div class="row"> 
                      <div class="col-md-6"><div class="form-group"> <input  type="text"  required="required"  class="form-control" value="{{@session()->get('dob')}}" autocomplete="off" placeholder="Date of Birth" name="dob" id="datepicker" />
                      </div></div>
                      <div class="col-md-6"><input type="text" name="dvla" id="dvla" class="form-control" value="{{@session()->get('dvla')}}" onkeypress="return blockSpecialChar(event)"  maxlength="17" minlength="5" required="required"  placeholder="DVLA License Number" /></div>



                    </div>
                         <div class="row"> 
                          <div class="col-md-6"><div class="form-group"> <input  type="text"  required="required"  class="form-control"   autocomplete="off" placeholder="D.V.L.A Expiry Date" name="expiredate" id="datepicker2" />
                      </div></div>
                         </div>
                       
                      <div class="row">
                        <div class="col-md-6"><div class="form-group"> 

                          <input type="text" onkeypress="return blockSpecialChar(event)"  required="required"  class="form-control" value="{{@session()->get('national_insu_numb')}}" placeholder="National Insurance Number (Ex: AB 12 34 56 D)" maxlength="13" minlength="8"  name="national_insu_numb" id="national_insu_numb" />  <p>Ex: AB 12 34 56 D</p>
                        </div>
                        </div>

                      @if($vehicles->vehicle_category==1)  
                        <div class="col-md-6"><input maxlength="20" onkeypress="return blockSpecialChar(event)"  minlength="6"   type="text" name="pco_licence_no" id="pco_licence_no" class="form-control" required="required" value="{{@session()->get('pco_licence_no')}}" placeholder="PCO License Number" />
                        </div>
                        <div class="col-md-6">PCO Driver Badge<input name="doc_other" id="doc_other" class="form-control form-control-lg" type="file" placeholder="PCO Licence No" required="required"> <p class="driver-badge">Must be a photograph of the front of current PCO Driver Badge</p> 
                         </div>
                        <div class="col-md-6">PCO paper licence<input name="pco_paper_licence" id="pco_paper_licence" class="form-control form-control-lg" type="file" placeholder="PCO Licence No" required="required"> <p class="driver-badge">Your image, PCO Number, Name and Expiry Date must be clearly shown</p>
                         </div>
                         @endif
                         @if($insurance == 1)
             <div class="col-md-6">Policy Number<input maxlength="50"    minlength="6"   type="text" name="policy_number" id="pco_licence_no" class="form-control" required="required"   placeholder="Policy Number" />
                        </div> <div class="col-md-6">Cover Type<input maxlength="20"    minlength="6"   type="text" name="cover" id="pco_licence_no" class="form-control" required="required"   placeholder="Cover" />
                        </div>
                        <div class="col-md-6">Insurance Company Name <input        type="num" name="Insurance_company_name" id="pco_licence_no" class="form-control" required="required"   placeholder="Insurance Company Name" />
                        </div>
                        <!-- <div class="col-md-6">Insurance Company Email<input   type="email" name="Insurance_company_email" id="pco_licence_no" class="form-control" required="required"  placeholder="Insurance Company Email" />
                        </div> -->
                        <!-- <div class="col-md-6">Insurance Company Contact Number <input maxlength="20"    minlength="6"   type="number" name="Insurance_company_contact_number" id="pco_licence_no" class="form-control" required="required" value="{{@session()->get('pco_licence_no')}}" placeholder="Insurance Company Contact" />
                        </div> -->
                        <div class="col-md-6">Customer's Private Insurance Certificate<input name="customer_private_insurance" id="pco_paper_licence" class="form-control form-control-lg" type="file" placeholder="PCO Licence No" required="required"> <p class="driver-badge">Your image must be clearly shown</p>
                         </div>
                          @if($vehicles->vehicle_category==2)
                          <div  class="col-md-6">
                            
                          </div>
                          @else
                          @endif
              
                        <div class="col-md-6">Insurance Start Date <input maxlength="20" onchange="change_date()"  minlength="6"   type="date" name="Insurance_started_date" id="insurance_started_date" class="form-control" required="required" value="{{date('Y-m-d')}}"   /></div>




                          <div class="col-md-3">
                            <br>
                          <select name="hr"  class="form-control" required="required">
                               <option value="0">Hours</option>
                              <?php
                              for($j=0;$j<=23;$j++){
                              echo "<option value='".$j."'>".$j."</option>";
                              }
                              ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                          <br>
                          <select name="min"  class="form-control" required="required">
                            <option value="0">Minutes</option>
                              <?php
                              for($j=0;$j<=59;$j++){
                              echo "<option value='".$j."'>".$j."</option>";
                              }
                              ?>
                            </select>
                          </div> 
                    <div class="col-md-6">Insurance End Date <input maxlength="20" min="2021-04-27"    minlength="6"   type="date" name="Insurance_end_date"  value="{{date('Y-m-d')}}" id="insurance_end_date" class="form-control" required="required"  placeholder="Insurance End Date"  />
                        </div>
                        <div class="col-md-3">
                            <br>
                          <select name="ehr"  class="form-control" required="required" >
                               <option value="0">Hours</option>
                              <?php
                              for($i=0;$i<=23;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                          <br>
                          <select name="emin"  class="form-control" required="required">
                            <option value="0">Minutes</option>
                              <?php
                              for($i=0;$i<=59;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                        

                        
 <!--  <div class="accordion" id="accordionExample">
   
    <div class="card-header" id="headingOne">
      <button class="btn btn-primary btn-sm text-center" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Customer insurance 
        </button>
       
    </div>
  </div>

  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
    <div class="container">
     <div class="row">
        <div class="col-md-6">Policy Number<input name="pco_paper_licence" id="pco_paper_licence" class="form-control form-control-lg" type="text" placeholder="Policy Number" required="required">    
        </div>
        <div class="col-md-6">Cover<input name="pco_paper_licence" id="pco_paper_licence" class="form-control form-control-lg" type="text" placeholder="Cover" required="required"> 
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">Company Name<input name="pco_paper_licence" id="pco_paper_licence" class="form-control form-control-lg" type="text" placeholder="Company Name" required="required">
        </div>
        <div class="col-md-6">Company Contact Number<input name="pco_paper_licence" id="pco_paper_licence" class="form-control form-control-lg" type="text" placeholder="Contact Number" required="required">  
        </div>

      </div>
      <div class="row">
        <div class="col-md-6">Company Email<input name="pco_paper_licence" id="pco_paper_licence" class="form-control form-control-lg" type="text" placeholder="PCO Licence No" required="required">  
        </div>

        <div class="col-md-6">Private insurance certificate
          <input 
                          name="private_cartificate" id="private_cartificate" class="form-control form-control-lg" type="file" placeholder="PCO Licence No" accept="image/*" required="required"> <p class="driver-badge">Your image,must be clearly shown</p>
       </div>

      </div>


    </div>
  </div>

  -->                      @else
                       @endif
                        <!--  <div class="col-md-6">PCO Driver Badge(Back)<input name="pco_driver_back" id="pco_driver_back" class="form-control form-control-lg" type="file" placeholder="PCO Licence No" required="required">
                          <p class="driver-badge">
                           Must be a photograph of the front of current PCO Driver Badge<br>
                           Your image, PHL Number, Name and Expiry Date must be clearly shown<br>

                         </p> -->
                         
                         </div>
                          

                      
                        </div>
                  

                        @php
                      if($disable_action==1) echo '<div align="center" style="margin-left: 20px">
                        <button type="submit"  class="btn btn-primary btn-sm text-center">Upload & Next</button></div>'; else echo '<div class="alert alert-danger">'.$disable_action.'</div>';
                        @endphp
                      </div>
                    </form></div>
                  </div>
                </div> 
              </div> 


       
    </div>
    
  </div>
</div>
</div>
</div>
</div>
@include('layouts.footer')
</body>
@endsection
@section('scripts')
<script>
 

    $( function() {
    $( "#booking_swipe_date" ).datepicker(
      {
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate:new Date(),
       maxDate: +14,
     });
}
  });
        $( function() {
    $( "#booking_swipe_date" ).datepicker2(
      {
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate:new Date(),
       maxDate: +14,
     });
}
  });




</script>
@endsection

<style>
  .driver-badge{font-size: 8px; }
</style>
 <script type="text/javascript">


    function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
        }
  }
 
 
$(document).ready(function() {  

 

 validate();
 $('input').on('keyup', validate);




// $('#btn_upload_info').click(function(event){
$( "#btn_upload_info" ).click(function() {
event.preventDefault();
var formData = new FormData($('#upload_information')[0]);
formData.append('#file_dvla', $('input[type=file]')[0].file_dvla[0]);
//alert(formData);
// formData.append('#file_cnic', $('input[type=file]')[0].file_cnic[0]);
//  $('#f_name').val(    $('input[type=file]').val()     );
$.ajax({
// url:"http://ip.jsontest.com/",
url: '{{ url("booking/upload_information") }}',
method:"POST",
data:formData,
//dataType:'JSON',
contentType: false,
cache: false,
processData: false,
success:function(data)
{
alert(data);
},
error: function(data)
{
//console.log(data);
alert('error');
}
})
});
var navListItems = $('ul.setup-panel li a'),
allWells = $('.setup-content');
allWells.hide();
navListItems.click(function(e)
{
e.preventDefault();
var $target = $($(this).attr('href')),
$item = $(this).closest('li');
if (!$item.hasClass('disabled')) {
navListItems.closest('li').removeClass('active');
$item.addClass('active');
allWells.hide();
$target.show();
}
});
$('ul.setup-panel li.active a').trigger('click');
// DEMO ONLY //
$('#activate-step-2').on('click', function(e) {
$('ul.setup-panel li:eq(1)').removeClass('disabled');
// $('ul.setup-panel li a[href="#step-1').addClass('done');
$('ul.setup-panel li a[href="#step-2"]').trigger('click');
$(this).remove();
})
//list-group-item-heading
//   <span class="glyphicon glyphicon-ok"></span>
$('#activate-step-3').on('click', function(e) {
$('ul.setup-panel li:eq(2)').removeClass('disabled');
$('ul.setup-panel li a[href="#step-3"]').trigger('click');
$(this).remove();
}) ;
@if(app('request')->input('step')==3)
$('ul.setup-panel li:eq(3)').removeClass('disabled');
@endif
});
</script>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
$(function() {

var $form         = $(".require-validation");

$('form.require-validation').bind('submit', function(e) {
var $form         = $(".require-validation"),
inputSelector = ['input[type=email]', 'input[type=password]',
'input[type=text]', 'input[type=file]',
'textarea'].join(', '),
$inputs       = $form.find('.required').find(inputSelector),
$errorMessage = $form.find('div.error'),
valid         = true;
$errorMessage.addClass('hide');

$('.has-error').removeClass('has-error');
$inputs.each(function(i, el) {
var $input = $(el);
if ($input.val() === '') {
  $input.parent().addClass('has-error');
  $errorMessage.removeClass('hide');
  e.preventDefault();
}
});

if (!$form.data('cc-on-file')) {
e.preventDefault();
Stripe.setPublishableKey($form.data('stripe-publishable-key'));
Stripe.createToken({
number: $('.card-number').val(),
cvc: $('.card-cvc').val(),
exp_month: $('.card-expiry-month').val(),
exp_year: $('.card-expiry-year').val()
}, stripeResponseHandler);
}

});

function stripeResponseHandler(status, response) {
if (response.error) {
$('.error')
.removeClass('hide')
.find('.alert')
.text(response.error.message);
} else {
/* token contains id, last4, and card type */
var token = response['id'];

$form.find('input[type=text]').empty();
$form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
$form.get(0).submit();
}
}

});
</script>