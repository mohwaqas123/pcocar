@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page" style="margin-top: -51px;">
  <div class="content">
    <div class="container-fluid">
      @if(session()->has('message'))
      <div class="alert alert-success"  >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        {{ session()->get('message') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="row">
        <div class="col-md-6">
          <div class="card-box">
            <h4>Current Booking</h4>
            <p>Started: {{$booking->booking_start_date}} to {{$booking->booking_drop_off_date}} </p>
            
            <p>Duration: {{$booking->duration}} weeks</p>
            <p>Rent:£{{$booking->price}} </p>
            <p>Registration:{{$vehicle->licence_plate_number}}  {{$vehicle->car_make->name}} {{$vehicle->car_model->name}} {{$vehicle->year}}</p>
            
            
            
            <!-- end row -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="card-box">
            <h4>Customer Details</h4>
            
            <p>Name:  {{$booking->user->name}} </p>
            <p>Phone no:{{$booking->user->phone}} </p>
            
            <p>Address: {{$booking->address}}</p>
            <p>Licence: {{$booking->dvla}}</p>
            
            
            
          </div>
        </div>
        <div class="col-md-12">
          
          <div class="card-box" >
            
            <form action="{{route('swipebooking_add')}}"  method="post"   >
              @csrf
              <input type="hidden" name="id" value="{{$booking->id }}">
              <input type="hidden" name="old_vehicle_id" value="{{$booking->vehicle_id }}" />

              <div class="row">
                <div class="form-row col-md-6" id="customer">
                  <label for="make" class="col-form-label">Vehicle</label>
                  <select  id="licence_plate_number"  name="new_vehicle_id" placeholder="Staff Drop Down"
                    class="form-control select2" id="mySelect"  >
                   
                    @foreach($vehicle_available as $vehicle_available_us)
                    <!-- only for the discount price are show -->
                    <?php
                     $price  =  $vehicle_available_us->price / 2;
                    ?>


                    <option value="{{$vehicle_available_us->id}}"> {{$vehicle_available_us->name}}
                    {{$vehicle_available_us->year}}
                    {{$vehicle_available_us->licence_plate_number}}
                    {{ @$vehicle_available_us->car_make->name }},
                    Price:{{$price}}$

                  </option>
                    @endforeach
                  </select>
                </div>
               <!--  @if($booking->insurance_type == 1) 
                <div class="form-row col-md-6" >
                  <label class="col-form-label">Note<span class="text-danger">*</span></label>
                  <br><br>
                  <p>This customer send the request for provided the Private citificate</p>
                    
                </div>
                @else
                @endif -->



              </div>
             
        <!--         <div class="row">
                  <div class="form-row col-md-6">
                    <label for="make" class="col-form-label">Date<span class="text-danger">*</span></label>
 
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

 
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>  
          <input type="date" required="required" class="form-control date_appy hasDatepicker" value="{{date('Y-m-d')}}" autocomplete="off" placeholder="" name="booking_swipe_date" id="booking_swipe_date">


  <script>

 
    $( function() {
    $( "#booking_swipe_date" ).datepicker(
      {
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate:new Date(),
       maxDate: +14,
     });
  });
</script>
  
  
                  </div>
                  <div class="form-row col-md-2">
                    <label for="Hours" class="col-form-label">Hours<span class="text-danger">*</span></label>
                    <select name="min"  class="form-control" >
                      <?php
                      for($i=1;$i<=12;$i++){
                      echo "<option value='".$i."'>".$i."</option>";
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-row col-md-2">
                    <label for="Minutes" class="col-form-label">Minutes<span class="text-danger">*</span></label>
                    <select name="hr"  class="form-control" >
                      <?php
                      for($i=1;$i<=60;$i++){
                      echo "<option value='".$i."'>".$i."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div> -->
                <div class="row">
                  <div class="form-row col-md-12 mb-3">
                    <label for="make" class="col-form-label">Detail<span class="text-danger">*</span></label>
                    
                    <textarea class="form-control summernote" hieght="400" width="100%" name="detail" id="detail" title="error message" maxlength="700" required="required"></textarea>
                  </div></div>
                  
                   <button type="submit" id="btn" class="btn btn-primary">Submit</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('scripts')
    <script>
    $('#licence_plate_number').change(function(event) {
    var url_call = "{!! url('/') !!}/admin/get_vehicle";
    var vehicle_id = this.value;
    $.ajax({
        type : 'get',
        url  : url_call,
        //  data: {},
        data: { vehicle_id: vehicle_id},
        contentType: 'application/json; charset=utf-8',
        success :  function(data){
//alert(data);
console.log(data);
        $("#customer").empty();
         $("#customer").append(data);
 
     }
    });//ajax
});
</script>
    @endsection