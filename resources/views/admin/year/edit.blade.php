@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title">Year</h4>
                        {{--  <p class="text-muted font-14 m-b-30">
                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                        </p>  --}}
                        <div class="card-box">
                            <h4 class="m-t-0 header-title">Edit Year</h4>
                            @if($errors->any())
                                {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif

                            <form role="form" action="{{ route('adminyearUpdate') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                  <div class="form-group col-md-6">
                                    <label for="name">Year <span class="required-star">*</span></label>
                                    <input type="text" class="form-control @error('year') is-invalid @enderror" name="year" id="name" placeholder="Enter Year" value="{{ old('year', $year->year) }}" required>
                                    <input type="hidden" name="id" value="{{ $year->id }}">
                                    @error('year')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                  </div>

                                  <div class="col-md-6">
                                    <button type="submit" class="btn btn-info btn-flat">Update</button>
                                  </div>

                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
