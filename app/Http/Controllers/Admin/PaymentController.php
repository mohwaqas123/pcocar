<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Booking;
use App\models\Vehicle;
use App\models\PaymentHistory;


class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $booking = Booking::where('id', $request->id)->with('vehicle')->first();   
       $vehicles = Vehicle::where('id',  $booking->vehicle_id)->first(); 
       $data = [
        'booking_id' => $booking->id, 
        'payment_type' => 1,
        'user_id' => $booking->user->id,
        'amount' => $booking->price,
        'transaction_id ' => $booking->transaction_id,
        'vehicle_id' => $vehicles->id,
        'month_duration' => $booking->duration,
        'per_month_amount' => $booking->first_payment,
        'per_month_amount' => $booking->first_payment,
        'payment_mode' => 2,
        'payment_status' => 1,
      ];
        $payment_history = PaymentHistory::create($data);
           return redirect()->back()->with('message','Payment Recived Successfully.');



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
