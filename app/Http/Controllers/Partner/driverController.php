<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Partner;
use App\models\Booking;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use App\models\Driver;

class driverController extends Controller
{





     public function create(Request $request)
            {
             
          
                $make = Make::all();  $car_model = CarModel::all();  
                $url = "partnerdashboard";
            $data = [
            'car_model'=> $car_model, 'make' =>$make, 'breadcrum' => '<a
            style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  >
            '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Driver create',
            ];  
              
               return view('partner.driver.create')->with($data);
            }
             public function store(Request $request)

            {
      		 
    

          $driverData = [
            'name' => $request->name, 
            'phone' => $request->phone, 
             'email' => $request->email, 
             'driver_status'=>0,
             'created_from'=>0,
             
             'partner_id'=> $request->session()->get('p_user_id')
             

        ];
       
        $vehicle = Driver::create($driverData);
        	 $Total_driver_add_by_partner=Driver::where('partner_id',  
                    $request->session()->get('p_user_id'))->get();
        $data= [
        	  'Total_driver_add_by_partner'=>$Total_driver_add_by_partner,
        ];
      return view('partner.driver.list')->with($data);
            }
           public function list(Request $request)

            {
             
       
                $Total_driver_add_by_partner=Driver::where('partner_id',  
                    $request->session()->get('p_user_id'))->get();
                 // $driver = Driver::orderBy('id', 'DESC')->paginate(10);
                $url = "partnerdashboard";

                $data = [
                    'Total_driver_add_by_partner'=>$Total_driver_add_by_partner,
                    'title' => 'Driver',
                    'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Driver List',
                
                ];
          
        return view('partner.driver.list')->with($data);
            }
 public function edit($id)
 {
    
    

    $driver = Driver::find($id);
        if ($driver == null) {
            return redirect()->back()->with('error', 'No Recor Found');
        }
             $url = "partnerdashboard";
         $data = [
                    'driver' => $driver,
                    
                    'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Driver Edit',
                
                ];


        return view('partner.driver.edit')->with($data);




       // $Total_driver_add_by_partner=Driver::where('In_status', '=', '2')->get();
        
       //  return view('partner.driver.list', ['Total_driver_add_by_partner'=>$Total_driver_add_by_partner, 'title' => 'Driver']);
          
    }  
    public function update(Request $request)
 {    

    $validator = $request->validate([
            'email' => 'required|max:191|unique:drivers',
            'name' => 'required',
            'phone' =>'required',
            
        ]);
   
       
     $driver = Driver::where('id', $request->id)->first(); 
        $driverData = [
                'name' => $request->name,  
                'phone' => $request->phone,
                'email' =>$request->email, 
                'driver_status'=>0,
                    //  'created_from'=>0,
             'partner_id'=>$request->session()->get('p_user_id') 
];

 

$driver->update($driverData);
$driver->save(); 

 
 $Total_driver_add_by_partner=Driver::where('partner_id',  
                    $request->session()->get('p_user_id'))->get();


 		$data= [
     	'Total_driver_add_by_partner'=>$Total_driver_add_by_partner,
     	'driver'=>$driver,


        ];

			return view('partner.driver.list')->with($data);
  
       }
    public function delete(Request $request,$id)
	{
    $driver = Driver::findOrFail($id);
	$driver->delete();
 
	$Total_driver_add_by_partner=Driver::where('partner_id',  
                    $request->session()->get('p_user_id'))->get();

	 
	 
 	return view('partner.driver.list',[
                     'Total_driver_add_by_partner' => $Total_driver_add_by_partner, ]);
    }

            
}
