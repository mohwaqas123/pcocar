@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')


 
<div class="container">
  
 

  <div class="row">
    <div class="col-md-3 text-center">
        @include ('frontend.bookingleft') 
    </div>
    <div class="col-md-9">
        <ul class="progress">
    <li class="completed">
        <span>
            <span class="order">1 </span>Step 3 <span class="inner">(Login)</span>
        </span>
        <div class="diagonal"></div>
    </li>
    <li class="completed">
        <span>
            <span class="order">2 </span>Step 2 <span class="inner">(Information)</span>
        </span>
        <div class="diagonal"></div>
    </li>
    <li>
        <span>
            <span class="order">3 </span>Step 3 <span class="inner">(Agreement)</span>
        </span>
    </li>

     <li>
        <span>
            <span class="order">4 </span>Step 4 <span class="inner">(Payment)</span>
        </span>
    </li>
</ul>
      
   



    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
          <div class="col-md-12 well">
            <h4 class="text-center">Documents & Information</h4>
            <div class="row">
              
              <div class="col-md-12">
                <form action="{{route('upload_information') }}" enctype="multipart/form-data" id="upload_information" method="post">   {{ csrf_field() }}
                <input type="hidden" value="{{app('request')->input('id')}}" name="id" />
                <input type="hidden" value="{{$months ?? 4 }}" name="duration" />
                <input type="hidden" value="{{$miles ?? 1000}}" name="mile" />
                <input type="hidden" value="{{$price ?? '' }}" name="price" />

                <div class="row">
                  <div class="col-md-12">
                    
                 <div class="row">
             <div class="col-md-6"><div class="form-group">DVLA Licence(Front)<input name="file_dvla" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="DVLA" required=""></div></div>
                      <div class="col-md-6"><div class="form-group">DVLA Licence(Back)<input name="file_cnic" id="file_cnic" class="form-control form-control-lg" type="file" placeholder="Passport" required=""></div></div> 
                    </div>    
                 
                    
                  </div>
                  <div class="col-md-12">
                   
                    
                  <div class="row">
                      <div class="col-md-6"><div class="form-group"><input id="phone" name="phone" type="number" class="form-control" value="" placeholder="Phone"  required="required"  /></div></div>
                      <div class="col-md-6"><div class="form-group"><input id="autocomplete" name="address" type="text" class="form-control" value="" placeholder="Address"  required="required"  /></div></div>
                    </div>  
                    
                    
                    <div class="row"> 
                      <div class="col-md-6"><div class="form-group"> <input type="text"  required="required"  class="form-control" value="" placeholder="Date of Birth" name="dob" id="datepicker" />
                      </div></div>
                      <div class="col-md-6"><input type="text" name="dvla" id="dvla" class="form-control" value="" onkeypress="return blockSpecialChar(event)"  maxlength="20" minlength="5"  placeholder="DVLA License Number" /></div></div>
                       
                      <div class="row">
                        <div class="col-md-6"><div class="form-group"> 

                          <input type="text" onkeypress="return blockSpecialChar(event)"  required="required"  class="form-control" value="" placeholder="National Insurance Number (Ex: AB 12 34 56 D)" maxlength="12" minlength="9"  name="national_insu_numb" id="national_insu_numb" />  <p>Ex: AB 12 34 56 D</p></div></div>
                        <div class="col-md-6"><input maxlength="20" minlength="6"   type="text" name="pco_licence_no" id="pco_licence_no" class="form-control" value="" placeholder="PCO License Number" />
                          </div></div>
                        <button type="submit"  class="btn btn-primary btn-sm text-center">Upload & Next</button>
                        
                      </div>
                    </form></div>
                  </div>
                </div> 
              </div> 


       
    </div>
    
  </div>
</div>
</div>
</div>
</div>
@include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection

 <script type="text/javascript">
    function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
        }
 
 
$(document).ready(function() {  





 validate();
 $('input').on('keyup', validate);




// $('#btn_upload_info').click(function(event){
$( "#btn_upload_info" ).click(function() {
event.preventDefault();
var formData = new FormData($('#upload_information')[0]);
formData.append('#file_dvla', $('input[type=file]')[0].file_dvla[0]);
//alert(formData);
// formData.append('#file_cnic', $('input[type=file]')[0].file_cnic[0]);
//  $('#f_name').val(    $('input[type=file]').val()     );
$.ajax({
// url:"http://ip.jsontest.com/",
url: '{{ url("booking/upload_information") }}',
method:"POST",
data:formData,
//dataType:'JSON',
contentType: false,
cache: false,
processData: false,
success:function(data)
{
alert(data);
},
error: function(data)
{
//console.log(data);
alert('error');
}
})
});
var navListItems = $('ul.setup-panel li a'),
allWells = $('.setup-content');
allWells.hide();
navListItems.click(function(e)
{
e.preventDefault();
var $target = $($(this).attr('href')),
$item = $(this).closest('li');
if (!$item.hasClass('disabled')) {
navListItems.closest('li').removeClass('active');
$item.addClass('active');
allWells.hide();
$target.show();
}
});
$('ul.setup-panel li.active a').trigger('click');
// DEMO ONLY //
$('#activate-step-2').on('click', function(e) {
$('ul.setup-panel li:eq(1)').removeClass('disabled');
// $('ul.setup-panel li a[href="#step-1').addClass('done');
$('ul.setup-panel li a[href="#step-2"]').trigger('click');
$(this).remove();
})
//list-group-item-heading
//   <span class="glyphicon glyphicon-ok"></span>
$('#activate-step-3').on('click', function(e) {
$('ul.setup-panel li:eq(2)').removeClass('disabled');
$('ul.setup-panel li a[href="#step-3"]').trigger('click');
$(this).remove();
}) ;
@if(app('request')->input('step')==3)
$('ul.setup-panel li:eq(3)').removeClass('disabled');
@endif
});
</script>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
$(function() {

var $form         = $(".require-validation");

$('form.require-validation').bind('submit', function(e) {
var $form         = $(".require-validation"),
inputSelector = ['input[type=email]', 'input[type=password]',
'input[type=text]', 'input[type=file]',
'textarea'].join(', '),
$inputs       = $form.find('.required').find(inputSelector),
$errorMessage = $form.find('div.error'),
valid         = true;
$errorMessage.addClass('hide');

$('.has-error').removeClass('has-error');
$inputs.each(function(i, el) {
var $input = $(el);
if ($input.val() === '') {
$input.parent().addClass('has-error');
$errorMessage.removeClass('hide');
e.preventDefault();
}
});

if (!$form.data('cc-on-file')) {
e.preventDefault();
Stripe.setPublishableKey($form.data('stripe-publishable-key'));
Stripe.createToken({
number: $('.card-number').val(),
cvc: $('.card-cvc').val(),
exp_month: $('.card-expiry-month').val(),
exp_year: $('.card-expiry-year').val()
}, stripeResponseHandler);
}

});

function stripeResponseHandler(status, response) {
if (response.error) {
$('.error')
.removeClass('hide')
.find('.alert')
.text(response.error.message);
} else {
/* token contains id, last4, and card type */
var token = response['id'];

$form.find('input[type=text]').empty();
$form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
$form.get(0).submit();
}
}

});
</script>