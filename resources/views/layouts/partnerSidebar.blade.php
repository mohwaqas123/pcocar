<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="#" class="logo">
                <span>
                    <img src="assets/images/logo.png" alt="" height="22">
                </span>
                <i>
                    <img src="assets/images/logo_sm.png" alt="" height="28">
                </i>
            </a>
        </div>

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{ asset('public/theme/assets/images/admin.png') }}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>

 
            <h5><a href="#"></a> </h5>
            <p class="text-muted">{{Session::get('p_name')}}</p>

              <a href="{{ route('adminLogout') }}" class="dropdown-item notify-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>



        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->
                 <li>
                    <a href="{{ route('partnerdashboard') }}">
                        <i class="fi-air-play"></i><span> Dashboard </span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-automobile"></i> <span> Vehicle    </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                    
                        <li>
                    <a href="{{ route('p_vehicleCreate') }}">  <span>Add Vehicle </span>  </a>
                    
                    </li>
                    <li>
                        <a href="{{ route('p_vehicleList') }}">  <span> Vehicle List  </span>  </a>
                    
                    </li>

                     
                    </ul>
                </li>
                 <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Bookings    </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                         <li><a href="{{ route('partner_vehicle_booking') }}">Active Booking</a></li>
                    </ul>
<<<<<<< HEAD
                </li>
=======
                </li> 
                 <li>
                    <a href="javascript: void(0);"><i class="dripicons-bookmarks"></i> <span> Damage    </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                    <li>
                    <a href="{{ route('p_damageCreate') }}">Record Damage  
                    </a>
                    </li> 
                    <li>
                        <a href="{{ route('p_damageList') }}">All Damages</a>
                    </li>
                    </ul>
                </li>

>>>>>>> 9c370ab2e9863d7207f81ef8fe16cd608985da92
                


              <!--   <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Drivers Account  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('partner_driver_list') }}">Drivers Account List</a></li>
                        <li><a href="{{ route('partner_driver_add') }}">Drivers Account Add</a></li>
                       


                        
                    </ul>
                </li> 

                 <li>
                    <a href=""><i class="fi-layers"></i> <span>Payments</span>  </a>
                    
                </li>  -->

<!--             <li>
                    <a href=""><i class="fi-layers"></i> <span>Penalty charge notices  </span>  </a>
                    
                </li>
 
                <li>
                    <a href=""><i class="fi-layers"></i> <span>Damage Claim  </span>  </a>
                    
                </li> -->
            </ul>

        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>


