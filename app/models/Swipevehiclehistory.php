<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Swipevehiclehistory extends Model
{
	protected $table = 'swipevehiclehistory';
 	protected $fillable = [
     			'booking_id', 	'user_id', 'vehicle_id',
     				];

}
