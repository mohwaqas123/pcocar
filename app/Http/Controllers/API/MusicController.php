<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Device;
use App\models\Category;
use App\models\Song;
use App\models\Banner;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;
use Storage;
use Illuminate\Http\File;

class MusicController extends Controller
{
    private $responseConstants;
    private $paginationConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
        $this->paginationConstants = Config::get('constants.PAGINATION_CONSTANTS');
    }

    public function musicAgainstCategory(Request $request)
    {
        $rules = [
            'category_id' => 'required',
            'page_no' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }
        
        $allSongs = Song::where('category_id', $request->category_id);
        $allBanners = Banner::where('status', 1)->get();
        $allCategories = Category::where('status', 1)->get();

        $totalPages = 0;

        if($allSongs->count() > 0)
        {
            $pages = $request->page_no *  $this->paginationConstants['KEY_RECORD_PER_PAGE'];

            $totalPages = $allSongs->count() / $this->paginationConstants['KEY_RECORD_PER_PAGE'];
            $totalPages = ceil($totalPages);

            $allSongs = $allSongs->orderBy('created_at', 'ASC')
            ->offset($pages)
            ->limit($this->paginationConstants['KEY_RECORD_PER_PAGE'])
            ->get();

            if($allSongs != null)
            {
                $allSongs->makeHidden([
                    'created_at', 'updated_at'
                ]);
            }
            
            if($allBanners != null)
            {
                $allBanners->makeHidden([
                    'created_at', 'updated_at'
                ]);
            }
            
            if($allCategories != null)
            {
                $allCategories->makeHidden([
                    'created_at', 'updated_at'
                ]);
            }

            $data['banners'] = $allBanners;
            $data['categories'] = $allCategories;
            $data['songs'] = $allSongs;

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'All songs list.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'current_page' => (int)$request->page_no,
                'total_pages' => $totalPages,
                'data' => $data,
            ]);
        }

        $data['banners'] = $allBanners;
        $data['categories'] = $allCategories;
        $data['songs'] = $allSongs;

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'All songs list.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'current_page' => $request->page_no,
            'total_pages' => $totalPages,
            'data' => $data,
        ]);
    }
}
