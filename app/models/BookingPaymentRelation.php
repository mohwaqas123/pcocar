<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class BookingPaymentRelation extends Model
{
    //
    protected $table = 'booking_payment_relation';


    protected $fillable = [  
          'booking_id' , 
          'weekly_payment' ,
           'price',
           'payment_status',
          'due_date',
         
    ]; 



    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
 
}
