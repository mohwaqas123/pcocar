<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class GpsLocationMilage extends Model
{
       protected $table = 'gps_location_milage';

    protected $fillable = [
        'name',
        'imei',
        'device',
        'odometer',
        'c_date',
         
    ];
}
