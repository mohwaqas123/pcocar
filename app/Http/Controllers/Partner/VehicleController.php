<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use App\models\VehicleImage;
use Session;
//use Auth;

class VehicleController extends Controller
{

    public function index(Request $request)
    {

        $vehicles = Vehicle::where('partner_id',$request->session()->get('p_user_id') )->orderby('id', 'Desc')->paginate(20);
        $make = Make::all();

        $url = "/partnerdashboard";
        $data = [
                'vehicles'=> $vehicles,
                'breadcrum' => '
                <h4 class="page-title">Dashboard </h4>
                                    <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="color:black; &nbsp" href="'. url( $url ).'"  >Dashboard</a></li>
                            <li class="breadcrumb-item">Vehicle List</li>
                            </ol>',
                
        ];  

 
        return view('partner.vehicle.list')->with($data);
    }




  public function temp_delete_img(Request $request){
    //$fname = "temp_store".' \ '. ;

    $img = VehicleImage::where('id', $request->id)->pluck('images');
    $v_image = 'vehicleImages/'.$img;   
    Storage::delete($v_image);
    
    $vehicle = VehicleImage::find($request->id);
    $vehicle->delete(); 
    
    //$pathToYourFile = public_path(Auth::user()->id."\\".$request->get('f_name'));
    //unlink($pathToYourFile);
    //return $pathToYourFile;
    return redirect()->back()->with('success','Removed image Successfully');
    


 }


    public function create()
    {
        $make = Make::all(); 
        $car_model = CarModel::all(); 
           $url = "/partnerdashboard";
          $data = [
               'car_model'=> $car_model,
               'make' =>$make, 
                  'breadcrum' => '
                <h4 class="page-title">Vehicle </h4>
                                    <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="color:black; &nbsp" href="'. url( $url ).'"  >Dashboard</a></li>
                            <li class="breadcrumb-item">Add Vehicle</li>
                            </ol>',
           ];  
                  
        return view('partner.vehicle.create')->with($data); 
 
    }

    public function store(Request $request)
    {
        //dd(json_encode($request->chk));

        $validatedData = $request->validate([
            'name' => 'required',
            'make' => 'required',
           // 'passenger' => 'required',
            'price' => 'required',
            'description' => 'required',
            'transmission' => 'required',
            'model' => 'required',
            'year' => 'required',
            'thumbnail' => 'required',
            'postal_pickup_dropoff'=> 'required'
        ]);



         $doc_mot='';
         $doc_logback='';
         $img_exterior_front='';
         $doc_phv='';
         $img_exterior_back='';
         $img_exterior_front2='';
         $img_exterior_dashboard='';

        //documents upload process
         if ($request->hasFile('doc_mot') || $request->hasFile('doc_logback') || $request->hasFile('doc_phv') || $request->hasFile('img_exterior_front') || $request->hasFile('img_exterior_back') || $request->hasFile('img_exterior_front2') || $request->hasFile('img_exterior_dashboard')) {
            $vehicleDocFolder = 'vehicleDocs';

            if (!Storage::exists($vehicleDocFolder)) {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_mot'))
               $doc_mot = Storage::putFile($vehicleDocFolder, new File($request->doc_mot));

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
            
            if ($request->hasFile('doc_phv'))
               $doc_phv = Storage::putFile($vehicleDocFolder, new File($request->doc_phv));
            
            if ($request->hasFile('img_exterior_front'))
               $img_exterior_front = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front));
            
            if ($request->hasFile('img_exterior_back'))
               $img_exterior_back = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_back));
            
            if ($request->hasFile('img_exterior_front2'))
               $img_exterior_front2 = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front2));
            
            if ($request->hasFile('img_exterior_dashboard'))
               $img_exterior_dashboard = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_dashboard));
   

          //  $vehicle->update(['image'=> $imageUrl]);
        }
            
        $vehicleData = [
            'name' => $request->name,
            'make' => $request->make,
            'passenger' => $request->passenger,
            'price' => $request->price,
            'offer' => 0,
            'fuel' => $request->transmission,
         //   'no_of_Passengers' => 2,
            'description' => $request->description,
            'featurs' => json_encode($request->chk),
              'engine_Capacity' =>  $request->engine_Capacity,
            'toll_Charges' => 20,
            'model' => $request->model,
            'year' => $request->year,
            'vehicle_category' => $request->vehicle_category,
            'vehicle_type'=>1,
            'body_type' => $request->body_type,
            'fuel_type'=> $request->fuel_type,
            'city' => $request->city,
            'colour' => $request->colour,
             'licence_plate_number' => $request->licence_plate_number,
             'uber_type' => $request->uber_type,
             'postal_pickup_dropoff'=> $request->postal_pickup_dropoff, 
            'date_from' => $request->date_from,
             'date_to' => $request->date_to,
             'doc_mot'=> $doc_mot  ,
             'doc_logback'=> $doc_logback,
             'img_exterior_front'=> $img_exterior_front,
             'img_exterior_back'=> $img_exterior_back,
             'img_exterior_front2'=> $img_exterior_front2,
             'img_exterior_dashboard'=> $img_exterior_dashboard,
             'doc_phv'=> $doc_phv, 
             'partner_id' => $request->session()->get('p_user_id')


        ];
        
        $vehicle = Vehicle::create($vehicleData);


 
        if ($request->hasFile('thumbnail')) {
            $vehicleImagesFolder = 'vehicleImages';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            $imageUrl = Storage::putFile($vehicleImagesFolder, new File($request->thumbnail));
            $vehicle->update(['image'=> $imageUrl]);
        }

        if ($request->hasFile('images')) {
            $vehicleImagesFolder = 'vehicleImages';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            foreach($request->images as $image)
            {

              $imageUrl = Storage::putFile($vehicleImagesFolder, new File($image));
                VehicleImage::create(
                    [
                        'vehicle_id'=> $vehicle->id,
                        'images' => $imageUrl 
                    ]
                );
            } 
        }


        return redirect()->route('p_vehicleList')->with('success','Vehicle Added Successfully');
    }

    public function destory($id)
    {

        $vehicle = Vehicle::find($id);
 
        $vehicle->delete();

        return redirect()->route('p_vehicleList')->with('message','Vehicle deleted Successfully');
    }



      public function edit(Request $request)
    {
        $vehicle = Vehicle::find($request->id);
        $make = Make::all(); 
        $car_model = CarModel::all(); 

        $url = "/partnerdashboard";

          $data = [
               'car_model'=> $car_model,
               'make' =>$make,
               'vehicle_info'=>   $vehicle,
              'breadcrum' => '
                <h4 class="page-title">Dashboard </h4>
                                    <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="color:black; &nbsp" href="'. url( $url ).'"  >Dashboard</a></li>
                            <li class="breadcrumb-item">Edit Vehicle</li>
                            </ol>',

           ];  
                  
        return view('partner.vehicle.edit')->with($data); 
 
    }




    public function update(Request $request)
    {
  
        $validatedData = $request->validate([
            'name' => 'required',
            'make' => 'required',
       //     'passenger' => 'required',
            'price' => 'required',
            'description' => 'required',
            'transmission' => 'required',
            'model' => 'required',
            'year' => 'required',
          //  'thumbnail' => 'required',
        ]);

        $vehicle = Vehicle::where('id', $request->id)->first();
 

      $doc_mot=$vehicle->doc_mot;
         $doc_logback=$vehicle->doc_logbac;
         $img_exterior_front=$vehicle->img_exterior_front;
         $doc_phv=$vehicle->doc_phv;
         $img_exterior_back=$vehicle->img_exterior_back;
         $img_exterior_front2=$vehicle->img_exterior_front2;
         $img_exterior_dashboard=$vehicle->img_exterior_dashboard;

        //documents upload process
         if ($request->hasFile('doc_mot') || $request->hasFile('doc_logback') || $request->hasFile('doc_phv') || $request->hasFile('img_exterior_front') || $request->hasFile('img_exterior_back') || $request->hasFile('img_exterior_front2') || $request->hasFile('img_exterior_dashboard')) {
            $vehicleDocFolder = 'vehicleDocs';

            if (!Storage::exists($vehicleDocFolder)) {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_mot'))
               $doc_mot = Storage::putFile($vehicleDocFolder, new File($request->doc_mot));

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
            
            if ($request->hasFile('doc_phv'))
               $doc_phv = Storage::putFile($vehicleDocFolder, new File($request->doc_phv));
            
            if ($request->hasFile('img_exterior_front'))
               $img_exterior_front = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front));
            
            if ($request->hasFile('img_exterior_back'))
               $img_exterior_back = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_back));
            
            if ($request->hasFile('img_exterior_front2'))
               $img_exterior_front2 = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front2));
            
            if ($request->hasFile('img_exterior_dashboard'))
               $img_exterior_dashboard = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_dashboard));
   

          //  $vehicle->update(['image'=> $imageUrl]);
        }


           
        $vehicleData = [
            'name' => $request->name,
            'make' => $request->make,
            'passenger' => $request->passenger,
            'price' => $request->price,
            'colour' => $request->colour,
            'offer' => 0,
            'fuel' => $request->transmission,
            'no_of_Passengers' => 2,
            'description' => $request->description,
            'featurs' => json_encode($request->chk),
            'engine_Capacity' =>  $request->engine_Capacity,
           // 'toll_Charges' => 20,
            'model' => $request->model,
            'year' => $request->year,
            'vehicle_category' => $request->vehicle_category,
'vehicle_type'=>1,
            'body_type' => $request->body_type,
            'fuel_type'=> $request->fuel_type,
            'city' => $request->city,


             'licence_plate_number' => $request->licence_plate_number,
             'uber_type' => $request->uber_type,
             'postal_pickup_dropoff'=> $request->postal_pickup_dropoff, 
            'date_from' => $request->date_from,
             'date_to' => $request->date_to,

              'doc_mot'=> $doc_mot  ,
             'doc_logback'=> $doc_logback,
             'img_exterior_front'=> $img_exterior_front,
             'img_exterior_back'=> $img_exterior_back,
             'img_exterior_front2'=> $img_exterior_front2,
             'img_exterior_dashboard'=> $img_exterior_dashboard,
             'doc_phv'=> $doc_phv, 


        ];
 
        $vehicle->update($vehicleData); 
        $vehicle->save();




         if ($request->hasFile('thumbnail')) {
            $vehicleImagesFolder = 'vehicleImages';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            //remove image
            $v_image = 'vehicleImages/'.$vehicle->image;   
            Storage::delete($v_image);


            $imageUrl = Storage::putFile($vehicleImagesFolder, new File($request->thumbnail));
            $vehicle->update(['image'=> $imageUrl]);


        }



//multiple image gallary options
        if ($request->hasFile('images')) {
            $vehicleImagesFolder = 'vehicleImages';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            foreach($request->images as $image)
            {

              $imageUrl = Storage::putFile($vehicleImagesFolder, new File($image));
                VehicleImage::create(
                    [
                        'vehicle_id'=> $vehicle->id,
                        'images' => $imageUrl 
                    ]
                );
            } 
        }

        

 return redirect()->route('p_vehicleList')->with('message','Vehicle Updated Successfully');

    }




}

