@extends('layouts.app')

@section('styles')

@endsection

@section('content')

@include('layouts.header') 


<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

<div class="row" id="contatti">
<div class="container mt-5" >

    <div class="row">
      <div class="col-md-6 maps" >
          <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=373%20-%20High%20Road%20ILFORD%20%2C%20IG1%201TF&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/"></a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:600px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:500px;}</style></div>


<div>
<p>Booking Service / Support : 9:00am - 6:00pm [Monday - Sunday]</p>
<p>Emergency : 24/7 Monday - Sunday</p>
<p>Collection Time : 9:00AM - 6:00PM [Monday - Friday]</p>
</div>


      </div>
      <div class="col-md-6">
        <h2 class="text-uppercase mt-3 font-weight-bold">CONTACT US</h2>
        <form action="">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <input type="text" class="form-control mt-2" placeholder="Enter Name" required>
              </div>
            </div>
          
            <div class="col-lg-6">
              <div class="form-group">
                <input type="email" class="form-control mt-2" placeholder="Enter Email" required>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <input type="number" class="form-control mt-2" placeholder="Enter Phone" required>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Detail" rows="3" required></textarea>
              </div>
            </div>
            <div class="col-12">
             <input type="submit" class="btn btn-primary" value="Submit" />
            </div>
            
          </div>
        </form>
        <div class="text-white"> 

        <i class="fas fa-phone mt-3"></i> <a href="tel:+4407812214589">BOOKBY MOBILE : 07812214589</a><br>
        <i class="fas fa-phone mt-3"></i> <a href="tel:+4407847253191">BOOK BY WHATSAPP : 07847253191 </a><br>
        <i class="fas fa-phone mt-3"></i> <a href="tel:+4402081333138">BOOK BY LAND LINE : 02081333138</a><br>

        
        <i class="fa fa-envelope mt-3"></i> <a href="">info@prcocar.com</a><br>
 
        <div class="my-4">
        <a href=""><i class="fab fa-facebook fa-3x pr-4"></i></a>
        <a href=""><i class="fab fa-linkedin fa-3x"></i></a>
        </div>
        </div>
      </div>

    </div>



</div>
</div>

 




 

@include('layouts.footer')

    </body>

    @endsection

    @section('scripts')

    @endsection



 <style>
 	..gmap_canvas{width:500px;}
 </style>