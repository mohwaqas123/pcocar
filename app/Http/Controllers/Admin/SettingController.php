<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Setting;
class SettingController extends Controller
{
 
	 public function admin_setting(Request $request)
    {
    	$url = "/admin/dashboard";
             $title = 'Admin setting';

		$setting = Setting::where('id',  1)->first(); 

          $data = [
       'msg'=> '',
               'title' =>$title,
               'setting' => $setting,
    
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .' Admin setting',
                
        ]; 
 			return view('admin.setting.create')->with($data);
    }
     public function setting_admin_update(Request $request)
    {

    	$url = "/admin/dashboard";
        $title = 'Admin setting';

        $setting = Setting::where('id', 1)->first();
        
 
       $data = [
            'name' => $request->name,
            'stripe_mode' => $request->stripe_mode,
            'stripe_live_published_key' => $request->stripe_live_published_key,
            'stripe_live_secret_key' => $request->stripe_live_secret_key,
            'stripe_live_prod_key' => $request->stripe_live_prod_key,
            'stripe_test_published_key' => $request->stripe_test_published_key,
            'stripe_test_secret_key' => $request->stripe_test_secret_key,
            'stripe_test_prod_key' => $request->stripe_test_prod_key,
            'flat_discount_type' => $request->flat_discount_type,
            'flat_discount_rate' => $request->flat_discount_rate,
            'promotion_type' => $request->promotion_type,
      //      'discount_rate'=> $request->discount_rate,




          ];
 
           
          $setting->update($data);
 
            


              $data = [
       
               'title' =>$title,
               'setting' => $setting,
               'msg'=> 'Setting is Updated Successfuly',
    
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .' Admin setting',
                
        ]; 
 
           
      return view('admin.setting.create')->with($data);
    }



}
