<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('user/register', 'API\RegisterController@register');
// Route::post('user/login', 'API\LoginController@login');

Route::group(['prefix' => 'user'], function () {
    Route::post('/register', 'API\RegisterController@register');
    Route::post('/login', 'API\LoginController@login');
    Route::post('/view-profile-without-auth', 'API\UserController@viewProfileWithoutAuth');

    Route::post('/forget-password', 'API\LoginController@forgetPassword');
    Route::post('/reset-password', 'API\LoginController@resetPassword');

    Route::post('/first-time-app', 'API\UserController@saveDevice');
    
    Route::post('/check-username', 'API\UserController@checkUserName');
    Route::post('/check-email', 'API\UserController@checkEmail');
    Route::post('/check-phone', 'API\UserController@checkPhone');
    Route::post('/delete-phone', 'API\UserController@deletePhone');
    Route::post('/check-otp', 'API\UserController@checkOTP');

    Route::post('/category-list', 'API\GeneralController@categoryList');
    Route::post('/category-list-with-songs', 'API\GeneralController@categoryListWithSongs');
    Route::post('/banner-list', 'API\GeneralController@bannerList');
    Route::post('/video-list', 'API\GeneralController@videoList');
    Route::post('/music-list-against-category', 'API\MusicController@musicAgainstCategory');

    Route::post('/tag-list', 'API\GeneralController@tagList');

    //search
    Route::post('/search', 'API\GeneralController@Search');

    Route::post('/search/tag', 'API\GeneralController@SearchTag');

    Route::post('/search/tags', 'API\GeneralController@SearchTags');

    Route::post('/search/song-video', 'API\GeneralController@SearchSongVideo');


    Route::middleware(['jwt.auth'])->group(function () {
        Route::post('/view-profile', 'API\UserController@viewProfile');
        Route::post('/update-profile', 'API\UserController@updateProfile');

        Route::post('/logout', 'API\UserController@logout');
        
        Route::post('/upload-video', 'API\VideoController@uploadVideo');
        Route::post('/video-list-auth', 'API\GeneralController@videoListAuth');
        Route::post('/video-like-unlike', 'API\VideoController@likeUnlikeVideo');
        Route::post('/share-video', 'API\VideoController@shareVideo');

        Route::post('/post-comment', 'API\CommentController@postComment');
        Route::post('/list-comment', 'API\CommentController@listComment');

        Route::post('/follow-unfollow', 'API\FollowController@FollowUnfollow');
        Route::post('/list-follow-unfollow', 'API\FollowController@listFollowUnFollow');


    });
});