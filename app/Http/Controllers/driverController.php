<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\models\Driver;
class driverController extends Controller
{
    public function index()
    {

    $driver = Driver::orderBy('id', 'DESC')->paginate(10);
     $url = "/admin/dashboard";
    $data = [
            'driver' => $driver,
            'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .' Driver List',
           ]; 
            
    return view('admin.driver.list')->with($data);
    }

    public function create()
    {

     $url = "/admin/dashboard";
    $data = [
            'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Add Driver ',
           ]; 
        return view('admin.driver.create')->with($data);
    }
    public function store(Request $request)
    {
          $validator = $request->validate([
            'name' => 'required|max:191|unique:makes',
            'email' => 'required',
            'phone' =>'required',
        ]);
          $driverData = [
            'name' => $request->name, 
            'phone' => $request->phone, 
             'email' => $request->email, 
             'driver_status'=>0,
             'created_from'=>0
         ];
            $vehicle = Driver::create($driverData);
 return redirect()->route('admindriver')->with('success', 'Record Created Successfully');
    }
    public function show($id)
    {
    }
     public function edit($id)
    {
       $driver = Driver::find($id);
        $url = "/admin/dashboard";
        $title = 'Edit Driver';
          $data = [
            'driver' => $driver,
            'title'=>   $title,
            'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .' Driver Edit',
           ]; 
    return view('admin.driver.edit')->with($data);

    }
 
    public function updatedriver(Request $request)
    {

        $driver = Driver::find($request->id); 
        $url = "/admin/dashboard";
        $title = 'Driver List';
          $data = [
             'name' => $request->name,
             'email' => $request->email,
             'phone' => $request->phone, 
              'driver'=> $driver,
               'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Driver Edit',
                    ];   
       $driver->update($data);
        return  view('admin.driver.edit')->with($data);
    }
    public function driverdestory($id)
    {
    $driver = Driver::findOrFail($id);
    $driver->delete();
    $driver = Driver::orderBy('id', 'DESC')->paginate(10);
        return view('admin.driver.list', ['driver'=>$driver, 'title' => 'Driver']);

    }

}

