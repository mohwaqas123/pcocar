<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class InsuranceCompany extends Model
{
		protected $table = 'insurance_company';
		protected $fillable = [
        'insurance_company','int_status'
    ];

}
