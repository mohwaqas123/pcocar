<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Expense;
use App\models\Vehicle;
use Illuminate\Http\File;
use App\models\Purchase;
use App\models\Expense_history;
use App\models\Invoice;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;

class ExpenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
         $expence_submitted = Expense::get();
         // dd($expence_submitted);
        $expence_in_progress= Expense::where('int_status', 2)->with('vehicle')->with('booking')->get();
        $recorded_further_evidence = Expense::where('int_status', 3)->with('vehicle')->with('booking')->get();
       
        $recorded_approved = Expense::where('int_status', 4)->with('vehicle')->with('booking')->get();

        $recorded_rejected = Expense::where('int_status', 5)->with('vehicle')->with('booking')->get();


        $recorded_on_hold = Expense::where('int_status', 6)->with('vehicle')->with('booking')->get();

        $url = "/admin/dashboard";
        $title = 'Damages Records list';
        $data = [
               'expence_submitted'=> $expence_submitted,
               'expence_in_progress'=> $expence_in_progress,
               'recorded_further_evidence'=> $recorded_further_evidence,
               'recorded_approved'=> $recorded_approved,
               'recorded_rejected'=> $recorded_rejected,
               'recorded_on_hold'=> $recorded_on_hold,

                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Expense'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Expense List' ,
  
        ];  
         
         return view('admin.expence.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
// $booking = Booking::orderby('id', 'Desc')->get();
         
         return view('admin.expence.list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $expense_view = Expense::where('id',$request->id)->with('vehicle')->with('booking')->first();
        $expense_history = Expense_history::where('expence_id',$request->id)->get();
        
             $url = "/admin/dashboard";
             $data = [
               'expense_view'=> $expense_view,               
               'expense_history'=> $expense_history,               
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Expense'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Expense view' ,


            ];  
        return view('admin.expence.view')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function prove(Request $request)
    {
          
         $expense_view = Expense::where('id',$request->id)->with('vehicle')->with('booking')->first();
        
         $expenseData = [
            'price' =>$expense_view->price,
            'detail' => $expense_view->detail,
            'name' => $expense_view->name,
            'dob' =>$expense_view->dob,
            'vat_number' => $expense_view->vat_number,
            'part' => $expense_view->part,
            'attachment' => $expense_view->attachment,
            'int_status' => 4,
            'vehicle_id' => $expense_view->vehicle->id,
            'booking_id' => $expense_view->booking->id,
        ];
    
        $expense_view->update($expenseData); 
        $expense_view->save();

        $vehicle_name =  $expense_view->vehicle->name;
        $vehicle_year =  $expense_view->vehicle->year;
        $user_name =     $expense_view->booking->user->name;
        $user_Email =    $expense_view->booking->user->email;
        $user_Phone =    $expense_view->booking->user->phone;
        $Expense_price = $expense_view->price;
        $detail        =    $expense_view->detail;
        $shop_name =    $expense_view->name;
        
         Mail::to($user_Email)->send(new \App\Mail\Expense_Prove(
          $vehicle_name,$vehicle_year,$user_name,$user_Email,$user_Phone,$Expense_price,$detail,$shop_name)); 

       return redirect()->back()->with('message','Expense is Approved.');

    }
    public function rejected(Request $request)
    {
         
        $expense_view = Expense::where('id',$request->id)->with('vehicle')->with('booking')->first();

         $expenseData = [
            'int_status' => 5,
         ];
       
        $expense_view->update($expenseData); 
        $expense_view->save();
        $expensehistory = [
          
            'expence_id' => $expense_view->id,
            'details' => $request->detail,
            'int_status' => 0,


        ];
       
        $expensehistory = Expense_history::create($expensehistory);




        $vehicle_name =  $expense_view->vehicle->name;
        $vehicle_year =  $expense_view->vehicle->year;
        $user_name =     $expense_view->booking->user->name;
        $user_Email =    $expense_view->booking->user->email;
        $user_Phone =    $expense_view->booking->user->phone;
        $Expense_price = $expense_view->price;
        $detail        =    $expense_view->detail;
        $shop_name =    $expense_view->name;
        $reasons =    $request->detail;

        Mail::to($user_Email)->send(new \App\Mail\Expense_Discard(
          $vehicle_name,$vehicle_year,$user_name,$user_Email,$user_Phone,$Expense_price,$detail,$shop_name,$reasons)); 

       return redirect()->back()->with('message','Expense is Disapproved.');

    }
    public function recorded_further_evidence(Request $request)
    {
               
         $expense_view = Expense::where('id',$request->id)
         ->with('vehicle')->with('booking')->first();
        $expenseData = [
            'int_status' => 6,
        ];
      
        $expense_view->update($expenseData); 
        $expense_view->save();

        $expensehistory = [
          
            'expence_id' => $expense_view->id,
            'details' => $request->detail,
            'int_status' => 0,

        ];
        $expensehistory = Expense_history::create($expensehistory);
        $vehicle_name =  $expense_view->vehicle->name;
        $vehicle_year =  $expense_view->vehicle->year;
        $user_name =     $expense_view->booking->user->name;
        $user_Email =    $expense_view->booking->user->email;
        $user_Phone =    $expense_view->booking->user->phone;
        $Expense_price = $expense_view->price;
        $detail        =    $expense_view->detail;
        $shop_name =    $expense_view->name;
        $reasons =    $request->detail;

        Mail::to($user_Email)->send(new \App\Mail\Expense_FurtherEvidence(
          $vehicle_name,$vehicle_year,$user_name,$user_Email,$user_Phone,$Expense_price,$detail,$shop_name,$reasons)); 

       return redirect()->back()->with('message','Expense query is on hold till further evidence received.');

    }
    public function inprogress(Request $request)
    {
            
         $expense_view = Expense::where('id',$request->id)->with('vehicle')->with('booking')->first();

        $expense_view->int_status =2; 
        $expense_view->save();
      
        $user_name =     $expense_view->booking->user->name;
        $user_Email =     $expense_view->booking->user->email;


     

       

        Mail::to($user_Email)->send(new \App\Mail\Expense_InProgess($user_name
          )); 

       return redirect()->back()->with('message','Expense is "In Progress".');

    }
    public function invoice_add(Request $request)
    {
            $vehicle = Vehicle::all(); 
            $purchase = Purchase::get(); 
            $url = "/admin/dashboard";
             $data = [
            'vehicle' =>$vehicle,
            'purchase' =>$purchase,
             'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Expense'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Add Invoice' ,

            ];
        
        return view('admin.invoice.add')->with($data);


    }

      public function add_invoice(Request $request)
    {

        $nid_back='';
        if ($request->hasFile('nid_back') ) 
         {
            $vehicleDocFolder = 'invoice';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }
            if ($request->hasFile('nid_back'))
               $nid_back = Storage::putFile($vehicleDocFolder, new File($request->nid_back));
        }
        if ($request->vat_number == '') 
        {
            $price2 =  0 ;
        } 
        else
        {
            $price = $request->price;
            $vat_number = $price / 1.2;
            $price2 =  $price -  $vat_number ;
        }
        $data = [
                'vehicle_id'=> $request->vehicle,
                'supplier_id'=> $request->supplier,
                'detail'=> $request->description,
                'price'=> $request->price,
                'vat_number'=> $request->vat_number,
                'nid_back'=> $nid_back,
                'vat_price' => $price2,
                'date'=> $request->dob,
                'invoice_number'=> $request->invoice_number,
            ];
           

        $invoice = Invoice::create($data);
      
     return redirect()->back()->with('message','Invoice are Added.');
        

       // return redirect('admin/invoice/show')->with('message','Invoice are Added.');


    }
     public function invoice_show(Request $request)
    {

            $invoice = Invoice::get();
            $url = "/admin/dashboard";
            $data = [
           'invoice' =>$invoice,
            'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Expense'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'All Invoice' ,

            ];
           
        return view('admin.invoice.list')->with($data);
    }


      public function invoice_show_detail(Request $request)
    {
            $invoice = Invoice::where('id', $request->id)->first();  
            
            $url = "/admin/dashboard";
            $data = [
           'invoice' =>$invoice,
                  'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Expense'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Admin invoice detail' ,
            
            ];
           
        return view('admin.invoice.show')->with($data);
    }


}
