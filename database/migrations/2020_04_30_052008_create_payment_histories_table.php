<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id'); 
            $table->decimal('amount',9,2);
            $table->string('transaction_id', 225)->nullable();
            $table->integer('vehicle_id');
            $table->integer('month_duration');
            $table->decimal('per_month_amount',9,2);  
            $table->date('expire_date'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_histories');
    }
}
