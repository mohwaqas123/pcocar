<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Accidental_records_image extends Model
{
    protected $table = 'accidental_records_image';
 	protected $fillable = [
         'accidental_id', 'images',
 	 ];
}
