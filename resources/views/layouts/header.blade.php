   <body>
@php
 
   function getCalculatDiscountPrice($week=1, $price ) {
    $discount=0;
       if($week >= 11 && $week < 24 ){
            $discount = ($price * 5 ) /100;
       }

       if($week >= 24 && $week < 48 ){
            $discount = ($price * 7 ) /100;
       }

       if($week >= 48 ){
            $discount = ($price * 10 ) /100;
       }

       $discounted_price = $price - $discount ;

        return  $discounted_price;

   }



   function getCalculatTotalDiscountPrice($week=1, $price ) {
    $discount=0;
       if($week >= 11 && $week < 24 ){
            $discount = ($price * 5 ) /100;
       }

       if($week >= 24 && $week < 48 ){
            $discount = ($price * 7 ) /100;
       }

       if($week >= 48 ){
            $discount = ($price * 10 ) /100;
       }

       $discounted_price = $week * ($price - $discount);

        return  $discounted_price;

   }
 
 @endphp
 
    <!-- Preloader -->
    <div class="preloader">
        <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-cookies" data-backdrop="false" aria-labelledby="modal-cookies" aria-hidden="true">
        <div class="modal-dialog modal-dialog-aside left-4 right-4 bottom-4">
            <div class="modal-content bg-dark-dark">
                <div class="modal-body">
                    <!-- Text -->
                    <p class="text-sm text-white mb-3">
                        We use cookies so that our themes work for you. By using our website, you agree to our use of cookies.
                    </p>
                    <!-- Buttons -->
                    <a href="pages/utility/terms.html" class="btn btn-sm btn-white" target="_blank">Learn more</a>
                    <button type="button" class="btn btn-sm btn-primary mr-2" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
  
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
        <div class="container">
            <!-- Brand -->
            <a class="navbar-brand" href="{{ url('/')}}">
                 <img src="{{ asset('public/theme3/assets/img/logo1.png')}}" alt="logo" style="height:60px" />
            </a>
            <!-- Toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mt-4 mt-lg-0 ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/')}}">Home </a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link @if(request()->segment(2) && request()->segment(2)==1) selected @endif  ?>" href="{{ url('/')}}/browse_vehicle_category/1">PCO MiniCab</a>
                    </li>
                      <li class="nav-item ">
                        <a class="nav-link @if(request()->segment(2) && request()->segment(2)==2) selected @endif" href="{{ url('/')}}/browse_vehicle_category/2">Private</a>
                    </li>

                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/')}}/aboutus">About Us</a>
                    </li>

                   <!--  <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/')}}/contactus">Contact us</a>
                    </li>  -->
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ url('/')}}/howitswork">How it works</a>
                    </li>
                </ul>
                <!-- Button -->
               
 <div class="btn-group">
  <button type="button" class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="{{url('login')}}">Customers</a>
    <a class="dropdown-item" href="{{url('login_partner')}}">Partners</a> 
  </div>
</div><!-- /btn-group -->



                <!-- Mobile button -->
                <div class="d-lg-none text-center">
                    
                   
                </div>
            </div>
        </div>
    </nav>
    <!-- Main content -->


