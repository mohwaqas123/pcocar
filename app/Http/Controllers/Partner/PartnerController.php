<?php
namespace App\Http\Controllers\Partner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\models\Partner;
use App\models\Booking;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use Illuminate\Support\Facades\Mail;
use Session;
use App\models\Driver;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;



class PartnerController extends Controller
{
 
    public function signup(Request $request){

      request()->validate([ 
            'email' => 'required|unique:users',   
            'password' => 'required',
            'name' => 'required|max:20',            
            ]);
         $data = [
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'phone' =>$request->phone,
        ];
     User::create($data);
     Mail::to($request->email)->send(new \App\Mail\Parntersignup( $request->name, $request->password,  $request->email)); 
     return redirect()->back()->with('message','User Registered Successfully');
    }
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
    public function partnerlogin(Request $request)
    {
        request()->validate([ 
            'email' => 'required',   
            'password' => 'required',
            //'name' => 'required|max:20',
            ]);  
        //$user = User::where('password',Hash::make($request->password))->where('email',$request->email)->first();
        $user = Partner::where('email',$request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
    session(['p_user_id' => $user->id, 'p_email' => $request->email, 'p_name' =>$user->name]);
//dd($request->session()->get('p_name'));
          // return redirect('booking')->back()->with('success','Login Successfully');
           return redirect()->route('partnerdashboard', ['user_detail' => $user ]);
       }
      else
        {
           return redirect()->back()->with('message','Email or Password is not correct');
        }
}
 public function partnerLogout(){
    session_unset(); 
    Session::flush();
    //return Redirect::to('/customerlogin');
    return redirect('/login_partner');
    }
public function customerdashboard(){ 
    $data = '';
        return view('customer.dashboard')->with($data); 
    } 
public function adminpartnerEdit(Request $request){ 
        $Partner = Partner::find($request->id);
        $url = "/admin/dashboard";
        $title = 'Customer List';
        $data =[
                'Partner'=> $Partner,
                    
                 'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'partner'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'partner Edit' ,
            ];  
        return view('partner.edit')->with($data); 
    }
   public function adminpartnerdestory($id)
    {
    $Partner = Partner::find($id);
    $Partner->delete();
    return redirect()->route('partner_list')->with('success','Vehicle deleted Successfully');
}
public function bookingView(Request $request){  
       $booking = Booking::where('id', $request->id)->with('vehicle')->first();   
       $vehicles = Vehicle::where('id',  $booking->vehicle_id)->first();     
       return view('customer.booking_detail', [
        'vehicle' => $vehicles, 
        'booking' => $booking,
        ]);
}
public function list(Request $request){   
        
       // $partner = Partner::select('id')->orderBy('name', 'ASC')->paginate(10);   
        $partner = Partner::orderby('id', 'Desc')->paginate(20); 
        $url = "/admin/dashboard";
         
         $data =[
                'partner'=> $partner,
                 'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'All Accounts',
            ];  
                return view('partner.list')->with($data);
 
}
 
    public function login(Request $request)
    {  request()->validate([ 
            'email' => 'required',   
            'password' => 'required',
            //'name' => 'required|max:20',

            ]);
        //$user = User::where('password',Hash::make($request->password))->where('email',$request->email)->first();

        $user = User::where('email',$request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {

           session(['user_id' => $user->id, 'email' => $request->email]);

          // return redirect('booking')->back()->with('success','Login Successfully');

           return redirect()->route('booking2', ['id' => $request->id, 'step2'=>1]);            
        }
        else
        {
           return redirect()->back()->with('message','User or Password is not correct');
        }
  }
 
    public function create()
    {
dd('fefefffffffffff');
        //

    }

public function store(Request $request)
    {
        dd('fefe');
           $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:partners', 
            'phone' => 'required',

        ]);

        $generated_pass = rand();   
            $data = [
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($generated_pass),
        'phone' =>$request->phone,
    ];
 return redirect()->back()->with('message','Partner Account is Registered Successfully');
 }
 

    public function show($id)

    {

        //

    }

    public function edit($id)
    {

        //

    }
    public function update(Request $request)

    {

        $validatedData = $request->validate([
            'name' => 'required',
           
            'phone' => 'required',
            'business_address' => 'required',
        ]);
        $partner = Partner::where('id', $request->id)->first();
        

        $img_exterior_dashboard='';
        if ($request->hasFile('img_exterior_dashboard')) 
         {
            $vehicleDocFolder = 'accidentalImages';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }
            if ($request->hasFile('img_exterior_dashboard'))
               $img_exterior_dashboard = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_dashboard));
       }

         $data = [
            'business_address' => $request->business_address,
            'pickup_address' => $request->pickup_address,
            'dropoff_address' => $request->dropoff_address,
            'name' => $request->name,
            'commission' => $request->commission,
            'address' => $request->address,
            'phone' => $request->phone,
            'profile_pic'=> $img_exterior_dashboard,
            'bank_name' => $request->bank_name,
         'account_number' => $request->account_number,
         'account_holder_name' => $request->account_holder_name,
         'sort_code' => $request->sort_code,

        ];
 
        $partner->update($data); 
        $partner->save();
     return redirect()->back()->with('message','partner update  Successfully');
    }






















      public function partner_profile(Request $request)
      {
    $partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
     $data= [
                    'partner' => $partner,
                ];
        return view('partner.profile')->with($data);
 
        }
 
    public function partner_profile_update(Request $request)

            {
        // $partner =   Partner::where('id', 1)->first();
   $partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
      
            $data = 
            [
                'partner' =>$partner,
           ];

        return view('partner.profile')->with($data);
   }
        public function partner_booking_vehicle(Request $request)
                {   
                    
             $booking=Booking::with('vehicle')->get();
             $url = "partnerdashboard";

            $data = [
                'booking'=>$booking,
                'partner_id'=>$request->session()->get('p_user_id') , 
                'breadcrum' => '
                <h4 class="page-title">Booking </h4>
                                    <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a style="color:black; &nbsp" href="'. url( $url ).'"  >Dashboard</a></li>
                            <li class="breadcrumb-item">Booking List</li>
                            </ol>',


                
                ];  
            return view('partner.booking.index')->with($data);
                // $booking = Booking::where('user_id', session()->get('user_id') )->orderby('id', 'Desc')->get();
              // $vehicles = Vehicle::where('partner_id',$request->session()->get('p_user_id') )->orderby('id', 'Desc')->paginate(20);
              //   $booking = Booking::orderby('id', 'Desc')->get();
              //   return view('admin.vichealbooking.index', ['booking'=>$booking ]);

            }
             public function partner_change_password()
           {   
                  return view('partner.password.changepassword');
            }
       public function change_password(Request $request)
                {    
                 request()->validate([ 
                ]);
          $partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
     
                $partner->password = Hash::make($request->password);  
                $partner->save(); 
                 $data= [
                        'message'=>"Your password updated successfully."];
                return view('partner.password.changepassword')->with($data);
          
            }
         public function p_customerview(Request $request)
        {


                    $booking = Booking::where('id', $request->id)->with('vehicle')->first();
                    $vehicles = Vehicle::where('id',  $booking->vehicle_id)->first();
                    return view('partner.booking.booking_detail',[
                     'vehicle' => $vehicles, 
                    'booking' => $booking,
                    ]) ;
 }
            
}

