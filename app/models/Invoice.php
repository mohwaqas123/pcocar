<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
	
     protected $fillable = [
        'supplier_id','vehicle_id','detail','nid_back','vat_price','vat_number','date','price','invoice_number',
      ];

 	public function supplier()
    {

        return $this->belongsTo('App\models\Purchase','supplier_id','id');
    }
    	public function vehicle()
    {
        return $this->belongsTo('App\models\Vehicle','vehicle_id','id');
    }



}
