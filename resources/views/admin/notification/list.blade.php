@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">	
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif

						<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%" >
							 	<thead>
                                <tr>
                                <th>id</th>
                                <th>User</th>
                                <th>Booking</th>
                                <th>Vehicle</th>
                                <th>Details</th>
                                <th style="width:90px" >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@foreach($notification as $notification_var)
                                <tr>
                                    <td>{{ $notification_var->id }}</td>
                                    <td>{{@$notification_var->user->name}}
                                        {{@$notification_var->user->email}}
                                        {{@$notification_var->user->phone}}</td>
                                    <td>Booking Id{{@$notification_var->booking->id}}
                                       Duration: {{@$notification_var->booking->duration}}  
                                        Price: {{@$notification_var->booking->price}}

                                    </td>
                                    <td>Name:{{ $notification_var->vehicle->name }}
                                     Plate Number:{{ $notification_var->vehicle->licence_plate_number }}</td>
                                    <td>{{$notification_var->details}}</td>
                                    <td> <a href="{{route('notificationview', $notification_var->id)}}"  class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"
                                                data-toggle="tooltip" data-placement="Top" data-original-title="View Notification" ><i class="fa fa-eye"></i></td>
                                     
                                </tr>

                                @endforeach
                                </tbody>
                                </tbody>
                               
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>




@endsection
@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection