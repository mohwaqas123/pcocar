<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="#" class="logo">
                <!--<span>-->
                <!--    <img src="assets/images/logo.png" alt="" height="22">-->
                <!--</span>-->
                <i>
                    <img src="assets/images/logo_sm.png" alt="" height="28">
                </i>
            </a>
        </div>

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{ asset('public/theme/assets/images/admin.png') }}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>
            <h5><a href="#"> Customer</a> </h5>
            <p class="text-muted">PCOCAR</p>

              <a href="{{ route('adminLogout') }}" class="dropdown-item notify-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>



        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->

                <li>
                    <a href="{{ route('customerdashboard') }}">
                        <i class="fi-air-play"></i><span> Dashboard </span>
                    </a>
                </li>
 

                <li>
                    <a href="{{ route('customer_booking_list') }}"><i class="fi-layers"></i> <span> Booking  </span>  </a>
                    
                </li>

                <li>
                    <a href="{{ route('customer_paymenthistory_list') }}"><i class="fi-layers"></i> <span> Payment History  </span>  </a>
                    
                </li>
                  <li>
                    <a href="{{ route('customer_accidental_list') }}"><i class="fi-layers"></i> <span> Damages Recorded  </span>  </a>
                    
                </li>
                 
                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Expense  </span> </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('customer_expense') }}"><i class="fi-layers"></i> <span>Add Expense</span>  </a></li>
                        <li><a href="{{ route('customer_expense_list') }}"><i class="fi-layers"></i>Expense List</a> </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Swap  </span> </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('customer_swape_req') }}"><i class="fi-layers"></i> <span>Swap</span>  </a></li>
                        <li><a href="{{ route('customer_swape_show') }}"><i class="fi-layers"></i> <span>All</span>  </a></li>
                    </ul>
                </li>
                
 

            </ul>

        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>


