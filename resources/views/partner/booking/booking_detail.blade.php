@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


<div class="content-page">
    <div class="content">
        <div class="container-fluid" style="position: absolute;">



         

 <table width="80%" border="1"  style="overflow-x: scroll;">
  <tbody>
    <tr style="background:#eee; font-weight:bold;">
      <th>Customer Detail</th> 
    </tr>
    <tr>
      <td>
        Name: {{$booking->user->name}} <br>
        Email: {{$booking->user->email}} <br>
        Phone: {{$booking->user->phone}} <br>

        DVLA License Number: {{$booking->dvla}} <br>
        DOB: {{$booking->dob}} <br>
        Address: {{$booking->address}} <br>
        National Insurance Number: {{$booking->national_insu_numb}} <br>
        @if($booking->pco_licence_no) PCO License Number: {{$booking->pco_licence_no}}  @endif
         <br>

      </td>
       
    </tr>
    
     <tr style="background:#eee; font-weight:bold;">
      <th>Vehicle Detail</th> 
    </tr>
    <tr>
      <td>
        {{$vehicle->car_make->name}} <br>
        {{$vehicle->car_model->name}} <br>
        {{$vehicle->year }} <br>
        Postal pickup dropoff: {{$vehicle->postal_pickup_dropoff }}
      </td>
       
    </tr>
    
    <tr style="background:#eee; font-weight:bold;">
      <th>Booking Detail</th> 
    </tr>
    <tr>
      <td>
        @php
         $discount_amount = $vehicle->price * ($booking->discount / 100);
        $total = $vehicle->price - $booking->discount;
        $grand_tota = $booking->price + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');  //   $total + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');



 
 
if($payment_mode=1) $p = "Credit Card Stripe"; else $p = "Bank Deposit option";

if($booking->duration==1)
   $m = '4 weeks';
else
  $m = $booking->duration.' weeks';


@endphp
        Booking Generated: {{$booking->created_at}}<br>
        Expire Date:  {{$booking->expiry_date}} <br>
        Price per week: £{{$booking->price}} <br>
        Duration: {{$booking->duration}}  <br>
        Discount: {{$booking->discount}}% <br>
        Insurance: £{{env('INSURANCE_CODE')}} <br>
        Security Deposit(First time only): £ {{env('DEPOSIT_SECURITY')}}<br>
        Total: <b>£{{$grand_tota}}</b>  <br> 
        Payment Mode:  {{$p}}
        
      </td>
       
    </tr>


    <tr style="background:#eee; font-weight:bold;">
      <th>Attached Documents</th> 
    </tr>
    <tr>
      <td> 
        <a href="{{url('/')}}/storage/app/{{$booking->doc_dvla}}">DVLA Licence(Front)</a> <br>
        <a href="{{url('/')}}/storage/app/{{$booking->doc_cnic}}">DVLA Licence(Back)</a><br>
       @if($vehicle->vehicle_category==1) 
       <a href="{{url('/')}}/storage/app/{{$booking->doc_other}}">PCO Driver Licence Front</a><br>
       <a href="{{url('/')}}/storage/app/{{$booking->pco_driver_back}}">PCO Driver Licence Back</a><br>       
       <a href="{{url('/')}}/storage/app/{{$booking->pco_paper_licence}}">PCO Paper Licence</a><br> 
        @endif
        <a href="{{url('/')}}/generate_pdf_email/{{$booking->id}}">Customer Agreement Document</a> <br>
      </td>
    </tr>


@if($payment_mode=1) 
     <tr style="background:#eee; font-weight:bold;">
      <th>Payment & Billing Detail</th> 
    </tr>
    <tr>
      <td> 
        <table style="width:100%">
            <tr>
                <td>Date</td>
                <td>Amount</td>
                <td>Status</td>
            </tr>
            <tr>
                <td>
                    {{$booking->created_at}}
                </td>    
                <td>
                    £{{$grand_tota}}
                </td>
                <td>
                    Paid
                </td>
            </tr>
  <?php
  $months = $booking->duration;
  if($months==1) $months=4;
$total_week = $months - 1;
$price =  $vehicle->price+ env('INSURANCE_CODE') ;
for($i=1;$i<=$total_week;$i++){
 
  $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 
  $date = strtotime( date('Y-m-d') );
  $date = strtotime($days_add, $date);
  $show_date = date('Y/m/d', $date);

  echo "<tr>
          <td>".$show_date."</td>
          <td>£".$price."</td>
          <td>Unpaid</td>
  </tr>";


    //  echo "£".$price.' @ '. $show_date.'<br>';
}
?>    

@endif      
        </table>
</td></tr></tbody></table>
<input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
<a href="{{route('adminRemoveBooking' , $booking->id)}}" class="btn btn-primary"> REMOVE BOOKING </a>

<br><br><br><br><br>
</div>
</div>
</div> 
@endsection
@section('scripts')
@endsection
<style>
.bold_font{font-weight: bold;}
</style>