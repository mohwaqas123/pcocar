<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Device;
use App\models\Category;
use App\models\Song;
use App\models\Banner;
use App\models\Video;
use App\models\LikeVideo;
use App\models\CommentVideo;
use App\models\ShareVideo;
use App\models\Following;
use App\models\VideoTag;
use App\models\Tag;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;
use Storage;
use Illuminate\Http\File;

class GeneralController extends Controller
{
    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
    }

    public function categoryList(Request $request)
    {
        $allCategories = Category::where('status', 1)->get();

        if($allCategories != null)
        {
            $allCategories->makeHidden([
                'created_at', 'updated_at'
            ]);
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'All categories list.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $allCategories,
        ]);
    }

    public function categoryListWithSongs(Request $request)
    {
        $allCategories = Category::where('status', 1)->get();

        if($allCategories != null)
        {
            $allCategories->makeHidden([
                'created_at', 'updated_at'
            ]);

            $allCategoriesWithSongs = [];

            foreach ($allCategories as $category) {
                
                $songs = Song::where('category_id', $category->id)->limit(3)->get();

                if($songs != null)
                {
                    $songs->makeHidden([
                        'created_at', 'updated_at'
                    ]);
                }
                
                $category['songs_list'] = $songs;
                
                $allCategoriesWithSongs[] = $category;

            }
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'All categories list with songs.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $allCategoriesWithSongs,
        ]);
    }

    public function bannerList(Request $request)
    {
        $allBanners = Banner::where('status', 1)->get();

        if($allBanners != null)
        {
            $allBanners->makeHidden([
                'created_at', 'updated_at'
            ]);
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'All banners list.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $allBanners,
        ]);
    }

    public function videoList()
    {
        $videos = Video::all();
        $likes = 0;
        $comments = 0;

        $data = [];

        if($videos != null)
        {
            $videos->makeHidden([
                'created_at', 'updated_at'
            ]);

            foreach($videos as $video)
            {
                $likes = LikeVideo::where('video_id', $video->id)->where('type', 1)->count();
                $comments_count = CommentVideo::where('video_id', $video->id)->count();

                $comments = CommentVideo::where('video_id', $video->id)->get();
                $user = User::where('id', $video->user_id)->first();
                
                foreach($comments as $comment)
                {
                    $commentUser = User::where('id', $comment->comment_by)->first();

                    $comment->comment_by = $commentUser;
                }

                $video['total_likes'] = $likes;
                $video['total_comments'] = $comments_count;
                $video['is_follow'] = 0;
                $video['comments'] = $comments;
                $video['user'] = $user;

                if($video != null)
                {
                    $video->makeHidden([
                        'created_at', 'updated_at'
                    ]);
                }
            }
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'All videos list.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $videos,
        ]);
    }

    public function videoListAuth(Request $request)
    {
        $rules = [
            'follower_type' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        if($request->follower_type == 0)
        {
            $user_id = JWTAuth::toUser($request->token)->id;
            $videos = Video::all();
            $likes = 0;
            $comments = 0;
            $is_like = 0;
            $shareCount = 0;
            $data = [];

            if($videos != null)
            {
                $videos->makeHidden([
                    'created_at', 'updated_at'
                ]);

                foreach($videos as $video)
                {
                    $likes = LikeVideo::where('video_id', $video->id)->where('type', 1)->count();
                    $comments_count = CommentVideo::where('video_id', $video->id)->count();
                    $shareCount = ShareVideo::where('video_id', $video->id)->where('user_id', $user_id)->count();
                    $comments = CommentVideo::where('video_id', $video->id)->get();
                    $vid = LikeVideo::where('video_id', $video->id)->where('type', 1)->where('liked_by', $user_id)->first();
                    if($vid != null)
                    {
                        $is_like = 1;
                    }

                    $user = User::where('id', $video->user_id)->first();
                    
                    if($video != null)
                    {
                        $video->makeHidden([
                            'created_at', 'updated_at'
                        ]);
                    }

                    foreach($comments as $comment)
                    {
                        $commentUser = User::where('id', $comment->comment_by)->first();

                        $comment->comment_by = $commentUser;
                    }

                    $video['total_likes'] = $likes;
                    $video['total_comments'] = $comments_count;
                    $video['total_shares'] = $shareCount;
                    $video['is_like'] = $is_like;
                    $video['is_follow'] = 1;
                    $video['comments'] = $comments;
                    $video['user'] = $user;
                }
            }

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'All videos list.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $videos,
            ]);
        }
        else
        {
            $user_id = JWTAuth::toUser($request->token)->id;
            $followings = Following::where('following_id', $user_id)->pluck('user_id');
            $videos = Video::whereIn('user_id', $followings)->get();
            $likes = 0;
            $comments = 0;
            $is_like = 0;
            $shareCount = 0;
            $data = [];

            if($videos != null)
            {
                $videos->makeHidden([
                    'created_at', 'updated_at'
                ]);

                foreach($videos as $video)
                {
                    $likes = LikeVideo::where('video_id', $video->id)->where('type', 1)->count();
                    $comments_count = CommentVideo::where('video_id', $video->id)->count();
                    $shareCount = ShareVideo::where('video_id', $video->id)->where('user_id', $user_id)->count();
                    $comments = CommentVideo::where('video_id', $video->id)->get();
                    $vid = LikeVideo::where('video_id', $video->id)->where('type', 1)->where('liked_by', $user_id)->first();
                    if($vid != null)
                    {
                        $is_like = 1;
                    }

                    $user = User::where('id', $video->user_id)->first();
                    
                    if($video != null)
                    {
                        $video->makeHidden([
                            'created_at', 'updated_at'
                        ]);
                    }

                    foreach($comments as $comment)
                    {
                        $commentUser = User::where('id', $comment->comment_by)->first();

                        $comment->comment_by = $commentUser;
                    }

                    $video['total_likes'] = $likes;
                    $video['total_comments'] = $comments_count;
                    $video['total_shares'] = $shareCount;
                    $video['is_like'] = $is_like;
                    $video['is_follow'] = 1;
                    $video['comments'] = $comments;
                    $video['user'] = $user;
                }
            }

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'All videos list.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $videos,
            ]);
        }

    }

    public function Search(Request $request)
    {
        if(isset($request->keyword))
        {
            $videos = Video::where('description', 'like', '%' . $request->keyword . '%')->get();
            $songs = Song::where('name', 'like', '%' . $request->keyword . '%')->get();
            $users = User::where('name', 'like', '%' . $request->keyword . '%')->get();

            $data['videos'] = $videos;
            $data['songs'] = $songs;
            $data['users'] = $users;

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Search List with keyword',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $data,
            ]);

        }
        else
        {
            $videos = Video::all();
            $songs = Song::all();
            $users = User::all();

            $data['videos'] = $videos;
            $data['songs'] = $songs;
            $data['users'] = $users;

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Search List without keyword',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $data,
            ]);
        }
    }

    public function tagList()
    {
        $tags = Tag::all();

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'tags list',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $tags,
        ]);
    }

    public function SearchTag(Request $request)
    {
        $rules = [
            'tag' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $tags = Tag::where('name', 'like', $request->tag.'%')->get();
        
        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'tags list with search',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $tags,
        ]);
    }

    public function SearchTags(Request $request)
    {
        $rules = [
            'tags' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $tagsID = Tag::whereIn('name', $request->tags)->pluck('id');

        $videosWithTagsID = VideoTag::whereIn('tag_id', $tagsID)->pluck('video_id'); 

        $videos = Video::whereIn('id', $videosWithTagsID)->get();

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'videos list against tags',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $videos,
        ]);
    }

    public function SearchSongVideo(Request $request)
    {
        $rules = [
            'song_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $videos = Video::where('song_id', $request->song_id)->get();

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'videos list against song id',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $videos,
        ]);
    }
}
