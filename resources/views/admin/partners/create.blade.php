@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<!-- date picker -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>   
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />



<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
 
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="card-box">

                            
@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

                            
                            @if($errors->any())
                            {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif
                            <form action="{{ route('partnerStore') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="bg-dev">
                                     

 <h4 class="breadcrumb-item active"   > 
                      
                    </h4>

                
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Enter Name<span class="text-danger">*</span></label>
                                            <input max="10" min="3" type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Name" required>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Email<span class="text-danger">*</span></label>
                                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Email" required>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Phone<span class="text-danger">*</span></label>
                                            <input type="number" name="phone" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Phone" required>
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Commission (%)<span class="text-danger">*</span></label>
                                            <input type="number" maxlength="2" max="99" name="commission" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Comimission" required>
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Address<span class="text-danger">*</span></label>
                                            <input type="test"   name="address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Address" required>
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Business Address<span class="text-danger">*</span></label>
                                            <input type="text"   name="business_address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Business Address" required="required"  >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Pick-up Address</label>
                                            <input type="text"   name="pickup_address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"  placeholder="Please Enter Pickup Address" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span> 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Drop-off Address</label>
                                            <input type="text"  name="dropoff_address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Drop off Address" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Bank Name</label>
                                            <input type="text"  name="bank_name" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Bank Number" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Account Number</label>
                                            <input type="text" 
                                              maxlength="17"
                                             name="account_number" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Account Number" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Account Holder Name</label>
                                            <input type="text"  name="account_holder_name" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Account Holder Name" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Sort Code</label>
                                            <input type="text"maxlength="6"  name="sort_code" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Sort Code" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div>
                                      <!--   <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">collection point Address<span class="text-danger">*</span></label>
                                            <input type="text" maxlength="2" max="99" name="collection_point_address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Drop off Address" required>
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div> -->
                                         <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Profile Image</label>
                                            <input type="file"   name="img_exterior_dashboard" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Address">
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>
                                            @endif
                                        </div>

                                         
                                    </div>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                         <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                            
                                    </form>






                                </div>
                            </div>


 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('scripts')
 
        @endsection

 