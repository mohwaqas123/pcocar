@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            

 
                                
<!-- Datatable -->
 
 <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />


@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif




<table id="datatable_tbl" class="table table-striped table-bordered" style="width:50%"  >
        <thead>
                         <tr>
                          <th>Customer-Name</th>
                         <td  style="width:50%"> {{@$booking->user->name}}</td>
                        </tr>
                            <tr>
                                <th>Customer-phone</th>
                               <td style="width:50%"> {{@$booking->user->phone}}</td>
                             </tr>
                            <tr>
                                 <th>Customer-Email</th>
                                 <td style="width:50%"> {{@$booking->user->email}}</td>
                            </tr>
                            <tr>
                                 <th>Price</th>
                                   <td> {{@$booking->user->Price}}</td>
                            </tr>
                          <!--  <tr>
                                <th>Date</th>
                                  <td> {{@$booking->user->date}}</td>
                             </tr> -->



            
        </thead> 


 
                              <!--   <tbody>
                                 
                                    <tr>
                                   
                                        <td>
                                             
                                           {{@$booking->user->name}}

                                        </td>
                                         <tr>
                                         <td>
                                             
                                           {{@$booking->user->phone}}

                                        </td> 
                                         <tr>
                                        <td>
                                             
                                           {{@$booking->user->email}}

                                        </td>
                                         <tr>

                                        
                                       
                                              
                               
                               
                                </tbody> -->
                            </table>
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
