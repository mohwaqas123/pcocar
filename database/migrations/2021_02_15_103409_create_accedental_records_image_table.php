<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccedentalRecordsImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accedental_records_image', function (Blueprint $table) {
            $table->id(); 
            $table->bigInteger('accedental_id')->unsigned()->index();
            $table->string('images')->nullable();
            $table->timestamps();
        });
         Schema::table('accedental_records_image', function (Blueprint $table) {
            $table->foreign('accedental_id')->references('id')->on('accedental_records')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accedental_records_image');
    }
}
