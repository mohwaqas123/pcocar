<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SwipeCancelOldAgreement extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name; 
   public $swipevehicle;
   public $new_vehicle;
   public $booking;
    

 
    public function __construct($swipevehicle,$new_vehicle,$old_vehicle,$name,$booking )
    {

         
        $this->name = $name; 
        $this->new_vehicle = $new_vehicle;
        $this->swipevehicle = $swipevehicle;  
        $this->booking = $booking;
        $this->old_vehicle = $old_vehicle;
         
        
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->subject('PCOCAR - Swipe Vehicle')->view('mail.swipe_cancel_old_agreement', [
            'name'=>$this->name, 
            'swipevehicle' => $this->swipevehicle, 
            'new_vehicle' => $this->new_vehicle , 
            'old_vehicle'=> $this->old_vehicle,
            'booking'=>$this->booking,
             
          ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
