@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<section class="slice slice-lg pt-lg-12 pb-0 pb-lg-12 bg-section-secondary">
  <div class="container">
    <div class="row">
      <div class="col-md-3">



<form action="{{ route('browse',   $vehicle_category ) }}" method="get" ifor 1 Monthd="frm_duration">
          {{csrf_field()}}
          <div class="search_box">
            <h3>Search</h3>
            <div class="form-group">
              <label for="inputEmail">Booking Duration</label>
              <p class="dis_duration">(Extra discount for more than 3 months )</p>
              <select name="duration" id="duration" class="form-control ">
                <option @if($duration && $duration==1) selected="selected" @endif value="1">Any</option>
                <option @if($duration && $duration==4) selected="selected" @endif value="4">4 Weeks</option>
                <option @if($duration && $duration==5) selected="selected" @endif value="5">5 Weeks</option>
                <option @if($duration && $duration==6) selected="selected" @endif value="6">6 Weeks</option>
                <option @if($duration && $duration==7) selected="selected" @endif value="7">7 Weeks</option>
                <option @if($duration && $duration==8) selected="selected" @endif value="8">8 Weeks</option>
                <option @if($duration && $duration==9) selected="selected" @endif value="9">9 Weeks</option>
                <option @if($duration && $duration==10) selected="selected" @endif value="10">10 Weeks</option>
                <option @if($duration && $duration==11) selected="selected" @endif value="11">11 Weeks</option>
                <option @if($duration && $duration==12) selected="selected" @endif value="12">12 Weeks </option>
                <option @if($duration && $duration==16) selected="selected" @endif value="16">16 Weeks   (Discount 5%)</option>
                <option @if($duration && $duration==20) selected="selected" @endif value="20">20 Weeks   (Discount 5%)</option>
                <option @if($duration && $duration==24) selected="selected" @endif value="24">24 Weeks   (Discount 10%)</option>
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail">Vehicle Make</label>
              <select name="car_make" id="car_make" onchange="get_model(this.value)" class="form-control">
                <option value="">Any</option>
                @foreach($car_make as $make)
                <option value="{{$make->id}}">{{$make->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail">Vehicle Model</label>
              <select name="car_model" id="car_model" class="form-control">
                <option value="">Any</option>
                @foreach($car_model as $model)
                <option value="{{$model->id}}">{{$model->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail">Vehicle Year</label>
              <select name="year" id="year" class="form-control">
                <option value="">Any</option>
                <option value="2020">2020</option>
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
              </select>
            </div>
             <div class="form-group">
              <label for="inputEmail">Vehicle Type</label>
              <select name="vehicle_category" id="vehicle_category" class="form-control">
                <option value="">Any</option>
                <option value="1">PCO</option>
                <option value="2">Private</option>
                
              </select>
            </div>
 

<!--   <div class="form-group">
   Licence Number Plate
    <input type="licence_plate_number" name="licence_plate_number" id="no" class="form-control">
 </div>   

  -->

  <div class="form-group">
   Price Range:<p id="amount">£1 - £250</p>
    <div id="slider-range" ></div>
    <input type="hidden" id="amount1" value="0" />
    <input type="hidden" id="amount2" value="1500" />
 </div>    

           
              <input  name="btn_submit" type="button" onClick="window.location.reload();" class="btn-primary btn-sm" value="Reset" />
          </div>  </form>


@php if(session()->get('staff_id') )  { @endphp
<form action="" method="get">
          {{csrf_field()}}
   <div class="form-group">
    Licence Number Plate
    <input required="required" type="licence_plate_number" name="licence_plate_number" id="licence_plate_number" class="form-control">
 </div>  
            
              <input  name="btn_searc_submit" id="btn_searc_submit" type="button" class="btn-primary btn-sm" value="Search" /></form>
@php
}
@endphp



          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-10"> <h2>Vehicles List  </h2></div>
              <div class="col-md-2" align="right">
                  <select name="filter" id="filter" class="form-control">
                  <option value="">Filter</option>
                  <option value="Desc">Price High to Low</option>
                  <option value="Asc">Price Low to High</option> 
                </select>  
              </div>
            </div>
  

            <!--
            <div class="row">
              @foreach($vehicles as $cab)
              @php
              $date1 = $cab->date_from;
              $date2 =$cab->date_to;
              $ts1 = strtotime($date1);
              $ts2 = strtotime($date2);
              $year1 = date('Y', $ts1);
              $year2 = date('Y', $ts2);
              $month1 = date('m', $ts1);
              $month2 = date('m', $ts2);
              $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
              @endphp
              <div class="col-md-4">
                <div class="card">
                  <div  class="float-right text-right margin-bottom px-3">
                    <span class="bold">£{{getCalculatDiscountPrice($duration,$cab->price)}}<span class="inner">/Week</span></span>
                    <p class="mt-0">for {{$diff}} Months</p>
                  </div>
                  
                  @php
                  if($duration && $duration!='')
                  $duration = $duration  ;
                  else
                  $duration =1;
                  @endphp
                  <a href="{{ route('car_detail', ['id' => $cab->id]) }}">
                  <img src="{{ asset('storage/app/'.$cab->image) }}" class="img-thumbnail" alt="...">  </a>
                  
                  <div class="ml-2">
                    <span class="car-make custom_font">{{$cab->year}} {{$cab->car_make->name}}</span><br>
                    {{$cab->car_model->name}} <span class="badge badge-pill">
                      @php
                      if($cab->fuel_type==0) echo "Diesel";
                      if($cab->fuel_type==1) echo "Petrol";
                      if($cab->fuel_type==2) echo "Gasoline";
                      @endphp
                    </span>-<span class="badge badge-pill">
                    @php if($cab->transmission==1) echo "Manual"; else echo "Automatic"; @endphp
                  </span>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          
          {!! $vehicles->links() !!} -->
          
 


<div class="loader" align="center" >
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
 Please Wait Loading...
</div>



<!-- 

<div class="cont">
  <div class="container">
    <div class="single-item">1</div>
    <div class="single-item">2</div>
    <div class="single-item">3</div>
    <div class="single-item">4</div>
    <div class="single-item">5</div>
    <div class="single-item">6</div>
    <div class="single-item">7</div>
    <div class="single-item">8</div>
    <div class="single-item">9</div>
    <div class="single-item">10</div>
    <div class="single-item">11</div>
    <div class="single-item">12</div>
    <div class="single-item">13</div>
    <div class="single-item">14</div>
    <div class="single-item">15</div>
    <div class="single-item">16</div>
    <div class="single-item">17</div>
    <div class="single-item">18</div>
    <div class="single-item">19</div>
    <div class="single-item">20</div>
    <div class="single-item">21</div>
    <div class="single-item">22</div>
    <div class="single-item">23</div>
  </div>
</div> -->

 
          <div class="" id="vehicle_list">
            
          </div>
           </div>
</div> 


     
</div>
  </section>
<!-- <div class="pagination">efe</div> -->


 


  
  @include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<style>
  .partner_name{font-size: 12px;}
  /*.small{display: none;}*/
  .spinner-grow{display: none;}
.img-thumbnail{height: 220px!important;}
.inner{font-size: 10px;}
.custom_font{    font-family: inherit;}
.car-make{font-size: 16px; font-weight: bold;}
.small{font-size:8px; background: red;
border-radius: 5px;
padding: 5px;
color: #fff;
margin-right: 10px;
}
.loader{ background:#eee}
label{margin-bottom:0px!important;}
.search_box{  /*  border: 1px solid #eee;*/
padding: 6px;
box-shadow: brown;
font-size: 12px;
}
.booknow{font-size: 0.902rem; height: 24px;
padding: 3px;}
.cartype{font-weight: bold; font-size: .900rem}
.dis_duration{margin-top: -3px;
    margin-bottom: -2px;     font-size: 11px;
    color: blue;
    font-weight: bold;}
.badge{    padding: .05rem .5rem!important; 
    border-radius: .175rem!important;}
#duration option{font-size: 12px; height: 20px; border-bottom: 1px solid}        







.single-item{
  width:200px;
  height:200px;
  display:flex;
  align-items:center;
  justify-content:center;
  background-color:#f3f3f3;
  margin: 20px;
  border-radius: 10px;
  color:#888;
}
.pagination{
  
  padding:20px;
  
  &,
  *{
    user-select: none;
  }
  
  a{
    display:inline-block;
    padding:0 10px;
    cursor:pointer;
    &.disabled{
      opacity:.3;
      pointer-events: none;
    cursor:not-allowed;
    }
    &.current{
      background:#f3f3f3;
    }
  }
}


</style>
<script>
function get_model(id){
// e.preventDefault();
var url_call = "{!! url('/') !!}/get_models";
$.ajax({
type : 'get',
url  : url_call,
data: { id:id  },
//    data: { ticket_id: ticket_id, status_value: status_value },
contentType: 'application/json; charset=utf-8',
success :  function(data){
$("#car_model").empty();
$("#car_model").append($("<option value=''>Any</option>"));
$.each(data, function () {
$("#car_model").append($("<option></option>").val(this['id']).html(this['name']));
});
}
});//ajax
}
</script>