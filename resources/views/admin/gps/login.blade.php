
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>PCOCAR Vehicle Mileage</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  </head>

  <body>


<div class="container">
    <div class="row">
    <div class="span12" align="center">
<br><br>      <div align="center"><img src="https://pcocar.com/public/theme3/assets/img/logo1.png" alt="logo" style="height:60px"></div>
      <div><h3>PCOCAR  Vehicles Mileage</h3></div><br><br>

       <form role="form"  action="{{ route('gps_post_login') }}"  method="post">
          {{csrf_field()}}
        <fieldset>
          <div id="legend">
            <legend class="">Login</legend>
          </div>
          <div class="control-group">
            <!-- Username -->
            <label class="control-label"  for="username">Username</label>
            <div class="controls">
              <input type="text" id="username" name="username" placeholder="" class="input-xlarge">
            </div>
          </div>
          <div class="control-group">
            <!-- Password-->
            <label class="control-label" for="password">Password</label>
            <div class="controls">
              <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
            </div>
          </div>
          <div class="control-group">
            <!-- Button -->
            <div class="controls"><br>
              <button class="btn btn-success">Login</button>
            </div>
          </div>
        </fieldset>
      </form>
    </div>
  </div>
</div>


 
         

</body></html>