@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
 <div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif



 <div class="card">
                                    <div class="card-body"> 
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                   <i class="fe-monitor"></i><span class="d-none d-sm-inline-block ml-2">New</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#profile" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                                    <i class="fe-user"></i> <span class="d-none d-sm-inline-block ml-2">In Process</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#messages" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="fe-mail"></i> <span class="d-none d-sm-inline-block ml-2">Settled</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#settings" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="fe-settings"></i> <span class="d-none d-sm-inline-block ml-2">Dispute </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane show active" id="home">
                  <table id="datatable_tbl" class="table table-striped table-bordered"  style="width:100%" >
                          
                                <thead>
                                <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Customer Name</th>
                                <th>Vehicle</th>
                                <th>Price</th>
                                <th>Created Date</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($recorded_new_admin as $make)
                                <tr>
                                    <td>{{ $make->id}}</td> 
                                     <td>{{ $make->title}}</td> 
                                    <td> 
         
                                     @if (@$make->user->name == '')
                                             @if (@$make->staff->name == '')
                                                    Supplier:{{@$make->Purchase->name}}

                                             @else
                                                    Staff:{{@$make->staff->name}}
                                             @endif           
                                        @else
                                            {{@$make->user->name}} 
                                        @endif
                                        </td>
                                    <td>{{ @$make->vehicle->licence_plate_number  }}</td>
                                    <td>£{{ $make->price}}</td>
                                    <td>{{ $make->created_at }} </td>
                                    <td>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top" data-original-title="Edit" href="{{ route ('adminrecordsEdit' , $make->id )}}"   class="mr-5"><i class="fa fa-edit"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete" href="{{ route('adminrecordsdestory', $make->id) }}"    
                                    onclick="return confirm('You want to remove your selected recorded?')"class="mr-5"><i class="fa fa-remove"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminrecordsview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                                            <!-- In Process table are started here -->
                                            <div class="tab-pane " id="profile">
                   <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                                 <thead>
                                <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Customer Name</th>
                                <th>Vehicle  </th>
                                <th>Price  </th>
                                 <th>Created Date</th>
                              <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($recorded_in_process as $make)
                                <tr>
                                    <td>{{ $make->id}}</td> 
                                     <td>{{ $make->title}}</td> 
                                    <td> {{ @$make->user->name }}</td>
                                    <td>{{ @$make->vehicle->licence_plate_number  }}</td>
                                    <td>£{{ @$make->price}}</td>
                                    <td>{{ $make->created_at }} </td>
                                    <td>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top" data-original-title="Edit" href="{{ route ('adminrecordsEdit' , $make->id )}}"   class="mr-5"><i class="fa fa-edit"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete" href="{{ route('adminrecordsdestory', $make->id) }}"    
                                    onclick="return confirm('You want to remove your selected recorded?')"class="mr-5"><i class="fa fa-remove"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminrecordsview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
 <!-- In Sattlement table are started here -->
                                            <div class="tab-pane" id="messages">
                                                <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                                 <thead>
                                <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Customer Name</th>
                                <th>Vehicle  </th>
                                <th>Price</th>

                                <th>Created Date</th>
                         
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($recorded_sattlement as $make)
                                <tr>
                                    <td>{{ $make->id}}</td> 
                                     <td>{{ $make->title}}</td> 
                                    <td> {{ $make->user->name }}</td>
                                    <td>{{ $make->vehicle->licence_plate_number  }}</td>
                                     <td>£{{ $make->price}}</td>
                                    <td>{{ $make->created_at }} </td>
                                    <td>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top" data-original-title="Edit" href="{{ route ('adminrecordsEdit' , $make->id )}}"   class="mr-5"><i class="fa fa-edit"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete" href="{{ route('adminrecordsdestory', $make->id) }}"    
                                    onclick="return confirm('You want to remove your selected recorded?')"class="mr-5"><i class="fa fa-remove"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminrecordsview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                                             <!-- In dispute table are started here -->

                                            <div class="tab-pane" id="settings">
                                                 <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                                 <thead>
                                <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Customer Name</th>
                                <th>Vehicle  </th>
                                <th>Price</th>

                                <th>Created Date</th>
                             
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($recorded_dispute as $make)
                                <tr>
                                    <td>{{ $make->id}}</td> 
                                     <td>{{ $make->title}}</td> 
                                    <td> {{ $make->user->name }}</td>
                                    <td>{{ $make->vehicle->licence_plate_number  }}</td>
                                     <td>£{{ $make->price}}</td>

                                    <td>{{ $make->created_at }} </td>
                                    <td>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top" data-original-title="Edit" href="{{ route ('adminrecordsEdit' , $make->id )}}"   class="mr-5"><i class="fa fa-edit"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete" href="{{ route('adminrecordsdestory', $make->id) }}"    
                                    onclick="return confirm('You want to remove your selected recorded?')"class="mr-5"><i class="fa fa-remove"></i> </a>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminrecordsview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                            
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>

@endsection