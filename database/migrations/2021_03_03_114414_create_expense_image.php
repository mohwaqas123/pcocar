<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_image', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('expence_id')->unsigned()->index();
            $table->string('images')->nullable();
            $table->timestamps();
        });
        Schema::table('expense_image', function (Blueprint $table) {
            $table->foreign('expense_id')->references('id')->on('expense')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_image');
    }
}
