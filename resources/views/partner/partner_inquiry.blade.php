@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<div class="container">
  
  
<section class="mt-5">
  <div class="row">
     
    <div class="col-md-12">
      
  
    
       

 @if(session()->has('message'))
        <div class="row justify-content-center">

    <div class="col-md-8 alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    </div>

@endif
 @if(session()->has('error'))
        <div class="row justify-content-center">

    <div class="col-md-8 alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    </div>

@endif

 

<main class="login-form">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h3 class="card-header">Fill the following to get on Board.</h2>
                    <div class="card-body">
                       <form class="" action="{{ route('partner_inquiry_send') }}" method="POST">
                    {{ csrf_field() }}
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">Name</label>
                                <div class="col-md-6">
                                    <input type="text"   name="name" class="form-control"  placeholder="Enter the Name" required >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">Business Name</label>
                                <div class="col-md-6">
                                    <input type="text"    class="form-control" placeholder="Enter the Business Name" name="business_name" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">E-mail</label>
                                <div class="col-md-6">
                                    <input type="email"  name="email" class="form-control" placeholder="Enter the Email"  required  >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">Contact Number</label>
                                <div class="col-md-6">
                                    <input type="number"  name="number" class="form-control" placeholder="Enter the Contact Number"  required  >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">Business Web-site</label>
                                <div class="col-md-6">
                                    <input type="text"   name="Business_web_site" class="form-control" placeholder="Enter the Business Web-site" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Number of Cars</label>
                                <div class="col-md-6">
                                    <input type="number"  name="number_of_cars" class="form-control" placeholder="Enter the Number of Cars"  required>, 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email_address" class="col-md-4 col-form-label text-md-right">Business details</label>
                                <div class="col-md-6">
                                    <input type="text"   name="business_details" class="form-control" placeholder="Enter the Business Details"  >
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="checkbox">
                                        <label>
                                           <!--  <input type="checkbox" name="remember"> Remember Me -->
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class=" form-group row col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                               
                            </div>
                              <!--  <a  href="partner_inquiry" class=" col-md-11 ffset-md-4 btn btn-link">
                                partner Inquiry    
                                </a> -->
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

</main>


 



<!-- 

                <div class="align-items-center text-center" id="signup">
                  <form class="" action="{{ route('customerlogin') }}" method="POST">
                    {{ csrf_field() }}
                    
                    <div class="form-group">
                      <input type="email" name="email" required="required" class="form-control" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                      <input type="password" required="required" name="password"  class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                      
                      <input value="Login" type="submit" class="form-control btn-primary"  >
                    </div>
                    New User? <a href="#" onclick="showSignup()">Signup</a>
                  </form>
                </div> -->
              
      
      
 
</div>
</div></section>
@include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<script>
$(function() {
$('#chk').click(function() {
if ($(this).is(':checked')) {
$('#id_of_your_button').removeAttr('disabled');
} else {
$('#id_of_your_button').attr('disabled', 'disabled');
}
});
});

 

  function showSignup(){
$('.signup').show();
$('.login').hide();
}
function showLogin(){
$('.signup').hide();
$('.login').show();
}
 
$(document).ready(function() {
// $('#btn_upload_info').click(function(event){
$( "#btn_upload_info" ).click(function() {
event.preventDefault();
var formData = new FormData($('#upload_information')[0]);
formData.append('#file_dvla', $('input[type=file]')[0].file_dvla[0]);
//alert(formData);
// formData.append('#file_cnic', $('input[type=file]')[0].file_cnic[0]);
//  $('#f_name').val(    $('input[type=file]').val()     );
$.ajax({
// url:"http://ip.jsontest.com/",
url: '{{ url("booking/upload_information") }}',
method:"POST",
data:formData,
//dataType:'JSON',
contentType: false,
cache: false,
processData: false,
success:function(data)
{
alert(data);
},
error: function(data)
{
//console.log(data);
alert('error');
}
})
});
var navListItems = $('ul.setup-panel li a'),
allWells = $('.setup-content');
allWells.hide();
navListItems.click(function(e)
{
e.preventDefault();
var $target = $($(this).attr('href')),
$item = $(this).closest('li');
if (!$item.hasClass('disabled')) {
navListItems.closest('li').removeClass('active');
$item.addClass('active');
allWells.hide();
$target.show();
}
});
$('ul.setup-panel li.active a').trigger('click');
// DEMO ONLY //
$('#activate-step-2').on('click', function(e) {
$('ul.setup-panel li:eq(1)').removeClass('disabled');
// $('ul.setup-panel li a[href="#step-1').addClass('done');
$('ul.setup-panel li a[href="#step-2"]').trigger('click');
$(this).remove();
})
//list-group-item-heading
//   <span class="glyphicon glyphicon-ok"></span>
$('#activate-step-3').on('click', function(e) {
$('ul.setup-panel li:eq(2)').removeClass('disabled');
$('ul.setup-panel li a[href="#step-3"]').trigger('click');
$(this).remove();
}) ;
@if(app('request')->input('step')==3)
$('ul.setup-panel li:eq(3)').removeClass('disabled');
@endif
});
</script>


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
$(function() {

var $form         = $(".require-validation");

$('form.require-validation').bind('submit', function(e) {
var $form         = $(".require-validation"),
inputSelector = ['input[type=email]', 'input[type=password]',
'input[type=text]', 'input[type=file]',
'textarea'].join(', '),
$inputs       = $form.find('.required').find(inputSelector),
$errorMessage = $form.find('div.error'),
valid         = true;
$errorMessage.addClass('hide');

$('.has-error').removeClass('has-error');
$inputs.each(function(i, el) {
var $input = $(el);
if ($input.val() === '') {
$input.parent().addClass('has-error');
$errorMessage.removeClass('hide');
e.preventDefault();
}
});

if (!$form.data('cc-on-file')) {
e.preventDefault();
Stripe.setPublishableKey($form.data('stripe-publishable-key'));
Stripe.createToken({
number: $('.card-number').val(),
cvc: $('.card-cvc').val(),
exp_month: $('.card-expiry-month').val(),
exp_year: $('.card-expiry-year').val()
}, stripeResponseHandler);
}

});

function stripeResponseHandler(status, response) {
if (response.error) {
$('.error')
.removeClass('hide')
.find('.alert')
.text(response.error.message);
} else {
/* token contains id, last4, and card type */
var token = response['id'];

$form.find('input[type=text]').empty();
$form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
$form.get(0).submit();
}
}

});
</script>