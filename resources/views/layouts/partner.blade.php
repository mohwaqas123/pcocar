<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8" />
    <title>Partner Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link href={{ asset('public/theme/plugins/datatables/dataTables.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/plugins/datatables/buttons.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href={{ asset('public/theme/plugins/datatables/responsive.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />

    <!-- Multi Item Selection examples -->
    <link href={{ asset('public/theme/plugins/datatables/select.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />

    <!-- App favicon -->
    <link rel="shortcut icon" href={{ asset('theme/assets/images/favicon.ico') }}>

    <!-- App css -->
    <link href={{ asset('public/theme/assets/css/bootstrap.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/icons.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/metismenu.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/style.css') }} rel="stylesheet" type="text/css" />

    @yield('styles')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div id="wrapper">
          @include('layouts.partnerSidebar')


          <div class="content-page">  


            <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            
                         
                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle"> <span class="ml-1">{{Session::get('p_name')}} <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                 <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h6 class="text-overflow m-0">Welcome  {{Session::get('p_name')}}!</h6>
                                </div>

                                <!-- item-->
                                <a href="{{ route('partner_change_password') }}" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>Change Password</span>
                                </a>
                                <a href="{{ route('partner_profiile_view') }}" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>Profile View</span>
                                </a>
                                  <a href="{{ route('partner_profiile') }}" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>Profile</span>
                                </a>

                                <!-- item-->
                                <a href="{{ route('partnerLogout') }}" class="dropdown-item notify-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('partnerLogout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>
                            </li>

                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    
                                        {!! html_entity_decode($breadcrum ?? '') ?? '' !!}
                                     
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>

            
       
 

        @yield('content')

      
    </div>
</body>

<!-- jQuery  -->
    <script src={{ asset('public/theme/assets/js/jquery.min.js') }}></script>
  <!--   <script src={{ asset('public/theme/assets/js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/metisMenu.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/waves.js') }}></script>
    <script src={{ asset('public/theme/assets/js/jquery.slimscroll.js') }}></script> -->

    <!-- App js -->
    <script src={{ asset('public/theme/assets/js/jquery.core.js') }}></script>
    <script src={{ asset('public/theme/assets/js/jquery.app.js') }}></script>

    <script src={{ asset('public/theme/assets/js/modernizr.min.js') }}></script>

    <!-- Required datatable js -->
    <script src={{ asset('public/theme/plugins/datatables/jquery.dataTables.min.js') }}></script>
    <script src={{ asset('public/theme/plugins/datatables/dataTables.bootstrap4.min.js') }}></script>






<!-- Datatable -->
<!--  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

 -->


    <!-- jQuery  -->
      <!--   <script src="{{ asset('public/admin/assets/js/jquery.min.js')}}"></script> -->
        <script src="{{ asset('public/admin/assets/js/popper.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/metisMenu.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/waves.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.slimscroll.js')}}"></script>

        <!-- Flot chart -->
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.min.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.time.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.resize.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.pie.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.crosshair.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/curvedLines.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.axislabels.js')}}"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="{{ asset('public/admin/plugins/jquery-knob/jquery.knob.js')}}"></script>

        <!-- Dashboard Init -->
        <script src="{{ asset('public/admin/assets/pages/jquery.dashboard.init.js')}}"></script>

        <!-- App js -->
        <script src="{{ asset('public/admin/assets/js/jquery.core.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.app.js')}}"></script>

    @yield('scripts')

</html>


 