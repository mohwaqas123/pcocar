<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccidentalRecordsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accidental_records_comments', function (Blueprint $table) {
            $table->id();
            $table->integer('vehicle_id');
             $table->integer('booking_id');
             $table->integer('accidental_id');  
             $table->integer('user_id');
             $table->tinyInteger('user_type')->comment('0=admin,1=customer')->nullable();
             $table->string('comments');
             $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accidental_records_comments');
    }
}
