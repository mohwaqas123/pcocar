@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                <tr>
                                <th style="width: 10px">Sr.#</th>
                                <th>Percentage Discount</th>
                                <th>Discount Code </th>           
                                <th>Status</th>
                                <th>Date_To</th>
                                <th>Date_From</th>
                                <th>Title</th>

                                <th style="width:90px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                    
                                @foreach($coupon as $dr)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $dr->percentdiscount}}</td>
                                    <td>{{ $dr->discountcode}}</td>
                                    <td style="width:100px;">
                                        @if ($dr->status == 1)
                                            Active
                                        @else
                                            In-active 
                                        @endif
                                    </td>
                                    <td>{{ $dr->date_from}}</td>
                                    <td>{{ $dr->date_to}}</td>
                                    <td>{{ $dr->Title}}</td>
                                    
                                    <td>
                                    <a class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top" data-original-title="Edit"
                                    href="{{ route('admin/discount/edit', $dr->id) }}" class="mr-5"><i class="fa fa-pencil"></i>
                                     </a>
                                     
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete"  href="{{ route('destory', $dr->id) }}" class="mr-5"><i class="fa fa-trash"></i> </a>
                                </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                           <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
