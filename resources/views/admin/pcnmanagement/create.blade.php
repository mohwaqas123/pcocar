@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


  <div class="content-page">
      <div class="content">

    

          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
            <div class="card-box"> 
                    <form role="form" action="{{ route('add_pcn') }}" enctype="multipart/form-data" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="row">
                                      <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">PCN Ref<span class="text-danger required">*</span></label>
                                    <input max="10" min="3" type="text" name="ref_number" class="form-control"   placeholder="Please Enter Number" required>    
                                      </div>
                                      <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">Vehicle<span class="text-danger required">*</span></label>
                                       <select  name="vehicle" 
                                        class="form-control licence_plate_number" id="select2" >
                                        @foreach($vehicle as $vehicle_us)
                                        <option  value="{{$vehicle_us->id}}"> 
                                          {{$vehicle_us->licence_plate_number}}
                                        </option>
                                        @endforeach
                                       </select>    
                                      </div>
                                      <div class="form-group col-md-6"  >
                                        <label for="make" class="col-form-label">Status <span class="text-danger required">*</span></label>
                                        <input   type="text" name="status" class="form-control " placeholder="Enter the Status" >
                                         
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Stage <span class="text-danger required">*</span></label>
                                        <input   type="text" name="stage" class="form-control "   placeholder="Enter the Stage" >
                                         
                                      </div>
                                      <div class="form-group col-md-6" >
                                        <label for="make" class="col-form-label">PCN Contravention Date <span class="text-danger required">*</span></label>
                                        <input   type="date" name="pcn_contravention" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the Stage" required="required">
                                         
                                      </div>
                                      <div class="form-group col-md-6" >
                                        <label for="make" class="col-form-label">Date of Notice <span class="text-danger required">*</span></label>
                                        <input   type="date" name="notice_date" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the Stage" required="required" >
                                         
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Date Received <span class="text-danger required">*</span></label>
                                        <input   type="date" name="received_date" class="form-control" id="" placeholder="Enter the Stage" required="required" >
                                         
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Type of Action<span class="text-danger required">*</span></label>
                                    <input   type="text" name="type_of_action" class="form-control" id="" placeholder="Enter the action">    
                                      </div>
                                      <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">Action Date  </label>
                                        <input   type="date" name="action_date" class="form-control" id="" placeholder="Enter the Stage" >
                                         
                                      </div>
                                      <div class="form-group col-md-6"  >
                                        <label for="make" class="col-form-label">Charging Authority  Name<span class="text-danger required">*</span></label>
                                    <input max="10" min="3" type="text" name="charging_authority_name" class="form-control" id="" placeholder="Enter the action" required>    
                                      </div>
                                      <div class="form-group col-md-6" id="customer">
                                        <label for="make" class="col-form-label">Driver Name<span class="text-danger required">*</span></label>
                                        <select    name="driver_name" placeholder="Customer Drop Down"class="form-control select2" >
                                          @foreach($user as $us)
                                        <option value="{{$us->id}}">{{$us->name}}
                                        </option>
                                        @endforeach
                                      </select>
                                      </div>
                                      <div class="form-group col-md-6"   >
                                    <label for="make" class="col-form-label">Other</label>
                                    <input  type="text" name="driver_email" class="form-control"   placeholder="Enter the name" >    
                                      </div>
                                      <!-- <div class="form-group col-md-6"  >
                                        <label for="make" class="col-form-label">Driver Phone<span class="text-danger required">*</span></label>
                                    <input type="number" name="driver_phone" class="form-control"   placeholder="Enter the phone" required>    
                                      </div> -->
                                         <div class="form-group col-md-6">
                                            <label for="name">PCN<span class="text-danger required">*</span></label>
                                             <input name="doc_mot"  class="form-control form-control-lg" type="file" placeholder="N.I.D(Back)" required="required" >
                                          </div>  
                                       
                                          
                                        </div>
                                          <!-- clone are added to the div  
                                          <div class="col-md-6">
                                             <input type="button"
                                             id="clone_btn" class="btn btn-primary" value="+Add New">
                                        </div>-->
                                         

                                        
                                            
                                          
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Add</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>
                                  </div>

                              </form>
                      
                      </div>
          </div>
              </div>
          </div>
      </div>
  </div>

    


@endsection
@section('scripts')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.js"></script>
<script>

   $('#select2').select2({});
 
// on first focus (bubbles up to document), open the menu
$(document).on('focus', '#select2-selection#select2-selection--single', function (e) {
  $(this).closest("#select2-container").siblings('select:enabled').select2('open');
});

// steal focus during close - only capture once and stop propogation
$('select#select2').on('select2:closing', function (e) {
  $(e.target).data("select2").$selection.one('focus focusin', function (e) {
    e.stopPropagation();
  });
});

  $( function() {

    $( "#booking_start_date" ).datepicker(
      {  
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate: -390,
       maxDate: new Date(),
     });
  });


$('.licence_plate_number').change(function(event) {
   
    var url_call = "{!! url('/') !!}/admin/pcn/get_customers";
     
    var vehicle_id = this.value;
    $.ajax({
        type : 'get',
        url  : url_call,
        //  data: {},
        data: { vehicle_id: vehicle_id},
        contentType: 'application/json; charset=utf-8',
        success :  function(data){
 
        console.log(data); 
        $("#customer").empty();
         $("#customer").append(data);
         // $("#driver_email").empty();
         // $("#driver_email").append(data);

 
     }
    });//ajax
});

 </script>

@endsection