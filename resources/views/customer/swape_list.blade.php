@extends('layouts.customer')
@section('styles')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            
                            
                            
                            <!-- Datatable -->
                            
                            <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
                            <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
                            
                            <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
                            <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                              <table id="datatable_tbl" class="table table-striped table-bordered" >
                                <thead>
                                <tr>
                                <th>Id</th>
                                <th>Customer Name</th>
                                <th>Vehicle</th>
                                <th>Details</th>
                                <th>Created Date</th>
                                 
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($notification as $notification_us)
                                    <tr>
                                        <td>{{$notification_us->id}}</td>
                                        <td>Name:{{$notification_us->user->name}} <br />
                                            Email:{{$notification_us->user->email}} <br />
                                            Phone:{{$notification_us->user->phone}}
                                        </td>
                                        <td>{{@$notification_us->vehicle->car_make->name}}<br>
                                            {{@$notification_us->vehicle->year}} <br>
                                            {{@$notification_us->vehicle->name}}
                                            {{@$notification_us->vehicle->model->name}}
                                            {{@$notification_us->vehicle->licence_plate_number}}<br>
                                            {{@$notification_us->booking->price}}
                                        </td>
                                        <td>{{@$notification_us->details}}</td>
                                         
                                        <td>
                                             
                                            Created: {{@$notification_us->date}} 
                                    
                                        </td>
                                       
                                        
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function() {
$('#datatable_tbl').DataTable();
} );
</script>
<script type="text/javascript">
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
"columnDefs": [
{ "orderable": false, "targets": [4,5,6] },
],
"bPaginate": false,
});
} );
</script>
@endsection