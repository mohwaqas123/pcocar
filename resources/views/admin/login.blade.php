<!doctype html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
    <title>PCOCAR | Admin </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href={{ asset('theme/assets/images/favicon.ico') }}>

    <!-- App css -->
    <link href={{ asset('public/theme/assets/css/bootstrap.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/icons.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/metismenu.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/style.css') }} rel="stylesheet" type="text/css" />

    </head>
    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg"></div>

        <div class="wrapper-page account-page-full">
            <div class="card mt-5">
                <div class="card-block">
                    <div class="account-box">
                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="index.html" class="text-success">
                                    <span><img src="{{asset('theme/assets/images/new_logo.png')}}" alt="" height="26"></span>
                                </a>
                            </h2>
           @if(session()->has('message'))
        <div class="row justify-content-center">

    <div class="col-md-12 alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    </div>

@endif

                            <form class="" action="{{ route('AdminLogin') }}" method="POST">
                                @csrf
                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">Email address</label>
                                        <input class="form-control" type="email" name="email" id="emailaddress" required="" placeholder="Enter your email">
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" name="password" required="" id="password" placeholder="Enter your password">
                                    </div>
                                </div>

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">2020 © PCOCAR</p>
            </div>

        </div>

    <script src={{ asset('public/theme/assets/js/jquery.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/metisMenu.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/waves.js') }}></script>
    <script src={{ asset('public/theme/assets/js/jquery.slimscroll.js') }}></script>

    <!-- App js -->
    <script src={{ asset('public/theme/assets/js/jquery.core.js') }}></script>
    <script src={{ asset('public/theme/assets/js/jquery.app.js') }}></script>

    <script src={{ asset('public/theme/assets/js/modernizr.min.js') }}></script>

    </body>
</html>
