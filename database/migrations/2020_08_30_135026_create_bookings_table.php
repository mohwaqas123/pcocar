<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('user_id');
            $table->integer('vehicle_id');
            $table->integer('duration');
            $table->integer('miles');
            $table->string('address');
            $table->string('dob');
            $table->string('dvla')->nullable();
            $table->string('national_insu_numb')->nullable();
            $table->string('pco_licence_no')->nullable();
            $table->string('doc_dvla')->nullable();
            $table->string('doc_cnic')->nullable();
            $table->string('doc_utility')->nullable();
            $table->string('doc_other')->nullable();
            $table->boolean('payment_status')->comment('0=not,1=done')->nullable();
            $table->boolean('agreement_status')->comment('0=not,1=done')->nullable();
 $table->integer('booking_status')->nullable();
$table->string('generate_ref_booking')->nullable();
$table->integer('promo')->nullable();
 
 
            
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
