@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<div class="container">
  
  <div class="row">
    <div class="col-md-3 text-center">
      
      @include ('frontend.bookingleft')
    </div>
    <div class="col-md-9">
      <ul class="progress">
        <li class="completed">
          <span>
            <span class="order">1 </span>Step 1 <span class="inner hidden-sm">(Login)</span>
          </span>
          <div class="diagonal"></div>
        </li>
        <li class="completed">
          <span>
            <span class="order">2 </span>Step 2 <span class="inner hidden-sm">(Information)</span>
          </span>
          <div class="diagonal"></div>
        </li>
        <li class="completed">
          <span>
            <span class="order">3 </span>Step 3 <span class="inner hidden-sm">(Agreement)</span>
          </span>
        </li>
        <li>
          <span>
            <span class="order">4 </span>Step 4 <span class="inner hidden-sm">(Payment)</span>
          </span>
        </li>
      </ul>
      
      
      <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
          <div class="col-md-12 well">
            <h4 class="text-center">Download Agreement</h4>
            <div class="row">
              
              
              <!--  <form
                role="form"
                action="{{ route('stripe.post') }}"
                method="post"
                class="require-validation"
                data-cc-on-file="false"
                data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                id="payment-form"> -->
                <form action="{{ route('booking4') }}"  method="post">
                  <input type="hidden" name="name" value="{{$name}}" />
                  <input type="hidden" name="booking_id" value="{{$booking_id}}" />

                  @csrf
                  
                  <div style="overflow-x: scroll; height: 400px">
                    
                    <div>
                      
                      
                      <h2 align="center">Subscription Hire Agreement </h2>
                      <ul>
                        
                        <p style="font-weight: bold;">
                          

                    Date:{{date('d-m-Y', strtotime($booking->booking_start_date))}}</p>
                        This Short Term Subscription Agreement (which comprises this Subscription Booking Form and Intercity Hire’s Terms of Subscription displayed below) ("<b>Subscription Agreement</b>") is entered into between Intercity Hire Limited (the "Company") of 23, STANLEY ROAD, LONDON, GBR, E4 7DB and {{$signature_name}} (the "Driver" ) of {{$booking->address}}, (collectively the "<b>Parties</b>" ). Intercity Hire operates the site www.pcocar.com on which the Vehicle to which the Driver wishes to subscribe is offered for subscription.<br>
                        The Driver subscribes to use the Vehicle supplied by Intercity Hire for the Subscription Fee during the Subscription Term (as defined below) (which may be amended if agreed between the Parties in writing in accordance with this Subscription Agreement), such arrangement being the "Subscription".<br>
                        <br>
                        <b>1. IDENTIFICATION OF THE VEHICLE AND NAMED DRIVERS</b>
                        <p>Intercity Hire agrees to provide to the Driver the following or similar vehicle (the "Vehicle"):</p>
                        <p>Make:  {{@$vehicle->car_make->name}}</p>
                        <p>Model:  {{@$vehicle->car_model->name}}</p>
                        <p>Registration Plate: {{@$vehicle->licence_plate_number}}</p>
                        
                        <b>2.  Only the Driver and the following individuals can drive the Vehicle during the Subscription: </b>


                        <p>&nbsp;&nbsp;&nbsp;&nbsp;2.1. {{$signature_name}}
    <ul>D.O.B: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$booking->dob}}</ul><br>
    <ul>D.V.L.A:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$booking->dvla}}</ul><br>
    <ul>Issuing Authority: Driver and Vehicle Licensing Authority England</ul><br>
    <ul>PCO Licence No:&nbsp;{{$booking->pco_licence_no}}</ul><br>
    <ul>D.V.L.A Expiry Date:&nbsp;{{$booking->expiredate}}</ul>
    <br>

       29/01/2025 UK (Driver)</p> 
                        <br>
                        <b>3. SUBSCRIPTION TERM: </b>
                        <p>The term of the Subscription will run for 6 months from the Subscription Start Date (unless cancelled by agreement of the Parties according to the Earliest Subscription End Date or cancelled in accordance with this Subscription Agreement):</p>
                        <p>Subscription Start Date:  {{date('d-m-Y', strtotime($booking->booking_start_date))}}</p>
                        <p>Earliest Subscription End Date: WEEKLY renewal until 
{{date('d-m-Y', strtotime($booking->booking_drop_off_date))}}, unless cancelled</p>
                        <p>4.  The time from which the Driver collects the Vehicle to the point the Driver returns the Vehicle is the "Subscription Term".</p>
                        <br> <b>5. SUBSCRIPTION FEE: </b>
                        <p>The Driver agrees to pay Intercity Hire the following fee for the Subscription (the "Subscription Fee"):</p>
                        @if($booking->insurance_type == 1)
                        <p>Subscription Price  [Rent]:  £{{$booking->price }} "Weekly Subscription Rate"</p>
                       <p>Frequency of payment: WEEKLY</p>
                       @else
                       <p>Subscription Price [Rent + Insurance] :  £{{$booking->price }} + £70 "Weekly Subscription Rate"</p>
                       @endif
                        <br>
                        <p> <b>6.  MILEAGE </b>
                          <p>The Driver agrees to the following Mileage Allowance and to pay the following surcharge if such Mileage Allowance is exceeded:</p>
                          <p>Mileage Allowance [per Week]: 1050 </p>
                          <p>Excess Mileage Charge:  10p per Mile</p>
                        </p>
                        <b>7.  INSURANCE </b>
                        @if($booking->insurance_type == 1)
                       <p><b>Insurance through Intercity Hire</b>  NO    </p>

                       @else
                       <p><b>Insurance through Intercity Hire</b>  Yes   </p>
                                          
                       
                         <div class="float-left">
                        <p><b>POLICYHOLDER</b></p>
                        <p>Policyholder Name: Intercity Hire Ltd </p>
                        <p>Address:23 Stanely Road, Chingford,England,E4 7DB</p>
                        </div>
                        <div class="float-right">
                         
                        <p><b>POLICY</b></p>

                        <li>Policy Type:Motor Fleet Insurance</li>
                        <li>Policy Number:ZLPA/8QZ7TO9</li>
                        <li>Type of vehicle(s):car</li>
                        <li>Use of vehicle(s):private Hire/SDP</li>
                        <hr>
                        <li>Level of cover:Fully Comprehensive</li>
                        <li>Territories:United Kingdom</li>
                        <li>Period of Insurance:
                  {{date('d-m-Y', strtotime($booking->booking_drop_off_date))}} to {{date('d-m-Y', strtotime($booking->booking_start_date))}}</li>
                        </div>
                      <div class="float-left">
                        <p><b>VEHICLE(S) COVERED</b></p>
                        <p>Any vehicle in the intercity Hire Ltd fleet leased to a driver by Zego in accordance with the underwriting criteria below.</p>
                         
                        </div>
                        <div class="float-left">
                        <p><b>UNDERWRITING CRITERIA</b></p>
                        <p>Cover is only provided to driver and in respect of vehicles who/which meet the underwriting criteria below:</p>
                        <li>Ages:23+</li>
                        <li>Years licence held:3</li>
                        <li>At Fault claim:max in 1 last 3 years</li>
                        <li>Non-Fault claim:max in 2 last 3 years</li>
                        <li>Driving Restriction: UK only.</li>
                        <li>DVLA licence points: max 6 Points</li>
                        <li>No motoring disqualification in the last 5 years</li>
                        <li>Max vehicle value £25000</li>
                        </div> <br>
                         <p>&nbsp;</p>  <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p> <p>&nbsp;</p> 
                        <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p> <p>&nbsp;</p>
                          <table>
                                <tr> 
                                <th>Policy Section&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>  
                                <th>Included?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th>  
                                <th>Sum Insured</th>                                  
                                <th>Excess</th>
                                 </tr>
                                  <tr>
    <td>1 </td>
    <td>Yes</td>
    <td>Third party Property damage:£20,000,000<br>
    Bodily injury:Unlimited per event,unlimited per policy</td>
    <td>N/A</td>
    </tr>
   <tr>
    <td>2 </td>
    <td>Yes</td>
    <td>fire or Theft: cost of repair or vehicle's market value</td>
    <td>1st Party Excess:£500</td>
    </tr>
    <tr>
    <td>3</td>
    <td>Yes</td>
    <td>Loss of or damage to the vehicles after an incident:Cost of repair or vehicle's
    market value windscreen:Unlimited Audio,communication and navigation equip: up to £1000</td>
    <td>1st Party Excess:£500 Audio,comm and nav.equip:£500 Windscreen:£100</td>
    </tr>
    </table>
    
                      
       <div class="float-left">
    <p><b>Insurance Company Name and Address::</b> La parisienne Assurances,120-122 Rue Reaumur,75083 paris,France and authorised by the Autoritede controle prudentiel et de resolution (ACPR) in France.Authrosied to operate in the United Kindom on a freedom of services basis.</p>
    </div>
     @endif
 
                           <p style="font-weight: bold;">If insurance is not provided to the Driver or any Named Drivers through Intercity Hire, insurance must be obtained prior to the Driver (and any Named Drivers) using the Vehicle. It is the responsibility of the Driver to obtain comprehensive motor insurance for themselves and to ensure that any Named Drivers are covered by a motor insurance policy. This insurance must be pre-approved by Intercity Hire in writing. Relevant insurance excess charges apply.</p>



                         @if($booking->insurance_type == 1)
                      
                        <p><b>Customer's Insurance Details</b></p>
                        <p>Policy Number: {{$csutomer_insurance_record->policy_number}}</p>
                        <p>Cover Type: {{$csutomer_insurance_record->cover}}</p>
                        <p>start: {{$csutomer_insurance_record->start_date}} </p>
                        <p>End:  {{$csutomer_insurance_record->end_date}}</p>

                        <p><b>Insurance Company Details</b></p> 
                        <p>Company:  {{$csutomer_insurance_record->company_name}}</p> 
                        <p>Email:  {{$csutomer_insurance_record->email}}</p>
                        <p>Contact Number: {{$csutomer_insurance_record->contact_number}} </p>
                        
                        <img src="{{ asset('storage/app/documents_uploads/pdf/'.$csutomer_insurance_record->customer_private_insurance) }}"  height="700" width="700">
                        @else
                        @endif
                        <br>
                          
                    <p><strong>TERMS OF SUBSCRIPTION</strong></p>
                        <p>&nbsp;</p>
                        <ol>
                          <li><strong>Subscription</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Hire Agreement is made between Intercity Hire and the Driver is constituted by the front pages (which we refer to as the "Subscription Booking Form") and these Terms of Subscription which form part of and are incorporated within this Hire Agreement made between Intercity Hire and the Driver. Any capitalised terms that are used but not defined in these Terms shall have the meaning given to them in the Subscription Booking Form</p>
                        <p>&nbsp;</p>
                        <p>1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver agrees to hire the Vehicle referred to in the Subscription Booking Form for the Subscription Term and any further period as provided for in this Agreement. The Vehicle is provided by Intercity Hire at the relevant Subscription Fee stated in the Subscription Booking Form. If the Driver does not return the Vehicle to Intercity Hire by the Subscription End Date (or such extended date as provided for in this Agreement), the Driver will be in breach of the conditions of this Agreement. For the avoidance of doubt, the Driver will not own the Vehicle.</p>
                        <p>&nbsp;</p>
                        <p>1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire may substitute the Vehicle for another vehicle (of a similar age, quality and specification) on at least 7 days&rsquo; notice at any point during this Agreement. Intercity Hire may also substitute the Vehicle for another vehicle in the event of a manufacturer&rsquo;s recall, or for other legitimate mechanical, health or safety purposes. The Driver will cooperate with the reasonable requirements of Intercity Hire as to substitution of the Vehicle and, following substitution, the new Vehicle will be the &ldquo;Vehicle&rdquo; for the purpose of this Agreement.</p>
                        <p>&nbsp;</p>
                        <ol start="2">
                          <li><strong>Responsibilities of the Driver</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>2.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire, or a third party on behalf of Intercity Hire, will deliver the Vehicle to an address specified by the Driver on the Subscription Start Date and collect it at its end (unless alternative arrangements are agreed by Intercity Hire and the Driver in writing). The Driver must inspect the Vehicle before the start of the Subscription Term. The Driver will make Intercity Hire aware of any defects in the Vehicle at the time when the Vehicle is delivered. In the absence of the Driver notifying Intercity Hire of any issues at the time when the Vehicle is received by the Driver, it shall be deemed that the Driver received the Vehicle in perfect working order (save for any existing damage recorded in the Vehicle Condition Report which the Driver will sign on delivery). The Driver will return the Vehicle to Intercity Hire in the same condition in which the Driver received it, save for the normal wear and tear in relation to distance travelled, including (but not limited to) tyres, fittings, documents and complete equipment outfit. The Driver shall return the Vehicle with no less than the amount of fuel that it had on delivery or pay Intercity Hire for the cost of the shortfall in fuel as compared to the fuel in the Vehicle upon delivery. The Driver must take care of, and keep secure, the Vehicle and its keys. The Driver must always lock the Vehicle when not using it (and will be charged for the replacement cost of any lost keys). The Driver must make sure that they use the correct fuel. The Driver is also responsible for tyre punctures.</p>
                        <p>&nbsp;</p>
                        <p>2.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire has the right to request and conduct a Vehicle inspection during the Subscription Term, subject to giving the Driver 7 days&rsquo; prior notice. The Driver shall act in good faith and reasonably comply with such a request.</p>
                        <p>&nbsp;</p>
                        <p>2.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must not sell, rent or dispose of the Vehicle or any of its parts. The Driver must not give anyone other than Intercity Hire any legal rights over the Vehicle.</p>
                        <p>&nbsp;</p>
                        <p>2.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must not (and must not let anyone else) modify or work on or attach or affix anything to the Vehicle without Intercity Hire&rsquo;s written permission. Any additions, alterations or modified parts fitted without such permission shall become part of the Vehicle and shall belong to Intercity Hire, and the Driver shall be responsible for any costs in returning the Vehicle to its pre-modified condition.</p>
                        <p>&nbsp;</p>
                        <p>2.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must let Intercity Hire know as soon as they become aware of any defect(s) in or damage to the Vehicle at any point if or when the Driver discovers any issues or defects during the Subscription Term.</p>
                        <p>&nbsp;</p>
                        <p>2.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver shall be liable for damage to, and theft of, the Vehicle from delivery of the Vehicle until Intercity Hire has collected the Vehicle from the Driver at the end of the Subscription Term. The Driver must meet with the collection manager at the Subscription End Date or such extended date as provided for in this Agreement (or as otherwise agreed between the Parties in writing and in accordance with this Agreement). Intercity Hire, or a third party on behalf of Intercity Hire must inspect the Vehicle to check that it is in good condition. Where the Driver has requested a collection of the Vehicle by Intercity Hire at a specified</p>
                        <p>&nbsp;</p>
                        <p>location, outside of the hours of 09:00 to 18:00 Monday to Saturday, the Subscription Term (and the Driver&rsquo;s liability for damage, theft and parking violations) shall extend to the earlier of: midday of the first working day following the requested collection time or the time of re-inspection by Intercity Hire; or of a third party on behalf of Intercity Hire.</p>
                        <p>&nbsp;</p>
                        <p>2.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver will be liable for repairs (which must be carried out by a repair centre approved by Intercity Hire) if the Vehicle needs more than Intercity Hire&rsquo;s standard valeting (cleaning), or if the Vehicle has been damaged either inside or outside (whether or not it is the Driver&rsquo;s fault). Any insurance coverage may cover such repairs and charges but in the event it does not, the Driver will be separately charged by Intercity Hire for any such repairs and charges. The Driver will be charged by Intercity Hire (as payment collection agent for the Company) using the bank details supplied by Driver to Intercity Hire for any such repairs and charges.</p>
                        <p>&nbsp;</p>
                        <p>2.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must check before they bring back the Vehicle that they have not left any belongings in the Vehicle. Intercity Hire is not liable for any personal belongings left in the Vehicle after the Vehicle has been returned.</p>
                        <p>&nbsp;</p>
                        <p>2.9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver will be liable for any Excess Mileage Charge as set out in the Subscription Booking Form if the Driver exceeds the Mileage Allowance during the Subscription Term. On any substitution of a Vehicle pursuant to condition 1.3, the Mileage Allowance and any Excess Mileage Charge shall be recalculated by Intercity Hire by reference to the distance in mileage travelled by the substitute Vehicle at the date of substitution.</p>
                        <p>&nbsp;</p>
                        <p>2.10&nbsp;&nbsp;&nbsp;&nbsp; The Driver shall comply with its insurance obligations, as set out in condition 7 below.</p>
                        <p>&nbsp;</p>
                        <p>2.11&nbsp;&nbsp;&nbsp;&nbsp; If the Driver wishes to alter the collection or drop-off arrangements, they must obtain prior consent from Intercity Hire for such alternative arrangements.</p>
                        <p>&nbsp;</p>
                        <ol start="3">
                          <li><strong>Responsibilities of Intercity Hire</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire, or a third party on behalf of Intercity Hire, will maintain the Vehicle to at least the manufacturer&rsquo;s recommended standard and the Driver must ensure that all servicing and repairs are carried out by a party approved by Intercity Hire</p>
                        <p>&nbsp;</p>
                        <p>3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire confirms that the Vehicle is roadworthy and suitable for driving at the Subscription Start Date. Intercity Hire, or a third party on behalf of Intercity Hire, will identify any existing damage to the Vehicle on the Vehicle Condition Report which will be signed by the Intercity Hire in accordance with condition 2.1 above.</p>
                        <p>&nbsp;</p>
                        <ol start="4">
                          <li><strong>Liability</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Subject to condition 4.2 below, Intercity Hire&rsquo;s maximum aggregate liability to the Driver for any and all claims arising out of or in connection with this Agreement shall in no event exceed &pound;10,000.</p>
                        <p>&nbsp;</p>
                        <p>4.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nothing in this Agreement excludes or restricts Intercity Hire&rsquo;s liability to the Driver for death or personal injury caused by Intercity Hire&rsquo;s negligence, for fraud and fraudulent misrepresentation, or for any other liability that cannot be excluded or limited under applicable law.</p>
                        <p>&nbsp;</p>
                        <p>4.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Notwithstanding condition 4.1 above, Intercity Hire shall not be liable for:</p>
                        <p>&nbsp;</p>
                        <p>4.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; indirect losses which happen as a side effect of the main loss or damage and which are not reasonably foreseeable by Intercity Hire and the Driver at the time of entering into this Agreement (such as loss of profits, income or loss of opportunity);</p>
                        <p>&nbsp;</p>
                        <p>4.3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; losses not caused by Intercity Hire&rsquo;s breach; or</p>
                        <p>&nbsp;</p>
                        <p>4.3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; failure or delay in performing any or all of its obligations under this Agreement, where such failure is caused, directly or indirectly, by events beyond Intercity Hire&rsquo;s reasonable control (including, but not limited to, network failure, fire, flood, earthquake, acts of God, acts of war, terrorism, riots, civil disorders, blockades, insurrections, pandemics, epidemics, any law or action taken by a government or public authority, any labour or trade disputes, strikes, industrial action or lockouts, or non-performance by suppliers or subcontractors).</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <ol start="5">
                          <li><strong>Conditions for using the Vehicle</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>5.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Vehicle must only be driven by the Driver, must be driven in a careful manner at all times and must be driven in compliance with all applicable road traffic laws (and any other laws or regulations applicable to drivers).</p>
                        <p>&nbsp;</p>
                        <p>5.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must have held a full current UK driving licence for at least one year, be at least 23 years old and have no endorsements or convictions on their driving licence during the Subscription Term which are unacceptable to Intercity Hire. Unacceptable endorsements or convictions may include, but are not limited to, any convictions that result in the Driver being disqualified from driving, or any major driving convictions, such as failing to report an accident. If the Driver receives any endorsements or convictions during the Subscription Term the Driver must inform Intercity Hire, and Intercity Hire may terminate this Subscription Agreement in accordance with condition 11. The Driver must also inform Intercity Hire of such endorsements or convictions if the Driver has obtained insurance through Intercity Hire, as this may affect the validity of the insurance. If any insurance becomes invalid at any point during the Subscription Term, it is the Driver&rsquo;s responsibility to obtain adequate replacement insurance because such insurance is a legal requirement.</p>
                        <p>&nbsp;</p>
                        <p>5.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must not:</p>
                        <p>&nbsp;</p>
                        <p>5.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; use the Vehicle to transport any hazardous, toxic, flammable, corrosive, radioactive, harmful, dangerous or illegal materials;</p>
                        <p>&nbsp;</p>
                        <p>5.3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; use the Vehicle for any illegal purpose or any purpose or any manner which would not be covered by the insurance and/or would invalidate the insurance;</p>
                        <p>&nbsp;</p>
                        <p>5.3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; use the Vehicle off-road, or for racing, pace making, testing the Vehicle&rsquo;s reliability and speed, or teaching someone to drive;</p>
                        <p>&nbsp;</p>
                        <p>5.3.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; use the Vehicle under the influence of alcohol or drugs or smoke inside the vehicle. This will result in the Driver&rsquo;s insurance becoming invalid (if the Driver has opted into insurance through Intercity Hire);</p>
                        <p>&nbsp;</p>
                        <p>5.3.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; carry a carrier payload which exceeds the maximum payload and individual axle plated weights, or for a purpose which requires an operator&rsquo;s licence where the Driver does not have one;</p>
                        <p>&nbsp;</p>
                        <p>5.3.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; drive the Vehicle outside the United Kingdom unless Intercity Hire has given the Driver written permission;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <ol start="6">
                          <li><strong>Subscription Fees, Fines, Tolls and other charges</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must pay the Initial Payment and the Subscription Fees at the rate and on the dates specified in the Subscription Booking Form. The Driver must obtain the prior written consent of Intercity Hire to change the Payment Date to a to a day in the month other than the Payment Date specified in the Subscription Booking Form. The Driver shall be liable for the following charges and pay them on demand during the Subscription Term:</p>
                        <p>&nbsp;</p>
                        <p>6.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; all charges which are payable after the discovery of damage following re-inspection of the Vehicle when returned by the Driver to Intercity Hire;</p>
                        <p>&nbsp;</p>
                        <p>6.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; all charges, fines and court costs including congestion charges, parking, traffic, speeding or other offences, and any civil penalty payable relating to the Vehicle (&ldquo;Fines&rdquo;) and any tolls, fees or charges including toll road fees, and the London Congestion Charge (&ldquo;Tolls&rdquo;). The Driver must also pay to the appropriate authority any Fines, Tolls and costs if and when the relevant authority demands this payment, and acknowledges that such obligations may be communicated to the Driver directly, through Intercity Hire or the relevant authority;</p>
                        <p>&nbsp;</p>
                        <p>6.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; value added tax and all other taxes and levies on any of the Fines, Tolls and charges, as appropriate including an administration fee for each Fine, Toll or charge that the Driver incurs during the term of their subscription;</p>
                        <p>&nbsp;</p>
                        <p>6.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any reasonably incurred, foreseeable losses, costs and charges resulting from the breach by the Driver of this Agreement (such losses being foreseeable where they are contemplated by</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>Intercity Hire and the Driver at the time this Agreement is entered into);</p>
                        <p>&nbsp;</p>
                        <p>6.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any other charges arising under this Agreement; and</p>
                        <p>&nbsp;</p>
                        <p>6.1.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any charges in relation to the repossession of the Vehicle, including, without limitation, third party agent costs, transportation required for the repossession of the Vehicle, legal proceedings in relation to the repossession of the Vehicle and any other costs, charges and expenses in relation to or in connection with the breach of this Agreement by the Driver.</p>
                        <p>&nbsp;</p>
                        <p>6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; For the avoidance of doubt, the Driver shall be liable for any Fines, Tolls and other charges, issued by public authorities or private parking companies, incurred during the Subscription Term even if such Fines, Tolls and other charges are not discovered until after the end of the Subscription Term.</p>
                        <p>&nbsp;</p>
                        <p>6.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It is the responsibility of the Driver to pay the relevant authorities directly for any Fines, Tolls and other charges that the Vehicle or the Driver incurs during the Subscription Term. The Driver must provide a written report of any offences committed by him or her to Intercity Hire. In the case of any Fine, the Driver acknowledges and agrees that Intercity Hire may pass on the Driver&rsquo;s details to the police or relevant authority, who may then contact the Driver directly. Intercity Hire is not liable for any escalation in value of a Fine, Toll or charge as a result of it being delivered to an out-of-date address. It is the Driver&rsquo;s responsibility to inform Intercity Hire of any change of address so that Fines may be delivered to them in sufficient time to prevent escalation. In the event that Intercity Hire incurs a fine, charge or admin fee levied by a third party as a result of the Driver&rsquo;s incurring of a Fine, charge or admin fee. Intercity Hire retains the right to charge the cost of such a fine, charge or admin fee to the Driver. In the event that the Driver does not pay the Fines, Tolls or other charges, Intercity Hire may pay such Fines, Tolls or other charges and then reclaim such Fines, Tolls or other charges from the Driver if it would be in Intercity Hire&rsquo;s interests to do so, including, without limitation, where the Vehicle may be at risk and/or there may be other enforcement in relation to unpaid Fines, Tolls or other charges.</p>
                        <p>&nbsp;</p>
                        <p>6.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver shall keep the Vehicle in good and roadworthy condition (fair wear and tear excepted) throughout the Subscription Term and return the Vehicle in good and roadworthy condition to Intercity Hire at the end of the Subscription Term.</p>
                        <p>&nbsp;</p>
                        <p>6.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Driver or any other person smoke inside the Vehicle in contravention of condition 5.3.4 above, the Driver shall be liable to pay Intercity Hire a penalty of &pound;150.</p>
                        <p>&nbsp;</p>
                        <p>6.6&nbsp;&nbsp;&nbsp;&nbsp; All charges including rental, insurance, congestion&nbsp; and all other toll charges must be paid on time.</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rental plus insurance must be paid within 24 hours of rental date unless pre-notified for extension or any missing payment. Congestion or any toll charges must be cleared in 7 working days from day of notification. Any delay will be considered towards breach of contract &amp; could incur any extra charges against all late payments as late processing fee. Missing 2 rental payments without pre-notification, would allow company to repossess vehicle and recover all outstanding payment through debt collection agency with an additional charge of &pound;500.00 + VAT as vehicle repossession plus recovery charges.</p>
                        <p>&nbsp;</p>
                        <ol start="7">
                          <li><strong>Insurance</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>7.1&nbsp;</p>
                        <p>Intercity Hire has arranged its motor insurance for PCO vehicles through Zego, an insurance intermediary who introduces business customers to Zego policies underwritten by La Parisienne. The policy provides coverage for accidental damage, fire &amp; theft and third-party liability claims. The details of any such insurance shall be included in the Subscription Booking Form. The insurance key facts document can be found in the Drivers My PCO CAR account and Intercity Hire website. If insurance is not arranged for the Driver through Intercity Hire, insurance must be obtained prior to the Driver using the Vehicle.</p>
                        <p>&nbsp;</p>
                        <p>7.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Driver does not buy comprehensive motor insurance through Intercity Hire by selecting the insurance option on the Subscription Booking Form, it is the Driver&rsquo;s responsibility to obtain comprehensive motor insurance for themselves. If the driver chooses to use an alternative comprehensive policy, this must be pre-approved by Intercity Hire in writing. Intercity Hire will require the Driver to advise the insurers that the Vehicle is on hire and the policy of insurance must name Intercity Hire as loss payee in respect of all claims other than third party claims. The Driver irrevocably authorises Intercity Hire to negotiate with the insurers to settle any insurance claim and to receive the insurance monies as appropriate. The Driver will accept any reasonable settlement we agree with the insurers.</p>
                        <p>&nbsp;</p>
                        <p>7.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Where the Driver has elected to receive their insurance through Intercity Hire, Intercity Hire reserves the right to amend the Driver&rsquo;s comprehensive motor insurance price in cases where modifications in the Driver&rsquo;s risk profile change and give rise to additional costs. In the event of an insurance price change, the Driver will receive fair notice of the changes and new price.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <ol start="8">
                          <li><strong>Accidents, Damage and Theft</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>8.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the event of an accident involving the Vehicle, the Driver must not admit responsibility or attempt to negotiate with third parties. The Driver should note the registration numbers of other vehicles, take photographs and/or video of the scene and vehicles involved and obtain the names and addresses of everyone involved, including witnesses and should also:</p>
                        <p>&nbsp;</p>
                        <p>8.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; make the Vehicle secure;</p>
                        <p>&nbsp;</p>
                        <p>8.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; tell the police without delay if anyone is injured or if the Driver was unable to exchange details with other drivers or property owners;</p>
                        <p>&nbsp;</p>
                        <p>8.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; report the incident to the Vehicle insurer as soon as is practically possible and no later than 24 hours after the incident;</p>
                        <p>&nbsp;</p>
                        <p>8.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; note the insurance details of other involved third parties; and</p>
                        <p>&nbsp;</p>
                        <p>8.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; comply with all other applicable road traffic and other laws.</p>
                        <p>&nbsp;</p>
                        <p>8.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the event of damage to the vehicle, the Driver must comply with the instructions of Intercity Hire (and/or the insurers of the Vehicle). The Driver must provide all reasonable co-operation and assistance to the insurer and Intercity Hire as to the conduct of any claim. If the Vehicle is stolen, the Driver must inform the police and Intercity Hire as soon as becoming aware of the theft.</p>
                        <p>&nbsp;</p>
                        <p>8.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If a warning light appears in the Vehicle or the Vehicle develops any fault during the Subscription Term, the Driver must contact Intercity Hire as soon as practicable to provide details of the fault.</p>
                        <p>&nbsp;</p>
                        <p>8.4&nbsp; In the event of any claim under the insurance set out in under condition 7.1, the Driver must;</p>
                        <p>&nbsp;</p>
                        <p>8.4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; comply with the instructions of Intercity Hire (and/or insurer) as to the Vehicle; and</p>
                        <p>&nbsp;</p>
                        <p>8.4.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; provide all reasonable co-operation and assistance to Intercity Hire (and/or insurer) as to the conduct of any claim.</p>
                        <p>&nbsp;</p>
                        <p>8.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the event of any accident involving the Driver which is likely to have been wholly or mainly the fault of a party other than the Driver (&ldquo;Non-Fault Accident&rdquo;), the Driver; comply with the instructions of Intercity Hire (and/or insurer) as to the Vehicle; and</p>
                        <p>&nbsp;</p>
                        <p>8.5.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; is obliged to follow Intercity Hire&rsquo;s instructions as to having the Vehicle repaired, either by making it available to Intercity Hire at a time of its choosing or by making arrangements at Intercity Hire&rsquo;s direction for the Vehicle to the repaired; and</p>
                        <p>&nbsp;</p>
                        <p>8.5.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; must provide all reasonable co-operation and assistance to Intercity Hire as to the conduct of any claim;</p>
                        <p>&nbsp;</p>
                        <p>8.5.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; must continue to pay the Subscription Fees under this Agreement during the Subscription Term (and for any further period while the Vehicle is in fact under repair and/or otherwise not available for use); and</p>
                        <p>&nbsp;</p>
                        <p>8.5.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; will have no right under this Agreement to receive a replacement vehicle from Intercity Hire while the Vehicle is under repair or otherwise unavailable to the Driver</p>
                        <p>&nbsp;</p>
                        <p>8.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the event of a Non-Fault Accident:-</p>
                        <p>&nbsp;</p>
                        <p>8.6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; will inform the Driver of the names and contact details of one or more vehicle hire providers who might assist (however, Intercity Hire will not itself provide a replacement vehicle under this Agreement until the Vehicle has been fully repaired or replaced); and</p>
                        <p>&nbsp;</p>
                        <p>8.6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; the Driver has authorised Intercity Hire to correspond with the insurer on its behalf and the Driver agrees to comply with condition 8.5 above.</p>
                        <p>&nbsp;</p>
                        <p>8.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In the event that the accident is not considered by (or its agent) to be the fault of another person who is insured, Intercity Hire may in its discretion agree to hire a replacement vehicle to the Driver under a new subscription agreement.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>8.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If any Vehicle is declared a total loss and Intercity Hire does not receive any insurance monies or if the insurance monies received are in any way reduced because of any failure by the Driver to observe the terms and conditions of this Agreement, the Driver shall pay Intercity Hire on demand the difference between the insurance monies Intercity Hire do receive and the termination payment set out in condition 11.10 calculated at the date of such loss.</p>
                        <p>&nbsp;</p>
                        <ol start="9">
                          <li><strong>Servicing, Maintenance and Breakdown Cover</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>9.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver is responsible to inform Intercity Hire if the vehicle requires routine servicing and maintenance from the Subscription Start Date to the Subscription End Date (which includes tyres, maintenance and arranging MOTs). Once advised, Intercity Hire, or a third party operating on behalf of Intercity Hire, will provide fair and reasonable notice of no less than three days to the Driver when arranging servicing or maintenance appointments, advise the Driver of these and coordinate schedules with the Driver. It is the Driver&rsquo;s responsibility to keep these agreed appointments and otherwise bear any rescheduling costs if an appointment is missed.</p>
                        <p>&nbsp;</p>
                        <p>9.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Breakdown cover provided by the RescueMyCar (or such other reputable motor organisation chosen by Intercity Hire) is included in the Subscription Fee when the fault is determined mechanical. Where the roadside assistance falls out with what is covered by the cost Intercity Hire pay to RescueMyCar (or such other reputable motor organisation), the Driver may be liable for the associated costs. It is the Driver&rsquo;s responsibility to keep agreed breakdown callouts and otherwise bear any cancellation costs if a callout missed. Drivers must refer to and follow the breakdown information as part of Drivers subscription confirmation. Breakdown cover information is also made available in the Drivers MYPCOCAR Hire account. Drivers should contact <a href="mailto:support@pcocar.com">support@pcocar.com </a>if there are any questions in relation to the breakdown policy.</p>
                        <p>&nbsp;</p>
                        <ol start="10">
                          <li><strong>Telematics</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>10.1&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire may, at its sole discretion, install a telematics solution ("Telematics") in the Vehicle at Intercity Hire's own cost. Telematics allows Intercity Hire to GS track Vehicles as well as extract mileage, fuel level, acceleration and braking data, as well as remotely lock / unlock or immobilise Vehicles. By using the Vehicle, the Driver consents to the collection of such data.</p>
                        <p>&nbsp;</p>
                        <ol start="11">
                          <li><strong>Cancellation, Default and Termination</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>11.1&nbsp;&nbsp;&nbsp;&nbsp; For the avoidance of doubt, the Subscription Term end only with the Driver&rsquo;s return of the Vehicle to Intercity Hire or a third party specified by Intercity Hire.</p>
                        <p>&nbsp;</p>
                        <p>11.2&nbsp;&nbsp;&nbsp;&nbsp; If a Driver elects to cancel a Subscription at least 72 hours before the Subscription Start Date specified in the Subscription Booking Form the Driver can cancel the Subscription without charge. All the Subscription Fees paid in advance by the Driver will be refunded. Intercity Hire shall retain the Joining Fee.</p>
                        <p>&nbsp;</p>
                        <p>11.3&nbsp;&nbsp;&nbsp;&nbsp; if less than 72 hours prior to the Subscription Start Date and the Driver has already paid the Monthly Subscription Fee and Initial Payment in advance, we will partially refund the Fees paid. We shall retain &pound;100 as a cancellation charge to cover the vehicle administration cost.</p>
                        <p>&nbsp;</p>
                        <p>11.4&nbsp;&nbsp;&nbsp;&nbsp; The Driver will be able to terminate this Agreement before the Subscription End Date. In order to do so, the Driver must give Intercity Hire 72-hour notice and must pay Intercity Hire an early termination fee. The early termination fee due will be 20% of outstanding Subscription Fee or one-week Subscription Fee - whichever is higher. This early termination fee must be paid before the cancellation can be confirmed and before Intercity Hire can arrange collection of the Vehicle.</p>
                        <p>&nbsp;</p>
                        <p>11.5&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire may cancel a subscription prior to the Subscription Start Date where reasonably required (for example, if an unforeseen Vehicle fault arises, or if the availability of the Vehicle changes) by notifying the Driver at any point prior to the Driver collecting the Vehicle (or having the Vehicle delivered to Driver, as the parties may agree), and any overpayments in relation to the Subscription Fee, and other amounts paid shall be refunded in full to the Driver.</p>
                        <p>&nbsp;</p>
                        <p>11.6&nbsp;&nbsp;&nbsp;&nbsp; If Intercity Hire attempts to collect the Vehicle from the Driver on the last day of the Subscription Term but is unable to do so due to the Driver&rsquo;s fault, Intercity Hire will allow a grace period of one day for the Driver to return the Vehicle. After this one-day grace period, the Driver shall be liable to pay the daily rate for any additional time for which the Driver uses the Vehicle. This shall be calculated using the Daily Subscription Rate included in the Subscription Booking Form (<em>e.g</em>., if a Driver returns a Vehicle two</p>
                        <ul>
                          <li>days late, they will be charged two day&rsquo;s Subscription Fees based on the Daily Subscription Rate).</li>
                        </ul>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>11.7&nbsp;&nbsp;&nbsp;&nbsp; Subject to any notice required by law, Intercity Hire may terminate this Agreement if any of the following occur:</p>
                        <p>&nbsp;</p>
                        <p>11.7.1&nbsp;&nbsp;&nbsp;&nbsp; the Driver fails to pay any Subscription Fee, Fine, Toll, charges or any other amount due under this Agreement on the date when it becomes due;</p>
                        <p>&nbsp;</p>
                        <p>11.7.2&nbsp;&nbsp;&nbsp;&nbsp; the Driver breaches their obligations under this Agreement including, without limitation, conditions 2, 5 or 7 of this Agreement</p>
                        <p>&nbsp;</p>
                        <p>11.7.3&nbsp;&nbsp;&nbsp;&nbsp; the Driver is no longer in possession or control of the Vehicle;</p>
                        <p>&nbsp;</p>
                        <p>11.7.4&nbsp;&nbsp;&nbsp;&nbsp; the Driver becomes bankrupt or enters into any arrangement or composition with creditors;</p>
                        <p>&nbsp;</p>
                        <p>11.7.5&nbsp;&nbsp;&nbsp;&nbsp; the Driver receives endorsements or convictions on their driving licence which are unacceptable to Intercity Hire;</p>
                        <p>&nbsp;</p>
                        <p>11.7.6&nbsp;&nbsp;&nbsp;&nbsp; the Driver&rsquo;s insurance has lapsed or become invalid;</p>
                        <p>&nbsp;</p>
                        <p>11.7.7&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire&rsquo;s relationship with its vehicle supplier is suspended or terminated, or the Vehicle is unavailable from Intercity Hire&rsquo;s vehicle supplier, or the Vehicle is excluded from the Intercity Hire website. If terminated for this reason, this Agreement will terminate, the Vehicle must be returned to Intercity Hire and the Driver will be refunded in full for any payments made under this Agreement for use of the Vehicle not received; or</p>
                        <p>&nbsp;</p>
                        <p>11.7.8&nbsp;&nbsp;&nbsp;&nbsp; any information supplied by the Driver to Intercity Hire in connection with this Agreement is inaccurate or misleading in any material way.</p>
                        <p>&nbsp;</p>
                        <p>11.8&nbsp;&nbsp;&nbsp;&nbsp; Upon early termination of this Agreement, the Driver shall immediately be liable for (a) any arrears of Subscription Fees and other amounts due and payable at the time (including for the avoidance of doubt any Fines, Tolls, and other charges) and (b) an early termination fee as calculated under condition 11.4</p>
                        <p>above and</p>
                        <p>(c) any other amounts which may become due and payable under this Subscription Agreement.</p>
                        <p>&nbsp;</p>
                        <p>11.9&nbsp;&nbsp;&nbsp;&nbsp; Upon expiry of the Subscription Term or on any early termination of this Subscription Agreement for any reason, the Driver's right to use of the Vehicle will cease and the Vehicle must immediately be returned to Intercity Hire or such location as Intercity Hire directs. Intercity Hire may repossess the Vehicle if the Driver fails to return the Vehicle and the Driver shall indemnify Intercity Hire in relation to any loss and damage resulting from the repossession of the Vehicle without prejudice to Intercity Hire&rsquo;s other rights and remedies under this Agreement.</p>
                        <p>&nbsp;</p>
                        <p>11.10&nbsp;&nbsp; If the Driver fails to return the Vehicle to Intercity Hire on the last day of the Subscription Term or on any earlier termination as required the Driver shall be liable for an overdue fee for each day that the Vehicle is not returned in accordance with condition 11.8 above (the Delay Period).The Driver may also be liable for additional insurance charges in relation to the Delay Period. The Driver will remain responsible for all the Driver's obligations under this Agreement for the full duration of the Delay Period.</p>
                        <p>&nbsp;</p>
                        <p>11.11&nbsp;&nbsp; Failure by the Driver to return the Vehicle to Intercity Hire promptly in accordance with condition 11.9 may result in Intercity Hire reporting the Driver to the relevant authorities. If insurance has been purchased through Intercity Hire, Intercity Hire may arrange for any insurance on the Vehicle to end (if it has not ended already) and may report that the Vehicle is no longer insured to third parties in its sole discretion.</p>
                        <p>&nbsp;</p>
                        <p>11.12&nbsp;&nbsp; On return of the Vehicle, the Driver must remove any debris, rubbish and the Driver's personal items from the Vehicle.</p>
                        <p>&nbsp;</p>
                        <p>11.13&nbsp;&nbsp; The Driver must then hand the keys to the Vehicle to Intercity Hire, or a third party on behalf of Intercity Hire, (or returned in a manner otherwise agreed with Intercity Hire).</p>
                        <p>&nbsp;</p>
                        <p>11.14&nbsp;&nbsp; Intercity Hire may amend the Subscription End Date to such earlier time as may be reasonably required in connection with its business purposes, provided that the Driver shall be entitled to a pro rata refund of the Subscription Fees (at the daily subscription rate identified in the Subscription Booking Form) for the number of days by which the Subscription Term has been shortened.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <ol start="12">
                          <li><strong>Continuous Payment Authority</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>12.1&nbsp;&nbsp;&nbsp;&nbsp; The Driver hereby authorises Intercity Hire to collect any amounts for which the Driver is liable under this Agreement by charging a credit or debit card, the details of which have been provided to Intercity Hire by the Driver. Making regular collections from the Driver's credit or debit card is called a continuous payment authority. The Driver shall only provide Intercity Hire with debit or credit card details for an account which belongs to the Driver and from which the Driver is authorised to make payments. The Driver acknowledges and agrees that Intercity Hire uses continuous payment authority to collect Subscription Fees on the Payment Dates and all other sums which are due and payable by the Driver under this Agreement including all the charges and fees set out in the Subscription Booking Form.</p>
                        <p>&nbsp;</p>
                        <p>12.2&nbsp;&nbsp;&nbsp;&nbsp; The Driver may amend their continuous payment authority by emailing <a href="mailto:finance@pcocar.com">finance@pcocar.com.</a> Drivers have the ability to change their due date for payment by emailing <a href="mailto:finance@pcocar.com">finance@pcocar.com</a> to discuss first.</p>
                        <p>&nbsp;</p>
                        <p>12.3&nbsp;&nbsp;&nbsp;&nbsp; In the event a payment fails for a Driver due to insufficient funds being available, Intercity Hire will attempt to take the payment again on the following day. If such payment fails, Intercity Hire will continue to take payment again on the following day whilst continuing to contact the Driver in order to establish the reason for the failure. If the Driver is having difficulties in making the payment, Intercity Hire will then suspend the payment attempts and discuss alternative repayment arrangements with the Driver, in line with applicable affordability checks.</p>
                        <p>&nbsp;</p>
                        <p>12.4&nbsp;&nbsp;&nbsp;&nbsp; If Intercity Hire make reasonable attempts to contact the Driver following the payment failures, but these attempts are still unsuccessful, Intercity Hire will retry the payment on the 6th day following the date on which the first payment fails (the Subsequent Payment Attempt). If the Subsequent Payment Attempt fails, Intercity Hire will suspend payment attempts again whilst continuing to try and make contact with the Customer to understand their position and the circumstances surrounding the non-payment. If all efforts to contact the Driver fail, or the Driver refuses to engage with Intercity Hire, Intercity Hire will retry the payment one more time, and then 3 times per week for 3 months.</p>
                        <p>&nbsp;</p>
                        <p>12.5&nbsp;&nbsp;&nbsp;&nbsp; Part payment may be sought on a monthly basis from a Driver if a payment plan has been established in line with affordability, our arrears and debt collection procedures. Part payments are subject to a minimum of &pound;50. Payment may be sought on a monthly basis from a Driver if a payment plan has been established in line with our arrears and debt collection procedures. Part payments are subject to a minimum of &pound;50.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <ol start="13">
                          <li><strong>Governing Law</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>13.1&nbsp;&nbsp;&nbsp;&nbsp; These Terms of Subscription and any Agreement shall be governed by and construed in accordance with English law. Both Intercity Hire and the Driver agree to submit to the non-exclusive jurisdiction of the English courts, which means (i) if the Driver lives in the EU they may enforce their rights in connection with this Agreement in the EU member state in which they live; or (ii) if the Driver lives in any other jurisdiction which gives them mandatory consumer protection rights, they may enforce their rights in connection with this Subscription Agreement in accordance with such rights.</p>
                        <p>&nbsp;</p>
                        <ol start="14">
                          <li><strong>General</strong></li>
                        </ol>
                        <p>&nbsp;</p>
                        <p>14.1&nbsp;&nbsp;&nbsp;&nbsp; Any notice or other communication given to the Driver in connection with this Agreement may be sent by email to the latest address maintained on the Intercity Hire website, <a href="http://www.pcocar.com/">www.pcocar.com </a>or by post to such address as Intercity Hire holds for the Driver. Any notice or other communication given to Intercity Hire in connection with this Agreement should be sent by email to <a href="mailto:support@pcocar.com">support@pcocar.com</a> or by post to Intercity Hire Limited, 8 Orsman Road, London N1 5QJ, UK.</p>
                        <p>&nbsp;</p>
                        <p>14.2&nbsp;&nbsp;&nbsp;&nbsp; Except as otherwise agreed in this Agreement and required by law, each party agrees with the other to keep secret and not share (except with its employees, contracts and advisers (where relevant)) any confidential information it receives from the other party through this Agreement.</p>
                        <p>&nbsp;</p>
                        <p>14.3&nbsp;&nbsp;&nbsp;&nbsp; If any part of these terms is disallowed or found to be ineffective by a court or regulator, the other provisions shall continue to apply.</p>
                        <p>&nbsp;</p>
                        <p>14.4&nbsp;&nbsp;&nbsp;&nbsp; If either party does not take action against the other party, the party who chose not to take action is still entitled to use its rights and remedies in any other situation when this Agreement is breached.</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>14.5&nbsp;&nbsp;&nbsp;&nbsp; This Agreement is for the benefit of the Driver and Intercity Hire where stated in this Agreement, and no term of this Agreement will be enforceable by any other person that is not a party to it including any enforcement through the Contracts (Rights of Third Parties) Act 1999.</p>
                        <p>&nbsp;</p>
                        <p>14.6&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire expressly reserves the right to sub-contract any of its obligations under this Agreement to a third party, provided that Intercity Hire shall remain primarily responsible for, and liable to the Driver for, the performance of such obligations.</p>
                        <p>&nbsp;</p>
                        <p>14.7&nbsp;&nbsp;&nbsp;&nbsp; We can transfer our rights under this Agreement. We can also in certain circumstances transfer our obligations. You may not transfer your rights or obligations under this Agreement.</p>
                        <p>&nbsp;</p>
                        <p>14.8&nbsp;&nbsp;&nbsp;&nbsp; The Driver agrees this Agreement may be entered into and signed by way of electronic signature or advanced electronic signature as defined in the Electronic Communications Act 2000 or articles 3(10) or 3(11) of Regulation EU N09012014. Any signature made by the Driver in a way which complies with the Electronic Communications Act 2000 will be effective and binding on the Driver.</p>
                        <p>&nbsp;</p>
                        <p>14.9&nbsp;&nbsp;&nbsp;&nbsp; By electronically signing the Subscription Agreement the Driver agrees that:</p>
                        <p>&nbsp;</p>
                        <p>14.10&nbsp;&nbsp; The Driver is entering this agreement wholly or predominantly for the purposes of a business carried on by themselves or intended to be carried on by themselves.</p>
                        <p>&nbsp;</p>
                        <p>14.11&nbsp;&nbsp; The Driver will not have the benefit of the protection and remedies that would be available under the Financial Services and Markets Act 2000 or under the Consumer Credit Act 1974 if the agreement were a regulated agreement under those Acts. The Driver is aware that if in any doubts as to the consequences of the agreement not being regulated by the Financial Services and Markets Act 2000 or the Consumer Credit Act 1974, then they should seek independent legal advice.</p>
                        <p>&nbsp;</p>
                        
                      </div>                    
                    </div>

<div align="center">
<input id="chk" type="checkbox" > I have read and agree to term & conditions<br>
 <input disabled="disabled" type="submit" class="btn btn-primary" id="submit"  value="Make Payment"  />
</div>

                  </form>
                  </div>
                </div>
                
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('layouts.footer')
  </body>
  @endsection
  @section('scripts')
  @endsection
  <script>
  
  function showSignup(){
  $('.signup').show();
  $('.login').hide();
  }
  function showLogin(){
  $('.signup').hide();
  $('.login').show();
  }
  
  $(document).ready(function() {
  // $('#btn_upload_info').click(function(event){
  $( "#btn_upload_info" ).click(function() {
  event.preventDefault();
  var formData = new FormData($('#upload_information')[0]);
  formData.append('#file_dvla', $('input[type=file]')[0].file_dvla[0]);
  //alert(formData);
  // formData.append('#file_cnic', $('input[type=file]')[0].file_cnic[0]);
  //  $('#f_name').val(    $('input[type=file]').val()     );
  $.ajax({
  // url:"http://ip.jsontest.com/",
  url: '{{ url("booking/upload_information") }}',
  method:"POST",
  data:formData,
  //dataType:'JSON',
  contentType: false,
  cache: false,
  processData: false,
  success:function(data)
  {
  alert(data);
  },
  error: function(data)
  {
  //console.log(data);
  alert('error');
  }
  })
  });
  var navListItems = $('ul.setup-panel li a'),
  allWells = $('.setup-content');
  allWells.hide();
  navListItems.click(function(e)
  {
  e.preventDefault();
  var $target = $($(this).attr('href')),
  $item = $(this).closest('li');
  if (!$item.hasClass('disabled')) {
  navListItems.closest('li').removeClass('active');
  $item.addClass('active');
  allWells.hide();
  $target.show();
  }
  });
  $('ul.setup-panel li.active a').trigger('click');
  // DEMO ONLY //
  $('#activate-step-2').on('click', function(e) {
  $('ul.setup-panel li:eq(1)').removeClass('disabled');
  // $('ul.setup-panel li a[href="#step-1').addClass('done');
  $('ul.setup-panel li a[href="#step-2"]').trigger('click');
  $(this).remove();
  })
  //list-group-item-heading
  //   <span class="glyphicon glyphicon-ok"></span>
  $('#activate-step-3').on('click', function(e) {
  $('ul.setup-panel li:eq(2)').removeClass('disabled');
  $('ul.setup-panel li a[href="#step-3"]').trigger('click');
  $(this).remove();
  }) ;
  @if(app('request')->input('step')==3)
  $('ul.setup-panel li:eq(3)').removeClass('disabled');
  @endif
  });
  </script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  <script type="text/javascript">
  $(function() {
  var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
  var $form         = $(".require-validation"),
  inputSelector = ['input[type=email]', 'input[type=password]',
  'input[type=text]', 'input[type=file]',
  'textarea'].join(', '),
  $inputs       = $form.find('.required').find(inputSelector),
  $errorMessage = $form.find('div.error'),
  valid         = true;
  $errorMessage.addClass('hide');
  $('.has-error').removeClass('has-error');
  $inputs.each(function(i, el) {
  var $input = $(el);
  if ($input.val() === '') {
  $input.parent().addClass('has-error');
  $errorMessage.removeClass('hide');
  e.preventDefault();
  }
  });
  if (!$form.data('cc-on-file')) {
  e.preventDefault();
  Stripe.setPublishableKey($form.data('stripe-publishable-key'));
  Stripe.createToken({
  number: $('.card-number').val(),
  cvc: $('.card-cvc').val(),
  exp_month: $('.card-expiry-month').val(),
  exp_year: $('.card-expiry-year').val()
  }, stripeResponseHandler);
  }
  });
  function stripeResponseHandler(status, response) {
  if (response.error) {
  $('.error')
  .removeClass('hide')
  .find('.alert')
  .text(response.error.message);
  } else {
  /* token contains id, last4, and card type */
  var token = response['id'];
  $form.find('input[type=text]').empty();
  $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
  $form.get(0).submit();
  }
  }
  });
  </script>