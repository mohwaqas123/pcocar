@extends('layouts.app')

@section('styles')

@endsection

@section('content')

@include('layouts.header') 

<!------ <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
Include the above in your HEAD tag ---------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

<div class="row" id="contatti">
<div class="container mt-5" >
<b  >London Office</b>
    <div class="row">
      <div class="col-md-6 maps" >
          <div class="mapouter">
            <div class="gmap_canvas">
              
              <iframe width="600" height="250" id="gmap_canvas" src="https://maps.google.com/maps?q=373%20-%20High%20Road%20ILFORD%20%2C%20IG1%201TF&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
              <b class="float-left" >Manchester Office</b>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1885.6321124692004!2d-2.2404646845999796!3d53.48558398000838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bb1b8850cd28d%3A0xdeee2b3fa03bec95!2sShudehill%20Interchange!5e1!3m2!1sen!2s!4v1615887493662!5m2!1sen!2s" width="600" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>




            <a href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/"></a>
          </div>
          <style>
            .mapouter{position:relative;text-align:right;height:500px;width:600px;}
            .gmap_canvas {overflow:hidden;background:none!important;height:500px;width:500px;}
        </style>
        </div>


<div>

<p class="mt-5 mb-1">Booking Service / Support : 9:00am - 6:00pm [Monday - Sunday]</p>
<p class="mb-1">Emergency : 24/7 Monday - Sunday</p>
<p>Collection Time : 9:00AM - 6:00PM [Monday - Friday]</p>
</div>


      </div>
     
      <div class="col-md-6">
         @if(session()->has('message'))
    <div class="alert alert-success " id="myElem">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
       <span aria-hidden="true">&times;</span>
      </button>

        {{ session()->get('message') }}
    </div>
@endif
        <h2 class="text-uppercase mt-3 font-weight-bold">CONTACT US</h2>
        <form method="post" enctype="multipart/form-data" action="{{url('contactus')}}">
          {{ csrf_field() }}  
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <input type="text" pattern="[A-Za-z\s]+" name="name" class="form-control mt-2" maxlength="30" placeholder="Enter Name" required>
              </div>
            </div>
          
            <div class="col-lg-6">
              <div class="form-group">
                <input type="email" name="email"  class="form-control mt-2" maxlength="30" placeholder="Enter Email" required>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <input type="" class="form-control mt-2"  pattern="^[0-9-+\s()]*$"  name="phone" maxlength="20" placeholder="Enter Phone" required>
              </div>
            </div>
            <div class="col-12">
              <div class="form-group">
                <textarea class="form-control" maxlength="50" id="exampleFormControlTextarea1" placeholder="Detail" rows="3" ></textarea>
              </div>
            </div>
            <div class="col-12">
             
             <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            
          </div>
        </form>
        <div class="text-white"> 
        <i class="fas fa-phone mt-3"></i> <a href="#">BOOK BY LANDLINE : 02081333138</a><br>

        <i class="fas fa-phone mt-3"></i> <a href="#">BOOK BY MOBILE : 07812214589</a><br>
        <i class="fas fa-phone mt-3"></i> <a href="#">BOOK BY WHATSAPP : 07847253191 </a><br>

        
        <i class="fa fa-envelope mt-3"></i> <a href="">info@pcocar.com</a><br>
 
        <div class="my-4">
        <a href=""><i class="fab fa-facebook fa-3x pr-4"></i></a>
        <a href=""><i class="fab fa-linkedin fa-3x"></i></a>
        </div>
        </div>
      </div>

    </div>



</div>
</div>

 




 </body>

@include('layouts.footer')

    

    @endsection

    @section('scripts')

    @endsection



 <style>
 	.gmap_canvas{width:500px;}
     </style>