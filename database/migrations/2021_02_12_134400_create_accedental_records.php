<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccedentalRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accedental_records', function (Blueprint $table) {
            $table->id();
             $table->integer('vehicle_id');
             $table->integer('booking_id'); 
             $table->integer('user_id');
            $table->decimal('amount',9,2);
             $table->string('registeration');
             $table->string('parts')->nullable();
             $table->text('video')->nullable();
             $table->string('detail');
             $table->boolean('int_status');
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accedental_records');
    }
}
