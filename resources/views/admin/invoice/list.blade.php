@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">	
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif

						<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%" >
							 	<thead>
                                <tr>
                                <th>id</th>
                                 <th>Date</th>
                                 <th>Invoice No</th>
                                <th>Vehicle</th>
                                <th>Supplier</th>
                                <th>VAT</th>
                                <th>Price</th>
                                <th>Details</th>
                               
                                <th style="width:90px" >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@foreach($invoice as $invoice_var)
                                <tr>
                                    <td>{{ $invoice_var->id }}</td>
                                    <td>{{$invoice_var->date}}</td>
                                    <td>{{$invoice_var->invoice_number}}</td>
                                    <td> 
                                    {{ $invoice_var->vehicle->licence_plate_number }}</td>
                                    <td>{{@$invoice_var->supplier->name}}</td>
                                    <td>{{@$invoice_var->vat_price}}</td>
                                    <td>{{@$invoice_var->price}}</td>
                                    <td>{{$invoice_var->detail}}</td>
                                    
                                    <td>   
                                    <a  href="{{ route('admininvoiceshow', $invoice_var->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"
                                    data-toggle="tooltip" data-placement="Top" data-original-title="View" ><i class="fa fa-eye"></i>
                                    </a></td>
                                
                                </tr>

                                @endforeach
                                </tbody>
                                </tbody>
                               
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>




@endsection
@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection