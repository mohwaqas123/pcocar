<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Partnersignup extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name; 
   public $password;
   public $email;
   
     

    public function __construct($name,$password,$email )
    {

         
        $this->name = $name; 
        $this->email = $email; 
        $this->password = $password;
         
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail.partnersignup', ['name' => $this->name ,'email'=>$this->email, 'password'=> $this->password ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
