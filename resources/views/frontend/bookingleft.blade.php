<div class="card">
        <div class="card-header">
          <h5 class="card-title">{{@$vehicles->car_make->name}} {{@$vehicles->car_model->name}} {{@$vehicles->year}}</h5>
        </div>
        <img style="height:255px" src="{{ asset('storage/app/'.@$vehicles->image) }}" class="img-responsive" alt="" />
        <div>
          <table class="table">
            <tr>
              <td>Price (Per Week) </td>
              <td>£{{ $price }}</td>
            </tr>
            <tr>
              <td>Duration</td>
              <td>
@if($months==1)
   4 weeks
@else
  {{$months}} weeks
@endif
 </td>
            </tr>
            
             @if($insurance==0)
            <tr>
              <td>Insurance Cost {{$insurance}}  </td>
              <td>£{{env('INSURANCE_CODE')}}</td>
            </tr>
            
            @endif
             @if($deposit==0)

            <tr>
              <td>Deposit Security</td>
              <td>£{{env('DEPOSIT_SECURITY')}}</td>
            </tr>
            @endif
            
              <tr>
              @php
              $price_total = $price + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');  
              @endphp
              <td align="center" colspan="2" class="price bold" >
                Total £{{ $price_final_total_book }}
                @if($discount)
                
               
                @endif
              </td>
            </tr>
          </table>
          
          <a href="{{url('/')}}" onclick="return confirm('You want to remove your selected booking?')"><button type="button" class="btn btn-sm btn-danger btn-icon mb-2" >
           <span class="btn-inner--text">Remove</span><span class="btn-inner--icon">
          <i data-feather="x-circle"></i>
        </span>
        </button></a>
      </div>
    </div>