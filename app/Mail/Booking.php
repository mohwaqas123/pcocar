<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Booking extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name; 
   public $title;
   public $booking;
   public $vehicle;
   public $months;
   public $discount;
   public $payment_mode;  
   public $price_per_week;
   public $price_total;
   public $booking_start_date;
   public $insurance;

    public function __construct($payment_mode,$name,$title,$booking, $vehicle, $months, $discount,$price_per_week,$price_total,$booking_start_date,$insurance)
    {

         
        $this->name = $name; 
        $this->title = $title;
        $this->booking = $booking;
        $this->vehicle = $vehicle;
        $this->months = $months;
        $this->discount = $discount;
         $this->price_per_week = $price_per_week;
          $this->price_total = $price_total;
        $this->payment_mode = $payment_mode;
        $this->booking_start_date = $booking_start_date;
        $this->insurance = $insurance;

        
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail.booking', [
            'payment_mode'=>$this->payment_mode, 
            'booking' => $this->booking, 
            'name' => $this->name , 
            'vehicle'=> $this->vehicle , 
            'months'=>$this->months, 
            'discount'=>$this->discount,
            'price_per_week'=>$this->price_per_week,
            'price_total'=>$this->price_total,
            'booking_start_date'=>$this->booking_start_date,
            'insurance'=>$this->insurance,

          ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
