@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                       <h4 class="breadcrumb-item active"   > 
                        
                    </h4>
                     
                        <div class="card-box">
                          <h4> 
                        
                    </h4>
                            @if($errors->any())
                                {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif

 <form role="form"  action="{{ route('chargeamountStripe', $customer->id ) }}"  method="post" onsubmit='disableButton()'>
                                {{csrf_field()}}
                                <input type="hidden" value="{{$customer->id}}" name="customer" />
                                 <input type="hidden" value="{{$customer->stripe_customer_id}}" name="stripe_customer_id" />
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Charge <span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror"  name="amount" id="amount" placeholder="Enter Charge Amount"  required>
 
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="name">Booking Id <span class="text-danger required">*</span></label>
                                            <select id="" name="booking_id" class="form-control select2" required>
                 @foreach($booking as $customer_booking)
               <option value="{{$customer_booking->id}}">
                {{$customer_booking->vehicle->licence_plate_number}}
                   {{$customer_booking->vehicle->name}}
                 {{$customer_booking->vehicle->car_make->name}}
                 {{$customer_booking->vehicle->year}}
                    </option>
                  @endforeach>
                  </select>
                                        </div>
                                        </div>
                                    <div class="row">

                                        <div class="form-group col-md-6">
                                            <label for="name">Detail <span class="text-danger required">*</span></label>
                                            <textarea  type="text" class="form-control @error('name') is-invalid @enderror" name="detail" id="detail" placeholder="Enter Detail"  required></textarea>
                                            
                                             
                                        </div>
                                            
                                    </div>

       
 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input
                                        action="action"
                                        onclick="window.history.go(-1); return false;"
                                        type="submit"
                                        value="Back"
                                        class="btn btn-info btn-flat"
                                        />

                                        @if($customer->stripe_customer_id=='') 
                                        <br>
                                            <div class="alert alert-danger" role="alert"><p>This customer account is not found in Stripe Account</p>
                                            <p>Please make sure this customer is already registered in stripe</p></div>
                                        @else
                                            <button type="submit" id="btn" class="btn btn-info btn-flat">Charge</button> 
                                        @endif
                                           
                                          </div>
                                    </div>
                                </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
     function disableButton() {
        
        var btn = document.getElementById('btn');
        btn.disabled = true;
        btn.innerText = 'Posting...'
    }
</script>
@endsection
