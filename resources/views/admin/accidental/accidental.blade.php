@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page">
  <div class="content">
    <form method="POST" enctype="multipart/form-data" action="{{ route('accidentalStore') }}" >
      <div class="container-fluid">
        
        <div class="row">
          <div class="col-md-8">
      <div class="row">
          <div class="col-12">
            
            <div class="card-box">
              <div class="card-box">
                
                @csrf
                <div class="form-row col-md-12 mb-3">
                  <label for="make" class="col-form-label">Title<span class="text-danger">*</span></label>
                  <input type="text" name="title" class="form-control" placeholder="Please enter title of Damage Record">
                </div>
                
                <div class="form-row col-md-12 mb-3">
                  <label for="make" class="col-form-label">Vehicle<span class="text-danger">*</span></label>
                  <select id="licence_plate_number"  name="licence_plate_number" class="form-control select2" required>
                    @foreach($vehicle as $us)
                    <option value="{{$us->id}}">{{$us->licence_plate_number}},
                      {{$us->car_model->name}},
                      {{$us->car_make->name}},
                    {{$us->year}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-row col-md-12 mb-3" id="customer">
                  <label for="make" class="col-form-label">Customer</label>
                  <select    name="name" placeholder="Customer Drop Down"
                    class="form-control select2" >
                    @foreach($user as $us)
                    <option value="{{$us->id}}">{{$us->name}}
                    </option>
                    @endforeach
                  </select>
                </div>
                <div class="form-row col-md-12 mb-3" id="customer">
                  <label for="make" class="col-form-label">Staff</label>
                  <select    name="staff" placeholder="Staff Drop Down"
                    class="form-control select2" id="mySelect"  >
                    <option value="0">None</option>
                    @foreach($staff as $staff_us)
                    <option value="{{$staff_us->id}}">{{$staff_us->name}}
                    </option>
                    @endforeach
                  </select>
                </div>



                <div class="form-row col-md-12 mb-3" id="customer">
                  <label for="make" id="mySelect" class="col-form-label">Supplier</label>
                  <select    name="purchase" placeholder="Purchase Drop Down"
                    class="form-control select2"   >
                    <option value="0">None</option>
                    @foreach($purchase as $purchase_us)
                    <option value="{{$purchase_us->id}}">{{$purchase_us->name}}
                    </option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group col-md-12 mb-3">
                  <label for="make" class="col-form-label">Vehicle Parts<span class="text-danger">*</span></label>
                  <div class="row">
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Bumper">Bumper
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Tyer">Tyer
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Decklid">Decklid
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Front clip">Front clip
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Radiator">Radiator
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Brakes">Brakes
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Front Mirror">Mirror
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Battery">Battery
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Front Axle">Axle
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="roof box ">Roof Box
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Radio">Radio
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]"value="Mirror">Mirror
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Head Light">Head Light
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Handle">Handle
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Door">Door
                    </div>
                    <div class="col-md-3">
                      <input type="checkbox" name="chk[]" value="Turn Signal">Turn Signal
                    </div>
                  </div>
                </div>
                
                <div class="form-row col-md-12 mb-3">
                  <label for="make" class="col-form-label">Price<span class="text-danger">*</span></label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror"  name="price"   placeholder="Enter the Amount"  required>
                </div>
                
                <div class="form-row col-md-12 mb-3">
                  <label for="make" class="col-form-label">Detail<span class="text-danger">*</span></label>
                  
                  <textarea class="form-control summernote" hieght="400" width="100%" name="detail" id="description" title="error message" maxlength="700" required="required"></textarea>
                  
                  <!--    <textarea name="detail" style="height: 300px" rows="15" cols="40" class="form-control tinymce-editor" required="required"></textarea> -->
                </div>
                
                
                <div class="form-row mb-3">
                  <p class="col-form-label">Upload  Images
                    <br>
                    <input type="file" id="images" name="images[]" multiple>
                  </p>
                  
                  
                  @if ($errors->has('images'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('images') }}</strong>
                  </span>
                  @endif
                </div>
                
                    
                  </div>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">

          </div>
          <div class="col-md-4">
              <div class="card">
                <div class="card-body">
                    <!-- <div class="d-flex flex-column align-items-center text-center"> 
                      <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="Admin" class="rounded-circle" width="150">
                      <div class="mt-3"><h4>John Doe</h4>
                        <p class="text-secondary mb-1">Full Stack Developer</p><p class="text-muted font-size-sm">Bay Area, San Francisco, CA</p>  
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
        </div>


  
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
<style>
#customFile .custom-file-input:lang(en)::after {
content: "Select file...";
}
#customFile .custom-file-input:lang(en)::before {
content: "Click me";
}
/*when a value is selected, this class removes the content */
.custom-file-input.selected:lang(en)::after {
content: "" !important;
}
.custom-file {
overflow: hidden;
}
.custom-file-input {
white-space: nowrap;
}
.select2-container .select2-selection--single{
height:34px !important;
}
.select2-container--default .select2-selection--single{
border: 1px solid #ccc !important;
border-radius: 0px !important;
}
.bg-dev{border: 1px solid #000;}
.header-title{background: #eee;
padding: 5px;}
</style>
@endsection
@section('scripts')
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
var fileName = $(this).val().split("\\").pop();
$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});


$('#licence_plate_number').change(function(event) {
    var url_call = "{!! url('/') !!}/admin/get_customers";
    var vehicle_id = this.value;
    $.ajax({
        type : 'get',
        url  : url_call,
        //  data: {},
        data: { vehicle_id: vehicle_id},
        contentType: 'application/json; charset=utf-8',
        success :  function(data){
//alert(data);
console.log(data);
        $("#customer").empty();
         $("#customer").append(data);
 
     }
    });//ajax
});


function myFunction() {
alert("Hello! I am an alert box!!");
}

















</script>
@endsection