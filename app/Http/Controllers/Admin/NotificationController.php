<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Notification;


class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notification = Notification::get(); 
        $url = "/admin/dashboard";
        $data = [
               'notification'=> $notification,
                'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Notification List',
        ];
        return view('admin.notification.list')->with($data);


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $notification = Notification::find($request->id); 
       
        $url = "/admin/dashboard";
        $title = 'Driver List';
          $data = [
 
              'notification'=> $notification,
               'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Notification  Show',
                    ];   
             
        return  view('admin.notification.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
