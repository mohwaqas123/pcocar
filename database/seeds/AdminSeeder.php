<?php

use Illuminate\Database\Seeder;
use App\models\User;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin cybexo',
            'email' => 'admin@cybexo.com',
            'username' => 'admin',
            'phone' => '1234567890',
            'date_of_birth' => Carbon::now()->toDateTimeString(),
            'device_type' => 3,
            'device_id' => 00000000,
            'user_type' => 1,
            'password' => bcrypt('123456'),
        ]);
    }
}
