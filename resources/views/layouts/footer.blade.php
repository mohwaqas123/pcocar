

<footer class="position-relative" id="footer-main">

    <div class="footer pt-lg-7 footer-dark bg-dark">
        <!-- SVG shape -->
        <div class="shape-container shape-line shape-position-top shape-orientation-inverse">
            <svg width="2560px" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 2560 100" style="enable-background:new 0 0 2560 100;" xml:space="preserve" class=" fill-section-secondary">
            <polygon points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
    <!-- Footer -->
    <div class="container pt-4">
        <div class="row">
            <div class="col-lg-6 mb-5 mb-lg-0">
                <!-- Theme's logo -->
                <img src="{{ asset('public/theme3/assets/img/logo2.png')}}" alt="logo" style="height:60px" />
                <!-- Webpixels' mission -->
                <!--<p class="mt-4 text-sm opacity-8 pr-lg-4">PCOCAR Limited is authorised and regulated by the Financial Conduct Authority for entering into regulated hire agreements</p>-->
                <br> <br>
                <h3 style="color:white;" >For Booking</h3>
                <i class="fas fa-phone mt-3"></i> <a class="footer-links" href="tel:+4407812214589">Book By MOBILE : 02081333138</a><br>


                <i class="fas fa-phone mt-3" style="color:white;"></i> 


                <a class="footer-links" href="tel:+07847253191" >Book By WHATSAPP : 07847253191 ,07503744583,<br> &nbsp &nbsp &nbsp &nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp &nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp07503258157 </a>
                <br> 
                <i class="fas fa-phone mt-3"></i> <a class="footer-links" href="tel:+4402081333138">Book By LANDLINE : 02081333138</a><br>
                <!-- href="https://api.whatsapp.com/send?phone=+440784725319" Social -->
                
            </div>
     

            <div class="col-lg-2 col-6 col-sm-4 ml-lg-auto mb-5 mb-lg-0">
                <h6 class="heading mb-3">Account</h6>
                <ul class="list-unstyled">
                   <?php if(session()->get('staff_id') ){ ?> <li><a href="{{ route('stafflogout') }}">Staff Logout</a></li> <?php }  else {?>
                     <li><a href="#" data-toggle="modal" data-target="#modalLoginForm">Staff Login</a></li>
                 <?php } ?><!-- 
                    <li><a href="#">Settings</a></li>
                    <li><a href="#">Billing</a></li>
                    <li><a href="#">Notifications</a></li> -->
                </ul>
            </div>
            <div class="col-lg-2 col-6 col-sm-4 mb-5 mb-lg-0">
                <h6 class="heading mb-3">About</h6>
                <ul class="list-unstyled">
<!--                     <li><a href="#">Services</a></li>
                    <li><a href="#">Pricing</a></li> -->
                    <li><a href="{{url('/contactus')}}">Contact</a></li>
                    <!-- <li><a href="#">Careers</a></li> -->
                </ul>
            </div>
           <!--  <div class="col-lg-2 col-6 col-sm-4 mb-5 mb-lg-0">
                <h6 class="heading mb-3">Company</h6>
                <ul class="list-unstyled">
                    <li><a href="#">Community</a></li>
                    <li><a href="#">Help center</a></li>
                    <li><a href="#">Support</a></li> -->
                </ul>
            </div>
        </div>
        <hr class="divider divider-fade divider-dark my-4">
        <div class="row align-items-center justify-content-md-between pb-4">
            <div class="col-md-6">
                <div class="copyright text-sm font-weight-bold text-center text-md-left">
                    &copy; 2021 <a href="#" class="font-weight-bold" target="_blank">PCOCAR</a>. All rights reserved
                </div>
            </div>
            
        </div>
    </div>
</div>
</footer>
<style>
   .get_discount{font-size:12px;} 
   .error{font-size:10px; margin-bottom: 0px; text-align: left; padding-bottom: 0px}
   .discount_applied{font-size: 9px;
    color: #0bb32f;
    font-weight: bold;}
.footer-links{color:#fff;}
.staff-msg{display: none;}
.selected{color: rgba(0, 138, 255, .9)!important}
.bg-dark{background-color: #343a40;}

</style>
<!--Start of Tawk.to Script -->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5f883e9ca2eb1124c0bd38b2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>  

<!-- 
<script async data-id="66030" src="https://cdn.widgetwhats.com/script.min.js"></script>
 -->
<!--End of Tawk.to Script-->



<form class="" action="{{ route('stafflogin') }}" method="POST">
                {{ csrf_field() }}
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Staff Sign in</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

<div class="alert alert-danger staff-msg" role="alert">
  <strong>Sorry!</strong> Your email or password is not correct
</div>


      <div class="modal-body mx-3">
        <div class="md-form mb-5">
           <label data-error="wrong" data-success="right" for="defaultForm-email">User Name</label>  
          <input type="email" autocomplete="false" id="staff_email" name="staff_email" class="form-control validate">
         
        </div>

        <div class="md-form mb-4">
         <label data-error="wrong" data-success="right" for="defaultForm-pass">Password</label>
          <input type="password" autocomplete="false" id="staff_password" name="staff_password" class="form-control validate">
          
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
       
        <button class="btn btn-primary" id="staff_login">Staff Login</button>
      </div>
    </div>
  </div>
</div></form>
 