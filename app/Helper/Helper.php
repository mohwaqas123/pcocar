<?php
namespace App\Helper;
use App\models\Coupon;
use App\models\Setting;

class Helper
{


  public static function get_discount_duration_price($week,$price){
    if($week >= 11 && $week < 24 ){ 
            $disc = 5;
       }

       if($week >= 24 && $week < 48 ){  
            $disc = 10;
       }

       if($week >= 48 ){ 
            $disc = 10;
       }
        if($week < 12 ){
           $disc=0;
        }
        $discount_amount = $price * ($disc/100);
        $price_after_discount = $price - $discount_amount;
        return $price_after_discount;

  }

   public static function get_expire_date($duration)
    {
         $added_timestamp = strtotime('+'.$duration.' week', time());
         $result = date('Y-m-d', $added_timestamp);
         return $result;
    }
 
    public static function get_expire_date_from_any_date($duration,$from_date)
    {
         $added_timestamp = strtotime('+'.$duration.' week', strtotime($from_date));
         $result = date('Y-m-d', $added_timestamp);
         return $result;
    }

  public static function get_start_date($expire_date_old_booking)
    {
         $added_timestamp = strtotime('+1 day', strtotime($expire_date_old_booking));
         $result = date('Y-m-d', $added_timestamp);
         return $result;
    }

 


 public static function check_discount(){
 	$date = today()->format('Y-m-d');
 	$coupon = Coupon::where('status', 1)->where('date_to', '>=', $date)->first();
 	return $coupon;
 }


 public static  function get_discount_percent_func($week){
   if($week >= 11 && $week < 24 ){ 
            $disc = 5;
       }

       if($week >= 24 && $week < 48 ){  
            $disc = 10;
       }

       if($week >= 48 ){ 
            $disc = 10;
       }
        if($week < 12 ){
           $disc=0;
        }
    return $disc;    
}

public static function check_promotion(){
    $setting = Setting::where('id', 1)->first();
    return  $setting->promotion_type;
}



public static function get_flat_discount_amount(){
    $setting = Setting::where('id', 1)->get();
   /* $data = [
           'promotion_type'=> $setting->promotion_type,
           'flat_discount_rate'=> $setting->flat_discount_rate,

    ];*/
    return  $setting;
}




public static function get_setting(){
    $setting = Setting::where('id', 1)->get();     
    return  $setting;
}


public static function get_price_with_check_discount($price){
     $setting = Setting::where('id', 1)->get();
     if($setting[0]->flat_discount_type==1){

      $price_final = $price - ($price * $setting[0]->flat_discount_rate)/100;

          //$price_final = $price -  $setting[0]->flat_discount_rate; 
     }
     else{
          $price_final = $price;
     }
     return $price_final; 
 
}




}


?>