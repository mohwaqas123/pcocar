@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<div class="container">
  
  <div class="row">
    <div class="col-md-3 text-center">
      
      
     @include ('frontend.bookingleft') 
    <!--  <h2 class="car_detail_name">{{$vehicles->name}}</h2>
    <h1 class="car_sub_detail">{{$vehicles->car_make->name}} {{$vehicles->car_model->name}}</h1>
    <img style="width:255px" src="{{ asset('storage/app/'.$vehicles->image) }}" class="img-responsive" alt="" />
    <p>For {{$months ?? request()->after_month}} Week   </p>
    <p>For {{$miles ?? request()->mile}} Miles Limit</p>
    <p>Price <span style="font-size: 21px;">£{{ $price ?? request()->price}}</span>  </p>
    
    -->
  </div>
  <div class="col-md-9">
    
    
    <ul class="progress">
      <li class="completed">
        <span>
          <span class="order">1 </span>Step 1 <span class="inner hidden-sm">(Login)</span>
        </span>
        <div class="diagonal"></div>
      </li>
      <li>
        <span>
          <span class="order">2 </span>Step 2 <span class="inner hidden-sm">(Information)</span>
        </span>
        <div class="diagonal"></div>
      </li>
      <li>
        <span>
          <span class="order">3 </span>Step 3 <span class="inner hidden-sm">(Agreement)</span>
        </span>
      </li>
      <li>
        <span>
          <span class="order">4 </span>Step 4 <span class="inner hidden-sm">(Payment)</span>
        </span>
      </li>
    </ul>
   
    
    <div class="row setup-content" id="step-1">
      
      <div class="col-md-12 well text-center login">
        <h4 class="text-center">Login</h4>
        <div class="row d-flex justify-content-center">
          <div class="col-md-offset-3"></div>
          <div class="col-md-6">
             @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
    
    @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif
            <div class="align-items-center text-center" id="signup">
              <form class="" action="{{ route('userlogin') }}" method="POST">
                <input type="hidden" id="refreshed" value="no">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$vehicles->id}}" />
                <div class="form-group">
                  <input type="email" name="email" required="required" class="form-control" placeholder="Enter Email">
                </div>
                <div class="form-group">
                  <input type="password" required="required" name="password"  class="form-control" placeholder="Enter Password">
                </div>
                <div class="form-group">
                  <!--  <input id="activate-step-2" value="Login" type="submit" class="form-control btn-primary"  > -->
                  <input value="Login" type="submit" class="form-control btn-primary"  >
                </div>
                New User? <a href="#" onclick="showSignup()">Signup</a>
              </form>
            </div>
          </div>
          <div class="col-md-offset-3"></div>
        </div>
      </div>
      <div class="col-md-12 well text-center signup" @if ($errors->has('email')) style="display: inline"   @endif   >
        <h4 class="text-center">New Signup</h4>
        <div class="row d-flex justify-content-center">
          <div class="col-md-offset-3"></div>
          <div class="col-md-6">
            <div class="align-items-center text-center" id="signup">
              <form class="" action="{{ route('usersignup') }}" method="POST">    {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$vehicles->id}}" />
                
                <div class="form-group">
                  <input type="email" name="email" required="required" class="form-control" placeholder="Enter Email">
                  @if ($errors->has('email'))
                  <span class="error">{{ $errors->first('email') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <input name="password" type="password" required="required"  class="form-control" placeholder="Enter Password">
                  @if ($errors->has('password'))
                  <span class="error">{{ $errors->first('password') }}</span>
                  @endif
                </div>
                <div class="form-group">
                  <input type="password" required="required"  class="form-control" placeholder="Enter Confirm Password">
                </div>
                <div class="form-group">
                  <input type="text" name="name" maxlength="50" required="required"  class="form-control" placeholder="Enter Name">
                </div>
                <div class="form-group">
                  <input type="text" name="phone" maxlength="16" minlength="6" onkeypress="return isNumber(event)" required="required"  class="form-control" placeholder="Enter Phone">
                </div>
                <div class="form-group">
                  <!-- <input id="activate-step-2" value="Register" type="submit" class="form-control btn-primary"  > -->
                  <input value="Register" type="submit" class="form-control btn-primary"  >
                </div>
                Already Account? <a href="#" onclick="showLogin()">Login</a>
              </form>
            </div>
          </div>
          <div class="col-md-offset-3"></div>
        </div>
        
      </div>
      
    </div>
    
    
  </div>
  
</div>
</div>
</div>
</div>
</div>
@include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<script>
  function showSignup(){
$('.signup').show();
$('.login').hide();
}
function showLogin(){
$('.signup').hide();
$('.login').show();
}
$(function() {
$('#chk').click(function() {
if ($(this).is(':checked')) {
$('#id_of_your_button').removeAttr('disabled');
} else {
$('#id_of_your_button').attr('disabled', 'disabled');
}
});
});

$(document).ready(function() {
// $('#btn_upload_info').click(function(event){
$( "#btn_upload_info" ).click(function() {
event.preventDefault();
var formData = new FormData($('#upload_information')[0]);
formData.append('#file_dvla', $('input[type=file]')[0].file_dvla[0]);
//alert(formData);
// formData.append('#file_cnic', $('input[type=file]')[0].file_cnic[0]);
//  $('#f_name').val(    $('input[type=file]').val()     );
$.ajax({
// url:"http://ip.jsontest.com/",
url: '{{ url("booking/upload_information") }}',
method:"POST",
data:formData,
//dataType:'JSON',
contentType: false,
cache: false,
processData: false,
success:function(data)
{
alert(data);
},
error: function(data)
{
//console.log(data);
alert('error');
}
})
});
var navListItems = $('ul.setup-panel li a'),
allWells = $('.setup-content');
allWells.hide();
navListItems.click(function(e)
{
e.preventDefault();
var $target = $($(this).attr('href')),
$item = $(this).closest('li');
if (!$item.hasClass('disabled')) {
navListItems.closest('li').removeClass('active');
$item.addClass('active');
allWells.hide();
$target.show();
}
});
$('ul.setup-panel li.active a').trigger('click');
// DEMO ONLY //
$('#activate-step-2').on('click', function(e) {
$('ul.setup-panel li:eq(1)').removeClass('disabled');
// $('ul.setup-panel li a[href="#step-1').addClass('done');
$('ul.setup-panel li a[href="#step-2"]').trigger('click');
$(this).remove();
})
//list-group-item-heading
//   <span class="glyphicon glyphicon-ok"></span>
$('#activate-step-3').on('click', function(e) {
$('ul.setup-panel li:eq(2)').removeClass('disabled');
$('ul.setup-panel li a[href="#step-3"]').trigger('click');
$(this).remove();
}) ;
@if(app('request')->input('step')==3)
$('ul.setup-panel li:eq(3)').removeClass('disabled');
@endif
});
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
var $form         = $(".require-validation");
$('form.require-validation').bind('submit', function(e) {
var $form         = $(".require-validation"),
inputSelector = ['input[type=email]', 'input[type=password]',
'input[type=text]', 'input[type=file]',
'textarea'].join(', '),
$inputs       = $form.find('.required').find(inputSelector),
$errorMessage = $form.find('div.error'),
valid         = true;
$errorMessage.addClass('hide');
$('.has-error').removeClass('has-error');
$inputs.each(function(i, el) {
var $input = $(el);
if ($input.val() === '') {
$input.parent().addClass('has-error');
$errorMessage.removeClass('hide');
e.preventDefault();
}
});
if (!$form.data('cc-on-file')) {
e.preventDefault();
Stripe.setPublishableKey($form.data('stripe-publishable-key'));
Stripe.createToken({
number: $('.card-number').val(),
cvc: $('.card-cvc').val(),
exp_month: $('.card-expiry-month').val(),
exp_year: $('.card-expiry-year').val()
}, stripeResponseHandler);
}
});
function stripeResponseHandler(status, response) {
if (response.error) {
$('.error')
.removeClass('hide')
.find('.alert')
.text(response.error.message);
} else {
/* token contains id, last4, and card type */
var token = response['id'];
$form.find('input[type=text]').empty();
$form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
$form.get(0).submit();
}
}
});
</script>