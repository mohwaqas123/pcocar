<?php

namespace App\Http\Controllers;
//use Illuminate\Support\Facades\File;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Hash;
use Session;
use Stripe;
use App\models\User;
//use App\Merchant;
use App\models\PaymentHistory;
use App\models\Vehicle;
use App\models\Booking;
 //use App\BookingPaymentRelation;
//use App\History_log;
use Auth;
//use App\Packageoptions;
use Mail;
 

 
  
class StripePaymentController extends Controller
{

     public function __construct()
    {
         //$this->middleware('auth');
    }
 
 

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe(Request $request)
    {
        $package = Packages::where('id', $request->get('p_id') )->first(); 
         $data = [
            'price' => $request->price, 
            'customer_name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'package' => $package,
        ];
        return view('stripe')->with($data);
    }


/*public function stripe_getting_history(Request $request)
    {
          $stripe = new \Stripe\StripeClient(
          'sk_test_QMX38t3rVMu1SxAaM1FS4OjJ00wLlUhQd8' 
          );
        return  $stripe->issuing->transactions->retrieve(
          'ch_1GuEteLP8fXawno0l2m3N7s4',
          []
          );
    }*/

  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    { 
        
        $booking_detail = Booking::where('id', $request->session()->get('booking_id') )->first();
        //Global Parameters



        $product_id = env('PRODUCT_ID') ;//"prod_IT3Wuj24n07SH1";  live                         //"prod_IO76QwwM1ZkCI0";   test
        $currency = 'GBP';  
       
        $nickname = $booking_detail->vehicle->licence_plate_number.'-'.$booking_detail->id;
        if($request->session()->get('insurance')==1){
           $amount = $booking_detail->first_payment* 100;// $request->amount - env('INSURANCE_CODE') ;
          $subscription_amount_charge_every_week = $booking_detail->price  * 100;
        }
        else{
           $amount =$booking_detail->first_payment* 100; //$request->amount ;
          $subscription_amount_charge_every_week = ($booking_detail->price + env('INSURANCE_CODE') ) * 100;
        }
        
      //dd($subscription_amount_charge_every_week);  
        $booking_duration =  $booking_detail->duration;
  
       \Stripe\Stripe::setApiKey(env('STRIPE_SECRET') );
      //  \Stripe\Stripe::setApiKey('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');
        
      // dd($request->stripeToken);


 //=============================================================  + env('INSURANCE_CODE')
 //===================== Reoccuring Payment Process ============
 //=============================================================
      //  if($users_strip_account_exist==1){ dd( $users_strip_account_exist);
               

            //Step1: Create Price for specific Product
               $price = Stripe\Price::create([
                'product' => $product_id,
                'unit_amount' => $subscription_amount_charge_every_week,
                'currency' => $currency,
                'nickname'=>$nickname,
                'recurring' => [
                  'interval' => 'week',
                ],
              ]);
$price_id = $price->id;//'price_1HnIDqLP8fXawno07A0HZ6E0';
 
               //Step2: Create Customer for Reoccuring billing
              $customer_id = User::where('id', session()->get('user_id') )->first(); 
              if($customer_id->stripe_customer_id){  
                $stripe_customer_id = $customer_id->stripe_customer_id;
              }
              else{   
               $create_customer = Stripe\Customer::create([
                    'description' => 'PCOCAR Customer',
                    'email' => $request->customer_email,
                    'name' => $request->customer_name,
                    "source" => $request->stripeToken,
                     // "source" => $request->stripeToken,

                ]);
               $stripe_customer_id =  $create_customer->id;
               
 
               $customerData = [  'stripe_customer_id' => $stripe_customer_id , ]; 
               $customer_id->update($customerData);
             }
 

$return = Stripe\Charge::create ([
                "amount" => $amount,
                "currency" =>  $currency, 
                "description" => "PCOCAR Booking Vehicle ".$nickname,
                'customer' =>  $stripe_customer_id,
        ]);




if($return->status == 'succeeded'){ 


                //Step4: Assign Subsribed Plan
                $startDateTime = $booking_detail->booking_start_date; 
                $startDate =strtotime($startDateTime);// time(); 
                
                $timespn =  strtotime('+1 week', $startDate) ;
                
                $booking_duration = "+".  $booking_duration ." week";
                $timespn_cancel =  strtotime( $booking_duration, $startDate) ;
                              
            // $stripe->subscriptionSchedules->create([
              $subscription = Stripe\SubscriptionSchedule::create([
                    'customer' =>    $stripe_customer_id,
                    'start_date' =>  $timespn,
                    'end_behavior' => 'release',
                    'phases' => [
                      [
                          'end_date'=>$timespn_cancel,
                          'items' => [
                                                 //   ['price' => $price_id ],
                                                    ['price_data'=>[
                                                        'currency'=>$currency,
                                                        'product'=>$product_id,
                                                       //'unit_amount'=>$subscription_amount_charge_every_week,
                                                        'unit_amount_decimal' =>$subscription_amount_charge_every_week,
                                                         'recurring'=> ['interval'=>'week' , 'interval_count'=>1 ]     ]  ],  
                                                  ],   

                       // 'iterations' => 12,
                      ],
                    ],
                  ]);



        $transaction_id = $return->id;
        $date = $booking_detail->booking_start_date;//date('Y-m-d');
        $package_days =30;// '+ '.$request->package_days. ' days'; 
        $expire_date=date('Y-m-d', strtotime($date.  $package_days));


$date_time = strtotime($date);
$added_months = "+".$request->expire_month_after." month";
$final_date = date("Y-m-d", strtotime($added_months, $date_time));



        
   

      $name =  $request->customer_name;
      $title = "Booking Status"; 
      $email_to =  'payments@pcocar.com';//$user->email;//'waqas.ger@gmail.com';
        $booking = Booking::where('id', $request->session()->get('booking_id'))->with('vehicle')->with('user')->first();
        $vehicle = Vehicle::where('id', $booking->vehicle_id)->with('car_make')->with('car_model')->first();
        $ses_values = $request->session(); 
         $price_per_week = $request->session()->get('price');
        $price_total = $request->session()->get('price_final_total_book');   
        $booking_start_date =  $request->session()->get('booking_start_date');         
 


    $booking->payment_status =1; //if payment success update payment status
    $booking->save(); 
    
    if($booking->insurance_type == 1)
    {

      $amount = $request->amount / 100 ;
      $amount2 = $amount - 70 ;
        
    }
    else
    {
      $amount2 = $request->amount / 100 ;
    }


      
     $data = [
            'user_id' => $request->session()->get('user_id'),
            'amount' => $amount2 ,
            'transaction_id' =>$return->id,
            'expire_date' => $date,
            'month_duration' => $request->expire_month_after,
            'per_month_amount'=>   $amount2,
            'vehicle_id' =>  $request->session()->get('v_id'),  
            'booking_id' => $booking->id,
            'payment_type'=>1,
            'payment_status'=>1,

        ];
     
        $user = PaymentHistory::create($data);



$payment_mode=1;
$insurance= $request->session()->get('insurance');
 
Mail::to([$email_to,'bookings@pcocar.com'])->cc($request->session()->get('email'))->send(new \App\Mail\Booking(
    $payment_mode,
    $name, 
    $title,
     $booking , 
     $vehicle , 
     $request->duration,  
     $request->session()->get('discount'),
     $price_per_week,
     $price_total,
     $booking_start_date,
     $insurance

   )); 

   //$veh = Vehicle::findOrFail($request->id);     
        $vehicle->bookingStatus =1; 
        $vehicle->save();
       
    // $this->payment_history_generate($request->session()->get('insurance'), $booking , $request->session()->get('booking_start_date'));   

     //$this->booking_payment_relation($booking);   


 return redirect()->route('done',['booking_id'=>$request->booking_id])->with('success','Payment Done Successfully');
}
else{

dd('Stripe error');

}


    }




public function payment_history_generate($insu , $booking, $r){





      for($i=1;$i<= ($booking->duration-1); $i++){ 

        
         
        /* if($i==1){   
          $price = ($booking->price / 100) + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY') ;
          $payment_status =1;  
        }
         else{ 
          $price =($booking->price / 100) + env('INSURANCE_CODE') ;
            $payment_status =0;  
         }
*/
         if($insu==1){
          $price = $booking->price;
         }
         else{
          $price = $booking->price   + env('INSURANCE_CODE') ;
         }
          
            $payment_status =0;  


           $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 
  $date = strtotime(  $r   ); //starting date
  $date = strtotime($days_add, $date);
  $show_date = date('Y-m-d', $date);


  
          $data = [
        'booking_id' => $booking->id,
        'user_id' => $booking->user_id,
        'amount' => $price,
        'vehicle_id' =>$booking->vehicle_id,
        'payment_status'=> $payment_status ,
        'payment_mode' =>2,
        'expire_date' => $show_date, 
        'payment_type'=>1,
        
    ];
    
     PaymentHistory::create($data);



//BookingPaymentRelation

      }
}




 

 public function update_payment_status(Request $request){
  // $endpoint_secret = config('services.stripe.webhooksecret');
    //  $input = @file_get_contents('php://input');
   /*   $event_json = json_decode($request);
      
//return $event_json; 
      //print_r($input);
      if($event_json->type == 'charge.succeeded') {
         ///echo 'You made some money! Hooray!';
         $vehicle = Vehicle::where('id', 22)->with('car_make')->with('car_model')->first();
          $vehicle->partner_id =1; 
        $vehicle->save();
}

*/



 $vehicle = Vehicle::where('id', 22)->with('car_make')->with('car_model')->first();
          $vehicle->partner_id =1; 
        $vehicle->save();
 


 }


}