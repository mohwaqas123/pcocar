<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Device;
use App\models\Category;
use App\models\Song;
use App\models\Banner;
use App\models\Video;
use App\models\LikeVideo;
use App\models\CommentVideo;
use App\models\ShareVideo;
use App\models\Follower;
use App\models\Following;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;
use Storage;
use Illuminate\Http\File;

class FollowController extends Controller
{
    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
    }

    public function FollowUnfollow(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required',   
            'user_id' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $follwer_id = JWTAuth::toUser($request->token)->id;
        
        if($request->status == 1)
        {
            
            $user = User::where('id', $request->user_id)->first();

            if($user == null)
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_SUCCESS'],
                    'message' => 'This user does not exist.',
                    'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
                ]);
            }
            
            $old_follower = Follower::where('follower_id', $follwer_id)->where('user_id', $request->user_id)->first();
           
            if($old_follower == null)
            {
                Follower::create([
                    'follower_id' => $follwer_id,
                    'user_id' => $request->user_id
                ]);

                Following::create([
                    'following_id' => $request->user_id,
                    'user_id' => $follwer_id
                ]);

                return response()->json([
                    'status' => $this->responseConstants['STATUS_SUCCESS'],
                    'message' => 'Follow Successfully.',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }
            else
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_SUCCESS'],
                    'message' => 'You already follow this user.',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }
        }
        else
        {
            $old_follower = Follower::where('follower_id', $follwer_id)->where('user_id', $request->user_id)->first();
            $old_following = Following::where('following_id', $request->user_id)->where('user_id', $follwer_id)->first();

            if($old_follower == null && $old_following == null)
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_SUCCESS'],
                    'message' => 'this follower does not exist.',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }

            $old_follower->delete();
            $old_following->delete();

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'UnFollow Successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
    }

    public function listFollowUnFollow(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'follower' => 'required',   
            'user_id' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user = User::find($request->user_id);

        $user_id = $user->id;

        if($user_id == null)
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'User does not find.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }

        if($request->follower == 1)
        {
            $followers = Follower::where('follower_id', $user_id)->get();

            foreach($followers as $follow)
            {
                $follower = Following::where('following_id', $user_id)->where('user_id', $follow->user_id)->first();
                
                if($follower == null)
                {
                    $is_follow = 0;
                }
                else
                {
                    $is_follow = 1;
                }

                $follow['is_follow'] = $is_follow;
            }
            
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Followers List Successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $followers
            ]);
        }
        else
        {
            $followings = Following::where('following_id', $user_id)->get();

            foreach($followings as $follow)
            {
                $follow['user'] = User::where('id', $user_id)->first();

                $follower = Following::where('following_id', $user)->where('user_id', $follow->user_id)->first();
                
                if($follower == null)
                {
                    $is_follow = 0; 
                }
                else
                {
                    $is_follow = 1;
                }

                $follow['is_follow'] = $is_follow;
            }
            
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Following List Successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $followings
            ]);
        }
    }
}
