@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="breadcrumb-item active"   >
                        
                        </h4>
                       
                        <div class="card-box">
                            <h4>
                            
                            </h4>
                            @if($errors->any())
                            {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif
                            <form role="form" action="{{route('addcharges_payment_history')}}"   method="post">
                                {{csrf_field()}}
                              <input type="hidden" value="{{$id}}" name="user_id" />  
                                <div class="card-body">

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Booking <span class="text-danger required">*</span></label>
                                             <select  class="form-control" name="booking">
                                                 @foreach ($booking as $bookings)
                                                 <option value="{{$bookings->id}}">{{$bookings->id}}-{{$bookings->vehicle->licence_plate_number}} </option>
                                                  @endforeach
                                             </select>
                                            
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Charges Type <span class="text-danger required">*</span></label>
                                             <select class="form-control" name="charges_type">
                                                 <option value="1">Weekly Booking Payment</option>
                                                 <option value="3">PCN</option>
                                                 <option value="4">Expenses Charges</option>
                                                 <option value="5">Toll Charges</option>
                                                 <option value="2">Others</option>
                                             </select>
                                            
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Charge <span class="text-danger required">*</span></label>
                                            <input type="number" class="form-control @error('name') is-invalid @enderror" maxlength="4" name="amount" id="amount" placeholder="Enter Charge Amount"  required />
                                            
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Detail <span class="text-danger required">*</span></label>
                                            <textarea  type="text" class="form-control @error('name') is-invalid @enderror" name="description" id="description" placeholder="Enter Detail"  required></textarea>
                                            
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input
                                            action="action"
                                            onclick="window.history.go(-1); return false;"
                                            type="submit"
                                            value="Cancel"
                                            class="btn btn-info btn-flat"
                                            />
                                            <button type="submit" class="btn btn-info btn-flat">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')
    @endsection