@extends('layouts.partner')
@section('styles')
@endsection
@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                       
                        <div class="card-box">
                             
                            @if($errors->any())
                                {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif

                            <form role="form" action="{{ route('partner_add_driver_update') }}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="id" value="{{ $driver->id }}">

                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Name<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$driver->name}}" id="name" placeholder="Enter State Name">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                        </div>  
 <div class="row">
                                           <div class="form-group col-md-6">
                                            <label for="country name">Email <span class="text-danger required">*</span></label>
                                           <input type="text" class="form-control @error('name') is-invalid @enderror" name="email" value="{{$driver->email}}" id="name" placeholder="Enter State Name">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                           </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="status">Phone<span class="text-danger required">*</span></label>
                                           <input type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{$driver->phone}}" id="phone" placeholder="Enter Phone" required="required">
                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Update</button>
                                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>


                              </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
