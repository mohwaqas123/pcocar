@extends('layouts.partner')
@section('styles')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.select2-container .select2-selection--single{
height:34px !important;
}
.select2-container--default .select2-selection--single{
border: 1px solid #ccc !important;
border-radius: 0px !important;
}

.bg-dev{border: 1px solid #000;}
.header-title{background: #eee;
padding: 5px;}

</style>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="card-box">
              

  @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
 <form action="{{ route('partner_profile_update') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                               
                                    
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Enter Name<span class="text-danger">*</span></label>
                                            <input max="10" min="3" type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$partner->name}}" id="" placeholder="Please Enter Name" required>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                         
                                    </div>
                                 <!--    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Email<span class="text-danger">*</span></label>
                                            <input type="email" value="{{$partner->email}}" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Email" required>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        
                                    </div> -->

                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Phone<span class="text-danger">*</span></label>
                                            <input type="number" name="phone" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="" value="{{$partner->phone}}" placeholder="Please Enter Phone" required>
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="strong name" class="col-form-label">Address<span class="text-danger">*</span></label>
                                            <input type="text" name="address" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="" value="{{$partner->address}}" placeholder="Please Enter Address" required>
                                            @if ($errors->has('address'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Detail<span class="text-danger">*</span></label>
                                            <input type="text" name="detail" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="" value="{{$partner->detail}}" placeholder="Please Enter Detail" required>
                                            @if ($errors->has('detail'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('detail') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div> -->
                                     <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Business Address</label>
                                             <input type="text"   value="{{$partner->business_address}}"  name="business_address" 
                                             class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Business Address"  >
                                            
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Pick-up Address</label>
                                            <input type="text" value="{{$partner->pickup_address}}" name="pickup_address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}" id="" placeholder="Please Enter Pickup Address" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span> 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Drop-off Address</label>
                                            <input type="text" value="{{$partner->dropoff_address}}" name="dropoff_address" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Drop off Address" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span> 
                                            @endif
                                        </div>
                                         <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Bank Name</label>
                                            <input type="text" value="{{$partner->bank_name}}" name="bank_name" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Bank Number" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span> 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Account Number</label>
                                            <input type="text" 
                                              maxlength="17"  value="{{$partner->account_number}}"
                                             name="account_number" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Account Number" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span> 
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Account Holder Name</label> 
                                            <input type="text"  name="account_holder_name" value="{{$partner->account_holder_name}}" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Account Holder Name" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>  
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="song name"  class="col-form-label">Sort Code</label>
                                            <input type="text"maxlength="6"  value="{{$partner->sort_code}}" name="sort_code" class="form-control {{ $errors->has('commission') ? ' is-invalid' : '' }}"   placeholder="Please Enter Sort Code" >
                                            @if ($errors->has('commission'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('commission') }}</strong>
                                            </span>Drop-off 
                                            @endif
                                        </div>






                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </form>


 
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">



function remove_img(id){

     $.ajax({


                  // url:"http://ip.jsontest.com/",
                  url: '{{ url("temp_delete_img") }}',
                   method:"get",
                   //data: $('#frmConversation').serialize(),
                  data: {id:'4.jpg'},
                   //dataType:'JSON',
                   contentType: 'application/json; charset=utf-8',
                   cache: false,
                   processData: false,
                   success:function(data)
                   {
                       alert(data);
                   },
                   error: function(error)
                        {
                            //console.log(data);
                           // alert(error);
                        }
                  });
}



$('.select2').select2();
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
});
} );
</script>
@endsection