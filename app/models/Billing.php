<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

 

class Billing extends Model
{

	protected $table = 'makes';

    protected $fillable = [
        'name'
    ];

    public function car_model()
    {
        return $this->hasMany('App\models\CarModel');
    }
}
