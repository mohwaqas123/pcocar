<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
 use Session;
 use Auth;
 use App\Helper\Helper;
 use App\models\Coupon;

class browsesController extends Controller
{
    public function index(Request $request)
    {
 
       // $vehicles = Vehicle::with('car_make')->orderby('id', 'Desc')->get();
        if($request->t)
            $vehicles = Vehicle::where('vehicle_category',$request->t)->paginate(10);
        else
            $vehicles = Vehicle::paginate(10);


        //return view('frontend.browse', ['vehicles' => $vehicles]);
        $make = Make::all();
        $model = CarModel::all();
    
      if($request->duration) $duration = $request->duration; else $duration = '';
        return view('frontend.browse', [
                'vehicles' => $vehicles , 
                'car_make'=> $make,
                'car_model' => $model,
                'duration' => $duration,
                
            ]);
    }

    public function promotion_apply(Request $request){ 
      $validator = $request->validate([
            'discountcode' => 'required', 
        ]); 
      $coupon = Coupon::where('discountcode',$request->discountcode)->get();
      
     if(count($coupon)>0){
           session()->forget('price'); 
           session([  'permotion_discount' => $coupon[0]->percentdiscount ]); 
           return $coupon[0]->percentdiscount ;
      }
      else
      {
        return 2;
           //return back()->withErrors(['Sorry your code is not correct.',]);
      }

 
    
}

    public function car_detail(Request $request)
    { 
 
         session(['permotion_discount' => '',]);
         session()->forget('permotion_discount');
       //  session()->forget('duration');
       //  $request->session()->flush();
       //  Session::forget();
       //  Session::flush();
       //  session()->flush();


        //$vehicles = Vehicle::where('id', $request->id)->with('car_make')->with('car_model')->orderby('id', 'Desc')->get();
        $vehicles = Vehicle::where('id', $request->id)->with('car_make')->with('car_model')->with('vehicle_images')->first(); 
         //dd($vehicles);
        // $vehicles = Vehicle::findOrFail($request->id);  
        //return view('frontend.browse', ['vehicles' => $vehicles]);
        
  //dd(session()->get('duration'));
        return view('frontend.car_detail', [
                'vehicles' => $vehicles, 
                'duration'=> session()->get('duration'),
                'discount'=>session()->get('discount'),
                'disc_percent'=> Helper::get_discount_percent_func( session()->get('duration')),
                'promotion' => Helper::check_discount(),
                'promotion_type'=>Helper::get_flat_discount_amount()[0]->promotion_type,
                'flat_discount_rate'=>Helper::get_flat_discount_amount()[0]->flat_discount_rate,
                'setting'=>Helper::get_flat_discount_amount()[0],
                'get_discount_duration_price' =>   Helper::get_discount_duration_price(session()->get('duration'),Helper::get_price_with_check_discount($vehicles->price)),
                'price' => Helper::get_price_with_check_discount($vehicles->price),
                

                 ]);
    }

}

