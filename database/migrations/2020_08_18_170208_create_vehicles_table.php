<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('make');
            $table->integer('model');
            $table->string('year');
            $table->string('fuel');
            $table->float('price');
            $table->string('offer');
            $table->integer('no_of_Passengers');
            $table->string('engine_Capacity');
            $table->integer('toll_Charges');
            $table->string('description');
            $table->string('featurs')->nullable();
            $table->string('garebox')->nullable();
            $table->tinyInteger('body_type')->comment('1=MPV')->nullable();
            $table->boolean('vehicle_category')->comment('1=pco,2=private');
            $table->boolean('vehicle_type')->comment('0=family,1=premium');
            $table->string('thumbnail')->nullable();
 
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
