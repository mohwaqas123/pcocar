@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        {{--  <p class="text-muted font-14 m-b-30">
                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                        </p>  --}}

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                          
                                <thead>
                                <tr>
                                <th style="width: 10px">Sr.#</th>
                                <th>Name</th>
                                <th style="width:90px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($makes as $make)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $make->name}}</td>
                                    <td>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top" data-original-title="Edit"  href="{{ route('adminmakeEdit', $make->id) }}" class="mr-5">Edit </a>
                                    <br>
                                    <br>
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete" href="{{ route('adminmakedestory', $make->id) }}"
                                    onclick="return confirm('You want to remove your selected Make?')"class="mr-5">Delete </a>
                                </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                            {{$makes->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
