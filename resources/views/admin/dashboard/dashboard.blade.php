 
@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page">
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
            <!-- <ul class="list-unstyled topbar-right-menu float-right mb-0">
                <li class="hide-phone app-search">
      


                    <form>
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
             
            </ul> -->
            <ul class="list-inline menu-left">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    
                </li>
            </ul>
        </nav>
    </div>
    <!-- Top Bar End -->
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-box">
                        <h4>Available Cars</h4>
                        <p>PCO: {{@$Total_vehicle_pco}} &nbsp; <a href="{{ route('total_vehicle_pco_available') }}">View</a></p>
                        <p>Private:{{@$Total_vehicle_private}}&nbsp;<a href="{{ route('total_vehicle_private_available') }}">View</a></p>
                        <p>Hidden:{{@$Total_vehicle_hidden}}&nbsp;<a  href="{{ route('vehiclehide_list') }}">View</a></p>
                         <!-- <input onclick="window.location='{{ url("admin/vehiclesList") }}'" 
                          type="button" class="btn btn-primary metismenu with-tooltip"  data-toggle="tooltip" data-placement="Top"  value="View List">&nbsp;
                         <input onclick="window.location='{{ url("admin/add_vehicle") }}'"  type="button" class="btn btn-primary" value="Add New"> -->
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-md-4">
                    <div class="card-box" style="min-height: 200px" >
                        <h4>Swap Request </h4>
                         @foreach ($notification as $notification_us)
                        <p>Booking id: {{$notification_us->booking->id}}User Name:{{$notification_us->user->name}}
                            </p> 
                        @endforeach
                          <hr>  
                        <a href="{{ route('admin_notification') }}">View</a>
                    </div>
                </div>
                 <div class="col-md-4">
                  <!--   <div class="card-box">
                        <h4>Models</h4>
                        <p>Total: {{@$Total_carmodel}}</p>
                        <p>Active: {{@$Total_carmodel_active}}</p>
                         <input type="button" onclick="window.location='{{ url("admin/car-models") }}'"  class="btn btn-primary" value="View List">&nbsp;
                         <input type="button" onclick="window.location='{{ url("admin/car-models/create") }}'"  class="btn btn-primary" value="Add New">
                    </div> -->
                </div>

                <div class="col-md-4">
                  <h4 class="card-box">Pending Bookings</h4>
              <div class="slimScrollDiv " style="position: relative; overflow: hidden; width: auto; height: 414px; ">
                    <div class="slimscroll"   style="max-height: 230px; overflow: auto; width: auto; height: 414px;">
                    <div class="card-box">
                        @foreach ($booking_pendding as $booking_us)
                        
                             <?php
                            $date_p = $booking_us->booking_start_date;//date('Y-m-d'); 
                       $ldate2 = date('Y-m-d  H:i:s', strtotime("$date_p". " +1 hours"))
                            ?>
                        @if($ldate2 > $date )
                        <p>Booking id: {{$booking_us->id}},{{$booking_us->user->name}}
                            {{$booking_us->vehicle->licence_plate_number}}
                            <a   href="{{ route('upload_information_second', $booking_us->id) }}">view</a>
                     </p>  
                         @else
                       <p>Booking id: {{$booking_us->id}},{{$booking_us->user->name}}
                            {{$booking_us->vehicle->licence_plate_number}}
                            <a   href="{{ route('upload_information_second', $booking_us->id) }}">view</a>
                     </p> 
                        @endif
                      @endforeach   <hr> 
                        <a href="{{ route('vehicle_booking_pending') }}">View</a>

                    </div>
                </div>
                </div>
                </div>
                <div class="col-md-4">
                  <h4 class="card-box">New Bookings</h4>
              <div class="slimScrollDiv " style="position: relative; overflow: hidden; width: auto; height: 414px; ">
                    <div class="slimscroll"   style="max-height: 230px; overflow: auto; width: auto; height: 414px;">
                    <div class="card-box">
                       @foreach ($booking as $booking_us)
                         <ul>
                            <li>Booking id: {{$booking_us->id}} </li>
                            <li>
                               User Name:{{$booking_us->user->name}}</li>
                            <li>Vehicle :{{$booking_us->vehicle->licence_plate_number}}
                            </li>
                           <li>Start:{{date('d-m-Y H:i:s', strtotime($booking_us->booking_start_date))}}  to
                            End:{{date('d-m-Y H:i:s', strtotime($booking_us->booking_drop_off_date))}} 
                            </li>
                            <a   href="{{ route('customerview', $booking_us->id) }}">view</a>
 
                            </ul>

                         <p></p>
                        @endforeach
                        <hr> 
                        <a href="{{ route('vehicle_booking') }}">View</a>
                    </div>
                </div>
                </div>
                </div>
                <div class="col-md-4">
                  <h4 class="card-box">Bookings About to End</h4>
              <div class="slimScrollDiv " style="position: relative; overflow: hidden; width: auto; height: 414px; ">
                    <div class="slimscroll"   style="max-height: 230px; overflow: auto; width: auto; height: 414px;">
                    <div class="card-box">
                       @foreach ($renew_booking as $booking_us)
                            <?php
                            $date_p = $booking_us->booking_drop_off_date;//date('Y-m-d');
                            $ldate2 = date('d-m-Y', strtotime("$date_p". " -1  days"));
                            $date3 = date('d-m-Y', strtotime("$date_p". " -2  days"));
                            $date4 = date('d-m-Y', strtotime("$date_p". " -3  days"));
                            $date5 = date('d-m-Y', strtotime("$date_p". " -4  days"));
                            $date6 = date('d-m-Y', strtotime("$date_p". " -5  days"));
                            $date7 = date('d-m-Y', strtotime("$date_p". " -6  days"));
                            ?>
                            @if($date == $ldate2 || $date == $date3 || $date == $date4 || $date == $date5  || $date == $date6 || $date == $date7)
                            <ul>
                            <li>Booking id: {{$booking_us->id}} </li>
                            <li>
                               User Name:{{@$booking_us->user->name}}</li>
                            <li>Vehicle :{{@$booking_us->vehicle->licence_plate_number}}
                            </li>
                            <li>
                              End:{{date('d-m-Y H:i:s',strtotime($booking_us->booking_drop_off_date))}} 
                            </li>
                             <a   href="{{ route('adminrenewbooking', $booking_us->id) }}">view</a>
                            </ul>
                            @else
                             @endif
                        @endforeach
                    </div>
                </div>
                </div>
                </div>

              </div>
             <!-- Row are closed  -->
        </div>
        
    </div>
         
           
            </div> 
       
        @endsection
        @section('scripts')
        @endsection