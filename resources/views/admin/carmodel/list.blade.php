@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                       <h4 class="breadcrumb-item active"   > 
       
                    </h4>
                        
                        <h4> 
                        
                    </h4>

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

                                <thead>
                                <tr>
                                <th style="width: 10px">Sr.#</th>
                                <th>Vehicle Name</th>
                                <th>Make Name</th>
                                <th>Status</th>
                                <th style="width:90px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($carmodels as $carmodel)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $carmodel->name }}</td>
                                    <td>{{ @$carmodel->make->name }}</td>
                                    <td style="width:100px;">
                                        @if ($carmodel->status == 0)
                                            In-active
                                        @else
                                            Active
                                        @endif
                                    </td>
                                    <td>
                                    <a class="btn btn-warning btn-sm" href="{{ route('editcarmodel', $carmodel->id) }}" class="mr-5">Edit </a>
                                     <br>
                                    <br>
                                    
                                    <a class="btn btn-warning btn-sm" href="{{ route('deletecarmodel', $carmodel->id) }}"  onclick="return confirm('You want to remove your selected Car Model?')"class="mr-5">Delete </a>
                                </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                            {{$carmodels->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
