<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PCN extends Model
{
    protected $table = 'pcn';
     protected $fillable = [
        'ref_number','vrm','status','stage','pcn_contravention','notice_date','received_date','type_of_action','action_date','charging_authority_name','driver_name','driver_email','driver_phone','doc_mot','letter_txt',
      ];
       public function vehicle()
    {
        return $this->belongsTo('App\models\Vehicle','vrm','id');
    }
       public function user()
    {
        return $this->belongsTo('App\models\User','driver_name','id');
    }
}
