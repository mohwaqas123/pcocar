@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
        	 @if(session()->has('message'))
			    <div class="alert alert-success">
				   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				   <span aria-hidden="true">&times;</span>
				  </button>
        		  {{ session()->get('message') }}
    			</div>
			@endif 
            <div class="row">
                <div class="col-md-9">
                    <div class="card-box">	
                    	<h1>Customer Details</h1>
                    	<p>Name : {{$staff->name}} </p>
                    	<p>Email :{{$staff->email}}</p>
                    	<p>Phone :{{$staff->phone}}</p>
                    	<p>Address :{{$staff->address}}</p>
                    	<p>Job Description : {{$staff->description}}</p>

                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="card-box">	
                    	<h1>Document</h1>
                    	<p>   
                           <a href="{{url('/')}}/storage/app/{{$staff->nid_front}}"> N.I.C(Front)</a>  
                        </p>
                        <p>   
                           <a href="{{url('/')}}/storage/app/{{$staff->nid_back}}"> N.I.C(Back)</a>  
                        </p>
                        <p>   
                           <a href="{{url('/')}}/storage/app/{{$staff->profile_img}}"> Profile Img</a>  
                        </p>
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>





@endsection
@section('scripts')
@endsection