<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\User;
use App\models\Staff;
use App\models\Vehicle;
use App\models\Purchase;
use App\models\Booking;
use App\models\Accedental_records;
use App\models\Accidental_records_comments;
use DB;
use Mail;
use App\models\Accidental_records_image;


class AccidentalRecordedController extends Controller
{


    public function get_customers(Request $request){ 
         $records = Booking::with('user')->where('vehicle_id',$request->vehicle_id)->get(); 
         //dd( $records->user->name);
            // return json_encode($records);


$customer = ' <label for="make" class="col-form-label">Customer</label>
                  <select    name="name" placeholder="Customer Drop Down"
                    class="form-control select2">
                     ';
                 $customer .= '<option value="0">None</option>';
            foreach($records as $rec){
               // if($rec->user->name)
                $customer .= "<option value='".@$rec->user->id ."'>".@$rec->user->name ." (".@$rec->user->email .")</option>";
            }

 $customer .= "</select>";
 return $customer;


    }
    

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

   //$records = Accedental_records::select('id')->orderBy('id', 'ASC')->paginate(10);
           
         
            $records = Accedental_records::with('user')->get(); 
            $recorded_new_admin = Accedental_records::where('int_status', 1)->get(); 
            $recorded_in_process = Accedental_records::where('int_status', 2)->get();
            $recorded_sattlement = Accedental_records::where('int_status', 3)->get();
            
            $recorded_dispute = Accedental_records::where('int_status', 4)->get(); 
            $url = "/admin/dashboard";
            $title = 'Damages Records list';
            
            $data = [
               'records'=> $records,
               'recorded_new_admin'=> $recorded_new_admin,
               'recorded_in_process'=> $recorded_in_process,
               'recorded_sattlement'=> $recorded_sattlement,
               'recorded_dispute'=> $recorded_dispute,
                'title' =>$title,

                 'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Damages'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Damages List' ,
 
                
        ];  
       
        return view('admin.accidental.index')->with($data);
    }
    public function create()

    {

        //

    }
    public function store(Request $request)

    {
         $doc_mot='';
         $doc_logback='';
         $img_exterior_front='';
         $doc_phv='';
         $img_exterior_back='';
         $img_exterior_front2='';
         $img_exterior_dashboard='';

        //documents upload process
         if ($request->hasFile('doc_mot') || $request->hasFile('doc_logback') || $request->hasFile('doc_phv') || $request->hasFile('img_exterior_front') || $request->hasFile('img_exterior_back') || $request->hasFile('img_exterior_front2') || $request->hasFile('img_exterior_dashboard')) 
         {
            $vehicleDocFolder = 'accidentalImages';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_mot'))
               $doc_mot = Storage::putFile($vehicleDocFolder, new File($request->doc_mot));

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
            
            if ($request->hasFile('doc_phv'))
               $doc_phv = Storage::putFile($vehicleDocFolder, new File($request->doc_phv));
            
            if ($request->hasFile('img_exterior_front'))
               $img_exterior_front = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front));
            
            if ($request->hasFile('img_exterior_back'))
               $img_exterior_back = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_back));
            
            if ($request->hasFile('img_exterior_front2'))
               $img_exterior_front2 = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front2));
            
            if ($request->hasFile('img_exterior_dashboard'))
               $img_exterior_dashboard = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_dashboard));
   

          //  $vehicle->update(['image'=> $imageUrl]);
        } 
          $data = [
            'user_id' =>$request->name,
            'staff_id' =>$request->staff,
            'purchase_id' =>$request->purchase,
            'price' =>$request->price,
            'booking_id' => $request->booking,
            'vehicle_id' => $request->licence_plate_number,
            'detail' => $request->detail,
            'title' => $request->title,
            'part' => json_encode($request->chk),
             'doc_mot'=> $doc_mot,
             'doc_logback'=> $doc_logback,
             'img_exterior_front'=> $img_exterior_front,
             'img_exterior_back'=> $img_exterior_back,
             'img_exterior_front2'=> $img_exterior_front2,
             'img_exterior_dashboard'=> $img_exterior_dashboard,
             'doc_phv'=> $doc_phv,
             'int_status'=> 1,
            ];
            
          $accedental = Accedental_records::create($data);
     
      
       
                if ($request->hasFile('thumbnail')) {
            $vehicleImagesFolder = 'accidentaldoc';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            $imageUrl = Storage::putFile($vehicleImagesFolder, new File($request->thumbnail));
            $accedental->update(['image'=> $imageUrl]);
        }

        if ($request->hasFile('images')) {
            $vehicleImagesFolder = 'accidentaldoc';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            foreach($request->images as $image)
            {

              $imageUrl = Storage::putFile($vehicleImagesFolder, new File($image));
                Accidental_records_image::create(
                    [
                        'accidental_id'=> $accedental->id,
                        'images' => $imageUrl 
                    ]
                );
            } 
   
       
       
    }

        $user = User::find($request->name);
        
        if ($user == '') 
        {
        } 
        else
         {

            $name =  $user->name;
        $email =  $user->email;
 
        Mail::to($email)->send(new \App\Mail\Accedental_records(
          $name)); 


        }

       
         return redirect()->route('admin/accidental/recorded/list')->with('message','Damages Records Added Successfully');
 
}
    public function show($id)

    {

        //

    }

 

    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

     $make = Accedental_records::find($id);
     $user = User::all(); 
     $vehicle = vehicle::all(); 
    $staff = Staff::all();  
    $purchase = Purchase::all();


     $url = "/admin/dashboard";
        $title = 'Edit Accedental Records';
          $data = [
            'make' => $make,
            'vehicle' => $vehicle,
            'user' => $user,
            'purchase' => $purchase,
            'staff' => $staff,
            'title'=>   $title,
            'parts'=>   $make,

            'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Damages'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Edit Damages Records' ,


           ]; 

            return view('admin.accidental.edit')->with($data);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request)

    {

  
       $accedental = Accedental_records::where('id', $request->id)->first();
  

         $doc_mot=$accedental->doc_mot;
         $doc_logback=$accedental->doc_logbac;
         $img_exterior_front=$accedental->img_exterior_front;
         $doc_phv=$accedental->doc_phv;
         $img_exterior_back=$accedental->img_exterior_back;
         $img_exterior_front2=$accedental->img_exterior_front2;
         $img_exterior_dashboard=$accedental->img_exterior_dashboard;

        //documents upload process
         if ($request->hasFile('doc_mot') || $request->hasFile('doc_logback') || $request->hasFile('doc_phv') || $request->hasFile('img_exterior_front') || $request->hasFile('img_exterior_back') || $request->hasFile('img_exterior_front2') || $request->hasFile('img_exterior_dashboard')) {
            $vehicleDocFolder = 'vehicleDocs';

            if (!Storage::exists($vehicleDocFolder)) {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_mot'))
               $doc_mot = Storage::putFile($vehicleDocFolder, new File($request->doc_mot));

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
            
            if ($request->hasFile('doc_phv'))
               $doc_phv = Storage::putFile($vehicleDocFolder, new File($request->doc_phv));
            
            if ($request->hasFile('img_exterior_front'))
               $img_exterior_front = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front));
            
            if ($request->hasFile('img_exterior_back'))
               $img_exterior_back = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_back));
            
            if ($request->hasFile('img_exterior_front2'))
               $img_exterior_front2 = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_front2));
            
            if ($request->hasFile('img_exterior_dashboard'))
               $img_exterior_dashboard = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_dashboard));
   

          //  $vehicle->update(['image'=> $imageUrl]);
        }
          $data = [
            'user_id' => $request->name,
            'booking_id' => $request->booking,
            'staff_id' =>$request->staff,
            'purchase_id' =>$request->purchase,
            'vehicle_id' => $request->licence_plate_number,
            'detail' => $request->detail,
            'part' => json_encode($request->chk),
             'doc_mot'=> $doc_mot  ,
             'doc_logback'=> $doc_logback,
             'img_exterior_front'=> $img_exterior_front,
             'img_exterior_back'=> $img_exterior_back,
             'img_exterior_front2'=> $img_exterior_front2,
             'img_exterior_dashboard'=> $img_exterior_dashboard,
             'doc_phv'=> $doc_phv,
             'int_status'=> $request->int_status,
            
        ];

   
           
        $accedental->update($data); 
        $accedental->save();

         if ($request->hasFile('thumbnail')) {
            
            $vehicleImagesFolder = 'accidentaldoc';

            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

            //remove image
            $v_image = 'vehicleImages/'.$vehicle->image;   
            Storage::delete($v_image);


            $imageUrl = Storage::putFile($vehicleImagesFolder, new File($request->thumbnail));
            $vehicle->update(['image'=> $imageUrl]);
        }
//multiple image gallary options
        if ($request->hasFile('images')) {
            $vehicleImagesFolder = 'accidentaldoc';
            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }
            foreach($request->images as $image)
            {
              $imageUrl = Storage::putFile($vehicleImagesFolder, new File($image));
                Accidental_records_image::create(
                    [
                        'accidental_id'=> $accedental->id,
                        'images' => $imageUrl 
                    ]
                );
            } 
        }


return redirect()->route('admin/accidental/recorded/list')->with('message','Damages Recorded Updated Successfully');




    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

   
     $makes = Accedental_records::findOrFail($id);
      $makes->delete();
        return redirect()->route('admin/accidental/recorded/list')->with('success', 'Record delete Successfully');
           

    }

    public function accidental()
    {

         $user = User::all();  
         $staff = Staff::all();  
         $purchase = Purchase::all();  
         $vehicle = vehicle::all(); 

         $url = "/admin/dashboard";
            $title = 'Add Vehicle';
          $data = [
               'user'=> $user,
               'vehicle'=> $vehicle,
               'purchase'=> $purchase,
               'staff'=> $staff,
                'title' =>$title,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Damage'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Record Damage' ,
                ];  
            
      return view('admin.accidental.accidental')->with($data);

    }
    public function customerview(Request $request)
    { 
        
        $booking = Accedental_records::where('id',$request->id)->with('vehicle')->with('user')->with('booking')->with('staff')->with('Purchase')->first();
        
        $comments = Accidental_records_comments::where('accidental_id',$request->id)->orderBy('created_at', 'desc')->get();
         $title = $booking->title;
         
    $user = User::all(); 
     $url = "/admin/dashboard";
 
          $data = [
               'booking'=> $booking,
               'comments'=> $comments,
            'title'=> $title,
               'user'=> $user,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Damage'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Damage' ,
  
            ];  
           
        return view('admin.accidental.view')->with($data);

    } 
    public function admin_accidental_comment(Request $request)

    { 
       
      $booking = Accedental_records::where('id',$request->id)->with('vehicle')->with('user')->with('booking')->first();
        
     
         $doc_logback='';
       

        //documents upload process
         if ($request->hasFile('doc_logback') ) 
         {
            $vehicleDocFolder = 'accidentalImages';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_logback'))
               $doc_logback = Storage::putFile($vehicleDocFolder, new File($request->doc_logback));
             } 

      $data = [
             'accidental_id' =>$booking->id,
                'user_id' =>0,
               'comments' => $request->detail,
               'attachment' => $doc_logback,
            ];       
 $Accidental_records_comments = Accidental_records_comments::create($data);

      if ($Accidental_records_comments->user == '') 
        {
          
        } 
        else
         {
          
           $name =  $booking->user->name;
        $email =  $booking->user->email;
        Mail::to($email)->send(new \App\Mail\Accedental_records_comments(
          $name)); 
            }

        if ($booking->user == null) 
        {

            $user_id = 0;
        } 
        else
         {
          $user_id = $booking->user->id;
            }

            $vehicleData = [ 
             'accidental_id' =>$booking->id,
             'user_id' =>$user_id,
             'vehicle_id' =>$booking->vehicle->id,
             'detail' =>$booking->detail,
             'int_status' =>2,
              ];
                $booking->update($vehicleData); 
                $booking->save();
 
   
     
 
        return redirect()->back()->with('message','Comments Posted Successfully');
    }
      public function settelment_records(Request $request)

    { 
     $booking = Accedental_records::where('id',$request->id)->with('vehicle')->with('user')->with('booking')->first();
    
      // if($booking->id == 0 )
      // {
      //   $booking->user->id == 0;
      // }
      // dd($booking->user->id);
      $data = [
              
            'accidental_id' =>$booking->id,
            'user_id' =>$booking->user->id,
            'staff_id' =>$booking->staff_id,
            'purchase_id' =>$booking->purchase_id,
            'vehicle_id' => $booking->vehicle->id,
            'detail' => $booking->detail,
            'int_status' =>3,
           ];       
           
        $booking->update($data); 
        $booking->save();

 
        return redirect()->back()->with('message','Report are Settled.');
    }
    public function unsettelment_records(Request $request)
    { 
     $booking = Accedental_records::where('id',$request->id)->with('vehicle')->with('user')->with('booking')->first();
      $data = [
              
            'accidental_id' =>$booking->id,
            'user_id' =>$booking->user->id,
            'vehicle_id' => $booking->vehicle->id,
            'detail' => $booking->detail,
            'int_status' =>4,
           ];            
        $booking->update($data); 
        $booking->save();
       
        return redirect()->back()->with('message','Report are Disputed.');
    }
     public function commentdestory($id)
    { 
         
    $makes = Accidental_records_comments::findOrFail($id);
    $makes->delete();

    return redirect()->back()->with('message','Comment are Delete.');
         
    } 
    public function commentedit(Request $request)
    { 
     
    $makes = Accidental_records_comments::findOrFail($request->id);
   
    $data = [
    'comments' => $request->description,
    'accidental_id' => $makes->accidental_id,
    'user_id' => $makes->user_id,
     ];
     
    $makes->update($data);
    $makes->save();

    return redirect()->back()->with('message','Comment are Edit.');
         
    } 
 
    public function edit_comments(Request $request)

    { 
       
    $makes = Accidental_records_comments::findOrFail($request->id);
  
    $data = [
    'comments' => $makes->comments, 
    'accidental_id' => $makes->accidental_id,
    'user_id' => $makes->user_id,
    'attachment' => $makes->attachment,
     ];

      // return redirect('admin.accidental.comments')->with($data);
 return response()->json($data); 
    
    }   
     

}

