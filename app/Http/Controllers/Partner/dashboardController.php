<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Partner;
use App\models\Booking;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use Session;
use App\models\Driver;
 

class dashboardController extends Controller
{
    public function partnerdashboard(Request $request)
    { 


      $Total_vehicle_pco = Vehicle::where('partner_id',$request->session()->get('p_user_id') )->where('vehicle_category', '1')->count();
      $Total_vehicle_private = Vehicle::where('partner_id',$request->session()->get('p_user_id') )->where('vehicle_category', '2')->count();


      $booking=Booking::where('vehicle_id' , $request->session()->get('p_user_id'))->with('vehicle')->count();
      $Total_driver_add_by_partner = Driver::where('partner_id',$request->session()->get('p_user_id'))->count();
       $Total_driver_add_by_partner_active = Driver::where('partner_id',$request->session()->get('p_user_id'))->where('driver_status', '0')->count();
      $partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
      $url = "partnerdashboard";
      $title = 'Car Make list';
      $data = [
        'Total_vehicle_pco' => $Total_vehicle_pco, 
        'Total_vehicle_private' => $Total_vehicle_private, 
        'Total_driver_add_by_partner_active' => $Total_driver_add_by_partner_active, 
        'partner' => $partner,  
        'booking' => $booking,  
        'title' =>$title,              
         'Total_driver_add_by_partner' => $Total_driver_add_by_partner,
         'breadcrum' => '<a style="color:black; href="'. url( $url ).'" > Dashboard</a>',
        ];
        return view('partner.dashboard')->with($data); 
}

}
