<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use App\models\Partner;
use App\models\Driver;
use App\models\Booking;
use Carbon\Carbon;
use App\models\Notification;

class dashboardController extends Controller
{
    public function index()
    {
      $notification = Notification::select('user_id', 'booking_id','vehicle_id','details' )->orderBy('id', 'DESC')->paginate(5);
      $booking = Booking::where('booking_status',1)->where('reference_booking',0)->where('insurance_type', 0)->where('payment_status',1)->orderBy('id', 'asc')->get();
      
      $date =Carbon::now()->format('d-m-Y');       
    $renew_booking = Booking::select('id','booking_drop_off_date','user_id','vehicle_id')->where('reference_booking_status',null)->with('user')->with('vehicle')->orderBy('id', 'asc')->get();
  // $booking_pendding  = Booking::where('agreement_status',1)->where('payment_status',null)->where('booking_status' , '=','1' )->where('reference_booking',null)->orderBy('id', 'DESC')->paginate(10);
    $booking_pendding  = Booking::where('payment_status',null)->where('reference_booking',0)->orderBy('id', 'DESC')->paginate(5);
    $Total_vehicle_pco = Vehicle::where('vehicle_category','1')->where('bookingStatus',0)->count();
      $Total_vehicle_private = Vehicle::where('vehicle_category','2')->where('bookingStatus',0)->count();
      $Total_vehicle_hidden = Vehicle::where('bookingStatus','3')->count();
      $Total_carmodel_active = CarModel::where('status','1')->count();
      $Total_carmodel = CarModel::count();
      $Total_make = Make::count();
      $Total_driver = Driver::count();
      $Total_driver_active = Driver::where('driver_status','0')->count();
      $Total_partner = Partner::count();
      $Total_Booking = Booking::count();
      $Total_booking_new = Booking::where('booking_status','1')->count();
      $Total_booking_close = Booking::where('booking_status','2')->orderBy('id', 'DESC')->paginate(5);
     
      $url = "/admin/dashboard";
      $title = 'Dashboard';
	$data = [ 
       'booking_pendding' => $booking_pendding,
       'date' => $date,
       'renew_booking' => $renew_booking,
       'booking_pendding' => $booking_pendding,
       'Total_vehicle_hidden' => $Total_vehicle_hidden,
        'Total_vehicle_pco' => $Total_vehicle_pco,
        'Total_vehicle_private' => $Total_vehicle_private,
        'Total_make' => $Total_make,
        'booking' => $booking,
        'Total_carmodel_active' => $Total_carmodel_active,
        'Total_carmodel' => $Total_carmodel,
        'Total_partner' => $Total_partner,
        'Total_driver' => $Total_driver,
        'Total_Booking' => $Total_Booking,
        'Total_driver_active' => $Total_driver_active,
        'Total_booking_new' => $Total_booking_new,
        'Total_booking_close' => $Total_booking_close,
        'notification' => $notification,
        'title' =>$title,
        'breadcrum' => 
        '<a style="color:black; &nbsp"  href="'. url( $url ).'" > Dashboard</a> ',
	];
    return view('admin.dashboard.dashboard')->with($data);
    }
}
