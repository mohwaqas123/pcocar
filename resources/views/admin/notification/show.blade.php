@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
        	 @if(session()->has('message'))
			    <div class="alert alert-success">
				   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				   <span aria-hidden="true">&times;</span>
				  </button>
        		  {{ session()->get('message') }}
    			</div>
			@endif 
            <div class="row">
                <div class="col-md-9">
                 


                    <div class="card-box">	
                    	<h1>Notification Details</h1>
                    	<p>User Name : {{$notification->user->name}} </p>
                        <p>Email :{{$notification->user->email}}</p>
                        <p>Phone :{{$notification->user->phone}}</p>
                        <p>Address :{{$notification->user->address}}</p>
                        <p> vehicle Plate : {{$notification->vehicle->licence_plate_number}}</p>
                        <p> Booking ID   : {{$notification->booking->id}}</p>
                        <p> Booking Price : {{$notification->booking->price}}</p>

                        <p> Booking Expire Date : {{$notification->booking->booking_drop_off_date}}</p>
                        <p> Details : {{$notification->details}}</p>
                    	



                    </div>
                 </div>
                
            </div>
        </div>
    </div>
</div>





@endsection
@section('scripts')
@endsection