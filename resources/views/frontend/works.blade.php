@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')


      <section class="slice py-7">
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-md-12 text-center">
                    <!-- Image -->
                    <figure class="w-100 text-center">
                        <img alt="Image placeholder" src="{{ url('/')}}/public/theme3/assets/img/header-car.png" class="img-fluid" />
                    </figure>
                </div>
             </div>
        </div>
    </section>

    <section class="slice slice-lg pt-lg-6 pb-0 pb-lg-6 bg-section-secondary">
        <div class="container">
            <!-- Title -->
            <!-- Section title -->
            <div class="row  justify-content-center text-center">
                <div class="col-lg-6">
                    <h2 class=" mt-4">Are you a Newbie? Welcome</h2>
                    <div class="mt-2">
                        <p class="lead lh-180">Here's how we make getting a car easy, in a few quick steps.</p>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body pb-5">
                            
                            <h5 class="h4 lh-130 mb-3">Step 1:<br> Choose Your Car</h5>
                             <p class="text-muted mb-0">All you need is to choose particular vehicle of desired category i.e. Private/PCO and checking out with our fleet or your own private insurance.</p> 
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body ">
                            <div class="pt-4">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/document.png" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                            <h5 class="h4 lh-130 mb-3">Documents We Need</h5>
                            <ul>
                                <li>Driving Licence (DVLA).</li>
                                <li>PCO Licence Counterpart + Badge.</li>
                                <li>Recent Utility Bill (Gas, Electric, Water Bill) Dated within last 3 months.</li>
                                <li>Bank Statement (Dated within the last 3 months).</li>
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<section class="slice slice-lg pt-lg-6 pb-0 pb-lg-6 bg-section-secondary">
        <div class="container">
            <!-- Title -->
            <!-- Section title -->
           
            <div class="row">

            	<div class="col-md-4">
                    <div class="card">
                        <div class="card-body pb-5">
                            <div class="pt-4 ">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/create_account1.png" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                            <h5 class="h4">Fully Inclusive</h5>
                           <ul>
                                <li>Cars fully serviced every 10,000 miles.</li>
                                <li>Monthly vehicle health checks.</li>
                                <li>Tyres & Mechanical support.</li>
                                <li>We make sure you receive a fully PCO Licensed vehicle, taxed and ready to drive.</li>
                                <li>Rent is charged on weekly basis.</li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="h4 lh-130 mb-3">Step 2:<br> Access To Dashboard</h5>
                             <p class="text-muted mb-0">Once booking has been approved & confirmed, you will be given access to friendly dashboard where all statistics regarding booking could be seen any time.</p> 
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>
            <!--     <section class="slice slice-lg pt-lg-6 pb-0 pb-lg-6 bg-section-secondary">
        <div class="container">
             Title 
            Section title 
           
            <div class="row">

            	

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body ">
                            
                            <h5 class="h4 lh-130 ">Step 3:<br> Verification.</h5>
                             <p class="text-muted mb-0">We will conduct a brief check on your driving history, if it passes you are good to go to the next step and if it fails we are sorry.</p> 
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body pb-5">
                            <div class="pt-4 pb-5">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/create_account1.png" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                             <h5 class="h4 lh-130 mb-3">Fully Inclusive</h5>
                            <p class="text-muted mb-0">All servicing free. No extra charges even for tyres or brakes</p>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>


    <section class="slice slice-lg pt-lg-6 pb-0 pb-lg-6 bg-section-secondary">
        <div class="container">

           
            <div class="row">

            	

                
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body pb-5">
                            <div class="pt-4 pb-5">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/create_account1.png" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                        <h5 class="h4 lh-130 mb-3">Fully Inclusive</h5>
                            <p class="text-muted mb-0">All servicing free. No extra charges even for tyres or brakes</p> 
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body pb-5">
                            
                            <h5 class="h4 lh-130 mb-3">Step 4:<br>  Pay and Get your Car.</h5>
                             <p class="text-muted mb-0">Once you are at this step ! you are just one step away from your car. You will be charged initial payment of one week which would include [One week rent + comprehensive insurance + Security deposit]. Once your payment is done, we will let you know the car pick-up time by email or call.</p> 
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>
------------>




<section class="slice slice-lg pt-lg-6 pb-0 pb-lg-6 bg-section-secondary">
        <div class="container">

           
            <div class="row">

                

                
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body pb-5">
                    <h1 style="text-align: center;"><b>Support</b></h1>
                        <p style="color:#252e5d;">Booking Service / Support : 9:00am - 6:00pm [Monday - Sunday]
                    <br>Emergency : 24/7 Monday - Sunday<br>
                        Collection Time : 9:00AM - 6:00PM [Monday - Friday]
                    </p> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body pb-5">
                    <h2><b>Collection & Drop off Points</b></h2>
                        <p style="color:#252e5d;">East London Walthamstow , <br>
                            Ilford, Stratford, Leyton, Leytonstone,<br> Seven kings, Romford, Rainham
                    </p> 
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </section>




@include('layouts.footer')
@endsection
@section('scripts')
@endsection


