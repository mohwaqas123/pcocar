@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
        	 @if(session()->has('message'))
			    <div class="alert alert-success">
				   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				   <span aria-hidden="true">&times;</span>
				  </button>
        		  {{ session()->get('message') }}
    			</div>
			@endif 
            <div class="row">
                <div class="col-md-12">
                    <?php
                        $total_price = $invoice->price - $invoice->vat_price;
                    ?>


                    <div class="card-box">
                     <h1>Vehicle Expense Detail</h1>
                      <table id="datatable_tbl" class="table table-striped table-bordered"   >
                        <thead>
                                <tr>
                                <th>Supplier Name</th>
                                <th>Email</th>
                                <th>Phone</th> 
                                <th>Address</th>
                                
                                <th>Vat_Number</th>

                            </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>{{$invoice->supplier->name}}</td>
                                    <td>{{$invoice->supplier->email}}</td>
                                    <td>{{$invoice->supplier->phone}}</td>
                                    <td>{{$invoice->supplier->address}}</td>
                                    <td>{{$invoice->vat_number}}</td>
                                 </tr>
                                </tbody>
                            </table>
                      <table id="datatable_tbl" class="table table-striped table-bordered"   >
                        <thead>
                                <tr>
                                 
                                <th>Invoice Number</th>
                                 </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>{{$invoice->invoice_number}}</td>
                                   
                                  </tr>
                                </tbody>
                            </table>
                        <table id="datatable_tbl" class="table table-striped table-bordered"   >
                        <thead>
                                <tr>
                                <th>ID</th>
                                <th>VRM</th>
                                <th>Details</th>
                                <th>Subtotal</th>
                                <th>20% VAT</th>
                                <th>Price</th>
                                
                                 
                                </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>{{$invoice->id}}</td>
                   <td>{{$invoice->vehicle->licence_plate_number}}</td>
                   <td>{{$invoice->detail}}</td>
                                    <td>{{$total_price}}</td>
                                    <td>{{$invoice->vat_price}}</td>
                                    <td>{{$invoice->price}}</td>
                                    
                                    
                                 </tr>
                                </tbody>
                            </table>
                             


                        
 
         <img src="{{ asset('storage/app/'.$invoice->nid_back) }}"  height="420" width="350">
                                             

   
                        </p>
                    </div>
                 </div>
              
                    	 
                    	
                        
                        
                    
            </div>
        </div>
    </div>
</div>





@endsection
@section('scripts')
@endsection