<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Accedental_records extends Model
{
   
     protected $fillable = [
        'booking_id', 'vehicle_id', 'detail' ,'registeration','int_status','user_id',
 			'part','image','video','price','purchase_id','staff_id',
 			'doc_mot',
        'doc_logback',
        'img_exterior_front',
        'img_exterior_front2',
        'img_exterior_back',
        'img_exterior_dashboard', 'title','partner_id','other','pre_damaged','is_deleted','date',

    ];
    public function user()
    {
        return $this->belongsTo('App\models\User','user_id','id');
    }
    public function vehicle()
    {
        return $this->belongsTo('App\models\Vehicle','vehicle_id','id');
    }
     public function booking()
    {
        return $this->belongsTo('App\models\Booking','booking_id','id');
    }
    public function staff()
    {
        return $this->belongsTo('App\models\Staff','staff_id','id');
    }
    public function Purchase()
    {
        return $this->belongsTo('App\models\Purchase','purchase_id','id');
    }
    public function Accedental_records_image()
    {
        return $this->hasMany('App\models\Accedental_records_image');
       
    }


}
