<div class="left side-menu">

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="#" class="logo">
                <span>
                    <img src="{{ asset('public/admin/assets/images/logo.png') }}" alt="" height="22">  
                </span>
                <i>
                    <img src="{{ asset('public/admin/assets/images/logo.png') }}" alt="" height="28">
                </i>
            </a>
        </div>

        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{ asset('public/theme/assets/images/admin.png') }}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div>
            <h5><a href="#">{{ Auth::guard('admin')->user()->name }}</a> </h5>
            <p class="text-muted">PCOCAR</p>

              <a href="{{ route('adminLogout') }}" class="dropdown-item notify-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>



        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->

                <li>
                    <a href="{{ route('adminDashboard') }}">
                        <i class="fi-air-play"></i><span> Dashboard </span>
                    </a>
                </li>

                

                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Vehicle  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('vehicleList') }}">List Vehicle</a></li>
                        <li><a href="{{ route('vehicleCreate') }}">Add Vehicle</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Make  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('adminmakeList') }}">List Makes</a></li>
                        <li><a href="{{ route('adminmakeCreate') }}">Add Make</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Car Models  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('listcarmodels') }}">List Model</a></li>
                        <li><a href="{{ route('createcarmodel') }}">Add Model</a></li>
                    </ul>
                </li>

               <!--  <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Booking  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('booking') }}">Active Booking</a></li>
                        <li><a href="">Pending Booking</a></li>
                         <li><a href="">Junk Booking</a></li>
                    </ul>
                </li>



 <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Registered Users  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="">List Users</a></li>
                        <li><a href="">Add Vehicle</a></li>
                    </ul>
                </li>-->
                  <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Booking  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('vehicle_booking') }}">Current Booking</a></li>
                        <li><a href="{{ route('vehicle_booking_old') }}">Completed Booking</a></li>
                         
                        
                    </ul>
                </li>
                  <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Partner Accounts  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('partner_list') }}">Parnters Account List</a></li>
                        <li><a href="{{ route('partner_add') }}">Parnters Account Add</a></li>
                        
                    </ul>
                </li>

                   <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Drivers Accounts  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('admindriver') }}">Drivers Account List</a></li>
                        <li><a href="{{ route('admindriveradd') }}">Drivers Account Add</a></li>
                        
                    </ul>
                </li>
<!-- 
                 <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Promotions  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false"> 
                        <li><a href="{{ route('adminyearCreate') }}">Add Promotions</a></li>
                    </ul>
                </li>

                 <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Accedental Records  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false"> 
                        <li><a href="{{ route('adminyearCreate') }}">Add Accedental</a></li>
                    </ul>
                </li>


                 <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Insurance Records  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false"> 
                        <li><a href="{{ route('adminyearCreate') }}">Add Insurance</a></li>
                    </ul>
                </li> -->
                  <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span>Discount</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('code/generate') }}">Code Generate</a></li>
                        <li><a href="{{ route('code/generate/list') }}">Code Generate List</a></li>

                       
                    </ul>
                </li>


            </ul>

        </div>
        <!-- Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>


