<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Webpixels">
    <title>PCOCAR </title>
    <!-- Preloader -->
    <style>
        @keyframes hidePreloader {
            0% {
                width: 100%;
                height: 100%;
            }

            100% {
                width: 0;
                height: 0;
            }
        }

        body>div.preloader {
            position: fixed;
            background: white;
            width: 100%;
            height: 100%;
            z-index: 1071;
            opacity: 0;
            transition: opacity .5s ease;
            overflow: hidden;
            pointer-events: none;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        body:not(.loaded)>div.preloader {
            opacity: 1;
        }

        body:not(.loaded) {
            overflow: hidden;
        }

        body.loaded>div.preloader {
            animation: hidePreloader .5s linear .5s forwards;
        }



    </style>
    <script>
        window.addEventListener("load", function() {
            setTimeout(function() {
                document.querySelector('body').classList.add('loaded');
            }, 300);
        });
    </script>
    <!-- Favicon -->


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <link rel="icon" href="{{ asset('public/theme3/assets/img/brand/favicon.png')}}" type="image/png"><!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/theme3/assets/libs/@fortawesome/fontawesome-free/css/all.min.css')}}">

    <!-- Quick CSS -->
    <link rel="stylesheet" href="{{ asset('public/theme3/assets/css/quick-website.css')}}" id="stylesheet">


</head>

 
@yield('content')

</html>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

 

    
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" />
   
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>

 
    $( function() {
    $( "#booking_start_date" ).datepicker(
      {
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate:new Date(),
       maxDate: +14,
     });
  });



//======Jquery code calcalate drop off date=============================
$(function(){
setTimeout(function(){
$(".date_appy").on("change", function(e) { 

   var weeks = $('#weeks').val();
   if(weeks==1) weeks = 4;
   var days =7 * weeks;
    
   var date = new Date($("#booking_start_date").val()); 
   date.setDate(date.getDate() + days); 
   var final_date = date.toInputFormat();
   $('#hid_drop_off').val(final_date); 
   $('#drop_off').text(final_date); 
}); 
    Date.prototype.toInputFormat = function() {
       var yyyy = this.getFullYear().toString();
       var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
       var dd  = this.getDate().toString();
       return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
    };

},1000);
});
 //===============drop off ===============================================


  $( function() {
    $( "#datepicker" ).datepicker(
      {
       dateFormat:'dd-mm-yy', 
       changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
       minDate: new Date(1950,1,31), 
       maxDate: new Date(1997,11,30)
     });
  });

$( function() {
    $( "#datepicker2" ).datepicker(
      {
       dateFormat:'dd-mm-yy', 
       changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
       minDate: new Date(1950,1,31), 
       maxDate: new Date(1997,11,30)
     });
  });


  </script>

<!-- range input slider js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
<script> 

 

     function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
        }
 
 

   function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


 
//============detail page on change of duration booking

  $('#month').change(
    function(){ 
         calculate_booking();
         
    });

function calculate_booking(){ 
  var url_call = "{!! url('/') !!}/calculate_booking";
    var duration = $('#month').val();
    var vehicle_id = $('#vehicle_id').val();
    var discount = $('#discount').val();
    $('#weeks').val( $('#month').val() );
 
            $.ajax({
                type : 'get',
                url  : url_call,
                data: {duration:duration, vehicle_id:vehicle_id , discount:discount}, 
                contentType: 'application/json; charset=utf-8',
                success :  function(data){
                    
                  $("#booking_cal").empty();
                  $("#booking_cal").append(data);
                },
                error: function() {
                  alert(data);
                }

              });//ajax
}
//=================================================================


$( document ).ready(function() {

 

$('#staff_login').click(function(e) {  
        e.preventDefault();
        $.ajax({
                type: "get", 
               // var url = "{!! url('/') !!}/stafflogin";
                url: "{!! url('/') !!}/stafflogin",
                data: { 
                  staff_email: $("#staff_email").val(), staff_password: $("#staff_password").val() ,
                },
                contentType: 'application/json; charset=utf-8',
                success:function(result)
                {
                     if(result == 2){
                      $('.staff-msg').show();
                     }
                     else{
                      location.reload(true);
                     }
                }
         });
      });
 
//=================================================================


 
          








 
$(function() {
$('#chk').click(function() {
  if ($(this).is(':checked')) {
    $('#id_of_your_button').removeAttr('disabled');
    $('#submit').removeAttr('disabled');
  } else { 
    $('#id_of_your_button').attr('disabled', 'disabled');
    $('#submit').attr('disabled', 'disabled');

  }
});
});




//==========Price Slider ================
 $("#slider-range" ).slider({  
      range: true,
      min: 0,
      max: 1500,
      values: [ 1, 250 ],
      change: function( event, ui ) {   
        $( "#amount" ).html( "£" + ui.values[ 0 ] + " - £" + ui.values[ 1 ] );
    $( "#amount1" ).val(ui.values[ 0 ]);
    $( "#amount2" ).val(ui.values[ 1 ]); 
    show_v();
        }

    });
//=============end price slider ==================



   
   show_v();


 
$('#duration').change(
    function(){
         show_v(); 
    });
 

$('#car_make').change(
    function(){
         show_v();
         
    })
 
$('#car_model').change(
    function(){
         show_v();
         
    });
  $('#year').change(
    function(){
         show_v();
         
    });

   $('#fuel_type').change(
    function(){
         show_v();
         
    });
   $('#filter').change(
    function(){
         show_v(); 
    });


   $('#vehicle_category').change(
    function(){
         show_v(); 
    });

 
 $( "#btn_searc_submit" ).click(function() {
      show_v(); 
});



function show_v(){  
    var url_call = "{!! url('/') !!}/get_all_vehicles";
    var duration = $('#duration').val();
    var car_make = $('#car_make').val();
    var car_model = $('#car_model').val(); 
    var year = $('#year').val(); 
    var fuel_type = $('#fuel_type').val(); 
    var filter = $('#filter').val();
    var amount1 = $('#amount1').val();
    var amount2 = $('#amount2').val();
    var licence_plate_number = $('#licence_plate_number').val();
    var vehicle_category = $('#vehicle_category').val();

 
 
            $.ajax({
                type : 'get',
                url  : url_call,
                data: { id:2,vehicle_category:vehicle_category,licence_plate_number:licence_plate_number, duration:duration, car_make:car_make , car_model:car_model, year:year , fuel_type:fuel_type , filter:filter, amount1:amount1, amount2:amount2 },
            //    data: { ticket_id: ticket_id, status_value: status_value },
                contentType: 'application/json; charset=utf-8',
                 beforeSend: function(){
                  $("#vehicle_list").hide();
                   //$('.loader').show();
                  },
                complete: function(){
                    $('.loader').hide();
                     $("#vehicle_list").show();
                },
                success :  function(data){
                   //alert(data);
                      if(data == '<div class="row"></div>'){
                         $("#vehicle_list").empty();
                         $("#vehicle_list").append('<p>Vehicle is not found</p>');
                      } 
                      else{
                   $("#vehicle_list").empty();
                   $("#vehicle_list").append(data);
                 }
                }

              });//ajax
}               
 

 


 $('#update').click(function() {  
        $('#v2').show();
        $('#v1').hide();

  });




  $range = $('input');
  $display = $('code');
  
  // init display text
  $display.text(10000);
  
  // use lodash debounce to display the final number
    $range.on('change', _.debounce(function() {
    $display.text($(this).val());
  }, 250));

});

function showVal(a){
    alert(a);
}
</script>
 <!--    <script src="{{ asset('public/theme3/assets/libs/jquery/dist/jquery.min.js')}}"></script> -->
    <script src="{{ asset('public/theme3/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('public/theme3/assets/libs/svg-injector/dist/svg-injector.min.js')}}"></script>
    <script src="{{ asset('public/theme3/assets/libs/feather-icons/dist/feather.min.js')}}"></script>
    <!-- Quick JS -->
    <script src="{{ asset('public/theme3/assets/js/quick-website.js')}}"></script>
    <!-- Feather Icons -->
    <script>
        feather.replace({
            'width': '1em',
            'height': '1em'
        })
    </script>


    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRcegjt6E0Gie8ud7W3rsRgg3mBlGPqso&amp;libraries=places"></script>
        

    <!-- contact us page show the Addresss in the city -->     
<script>
  google.maps.event.addDomListener(window, 'load', initialize);
    function initialize() {
      var input = document.getElementById('autocomplete');
      var options = {
         // types: ['(cities)'],
         componentRestrictions: {country: 'uk'}//uk only
    };
      var autocomplete = new google.maps.places.Autocomplete(input,options);
      autocomplete.addListener('place_changed', function () {
      var place = autocomplete.getPlace();
      // place variable will have all the information you are looking for.
      $('#lat').val(place.geometry['location'].lat());
      $('#long').val(place.geometry['location'].lng());
    });
  }
  
</script>
