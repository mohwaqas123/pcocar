@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<section class="slice slice-lg pt-lg-12 pb-0 pb-lg-12 bg-section-secondary">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <form action="{{ route('browse') }}" method="get" ifor 1 Monthd="frm_duration">
          {{csrf_field()}}
          <div class="search_box">
            <h3>Search</h3>
            <div class="form-group">
              <label for="inputEmail">Booking Duration</label>
              <p class="dis_duration">(Discount start from 12 week)</p>
              <select name="duration" id="duration" class="form-control ">
                <option @if($duration && $duration==1) selected="selected" @endif value="1">Any</option>
                <option @if($duration && $duration==4) selected="selected" @endif value="4">4 Weeks</option>
                <option @if($duration && $duration==8) selected="selected" @endif value="8">8 Weeks</option>
                <option @if($duration && $duration==12) selected="selected" @endif value="12">12 Weeks</option>
                <option @if($duration && $duration==16) selected="selected" @endif value="16">16 Weeks</option>
                <option @if($duration && $duration==20) selected="selected" @endif value="20">20 Weeks</option>
                <option @if($duration && $duration==24) selected="selected" @endif value="24">24 Weeks</option>
                <option @if($duration && $duration==26) selected="selected" @endif value="28">28 Weeks</option>
                <option @if($duration && $duration==28) selected="selected" @endif value="32">32 Weeks</option>
                <option @if($duration && $duration==32) selected="selected" @endif value="36">36 Weeks</option>
                <option @if($duration && $duration==40) selected="selected" @endif value="40">40 Weeks</option>
                <option @if($duration && $duration==44) selected="selected" @endif value="44">44 Weeks</option>
                <option @if($duration && $duration==48) selected="selected" @endif value="48">48 Weeks</option>
                <option @if($duration && $duration==52) selected="selected" @endif value="52">52 Weeks</option>
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail">Vehicle Make</label>
              <select name="car_make" id="car_make" onchange="get_model(this.value)" class="form-control">
                <option value="">Any</option>
                @foreach($car_make as $make)
                <option value="{{$make->id}}">{{$make->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail">Vehicle Model</label>
              <select name="car_model" id="car_model" class="form-control">
                <option value="">Any</option>
                @foreach($car_model as $model)
                <option value="{{$model->id}}">{{$model->name}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="inputEmail">Vehicle Year</label>
              <select name="year" id="year" class="form-control">
                <option value="">Any</option>
                <option value="2020">2020</option>
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
              </select>
            </div>
            
              <input  name="btn_submit" type="button" onclick="window.location='{{ url('/')}}/browse'" class="btn-primary btn-sm" value="Reset" />
          </div>  </form>
          </div>
          <div class="col-md-9">
            <div class="row">
              <div class="col-md-10"> <h2>Vehicles List  </h2></div>
              <div class="col-md-2" align="right">
                  <select name="filter" id="filter" class="form-control">
                  <option value="">Filter</option>
                  <option value="Desc">Price High to Low</option>
                  <option value="Asc">Price Low to High</option> 
                </select>  
              </div>
            </div>
            <!--
            <div class="row">
              @foreach($vehicles as $cab)
              @php
              $date1 = $cab->date_from;
              $date2 =$cab->date_to;
              $ts1 = strtotime($date1);
              $ts2 = strtotime($date2);
              $year1 = date('Y', $ts1);
              $year2 = date('Y', $ts2);
              $month1 = date('m', $ts1);
              $month2 = date('m', $ts2);
              $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
              @endphp
              <div class="col-md-4">
                <div class="card">
                  <div  class="float-right text-right margin-bottom px-3">
                    <span class="bold">£{{getCalculatDiscountPrice($duration,$cab->price)}}<span class="inner">/Week</span></span>
                    <p class="mt-0">for {{$diff}} Months</p>
                  </div>
                  
                  @php
                  if($duration && $duration!='')
                  $duration = $duration  ;
                  else
                  $duration =1;
                  @endphp
                  <a href="{{ route('car_detail', ['id' => $cab->id]) }}">
                  <img src="{{ asset('storage/app/'.$cab->image) }}" class="img-thumbnail" alt="...">  </a>
                  
                  <div class="ml-2">
                    <span class="car-make custom_font">{{$cab->year}} {{$cab->car_make->name}}</span><br>
                    {{$cab->car_model->name}} <span class="badge badge-pill">
                      @php
                      if($cab->fuel_type==0) echo "Diesel";
                      if($cab->fuel_type==1) echo "Petrol";
                      if($cab->fuel_type==2) echo "Gasoline";
                      @endphp
                    </span>-<span class="badge badge-pill">
                    @php if($cab->transmission==1) echo "Manual"; else echo "Automatic"; @endphp
                  </span>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          
          {!! $vehicles->links() !!} -->
          
 


<div class="loader" align="center" >
  <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
 Please Wait Loading...
</div>

          
          <div class="" id="vehicle_list">
            
          </div>
          
          
        </div>
      </div>
      
    </div>
  </section>
  
  @include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<style>
  .spinner-grow{display: none;}
.img-thumbnail{height: 220px!important;}
.inner{font-size: 10px;}
.custom_font{    font-family: inherit;}
.car-make{font-size: 16px; font-weight: bold;}
.small{font-size:8px; background: red;
border-radius: 5px;
padding: 5px;
color: #fff;
margin-right: 10px;
}
.loader{ background:#eee}
label{margin-bottom:0px!important;}
.search_box{  /*  border: 1px solid #eee;*/
padding: 6px;
box-shadow: brown;
font-size: 12px;
}
.booknow{font-size: 0.902rem; height: 24px;
padding: 3px;}
.cartype{font-weight: bold; font-size: .900rem}
.dis_duration{margin-top: -3px;
    margin-bottom: -2px;
    color: blue;
    font-weight: bold;}
.badge{    padding: .05rem .5rem!important; 
    border-radius: .175rem!important;}    
</style>
<script>
function get_model(id){
// e.preventDefault();
var url_call = "{!! url('/') !!}/get_models";
$.ajax({
type : 'get',
url  : url_call,
data: { id:id  },
//    data: { ticket_id: ticket_id, status_value: status_value },
contentType: 'application/json; charset=utf-8',
success :  function(data){
$("#car_model").empty();
$("#car_model").append($("<option value=''>Any</option>"));
$.each(data, function () {
$("#car_model").append($("<option></option>").val(this['id']).html(this['name']));
});
}
});//ajax
}
</script>