<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'percentdiscount', 'discountcode', 'status','date_from',
        'date_to','Title',
    ];
}
