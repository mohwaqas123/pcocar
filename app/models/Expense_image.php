<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Expense_image extends Model
{
	protected $table = 'expense_image';
    protected $fillable = [
        'expence_id','images'
    ];
}
