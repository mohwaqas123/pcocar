@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page">
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-right-menu float-right mb-0">
                <li class="hide-phone app-search">
                    <form>
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
             
            </ul>
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    <div class="page-title-box">
                        <h4 class="page-title">Dashboard </h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Welcome to PCOCAR Admin!</li>
                        </ol>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
    <!-- Top Bar End -->
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="card-box">
                        <h3>Vehicle Management</h3>
                        <p>PCO: 500</p>
                        <p>Private: 500</p>

                         <input onclick="window.location='{{ url("admin/vehiclesList") }}'"  type="button" class="btn btn-primary" value="View List">
                         <input onclick="window.location='{{ url("admin/add_vehicle") }}'"  type="button" class="btn btn-primary" value="Add New">
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-4">
                    <div class="card-box">
                        <h3>Makes</h3>
                        <p>Total: 500</p>
                        <p>Active: 500</p>
                         
                         <input type="button" onclick="window.location='{{ url("admin/make") }}'"  class="btn btn-primary" value="View List">
                         <input type="button" onclick="window.location='{{ url("admin/make/create") }}'"  class="btn btn-primary" value="Add New">
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-4">
                    <div class="card-box">
                        <h3>Models</h3>
                        <p>Total: 500</p>
                        <p>Active: 500</p>
                         

                         <input type="button" onclick="window.location='{{ url("admin/car-models") }}'"  class="btn btn-primary" value="View List">
                         <input type="button" onclick="window.location='{{ url("car-models/create") }}'"  class="btn btn-primary" value="Add New">
                        <!-- end row -->
                    </div>
                </div>


 
                
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title">Booking Statistics</h4>
                        <div id="website-stats" style="height: 350px;" class="flot-chart mt-5"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card-box">
                        <h4 class="header-title">Sales Overview</h4>
                        <div id="combine-chart">
                            <div id="combine-chart-container" class="flot-chart mt-5" style="height: 350px;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                        <h4 class="header-title mb-3">Latest Booking</h4>
                        <div class="table-responsive">
                            <table class="table table-hover table-centered m-0">
                                <thead>
                                    <tr>
                                        <th>Car</th>
                                        <th>Name</th>
                                        <th>Car</th>
                                        <th>For Months</th>
                                        <th>Reserved in orders</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <img src="{{ asset('public/theme/assets/images/2.png') }}" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm" />
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">Tomaslau</h5>
                                            <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                        </td>
                                        <td>
                                            <i class="mdi mdi-currency-btc text-primary"></i> BMW
                                        </td>
                                        <td>
                                            12 Months
                                        </td>
                                        <td>
                                            $4000
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset('public/theme/assets/images/3.png') }}" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm" />
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">Erwin E. Brown</h5>
                                            <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                        </td>
                                        <td>
                                            <i class="mdi mdi-currency-btc text-primary"></i> BMW
                                        </td>
                                        <td>
                                            12 Months
                                        </td>
                                        <td>
                                            $4000
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset('public/theme/assets/images/2.png') }}" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm" />
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">Margeret V. Ligon</h5>
                                            <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                        </td>
                                        <td>
                                            <i class="mdi mdi-currency-btc text-primary"></i> BMW
                                        </td>
                                        <td>
                                            12 Months
                                        </td>
                                        <td>
                                            $4000
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset('public/theme/assets/images/3.png') }}" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm" />
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">Jose D. Delacruz</h5>
                                            <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                        </td>
                                        <td>
                                            <i class="mdi mdi-currency-btc text-primary"></i> BMW
                                        </td>
                                        <td>
                                            12 Months
                                        </td>
                                        <td>
                                            $4000
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="{{ asset('public/theme/assets/images/2.png') }}" alt="contact-img" title="contact-img" class="rounded-circle thumb-sm" />
                                        </td>
                                        <td>
                                            <h5 class="m-0 font-weight-normal">Luke J. Sain</h5>
                                            <p class="mb-0 text-muted"><small>Member Since 2017</small></p>
                                        </td>
                                        <td>
                                            <i class="mdi mdi-currency-btc text-primary"></i> BMW
                                        </td>
                                        <td>
                                            12 Months
                                        </td>
                                        <td>
                                            $4000
                                        </td>
                                        <td>
                                            <a href="#" class="btn btn-sm btn-custom"><i class="mdi mdi-plus"></i></a>
                                            <a href="#" class="btn btn-sm btn-danger"><i class="mdi mdi-minus"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end row -->
            </div> <!-- container -->
            </div> <!-- content -->
            
        </div>
        @endsection
        @section('scripts')
        @endsection