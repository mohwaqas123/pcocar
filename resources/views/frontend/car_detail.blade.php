@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<form action="{{route('booking')}}" method="get" id="booking_frm">
  {{ csrf_field() }}
  <input type="hidden" value="{{$vehicles->id}}" name="id" />
 
  <input type="hidden" value="{{$vehicles->id}}" name="vehicle_id" id="vehicle_id" />
  <input type="hidden" value="{{$discount}}" name="discount" id="discount" />

  <input type="hidden" value="{{$setting->flat_discount_type}}" name="flat_rate_discount" id="flat_rate_discount" />
  <input type="hidden" value="{{$setting->promotion_type}}" name="promotion_discount" id="promotion_discount" />
   <input type="hidden" value="1" name="weeks" id="weeks" />
 


  <input type="hidden" value="1" name="step1" id="step1" />
  <section class="slice slice-lg pt-lg-12 pb-0 pb-lg-12 bg-section-secondary">
    <div class="container">
      <div class="row">
        
        <div class="col-md-7">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <!--   <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol> -->
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{ asset('storage/app/'.$vehicles->image) }}" class="img-responsive w-100" alt="" />
              </div>
              
              @foreach($vehicles->vehicle_images as $img)
              <div class="carousel-item">
                <img src="{{ asset('storage/app/'.$img->images) }}" class="img-responsive w-100" alt="..." />
              </div>
              @endforeach
              
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          
          <div class="styles__Container-sc-1fgs5wt-0 fEoxbA">
            <div class="desktop">
              <div class="styles__TitleContainer-sc-1fgs5wt-2 bbevCC d-flex justify-content-between">
                <div class="styles__Title-sc-1fgs5wt-1 jGBSbQ px-3">Features
                </div>
              </div>
              <div class="row">
                @php
                if($vehicles->featurs && $vehicles->featurs!='null'){
                $all_features = json_decode($vehicles->featurs);
                foreach($all_features as $features){
                @endphp
                <div class="col-md-6 kFafpT d-flex align-items-center"><i class="fa fa-check custom_style" aria-hidden="true"></i>{{$features}}</div>
                @php
                }
                }
                @endphp
                
              </div>
            </div>
          </div>
          
          
        </div>
       
        <div class="col-md-5 right-area">
          <h5 class="car_detail_name">{{$vehicles->car_make->name}}  {{$vehicles->car_model->name}} {{$vehicles->year}}  </h5>
          <!--  <h3 class="car_sub_detail">{{$vehicles->name}}</h3>
          <p class="small">From {{$vehicles->date_from}}</p> -->
          <div class="form-group">
            <label for="inputEmail">Booking Duration</label>
            
            <select name="month" id="month" class="form-control date_appy ">
              <option @if($duration && $duration==4) selected="selected" @endif value="4">4 Weeks</option>
              <option @if($duration && $duration==5) selected="selected" @endif value="5">5 Weeks</option>
                <option @if($duration && $duration==6) selected="selected" @endif value="6">6 Weeks</option>
                <option @if($duration && $duration==7) selected="selected" @endif value="7">7 Weeks</option>
                <option @if($duration && $duration==8) selected="selected" @endif value="8">8 Weeks</option>
                <option @if($duration && $duration==9) selected="selected" @endif value="9">9 Weeks</option>
                <option @if($duration && $duration==10) selected="selected" @endif value="10">10 Weeks</option>
                <option @if($duration && $duration==11) selected="selected" @endif value="11">11 Weeks</option>
                <option @if($duration && $duration==12) selected="selected" @endif value="12">12 Weeks </option>
                
              <option @if($duration && $duration==16) selected="selected" @endif value="16">16 Weeks(Discount 5%)</option>
              <option @if($duration && $duration==20) selected="selected" @endif value="20">20 Weeks(Discount 5%)</option>
              <option @if($duration && $duration==24) selected="selected" @endif value="24">24 Weeks(Discount 10%)</option>
            <!--   <option @if($duration && $duration==26) selected="selected" @endif value="28">28 Weeks</option>
              <option @if($duration && $duration==28) selected="selected" @endif value="32">32 Weeks</option>
              <option @if($duration && $duration==32) selected="selected" @endif value="36">36 Weeks</option>
              <option @if($duration && $duration==40) selected="selected" @endif value="40">40 Weeks</option>
              <option @if($duration && $duration==44) selected="selected" @endif value="44">44 Weeks</option>
              <option @if($duration && $duration==48) selected="selected" @endif value="48">48 Weeks</option>
              <option @if($duration && $duration==52) selected="selected" @endif value="52">52 Weeks</option> -->
            </select>
           
              
 
          </div>
          <div class="form-group">
            <label for="inputEmail">Miles Per Month: </label>3000
            
          <!--   <select name="miles" id="miles" class="form-control ">
              
              <option value="800">800</option>
              <option value="1200">1200</option>
              <option value="1600">1600</option>
              <option value="2000">2000</option>
              <option value="3000" selected>3000</option>
            </select> -->
          </div>


          <div id="booking_cal">
            @php
            
            
           
            if($setting->flat_discount_type==1){
            $v_price = $vehicles->price - ($vehicles->price  *  $flat_discount_rate/100 );
            $price_actual =$vehicles->price;
            $flat_rate_text = "(Flat Discount Applied ".$flat_discount_rate.'%)';
          }
          else{
          $v_price = $vehicles->price;
          $price_actual ='';
          $flat_rate_text='';
        }

        $total_with_dis = $v_price - $discount;


        
            @endphp
            <input type="hidden" value="{{$get_discount_duration_price}}" name="price" id="price" />
            <table style="width: 100%">
              <tr>
                <td>Price per week {{$flat_rate_text}}</td>  <td><del>{{$price_actual}}</del> £<sapn>{{$price}}</sapn> </td>
              </tr>
               
              <tr>
                <td>Total no of Week</td><td>{{$duration}}</td>
              </tr>
              <tr>
                <td>&nbsp; </td>  <td>&nbsp; </td>
              </tr>
              <tr>
                <td>Multi Week Discount*</td>   <td><span>{{$disc_percent}}%</span></td>
              </tr>
              <tr>
                <td>Price after Discount (Per Week)</td>   <td>£<span id="final_price">{{$get_discount_duration_price}}</span> </td>  
              </tr>
              
              <tr>
                <td>&nbsp; </td>  <td>&nbsp; </td>
              </tr>
              <tr id="insurance_cost_tr">
                <td>Insurance Cost (Per week)</td>   <td>£{{env('INSURANCE_CODE')}}</td>
              </tr>
              <tr id="insurance_deposit_tr">
                <td>Security Deposit</td>   <td>£{{env('DEPOSIT_SECURITY')}}</td>
              </tr>
              <tr>
                <td>&nbsp; </td>  <td>&nbsp; </td>
              </tr>
            </table>
            @php
            $insurance_total  =   env('INSURANCE_CODE');
            $grand_total =  $total_with_dis + $insurance_total + env('DEPOSIT_SECURITY');
            @endphp
           <p class="price" align="center">Total: £ <spam id="price_final_total">{{$grand_total}}</spam> </p>
             <input type="hidden" value="{{$grand_total}}" name="price_final_total_book" id="price_final_total_book" />
          </div>
<!-- promotion discount -->
          @if($setting->promotion_type==1)
             <div id="frm_discount">     
             {{csrf_field()}}
               @if($errors->any())
                <p class="error">{{$errors->first()}}</p>
               @endif
                 
                  <div class="row">
                    <div class="col-md-9"> 
                      <input maxlength="6"   type="text" value="" placeholder="Promo Code" class="form-control " name="discountcode" id="discountcode"   />
                    </div>
                      <div class="col-md-3">
                      <input type="button"   onclick="apply_discount()" id="btn_app_discount" class="btn-sm btn-primary" value="Apply" /> 

                      
                    </div>  
          </div> 

  </div> 

<div class="alert alert-success" role="alert"> 
  Thanks! Promo code  is applied
</div>
<div class="alert alert-danger" role="alert">
  Sorry! Promo Code is not valid, please try again
</div>
                    
     @endif




              @php
               $promo_text='';
           
              if(session('permotion_discount')){
              $price = $vehicles->price -  (($vehicles->price * $promotion->percentdiscount)/100);
              $promo_text = '<p class="discount_applied">'.$promotion->percentdiscount.'% Discount Promo Applied</p> ';
            }
              $price_total = $grand_total;

   @endphp
      


           
          
   <!--            Total £{{ number_format($price_total,2) }}
              @php echo $promo_text;  @endphp -->




          <br>
          <input type="checkbox" name="insurance" class="form-group insurance" value="1"> Private insurance &nbsp;

           
           @if(session()->get('staff_id') == 1)  
          <input type="checkbox" name="deposit" class="form-group deposit" value="2"> Without Deposit &nbsp;  
          @endif
          <div class="tab-example-result">
            <div id="accordion-1" class="accordion accordion-stacked">
              <!-- Accordion card 1 -->
              <div class="card">
                <div class="card-header py-4" id="heading-1-1" >
                <h6 class="mb-0 company_insurance "><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file mr-3"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>Standard Fleet Insurance</h6>
                <h6 class="mb-0 company_insurance_1" id="customer_insurance_style"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file mr-3"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>Private Insurance
                 </h6>
              </div>
              <div id="collapse-1-1"  aria-labelledby="heading-1-1" data-parent="#accordion-1" style="">
                <div class="card-body">
                  <div id="checkout">
                    <div id="options"> 
                      <p>Please check the following to continue</p>
                      <p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes ck"> Age: 23+ </p>
                      <p class="record_table"><input type="checkbox" class="form-group checkboxes"> Years license held: 3</p>
                      <p class="record_table"><input type="checkbox" class="form-group checkboxes"> license type: UK full </p>
                      <p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes ck"> At Fault Claims: Min 1 in last 3 years</p>
                      <p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes ck"> Non-fault Claims: max 2 in last 3 years</p>
                      <p class="record_table"><input type="checkbox" class="form-group checkboxes"> Driving Restriction: UK only</p>
                      <p class="record_table"><input type="checkbox" class="form-group checkboxes"> DVLA license points: Max 6 points</p>
                      <p class="record_table"><input type="checkbox" class="form-group checkboxes"> No motoring disqualification in the last 5 years</p>
                        <p class="record_table"><input type="checkbox" checked="checked" class="form-group checkboxes chk"> Max vehicle value £25,000 </p><br>
                      <input type="hidden" value="{{env('APP_PERCENT_MONTH')}}" id="app_percent_month" />
                     
                      
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
           <br>
        Booking Start Date:
         <input type="text"  required="required"  class="form-control date_appy" value="<?php echo date('Y-m-d'); ?>" autocomplete="off" placeholder="" name="booking_start_date" id="booking_start_date" />  

  <?php
  if(!$duration) 
      $duration = 4;

  $start_date = date('Y-m-d');  
$date = strtotime($start_date);
$add_weeks = "+".$duration ." week";
$date = strtotime($add_weeks, $date);
$date_drop_off = date('Y-m-d', $date);               
?>

        <b>Drop off Date:</b>
         <span id="drop_off" value="$date_drop_off">{{$date_drop_off}}</span>
          <input type="hidden" value="{{$date_drop_off}}" name="hid_drop_off" id="hid_drop_off"> 




        </div> 
        <br>
         
        <input class="form-control  btn-secondary"   id="submitbutton" disabled type="submit" value="Continue">  
        <p id="testremoved">Please check all the fields to continue.</p>   
        <br />
        <input value=" Back" type="button" onclick="window.history.go(-1); return false;" class="form-control  btn-secondary"  > 
        <p>Note: Discount will be applied if booking is more than 3 months.</p>
      </div>
    </div>
    
  </div>
</section> </form>
@include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
     $(document).ready(function() { 







$(".insurance").change(function() {
    var insurance_cost = {{env('INSURANCE_CODE') }}; 
    if(this.checked) {         
        $('#insurance_cost_tr').hide();
        // $('#insurance_deposit_tr').hide();
        var price_final_total = parseInt($('#price_final_total').text()) - parseInt(insurance_cost);  
        $("#price_final_total").text(parseFloat(price_final_total).toFixed(2));
         $('.ck').prop( "checked", true ); 
         $(".chk_hide").hide();
         $(".company_insurance").hide();
         $(".company_insurance_1").show('slow');
         // $('.ck').prop( "checked" , false); 
  }
    else{
        $('#insurance_cost_tr').show();
        // $('#insurance_deposit_tr').show();
        var price_final_total = parseInt($('#price_final_total').text()) + parseInt(insurance_cost);    
        $("#price_final_total").text(parseFloat(price_final_total).toFixed(2));
        $(".chk_hide").show('slow');
        $(".company_insurance_1").hide('slow');
         $(".company_insurance").show();
         $('.ck').prop( "checked",false); 
  /* var options = '<p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes"> Age: 23+ </p> <p class="record_table"><input type="checkbox" class="form-group checkboxes"> Years license held: 3</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> license type: UK full </p><p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes"> At Fault Claims: Min 1 in last 3 years</p><p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes"> Non-fault Claims: max 2 in last 3 years</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> Driving Restriction: UK only</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> DVLA license points: Max 6 points</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> No motoring disqualification in the last 5 years</p><p class="record_table"><input type="checkbox" checked="checked" class="form-group checkboxes chk"> Max vehicle value £25,000 <br><input type="hidden" value="'+{{env('APP_PERCENT_MONTH')}}+'" id="app_percent_month" />'; 
        $('#options').empty();
        $('#options').append(options);*/

    }
});







$(".deposit").change(function() {
    var insurance_cost = {{env('DEPOSIT_SECURITY') }}; 
    if(this.checked) {         
        $('#insurance_deposit_tr').hide();
        var price_final_total = parseInt($('#price_final_total').text()) - parseInt(insurance_cost);  
        $("#price_final_total").text(parseFloat(price_final_total).toFixed(2));
         // $('.ck').prop( "checked", true ); 
         // $(".chk_hide").hide();
         // $(".company_insurance").hide();
         // $(".company_insurance_1").show('slow');
         // $('.ck').prop( "checked" , false); 
  }
    else{
        $('#insurance_deposit_tr').show();
        var price_final_total = parseInt($('#price_final_total').text()) + parseInt(insurance_cost);    
        $("#price_final_total").text(parseFloat(price_final_total).toFixed(2));
      /*   $(".chk_hide").show('slow');
        $(".company_insurance_1").hide('slow');
         $(".company_insurance").show();
         $('.ck').prop( "checked",false); 
  var options = '<p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes"> Age: 23+ </p> <p class="record_table"><input type="checkbox" class="form-group checkboxes"> Years license held: 3</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> license type: UK full </p><p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes"> At Fault Claims: Min 1 in last 3 years</p><p class="record_table chk_hide"><input type="checkbox" class="form-group checkboxes"> Non-fault Claims: max 2 in last 3 years</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> Driving Restriction: UK only</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> DVLA license points: Max 6 points</p><p class="record_table"><input type="checkbox" class="form-group checkboxes"> No motoring disqualification in the last 5 years</p><p class="record_table"><input type="checkbox" checked="checked" class="form-group checkboxes chk"> Max vehicle value £25,000 <br><input type="hidden" value="'+{{env('APP_PERCENT_MONTH')}}+'" id="app_percent_month" />'; 
        $('#options').empty();
        $('#options').append(options);*/

    }
});
$('#btn_app_discount').click(function(event) {  
  // alert('sdasdasda');
          $(".insurance").prop("checked", false);
          $(".deposit").prop("checked", false);
         if($('#discountcode').val()=='')
             $('.alert-danger').show();
         var url_call = "{!! url('/') !!}/promotion_apply";
        var discountcode = $('#discountcode').val();  
                  $.ajax({
                      type : 'get',
                      url  : url_call,
                      data: { discountcode:discountcode },
                  //    data: { ticket_id: ticket_id, status_value: status_value },
                      contentType: 'application/json; charset=utf-8',
                      success :  function(data){
                           if(data==2){
                            $('.alert-danger').show();
                             $('.alert-success').hide();
                              $('#frm_discount').show();
                           } else {
                            $('.alert-success').show();
                             $('.alert-danger').hide();
                             $('#frm_discount').hide();
                       // alert($('#final_price').text());

             if( $('#discount').val()!='' || $('#discount').val()!=0 ){
                        final_price_tot = $('#final_price').text();
                        // alert(final_price_tot);
                        // -  $('#final_price').text() * $('#discount').val() / 100;
                       }
                       else
                       {
                        final_price_tot = $('#final_price').text();
                       }
                       // alert(final_price_tot);
          var  disct  = final_price_tot -  final_price_tot * data / 100;
          // alert(disct);
           $("#price").val(parseFloat(disct).toFixed(2));
           $("#final_price").text(parseFloat(disct).toFixed(2));
             var final_total_price = disct + {{env('INSURANCE_CODE')}} + {{env('DEPOSIT_SECURITY')}};
                                  $("#price_final_total").text(parseFloat(final_total_price).toFixed(2));

                                  $("#price_final_total_book").val(parseFloat(final_total_price).toFixed(2));
                                  $("#promotion_type").val(1);
                                  $("#month").prop("disabled", true);

                                   }

                    }
              });//ajax




      });


      $('input:checkbox').prop('checked', false); //when page load checkboxes unchecked
    $('.chk').prop('checked', true);      
          $('.chk').click(function() {
        if($(this).is('checked',true))
            $('.chk').prop('checked', true);
        else
          $('.chk').prop('checked', true);
    });



      $('.record_table').click(function(event) {
      if (event.target.type !== 'checkbox') {
      $(':checkbox', this).trigger('click');
      }
      });
      });
      $(function(){
      // on change event
      var bool;
      $("input.checkboxes").change(function() {
      bool = $(".checkboxes:not(:checked)").length !=0; 
      //alert($(".checkboxes:not(:checked)").length);
      // enable/disable
      if(bool==false){
      $("#submitbutton").prop('disabled', bool);
      $("#testremoved").hide();
      $("#submitbutton").prop('class', 'form-control  btn-primary');
      }
      else
      {
      $("#submitbutton").prop('disabled', 'true');
      $("#testremoved").show();
      $("#submitbutton").prop('class', 'form-control  btn-secondary');
      }
      })
      /* $("#month").change(function() {
      if(this.value>0){
      price = <?=$vehicles->price?>;
      actual_amount_after = price * $('#app_percent_month').val() / 100;
      new_price = <?=$vehicles->price?> -  actual_amount_after;
      total = this.value * new_price;
      $('#p').text(total);
      $('#price').val(total.toFixed(2)); // num.toFixed(2)
      }
      else{
      $('#price').val(<?=$vehicles->price?>);
      }
      });*/
      $("#month").change(function() {  

   $(".insurance").prop("checked", false);
   $(".deposit").prop("checked", false);


      week = this.value;
      if(week < 15){
        $('#discount').val(0); 
      }
      if(week > 16 && week <24){
        $('#discount').val(5); 
      }

      if(week > 23 && week <48){
        $('#discount').val(10); 
      }

      if(week > 47){
        $('#discount').val(10); 
      }
      /*
      var discount = 0;
      price = <?=$vehicles->price?>;
      week = this.value;
      if(week >11 && week < 23) discount = (price * 5)/100;
      if(week >23 && week < 48) discount = (price * 7)/100;
      if(week >48) discount = (price * 10)/100;
      discounted_price = price - discount;
      total = week * discounted_price;
      $('#p').text(total);
      $('#price').val(total.toFixed(2)); // num.toFixed(2)
      if(week >11){
      }
      */


      });




      });
</script>
<style>
  body, html {
     overflow-x: unset!important;
}
.record_table{margin-bottom: 0px!important}
.small{font-size: 9px;}
.strike{text-decoration: line-through;}
.right-area{    background-color: #bacee8;
    padding: 5px 0px;
}
.alert-success{display: none;} 
.alert-danger{display: none;}
#customer_insurance_style{display: none;}
</style>