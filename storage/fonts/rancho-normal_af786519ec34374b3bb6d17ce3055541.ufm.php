<?php return array (
  'codeToName' => 
  array (
    32 => 'space',
    33 => 'exclam',
    34 => 'quotedbl',
    35 => 'numbersign',
    36 => 'dollar',
    37 => 'percent',
    38 => 'ampersand',
    39 => 'quotesingle',
    40 => 'parenleft',
    41 => 'parenright',
    42 => 'asterisk',
    43 => 'plus',
    44 => 'comma',
    45 => 'hyphen',
    46 => 'period',
    47 => 'slash',
    48 => 'zero',
    49 => 'one',
    50 => 'two',
    51 => 'three',
    52 => 'four',
    53 => 'five',
    54 => 'six',
    55 => 'seven',
    56 => 'eight',
    57 => 'nine',
    58 => 'colon',
    59 => 'semicolon',
    60 => 'less',
    61 => 'equal',
    62 => 'greater',
    63 => 'question',
    64 => 'at',
    65 => 'A',
    66 => 'B',
    67 => 'C',
    68 => 'D',
    69 => 'E',
    70 => 'F',
    71 => 'G',
    72 => 'H',
    73 => 'I',
    74 => 'J',
    75 => 'K',
    76 => 'L',
    77 => 'M',
    78 => 'N',
    79 => 'O',
    80 => 'P',
    81 => 'Q',
    82 => 'R',
    83 => 'S',
    84 => 'T',
    85 => 'U',
    86 => 'V',
    87 => 'W',
    88 => 'X',
    89 => 'Y',
    90 => 'Z',
    91 => 'bracketleft',
    92 => 'backslash',
    93 => 'bracketright',
    94 => 'asciicircum',
    95 => 'underscore',
    96 => 'grave',
    97 => 'a',
    98 => 'b',
    99 => 'c',
    100 => 'd',
    101 => 'e',
    102 => 'f',
    103 => 'g',
    104 => 'h',
    105 => 'i',
    106 => 'j',
    107 => 'k',
    108 => 'l',
    109 => 'm',
    110 => 'n',
    111 => 'o',
    112 => 'p',
    113 => 'q',
    114 => 'r',
    115 => 's',
    116 => 't',
    117 => 'u',
    118 => 'v',
    119 => 'w',
    120 => 'x',
    121 => 'y',
    122 => 'z',
    123 => 'braceleft',
    124 => 'bar',
    125 => 'braceright',
    126 => 'asciitilde',
    160 => 'uni00A0',
    161 => 'exclamdown',
    162 => 'cent',
    163 => 'sterling',
    164 => 'currency',
    165 => 'yen',
    166 => 'brokenbar',
    167 => 'section',
    168 => 'dieresis',
    169 => 'copyright',
    170 => 'ordfeminine',
    171 => 'guillemotleft',
    172 => 'logicalnot',
    173 => 'sfthyphen',
    174 => 'registered',
    175 => 'macron',
    176 => 'degree',
    178 => 'twosuperior',
    179 => 'threesuperior',
    180 => 'acute',
    181 => 'mu',
    182 => 'paragraph',
    183 => 'periodcentered',
    184 => 'cedilla',
    185 => 'onesuperior',
    186 => 'ordmasculine',
    187 => 'guillemotright',
    188 => 'onequarter',
    189 => 'onehalf',
    190 => 'threequarters',
    191 => 'questiondown',
    192 => 'Agrave',
    193 => 'Aacute',
    194 => 'Acircumflex',
    195 => 'Atilde',
    196 => 'Adieresis',
    197 => 'Aring',
    198 => 'AE',
    199 => 'Ccedilla',
    200 => 'Egrave',
    201 => 'Eacute',
    202 => 'Ecircumflex',
    203 => 'Edieresis',
    204 => 'Igrave',
    205 => 'Iacute',
    206 => 'Icircumflex',
    207 => 'Idieresis',
    208 => 'Eth',
    209 => 'Ntilde',
    210 => 'Ograve',
    211 => 'Oacute',
    212 => 'Ocircumflex',
    213 => 'Otilde',
    214 => 'Odieresis',
    215 => 'multiply',
    216 => 'Oslash',
    217 => 'Ugrave',
    218 => 'Uacute',
    219 => 'Ucircumflex',
    220 => 'Udieresis',
    221 => 'Yacute',
    222 => 'Thorn',
    223 => 'germandbls',
    224 => 'agrave',
    225 => 'aacute',
    226 => 'acircumflex',
    227 => 'atilde',
    228 => 'adieresis',
    229 => 'aring',
    230 => 'ae',
    231 => 'ccedilla',
    232 => 'egrave',
    233 => 'eacute',
    234 => 'ecircumflex',
    235 => 'edieresis',
    236 => 'igrave',
    237 => 'iacute',
    238 => 'icircumflex',
    239 => 'idieresis',
    240 => 'eth',
    241 => 'ntilde',
    242 => 'ograve',
    243 => 'oacute',
    244 => 'ocircumflex',
    245 => 'otilde',
    246 => 'odieresis',
    247 => 'divide',
    248 => 'oslash',
    249 => 'ugrave',
    250 => 'uacute',
    251 => 'ucircumflex',
    252 => 'udieresis',
    253 => 'yacute',
    254 => 'thorn',
    255 => 'ydieresis',
    305 => 'dotlessi',
    338 => 'OE',
    339 => 'oe',
    710 => 'circumflex',
    730 => 'ring',
    732 => 'tilde',
    8211 => 'endash',
    8212 => 'emdash',
    8216 => 'quoteleft',
    8217 => 'quoteright',
    8218 => 'quotesinglbase',
    8220 => 'quotedblleft',
    8221 => 'quotedblright',
    8222 => 'quotedblbase',
    8226 => 'bullet',
    8230 => 'ellipsis',
    8249 => 'guilsinglleft',
    8250 => 'guilsinglright',
    8260 => 'fraction',
    8364 => 'Euro',
    8722 => 'minus',
  ),
  'isUnicode' => true,
  'EncodingScheme' => 'FontSpecific',
  'FontName' => 'Rancho',
  'FullName' => 'Rancho Regular',
  'Version' => 'Version 1.001',
  'PostScriptName' => 'Rancho-Regular',
  'Weight' => 'Medium',
  'ItalicAngle' => '0',
  'IsFixedPitch' => 'false',
  'UnderlineThickness' => '50',
  'UnderlinePosition' => '-75',
  'FontHeightOffset' => '23',
  'Ascender' => '897',
  'Descender' => '-321',
  'FontBBox' => 
  array (
    0 => '-80',
    1 => '-321',
    2 => '823',
    3 => '897',
  ),
  'StartCharMetrics' => '214',
  'C' => 
  array (
    32 => 210.0,
    33 => 170.0,
    34 => 246.0,
    35 => 461.0,
    36 => 410.0,
    37 => 576.0,
    38 => 540.0,
    39 => 128.0,
    40 => 232.0,
    41 => 222.0,
    42 => 364.0,
    43 => 386.0,
    44 => 148.0,
    45 => 329.0,
    46 => 139.0,
    47 => 352.0,
    48 => 490.0,
    49 => 221.0,
    50 => 428.0,
    51 => 463.0,
    52 => 465.0,
    53 => 469.0,
    54 => 472.0,
    55 => 428.0,
    56 => 456.0,
    57 => 455.0,
    58 => 154.0,
    59 => 155.0,
    60 => 234.0,
    61 => 318.0,
    62 => 239.0,
    63 => 414.0,
    64 => 574.0,
    65 => 559.0,
    66 => 511.0,
    67 => 472.0,
    68 => 557.0,
    69 => 461.0,
    70 => 426.0,
    71 => 514.0,
    72 => 549.0,
    73 => 279.0,
    74 => 441.0,
    75 => 446.0,
    76 => 365.0,
    77 => 687.0,
    78 => 529.0,
    79 => 543.0,
    80 => 459.0,
    81 => 531.0,
    82 => 481.0,
    83 => 508.0,
    84 => 356.0,
    85 => 507.0,
    86 => 407.0,
    87 => 629.0,
    88 => 424.0,
    89 => 492.0,
    90 => 444.0,
    91 => 211.0,
    92 => 309.0,
    93 => 211.0,
    94 => 341.0,
    95 => 487.0,
    96 => 543.0,
    97 => 366.0,
    98 => 398.0,
    99 => 324.0,
    100 => 374.0,
    101 => 324.0,
    102 => 248.0,
    103 => 354.0,
    104 => 359.0,
    105 => 175.0,
    106 => 181.0,
    107 => 354.0,
    108 => 173.0,
    109 => 585.0,
    110 => 360.0,
    111 => 322.0,
    112 => 374.0,
    113 => 371.0,
    114 => 296.0,
    115 => 318.0,
    116 => 203.0,
    117 => 371.0,
    118 => 332.0,
    119 => 517.0,
    120 => 306.0,
    121 => 366.0,
    122 => 325.0,
    123 => 221.0,
    124 => 190.0,
    125 => 221.0,
    126 => 406.0,
    160 => 210.0,
    161 => 170.0,
    162 => 418.0,
    163 => 443.0,
    164 => 391.0,
    165 => 476.0,
    166 => 186.0,
    167 => 377.0,
    168 => 543.0,
    169 => 469.0,
    170 => 273.0,
    171 => 322.0,
    172 => 467.0,
    173 => 329.0,
    174 => 469.0,
    175 => 543.0,
    176 => 186.0,
    178 => 248.0,
    179 => 249.0,
    180 => 543.0,
    181 => 345.0,
    182 => 464.0,
    183 => 135.0,
    184 => 543.0,
    185 => 158.0,
    186 => 240.0,
    187 => 322.0,
    188 => 482.0,
    189 => 504.0,
    190 => 563.0,
    191 => 414.0,
    192 => 559.0,
    193 => 559.0,
    194 => 559.0,
    195 => 559.0,
    196 => 559.0,
    197 => 559.0,
    198 => 796.0,
    199 => 472.0,
    200 => 461.0,
    201 => 461.0,
    202 => 461.0,
    203 => 461.0,
    204 => 279.0,
    205 => 279.0,
    206 => 279.0,
    207 => 279.0,
    208 => 557.0,
    209 => 529.0,
    210 => 543.0,
    211 => 543.0,
    212 => 543.0,
    213 => 543.0,
    214 => 543.0,
    215 => 316.0,
    216 => 538.0,
    217 => 507.0,
    218 => 507.0,
    219 => 507.0,
    220 => 507.0,
    221 => 492.0,
    222 => 459.0,
    223 => 472.0,
    224 => 366.0,
    225 => 366.0,
    226 => 366.0,
    227 => 366.0,
    228 => 366.0,
    229 => 366.0,
    230 => 546.0,
    231 => 324.0,
    232 => 324.0,
    233 => 324.0,
    234 => 324.0,
    235 => 324.0,
    236 => 175.0,
    237 => 175.0,
    238 => 175.0,
    239 => 175.0,
    240 => 323.0,
    241 => 360.0,
    242 => 322.0,
    243 => 322.0,
    244 => 322.0,
    245 => 322.0,
    246 => 322.0,
    247 => 329.0,
    248 => 323.0,
    249 => 371.0,
    250 => 371.0,
    251 => 371.0,
    252 => 371.0,
    253 => 366.0,
    254 => 374.0,
    255 => 366.0,
    305 => 175.0,
    338 => 718.0,
    339 => 537.0,
    710 => 543.0,
    730 => 543.0,
    732 => 543.0,
    8211 => 329.0,
    8212 => 507.0,
    8216 => 156.0,
    8217 => 156.0,
    8218 => 156.0,
    8220 => 269.0,
    8221 => 269.0,
    8222 => 269.0,
    8226 => 189.0,
    8230 => 494.0,
    8249 => 184.0,
    8250 => 184.0,
    8260 => 409.0,
    8364 => 521.0,
    8722 => 329.0,
  ),
  'CIDtoGID_Compressed' => true,
  'CIDtoGID' => 'eJzt0GWPGFQQhtGHBHcr7i5tgeLursULxd3d3a1YsUIpLe5S3N1d/w/7YT+S0GSXdBPOSW7mZjKZvPfWEC3W4i3Rko1qqZZumZZtuZZvhVZspVZulVZttVZvjdZsrdZundZtvdZvdGMa2wZt2EaNa+M2adM2a/O2aMu2auu2adu2a/t2aMd2aud2add2a/f2aM/2au/2ad/2a/8OaHwHdlAHd0iHdliHN6EjOrKJHdXRHdOxHdfxndCJndTJndKpndbpndGZndXZnTPUxw94vqe6t8n92bQW7r4eaUrTe6an+6OH+rV7BmYWasEe7rEe6IN+b/5m9GzzNW8L9GQv9Gkf92Lndl5TO7/Pu6BP+qyv+6Iv+6q/urDv+qZve6mLWrRH+7Hv+6GLm7O5e7BLu6TLuqLLu7LHu6prurpru64bur4bu6k5urlbu6XbuqPbe6snurNJ3dXdzdU8vT0MP/Bvfhryhpd7ZRhywMj28yzO/TZwfvkvgwAAAAAAAAAAAAAAAAAAMMK92muDtzd6sw8H6sxe76PB3v2D9blZ2vVe7/9D950h5Rt53p3dAWCYLDK7AwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/yd/A20iWQM=',
  '_version_' => 6,
);