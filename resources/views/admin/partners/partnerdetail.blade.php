@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
 

<div class="content-page">
     <div class="content">
        <div class="container-fluid" style="position: absolute;width:98%" >
                  <div class="col-md-3">
                    <div class="card-box">
            
                       <h3>Commission :{{$partner->commission}}% </h3>
            
                    </div>

                </div>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="card-box">
                            <h4>Partner Details</h4>
                            <p>Partner Name: {{@$partner->name}}</p>
                            <p>Partner Phone: {{@$partner->phone}}</p>
                            <p>Partner Email: {{@$partner->email }}</p>
                            <p>Partner Address: {{@$partner->address}}</p>
                            <p>Partner Business Address: {{@$partner->business_address}}</p>
                            <p>Partner Pickup Address: {{@$partner->pickup_address}}</p>
                            <p>Partner Dropoff Address: {{@$partner->dropoff_address}}</p>
                            <p>Partner Bank Name: {{@$partner->bank_name}}</p>
                            <p>Partner Account Holder Name: {{@$partner->account_holder_name}}</p>
                            <p>Partner Account Number: {{@$partner->account_number}}</p>
                            <p>Partner Sort Code: {{@$partner->sort_code}}</p>

                            </div>
                    
                        </div>
                        <div class="col-md-3">
                            <div class="card-box">
                            <h4>Partner Image</h4>
                            
                                @if($partner->profile_pic == Null)
                                 @else
                                 <img src="{{ asset('storage/app/'.$partner->profile_pic) }}"  height="50" width="50">
                                 @endif
                             
                            </div>
                    
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card-box">
                        
<h3>Vehicle List</h3>
<table id="datatable_tbl" class="table table-striped table-bordered"  >
        <thead>
            <tr>
                                     <th>ID</th>
                                    <th>Vehicle Detail</th>
                                    <th>Type</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Description</th>
                                    <th>Pic</th>
                                    <th>Created </th>  
                                    <th>Action</th>
            </tr>
        </thead> 


     
                                <tbody>
                                    @foreach ($vehicles as $vehicle)
                                        <tr>
                                            <td>{{ $vehicle->id }}</td>
                                            <td>{{ $vehicle->licence_plate_number }},  

                                                {{ @$vehicle->car_make->name }}, {{  @$vehicle->car_model->name }}, 

                                                {{ $vehicle->year }} </td>
                                            <td>@if($vehicle->fuel==0) Fuel @else  Transmission @endif</td>
                                 <td>@if($vehicle->vehicle_category==1) PCO @else  Private @endif</td>
 
                                            
                                            <td>£{{ $vehicle->price }}</td>
                                            <td>{{ $vehicle->description }}</td>
                                            <td>
                                                <img src="{{ asset('storage/app/'.$vehicle->image) }}"  height="50" width="50">
                                            </td>
                                                  <td><?php if($vehicle->partner_id) echo "Partner Car"; else echo "Admin"; ?> </td> 
                                            <td style="width: 15%;">
                                               <a href="{{ route('vehicleEdit', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="fa fa-pencil"></i></a>  
                                                <a href="{{ route('vehicleDestory', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-danger "><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
             </div>
        </div>
       </div>
    </div>
</div>




@endsection
@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
 