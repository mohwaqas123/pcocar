@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        <h4 class="breadcrumb-item active"   > 
                        
                    </h4>

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            
<h4 class="m-t-0 "> </h4>

                            <!-- <table id="songsListTable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Vehincle Detail</th>
                                    <th>description</th>
                                    <th>Thumbnail</th>
                                    <th>Action</th>
                                </tr>
                                </thead>  -->
                                
<!-- Datatable
 
 <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
 -->

@if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif




<table id="datatable_tbl" class="table table-striped table-bordered"    >
        <thead>
            <tr>
                                     <th>ID</th>
                                    <th>Vehicle Details</th>
                                    <th>Type</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Discounted Price</th>
                                    <th>Owner</th>  
                                    <th style="width:40px;">Action</th>
            </tr>
        </thead> 


     
                                <tbody>
                                    @foreach ($vehicles as $vehicle)
                                        <tr>
                                            <td>{{ $vehicle->id }}</td>
                            <td>{{ $vehicle->licence_plate_number }},   

                                {{ @$vehicle->car_make->name }}, 
                                 {{  @$vehicle->car_model->name }}, 
                                  {{ $vehicle->year }}
                                  {{$vehicle->colour}}
                                   </td>
    <td> 
    @if ($vehicle-> fuel_type == 0)
    petrol
    @elseif ($vehicle-> fuel_type == 4)
    Hybrid
    @elseif ($vehicle-> fuel_type == 3)
    Electric     
    @else 
    Electric Hybrid
    @endif
    </td> 
                                            





                                            <td>@if($vehicle->vehicle_category==1) PCO @else  Private @endif</td>
 
                                            
                                            <td>£{{ $vehicle->price }}</td>
                                            <?php
                                             $dis_price =  $vehicle->price / 2;
                                            ?>
                                            <td>£{{ $dis_price }}</td>
                                             
                                                  <td><?php if($vehicle->partner_id) echo "Partner"; else echo "Intercity Hire "; ?> </td> 
                                            <td style="width: 20%;">
                                               <a href="{{ route('vehicleEdit', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"
                                                 data-toggle="tooltip" data-placement="Top" data-original-title="Edit"><i class="fa fa-pencil"></i></a> 
                                                 <!-- <a href="{{ route('vehiclehide', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"                                                  data-toggle="tooltip" data-placement="Top" data-original-title="Hide">Hide</a> -->

                                          
                                            <a class="btn btn-danger btn-sm metismenu with-tooltip" 
                                             data-toggle="modal" data-toggle="tooltip" data-placement="Top" data-original-title="Booking vehicle" data-target="#new"
                                               onclick="commentedit({{$vehicle->id}})"
                                                data-target="#new"  >Hide
                                                </a>
                                              <!--     <a href="{{ route('vehiclevisible', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"
                                                 data-toggle="tooltip" data-placement="Top" data-original-title="Visible"></i>Visible</a> 
 -->
                                       <!--  @if ($vehicle->bookingStatus == 1) 
                                                <a href="{{ route('vehicleDestory', $vehicle->id) }}"
                                                 onclick="return confirm('You want to remove your selected Vehicle?')"class="btn btn-sm btn-icon waves-effect waves-light btn-danger metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                @else         
                                                @endif -->

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                             
                            </table>
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                         
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</div>
<div class="modal fade" id="new" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Why do you want to hide It? <span id='placer'></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           
        <form  action="{{ route('vehiclehide') }}"     method="post" enctype="multipart/form-data">
              {{csrf_field()}}
           
                 <input type="hidden" name="id" id="id" value="{{$vehicle->id}}">
                
                  <label for="make" class="col-form-label">Please Enter Reasons<span class="text-danger">*</span></label>
                <div class="col-md-12">
                    <textarea  type="text" class="form-control" rows="10" cols="70" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                </div>
                <center> <button type="submit" class="btn btn-primary">Submit</button></center>
          </form>
         
        </div>

      </div>
    </div>
  </div>
@endsection

@section('scripts')
 <script>
    function commentedit(id){  

 $('#id').val(id);
  }

     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
