@extends('layouts.partner')
@section('styles')
@endsection
@section('content')

<div class="content-page">
     <div class="content">
        <div class="container-fluid" style="position: absolute;width:98%" >
                  <div class="col-md-3">
                    <div class="card-box">
            
                       <h3>Commission :{{$partner->commission}}% </h3>
            
                    </div>

                </div>
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="card-box">
                            <h4>Partner Details</h4>
                            <p>Partner Name: {{@$partner->name}}</p>
                            <p>Partner Phone: {{@$partner->phone}}</p>   
                           <p>Partner Address: {{@$partner->address}}</p>
                            <p>Partner Business Address: {{@$partner->business_address}}</p>
                            <p>Partner Pickup Address: {{@$partner->pickup_address}}</p>
                            <p>Partner Dropoff Address: {{@$partner->dropoff_address}}</p>
                            <p>Partner Bank Name: {{@$partner->bank_name}}</p>
                            <p>Partner Account Holder Name: {{@$partner->account_holder_name}}</p>
                            <p>Partner Account Number: {{@$partner->account_number}}</p>
                            <p>Partner Sort Code: {{@$partner->sort_code}}</p>


                            </div>
                    
                        </div>
                        <div class="col-md-3">
                            <div class="card-box">
                            <h4>Partner Image</h4>
                            
                                @if($partner->profile_pic == Null)
                                 @else
                                 <img src="{{ asset('storage/app/'.$partner->profile_pic) }}"  height="50" width="50">
                                 @endif
                             
                            </div>
                    
                        </div>
                    </div>
                
             </div>
        </div>
       </div>
    </div>
</div>




@endsection
@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
 