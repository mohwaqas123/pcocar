<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Year;
use Validator;
use Redirect;
use Input;

class yearController extends Controller
{
    //list all country
    public function index()
    {
        $years = Year::select('id', 'year')->orderBy('year', 'ASC')->paginate(10);
        return view('admin.year.index', ['years'=>$years, 'title' => 'years']);
    }

    //show add country form
    public function showAddyear()
    {
        return view('admin.year.create');
    }

    //store new country
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year' => 'required|max:191|unique:years',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }

        $data = [
                'year' => $request->year,
            ];

        Year::create($data);
        return redirect()->route('adminyearList')->with('success', 'Record Created Successfully');
    }

    //show edit form
    public function showEdityear($id)
    {
        $year = Year::find($id);
        if ($year == null) {
            return redirect()->back()->with('error', 'No Recor Found');
        }

        return view('admin.year.edit', ['year' => $year ]);
    }

    // update country

    public function update(Request $request)
    {
        $year = Year::find($request->id);

        if ($year == null) {
            return redirect()->back()->with('error', 'No Record Found');
        }

        // if($request->name != $country->name){
        $rules['year'] = 'required|max:150|unique:years';
        // }
        // else{
        //     return redirect()->back()->with('failure', 'Cannot update same name of country');
        // }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }

        $yearData = [
            'year' => $request->year,
        ];

        $year->update($yearData);

        return redirect()->route('adminyearList')->with('success', 'Record Updated Successfully');
    }

}
