@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="content-page">

<div class="content">
<!-- 
@if($expense_view->int_status == 1 )
   
  <div class="alert alert-success">
    <h1>Expense are Submitted.</h1>
  </div>
 
  @else
   
  @endif
  @if($expense_view->int_status == 2 )
   
  <div class="alert alert-danger">
    <h1>Expense are InProgress.</h1>
  </div>
  @else
   @endif
   @if($expense_view->int_status == 3 )
   
  <div class="alert alert-danger">
    <h1>Companey Required further Evidence.</h1>
  </div>
  @else
   @endif
   @if($expense_view->int_status == 4)
   
  <div class="alert alert-danger">
    <h1>Expense are Prove.</h1>
  </div>
  @else
   @endif
   @if($expense_view->int_status == 5 )
   
  <div class="alert alert-danger">
    <h1>Expense are Rejected.</h1>
  </div>
  @else
   @endif
 -->

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Customer Details</b></p>
                        Name: {{$expense_view->booking->user->name}} <br>
                        Email: {{$expense_view->booking->user->email}} <br>
                         Phone: {{$expense_view->booking->user->phone}} <br>
              
                       DOB:{{date('d-m-Y', strtotime($expense_view->booking->user->dob))}} <br>
                         
                        <!-- end row -->
                    </div>
                 </div>
                 <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Vehicle Details</b></p>
                        Vehicle Name:  {{$expense_view->vehicle->name}} <br>
                        Model:  {{$expense_view->vehicle->year}} <br>
                        Price: {{$expense_view->vehicle->price}} <br>
                        VRM  {{$expense_view->vehicle->licence_plate_number}} <br>
                        <!-- Postal pickup dropoff: {{$expense_view->vehicle->postal_pickup_dropoff }} -->
                         
                
                    </div>
                </div>
                
                  <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Expense Details</b></p>
                        Name:  {{$expense_view->name}} <br>
                 <!--        Model:  {{$expense_view->vehicle->year}} <br> -->
                        Claimed amount: {{$expense_view->price}} <br>
                        vat_number: {{$expense_view->vat_number}} <br>
                        Details: {{$expense_view->detail}} <br>
                        @if($expense_view->attachment == '' )
                       
                          @else
                           <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$expense_view->attachment}}"><p>View Attachment</p>
                          @endif
                  </div>
                </div>
                 <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Expense Details History</b></p>
                        @foreach($expense_history as $expense_history_view)
                        @if($expense_history_view->int_status == 0)
                       <p>admin</p> 
                        @else
                       <p>Customer</p> 
                        @endif
                        Details: {{$expense_history_view->details}} <br>
                        Date: {{date('d-m-Y', strtotime($expense_history_view->created_at))}} <br>
                        <hr>
                        @endforeach
                     </div>
                </div>
               </div>

                </div>
		    </div>
        
        </div>
              
              


@endsection

@section('scripts')

@endsection
