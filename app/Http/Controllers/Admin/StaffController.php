<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Staff;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;



class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff = Staff::get(); 
        $url = "/admin/dashboard";
          $data = [
               'staff'=> $staff,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Staff'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Staff List' ,
 
        ];
        return view('admin.staff.list')->with($data);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $url = "/admin/dashboard";
            $data = [
                 'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Staff'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Add Staff' ,

 
            ];  
        return view('admin.staff.create')->with($data);
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'description' => 'required',
            

        ]);


         $nid_front='';
         $nid_back='';
         $profile_img='';
        
    if ($request->hasFile('nid_front') || $request->hasFile('nid_back') || $request->hasFile('profile_img')) 
         {
            $vehicleDocFolder ='staffdocs';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('nid_front'))
               $nid_front = Storage::putFile($vehicleDocFolder, new File($request->nid_front));

            if ($request->hasFile('nid_back'))
               $nid_back = Storage::putFile($vehicleDocFolder, new File($request->nid_back));
            
            if ($request->hasFile('profile_img'))
$profile_img = Storage::putFile($vehicleDocFolder, new File($request->profile_img));
            
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'description' => $request->description,
            'nid_front' => $nid_front,
            'nid_back' => $nid_back,
            'profile_img' => $profile_img,
            ];
            
        $staff = Staff::create($data);
        
         return redirect()->route('admin/staff/list')->with('message','Staff Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
            
            $staff = Staff::where('id', $request->id)->first();            
             $url = "/admin/dashboard";
             
              $data = [
                  'staff' => $staff,

                  'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Staff'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Staff Detail View' ,
 
                ]; 
                
            return view('admin.staff.view')->with($data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $staff = Staff::find($id);
       $url = "/admin/dashboard";
          $data = [
            'staff' => $staff,

                  'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Staff'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Staff Edit' ,

           ]; 

        return view('admin.staff.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $staff = Staff::where('id', $request->id)->first();
        

         $nid_front='';
         $nid_back='';
         $profile_img='';
        
        if ($request->hasFile('nid_front') || $request->hasFile('nid_back') || $request->hasFile('profile_img')) 
         {
            $vehicleDocFolder = 'staffdocs';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('nid_front'))
               $nid_front = Storage::putFile($vehicleDocFolder, new File($request->nid_front));

            if ($request->hasFile('nid_back'))
               $nid_back = Storage::putFile($vehicleDocFolder, new File($request->nid_back));
            
            if ($request->hasFile('profile_img'))
            $profile_img = Storage::putFile($vehicleDocFolder, new File($request->profile_img));
            
        }

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'description' => $request->description,
            'nid_front' => $nid_front,
            'nid_back' => $nid_back,
            'profile_img' => $profile_img,
            ];
        $staff->update($data); 
        $staff->save();
            return redirect()->route('admin/staff/list')->with('message','Staff Update Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
