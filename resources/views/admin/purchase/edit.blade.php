@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


  <div class="content-page"
      <div class="content">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
            <div class="card-box"> 
                    <form role="form" action="{{ route('update_purchase') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="row">
                                      <input type="hidden" name="id" value="{{ $purchase->id }}">
                                        <div class="form-group col-md-6">
                                            <label for="name">Supplier Name<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"  id="name" placeholder="Enter the Name" value="{{ $purchase->name}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Email<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="email"  id="name" placeholder="Enter the Email" value="{{ $purchase->email}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Phone<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="phone"  id="name" placeholder="Enter the phone" value="{{ $purchase->phone}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Address<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="address"  id="name" placeholder="Enter the Address" value="{{ $purchase->address}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Details </label>
                                             <textarea class="form-control summernote" hieght="400" width="100%" name="description" id="description" title="error message" maxlength="700" >{{$purchase->detail}}</textarea>




                                            <!-- <input type="text" class="form-control @error('name') is-invalid @enderror" name="description"  id="name" placeholder="Enter the Details"> -->








                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>


                                          
                                         




                                          <!-- <div class="form-group col-md-6">
                                            <label for="name">Price</label>
                                            <input type="radio" class="radioBtn" name="Radio" id="Radio" value="ABC" required >Excluding VAT
                                            <input class="radioBtn" type="radio" name="Radio" id="Radio" value="PQR" required >Including VAT
                                            <input type="number" class="form-control @error('name') is-invalid @enderror" name="price"  id="name" placeholder="Enter the price" value="{{ $purchase->price}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div> -->
                                          <div class="form-group col-md-6">
                                            <label for="name">Date<span class="text-danger required">*</span></label>
                                            <input type="date" class="form-control @error('name') is-invalid @enderror" name="dob"  id="name" placeholder="Pick up Date" id="datepicker" value="{{ $purchase->date}}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6" id="box">
                                            <label for="name">VAT NUMBER </label>
                                             <input max="10" min="3" type="text" name="vat_number" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the VAT Number" value="{{ $purchase->vat_number}}" >
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>

                                         <!--  <div class="form-group col-md-6">
                                            <label for="name">Invoice Slip<span class="text-danger required">*</span></label>
                                             <input name="nid_back"  class="form-control form-control-lg" type="file" placeholder="N.I.D(Back)" >
                                          </div> -->
                                          <div class="form-group col-md-6">
                                            <label for="name">Image</label>
                                             <input name="profile_img" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="Profile Img">
                                          </div>
                                          
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Add</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>
                                  </div>

                              </form>
                      
                      </div>
          </div>
              </div>
          </div>
      </div>
  </div>

    


@endsection
@section('scripts')
 <script>
$('input[type="radio"]').click(function(){
        if($(this).attr("value")=="ABC"){
            $("#box").hide('slow');
        }
        if($(this).attr("value")=="PQR"){
            $("#box").show('slow');

        }        
    });
$('input[type="radio"]').trigger('click');













    </script>

@endsection