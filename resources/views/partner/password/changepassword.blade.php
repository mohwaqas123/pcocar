@extends('layouts.partner')
@section('styles')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.select2-container .select2-selection--single{
height:34px !important;
}
.select2-container--default .select2-selection--single{
border: 1px solid #ccc !important;
border-radius: 0px !important;
}

.bg-dev{border: 1px solid #000;}
.header-title{background: #eee;
padding: 5px;}

</style>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="card-box">
              

  @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
 <form action="{{route('change_password')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                               
                                    <h4 class="m-t-0">Change the password</h4>
                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Enter Old password<span class="text-danger">*</span></label>
                                            <input name="password" type="password" required="required"  class="form-control" placeholder="Enter Password">
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
 
                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Change password<span class="text-danger">*</span></label>
                                            <input name="password" type="password" required="required"  class="form-control" placeholder="Enter Password">
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                      <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Change Confirm password<span class="text-danger">*</span></label>
                                            <input name="password" type="password" required="required"  class="form-control" placeholder="Enter Confirm password">
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </form>


 
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">



function remove_img(id){

     $.ajax({


                  // url:"http://ip.jsontest.com/",
                  url: '{{ url("temp_delete_img") }}',
                   method:"get",
                   //data: $('#frmConversation').serialize(),
                  data: {id:'4.jpg'},
                   //dataType:'JSON',
                   contentType: 'application/json; charset=utf-8',
                   cache: false,
                   processData: false,
                   success:function(data)
                   {
                       alert(data);
                   },
                   error: function(error)
                        {
                            //console.log(data);
                           // alert(error);
                        }
                  });
}



$('.select2').select2();
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
});
} );
</script>
@endsection