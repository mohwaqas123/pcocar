@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="m-t-0 header-title">Make List</h4>
                        {{--  <p class="text-muted font-14 m-b-30">
                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                        </p>  --}}

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                <th style="width: 10px">Sr.#</th>
                                <th>Name</th>
                                <th style="width:90px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $count = 1;
                                    @endphp
                                @foreach($years as $year)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $year->year }}</td>
                                    <td>
                                    <a class="btn btn-warning btn-sm" href="{{ route('adminyearEdit', $year->id) }}" class="mr-5">Edit </a>
                                </td>
                                </tr>
                                @php
                                    $count++;
                                @endphp
                                @endforeach
                                </tbody>
                            </table>
                            {{$years->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
