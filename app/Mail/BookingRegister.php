<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingRegister extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $name; 
   public $v_id; 
    public $email; 
     public $password; 

    public function __construct($name,$email,$password,$v_id)
    {

         
        $this->name = $name; 
        $this->v_id = $v_id;
        $this->email = $email;
        $this->password = $password;
         
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail.bookingregister', [ 'name' => $this->name , 'v_id'=> $this->v_id,'email'=> $this->email,'password'=> $this->password  ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
