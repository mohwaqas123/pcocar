@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">
        	 @if(session()->has('message'))
			    <div class="alert alert-success">
				   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				   <span aria-hidden="true">&times;</span>
				  </button>
        		  {{ session()->get('message') }}
    			</div>
			@endif 
            <div class="row">
                <div class="col-md-9">
                    <?php
                        $total_price = $purchase->price - $purchase->vat_price;
                    ?>


                    <div class="card-box">	
                    	<h1>Supplier Details</h1>
                    	<p> Date : {{$purchase->date}}</p>
                        <p>Supplier Name : {{$purchase->name}} </p>
                    	<p>Email :{{$purchase->email}}</p>
                    	<p>Phone :{{$purchase->phone}}</p>
                    	<p>Address :{{$purchase->address}}</p>
                    	<p> vat_number : {{$purchase->vat_number}}</p>
                       <!--  <p> Subtotal (excl.Tax)  : {{$total_price}}</p>
                    	<p> 20% VAT : {{$purchase->vat_price}}</p>
                        <p> Total inc.Tax : {{$purchase->price}}</p> -->
                    	



                    </div>
                 </div>
                 <div class="col-md-3">
                    <div class="card-box">	
                    	<h1>Document</h1>
                    	<p>   
                           <a href="{{url('/')}}/storage/app/{{$purchase->image}}">Image</a>  
                        </p>
                        <!-- <p>   
                           <a href="{{url('/')}}/storage/app/{{$purchase->invoice}}"> Invoice</a>  
                        </p> -->
                        
                    </div>
                 </div>
            </div>
        </div>
    </div>
</div>





@endsection
@section('scripts')
@endsection