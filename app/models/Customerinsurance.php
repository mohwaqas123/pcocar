<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Customerinsurance extends Model
{
   protected $table = 'customer_insurance';
   protected $fillable = [
           
        'booking_id', 'user_id', 'cover' , 'policy_number', 'company_name','email','contact_number','start_date','end_date','customer_private_insurance','file3',
    ];


    
public function user()
    {
        return $this->belongsTo('App\models\User','user_id','id');
    }
     public function booking()
    {
        return $this->belongsTo('App\models\Booking','booking_id','id');
    }

}
