<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use App\models\Booking;
use App\models\Setting;
use Session;
use PDF; 
use Auth;
use App\Helper\Helper;
 

class searchController extends Controller
{

 
   public function get_models(Request $request){
                  $carModel = CarModel::where( 'make_id', $request->id )->get();
                  //print_r($agent); die();
                  return $carModel;
    }

    public function get_all_vehicles(Request $request){

                //if($request->car_make) $carmake = $request->car_make; else $carmake='';
                /*if($request->car_make!='' && $request->car_model=='' && $request->year==''){
                  $vehicles = Vehicle::where('make', $request->car_make)->orderby('id', 'Desc')->paginate(50);
                }
                else if($request->car_model!='' && $request->car_make=='' && $request->year==''){
                   $vehicles = Vehicle::where('model', $request->car_model)->orderby('id', 'Desc')->paginate(50); 
                }
                else if($request->year!='' && $request->car_make=='' && $request->car_model==''){
                   $vehicles = Vehicle::where('year', $request->year)->orderby('id', 'Desc')->paginate(50); 
                }


                else{
                    $vehicles = Vehicle::orderby('id', 'Desc')->paginate(50); 
                }*/
                if($request->vehicle_category!=''){
                    //$v_list->where('vehicle_category', $request->vehicle_category);
                     session(['vehicle_category' =>$request->vehicle_category]);
                 }
session()->forget('discount');
session()->forget('duration');


if($request->session()->get('vehicle_category')!=0){ 
                /* $v_list = Vehicle::query()->where('bookingStatus',0)->where('city', 'like', '%' . $request->session()->get('city') . '%')->where('vehicle_category',$request->session()->get('vehicle_category'))->whereBetween('price', [$request->amount1, $request->amount2]);*/

                  $v_list = Vehicle::query()->where('bookingStatus',0)->where('vehicle_category',$request->session()->get('vehicle_category'))->whereBetween('price', [$request->amount1, $request->amount2]);
               }
else{ 
                     $v_list = Vehicle::query()->where('bookingStatus',0)->whereBetween('price', [$request->amount1, $request->amount2]);
                   }           
                 
                 if($request->car_make!=''){
                     $v_list->where('make', $request->car_make);
                 }
                 if($request->car_model!=''){
                     $v_list->where('model', $request->car_model);
                 }
                 if($request->year!=''){
                     $v_list->where('year', $request->year);
                 } 
                 if($request->fuel_type!=''){
                     $v_list->where('fuel_type', $request->fuel_type);
                 }
                  if($request->licence_plate_number!=''){
                      $v_list->where('licence_plate_number', $request->licence_plate_number);
                 }
                 if($request->vehicle_category!=''){
                    $v_list->where('vehicle_category', $request->vehicle_category);
                     session(['vehicle_category' =>$request->vehicle_category]);
                 }

      if($request->filter!=''){
                      $vehicles =  $v_list->orderby('price', $request->filter)->get();
                 }


                 else{
                   $vehicles =  $v_list->orderby('id', 'Desc')->get();
                 }

                  if($request->licence_plate_number && $request->licence_plate_number !='') 
                         $vehicles = Vehicle::query()->Where('licence_plate_number', 'like', '%' . $request->licence_plate_number . '%')->get();
 
                 // $vehicles =  $v_list->orderby('id', 'Desc')->get();

//$vehicles = Vehicle::orWhere('make',$request->car_make)->orWhere('model',$request->car_model)->orWhere('year',$request->year)->orderby('id', 'Desc')->paginate(50); 


                 /*$vehicles = Vehicle::where([
                          ['make', '=', $carmake],
                        //  ['model', '==', $model],
                 ])->orderby('id', 'Desc')->paginate(50);*/
                 $data = '<div class="row">';
                 foreach($vehicles as $cab){
  

$date1 = $cab->date_from;
$date2 =$cab->date_to;

$ts1 = strtotime($date1);
$ts2 = strtotime($date2);

$year1 = date('Y', $ts1);
$year2 = date('Y', $ts2);

$month1 = date('m', $ts1);
$month2 = date('m', $ts2);

$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
$img = url('/').'/storage/app/'.$cab->image;
if($cab->transmission==1) $cartype =  "Manual"; else $cartype = "Automatic";
 

//===========
 $discount=0;


$discounted_price =  $cab->price;//* $request->duration;
$get_price_check_flate = Helper::get_price_with_check_discount($discounted_price);


if( Helper::get_flat_discount_amount()[0]->flat_discount_type==1) 
  $del_price = '<del>'.number_format($cab->price,2).'</del>&nbsp';
else
  $del_price = '';


if($request->duration > 1 ){ 
  session(['duration' => $request->duration]);


  $week = $request->duration ;
  $price = $get_price_check_flate;

      if($week >= 13 && $week < 24 ){
            $discount = ($price * 5 ) /100;
            $disc = 5;
       }

       if($week >= 24 && $week < 48 ){
            $discount = ($price * 10 ) /100;
            $disc = 10;
       }

       if($week >= 48 ){
            $discount = ($price * 10 ) /100;
            $disc = 10;
       }
        if($week < 12 ){
           $discounted_price = $price;//  * $week;
       $discount_text = $del_price. '<span class="bold">£<span class="price">'.number_format($discounted_price,2).'</span><span class="inner">/Week</span></span> ';
      
        }
        else
        {
           $discounted_price = $price - $discount;// * $week;
       $discount_text = '<span class="small">Discount '.$disc.'% </span> '.$del_price. '<span class="bold">£<span class="price">'.number_format($discounted_price,2) .'</span><span class="inner">/Week</span></span> ';
        }
        session(['discount' => $discount]);

      

}
else
{
      
    //  if( Helper::get_flat_discount_amount()[0]->flat_discount_type==1) 
      $discount_text =$del_price. '<span class="bold">'.env('CURRENCY_SIGN').'<span class="price">'.number_format($get_price_check_flate,2).'</span><span class="inner">/Week</span></span> ';
    //  else
     //  $discount_text = '<span class="bold">'.env('CURRENCY_SIGN').'<span class="price">'.number_format($get_price_check_flate,2).'</span><span class="inner">/Week</span></span> ';
        
}

if(session()->get('staff_id') ){
    if(@$cab->partner_id)  
        $reg = ' <span class="reg">Reg# '.@$cab->licence_plate_number.'<p class="partner_name">'.@$cab->partners->name.' ('.@$cab->partners->commission.'% Commission)</p></span>';
    else
        $reg = ' <span class="reg">Reg# '.@$cab->licence_plate_number.'</span>';
} 
 else{
   $reg ='';
 } 


  
//==================end duration=======================================================================================================


  //$discount_text = '<span class="small">Discount 5% </span> <span class="bold">£<span class="price">'.$discounted_price.'</span><span class="inner">/Week</span></span> ';
  if($cab->vehicle_category==1) 
      $vehicle_category = '<div>PCO<span style="font-size: 12px; margin-right: 10px;"text-align:center;> MiniCab</span></div>'; 
          else 
      $vehicle_category='<div>Private <span style="font-size: 12px; margin-right: 10px;";> Delivery</span></div>';
         

      if($cab->fuel_type==3) 
      $vehicle_category_details = '<div>Electric<span style="font-size: 12px; margin-right: 10px;"text-align:center; text-align:center;></span></div>'; 
          else 
      $vehicle_category_details='<div>Hybrid <span style="font-size: 12px; margin-right: 19px;"text-align:center;> </span></div>';


    if($cab->fuel==0) 
      $vehicle_fuel = '<div>Auto<span style="font-size: 12px; margin-right: 10px;"text-align:center;></span></div>'; 
          else 
      $vehicle_fuel='<div>Manual <span style="font-size: 12px; margin-right: 10px;";> </span></div>';
 


       $data .= '<div class="col-md-4">
           <div class="card">
          <div class="  text-left" style=" colour:blue; position:absolute;  font-size: 16px;  font-weight: bold; margin-left: 2px;"  >
                '. $cab->colour. '
                '. $vehicle_fuel. '
                 


                 
                </div>
                  <div  class="text-center" style=" colour:blue; position:absolute;  font-size: 16px;  font-weight: bold; margin-left: 60px;">
                  '.$vehicle_category_details.'
                    </div>
          
           
              
                <div  class="float-right text-right  ">

                '.$vehicle_category.'
                 '.$reg.'
                   '. $discount_text.' 
                       
                    
                 
                </div> 
                <a href="'.route('car_detail', ['id' => $cab->id, 'discount'=> $discount]).'">
             <img src="'.$img.'" class="img-thumbnail" alt="...">  </a> <div class="mt-2 ml-2 mb-12" style=text-align:center; margin-bottom: 12px;>
                    <span class="car-make custom_font">'. $cab->car_make->name.'  '.$cab->car_model->name.' '.$cab->year .' </span> <br>  
                    <div algin=center style="    margin-bottom: 12px;" >';

          if($cab->bookingStatus!=1){
                $data .= '<a href='.route('car_detail', ['id' => $cab->id]).' class="btn-primary btn btn-sm" disabled>Book Now</a>';
          }
          else
          {
               $data .= '<span style="color:red">Already Booked</span>'; 
          }

          $data .= '</div>  
                    </div>  
            </div>
          </div>';


                 } 
               $data .= '</div>';   
                 return $data;
    }




    public function generate_pdf(Request $request)
    {

 
 
         // This  $data array will be passed to our PDF blade
             $data = [
                'title' => 'PCOCAR',
                'heading' => 'SUBSCRIPTION BOOKING FORM PCOCAR',
                'content' => '',
                'signature_name' => 'Muhammad Waqas',
                'signature'=> 'Waqas',        
                  ];
              
              $pdf = PDF::loadView('frontend.pdf_view', $data);  
              return $pdf->download('medium.pdf');
 
    }

    public function contactus(){
      return view('frontend.contact-us');
    }
    public function aboutus(){
    return view('frontend.aboutus');
    }
    public function partner()
    { return view('frontend.partner');
    }
    public function howitswork(){
    return view('frontend.works');
    }
    public function browse_vehicle_category(Request $request){
                         if($request->id==1){
                                         session(['vehicle_category' => 1, ]);
                         }
                         if($request->id==2){
                                        session(['vehicle_category' => 2,   ]);

                          }
                          $vehicles = Vehicle::where('bookingStatus',0)->orderby('id', 'Desc')->paginate(50);
                          $v_cat =0; 



                                  $make = Make::all();
                                  $model = CarModel::all();

                                      if($request->duration) $duration = $request->duration; else $duration = '';
                                  

                                  return view('frontend.browse', [
                                          'vehicles' => $vehicles , 
                                          'car_make'=> $make,
                                          'car_model' => $model,
                                          'duration' => $duration,
                                          'vehicle_category'=> $v_cat,

                                          
                                      ]);


}







    public function index(Request $request){   

 
 session()->forget('duration');  //remove duration value
 session()->forget('vehicle_category');
 //Session::flush();


if($request->btn_pco){
         session(['vehicle_category' => 1, 'city' =>$request->city,]);
}
if($request->btn_private){
        session(['vehicle_category' => 2,  'city' =>$request->city, ]);
}

 


 if($request->session()->get('vehicle_category')!=0){ 
                  
              /*    $vehicles = Vehicle::where('city', 'like', '%' . $request->city . '%')
                 // ->where('bookingStatus',0)
                  //->where('vehicle_category',$request->session()->get('vehicle_category'))
                  ->orderby('id', 'Desc')
                  ->paginate(50);
                 // ->ToSql();
            */

 $vehicles = Vehicle::where('bookingStatus',0)->orderby('id', 'Desc')->paginate(50);
 
                   $v_cat = $request->session()->get('vehicle_category');
                  //dd($vehicles);

      }
      else{    
                   session()->forget('vehicle_category');  
                   $vehicles = Vehicle::where('bookingStatus',0)->orderby('id', 'Desc')->paginate(50);
                   $v_cat =0; 

      }

        $make = Make::all();
        $model = CarModel::all();

        if($request->duration) $duration = $request->duration; else $duration = '';
        

        return view('frontend.browse', [
                'vehicles' => $vehicles , 
                'car_make'=> $make,
                'car_model' => $model,
                'duration' => $duration,
                'vehicle_category'=> $v_cat,

                
            ]);




  
/*    session()->forget('duration');  //remove duration value
   // Session::flush();
        
    
    if($request->btn_pco)
      session([
                'vehicle_category' => 1, 
            ]);


    if($request->btn_private)
      session([
                'vehicle_category' => 2, 
            ]);


 

        if($request->t){ 
            //$request->session()->flush();
           // $request->session()->put('vehicle_category', $request->t);
           $vehicle_category = $request->t;
          }
          else{
           $vehicle_category =1; 
            $vehicles = Vehicle::orderby('id', 'Desc')->paginate(50);
          }
            //Session::put('vehicle_category', $request->t);
            //$vehicles = Vehicle::where('vehicle_category',$request->t)->paginate(10);  
         

      if($request->btn_submit){  
          $vehicles = Vehicle::where('make',$request->car_make)->orWhere('model',$request->car_model)->paginate(10);
      }
      else{   
        $vehicles = Vehicle::where('bookingStatus',0)->where('city', 'like', '%' . $request->city . '%')->where('vehicle_category',$request->session()->get('vehicle_category'))->orderby('id', 'Desc')->paginate(50);
      } 


      if($request->session()->get('vehicle_category')!=0){
                  $vehicles = Vehicle::where('bookingStatus',0)->where('city', 'like', '%' . $request->city . '%')->where('vehicle_category',$request->session()->get('vehicle_category'))->orderby('id', 'Desc')->paginate(50);
                   $v_cat = $request->session()->get('vehicle_category');

      }
      else{        
                   session()->forget('vehicle_category'); 
                   $vehicles = Vehicle::where('bookingStatus',0)->orderby('id', 'Desc')->paginate(50);
                   $v_cat =0; 

      }





        
        //return view('frontend.browse', ['vehicles' => $vehicles]);
        $make = Make::all();
        $model = CarModel::all();

        if($request->duration) $duration = $request->duration; else $duration = '';
        

        return view('frontend.browse', [
                'vehicles' => $vehicles , 
                'car_make'=> $make,
                'car_model' => $model,
                'duration' => $duration,
                'vehicle_category'=> $v_cat,

                
            ]);*/
    }
//are used for the new booking process
 public function get_discount_percent($week){
   if($week >= 13 && $week < 24 ){ 
            $disc = 5;
       }

       if($week >= 24 && $week < 48 ){  
            $disc = 10;
       }

       if($week >= 48 ){ 
            $disc = 10;
       }
        if($week < 12 ){
           $disc=0;
        }
    return $disc;    
}
//are used for the renew booking process
public function get_discount_price($week,$price){
   if($week >= 13 && $week < 24  ){
            $discount = ($price * 5 ) /100;
             
       }

       if($week >= 24 && $week < 48 ){
            $discount = ($price * 10 ) /100;
             
       }

       if($week >= 48 ){
            $discount = ($price * 10 ) /100;
           
       }
        if($week < 12 ){
           //$discounted_price = $price  * $request->duration;
            $discount =0;

        
      
        }
        return $discount;
}
 
public function calculate_renewbooking(Request $request){
    
   $discount_amount = $this->get_discount_price($request->duration,$request->price);
   $price = $request->price - $discount_amount;
   return $price;
} 

public function calculate_booking(Request $request){  
 
  $setting = Setting::where('id', 1)->get();

  $vehicles = Vehicle::where('id', $request->vehicle_id)->first();
   
  $discount = $this->get_discount_price($request->duration,Helper::get_price_with_check_discount($vehicles->price));// $request->discount;
  $disc= $this->get_discount_percent($request->duration);



 
   $price_after_discount = Helper::get_price_with_check_discount($vehicles->price) - $discount;
   $total_with_dis = $price_after_discount;// * $request->duration; 


  //==================Promotion Discount Logic=========================


  if($request->session()->get('permotion_discount')){
   $price_after_discount =  $price_after_discount - ( $price_after_discount * ($request->session()->get('permotion_discount') / 100)); 
  }

 



//=========================================================================

$data = '';

$data .= '<input type="hidden" value="'.number_format($price_after_discount,2).'" name="price" id="price" /><table style="width: 100%">
              <tr>
                <td>Price per week</td>  <td><del>'.$vehicles->price.'</del> £<sapn>'. Helper::get_price_with_check_discount($vehicles->price).'</sapn> </td>
              </tr>
              <tr>
                <td>Total No of Weeks</td>   <td>'.$request->duration.'</td>
              </tr>
              <tr>
                <td>&nbsp; </td>  <td>&nbsp; </td>
              </tr>
              <tr>
                <td>Multi Week Discount</td>   <td><span>'.$disc.'</span>%</td>
              </tr>
 
              
               <tr>
                <td>Price after Discount(Per Week)</td>   <td>£<span id="final_price">'.number_format($price_after_discount,2).'</span> </td>
              </tr>

              <tr>
                <td>&nbsp; </td>  <td>&nbsp; </td>
              </tr>
              <tr  <tr id="insurance_cost_tr">
                <td>Insurance Cost (Per week)</td>   <td>£'.env('INSURANCE_CODE').'</td>
              </tr>
              <tr>
                <td>Security Deposit</td>   <td>£'.env('DEPOSIT_SECURITY').'</td>
              </tr>
              <tr>
                <td>&nbsp; </td>  <td>&nbsp; </td>
              </tr>
            </table>';

            $insurance_total  =   env('INSURANCE_CODE');
            $grand_total =   $price_after_discount + $insurance_total + env('DEPOSIT_SECURITY') ;

if($request->session()->get('permotion_discount')){
   $grand_total = $grand_total -  ( $price_after_discount * $request->session()->get('permotion_discount') / 100);
}



   $data  .='     <p class="price" align="center">Total: £ <spam id="price_final_total">'.number_format($grand_total,2).'</spam> /week</p><input type="hidden" value="'.number_format($grand_total,2).'" name="price_final_total_book" id="price_final_total_book" />';
 
return  $data;



}



    public function upload_information(Request $request){   
       
        if($request->hasFile('file_dvla'))
          { 
                $allowedfileExtension=['pdf','jpg','png','docx'];
               // $files = $request->file('file');

 

            $vehicleImagesFolder = 'documents_uploads'; 
            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }


            $file1 =Storage::putFile($vehicleImagesFolder, new File($request->file_dvla));
            $file2 =Storage::putFile($vehicleImagesFolder, new File($request->file_cnic)); 
            $file3 =Storage::putFile($vehicleImagesFolder, new File($request->file_utility));
            //$file4 =Storage::putFile($vehicleImagesFolder, new File($request->file_other));

$dataBooking = [
  //  'name' => $request->name,
    'user_id' => 1,//$request->name,
    'vehicle_id' => $request->id,
    'duration' => $request->duration,
    'miles' => $request->mile,
    'address' => $request->address,
    'dob' => $request->dob,
    'dvla' => $request->dvla,
    'national_insu_numb' => $request->national_insu_numb,
    'pco_licence_no' => $request->pco_licence_no,
    'doc_dvla' => $file1,
    'doc_cnic' => $file2,
    'doc_utility' => $file3,
   // 'doc_other' => $file4,
    'payment_status'=>0,
];

        $data = Booking::create($dataBooking);
        $booking_id = $data->id;
        return redirect()->route('step3',
            [
                'id'=>$request->id, 
                'step'=>3,
                'mile'=>$request->mile,
                'after_month'=>$request->duration, 
                'booking_id'=>$data->id,
                'price' => $request->price, 
                'booking_id' => $booking_id,


            ]
        )->with('success','Documents Added Successfully');


/*
        foreach($request->file as $documents)
            {
                $imageUrl = Storage::putFile($vehicleImagesFolder, new File($documents));
                 VehicleImage::create(
                    [
                        'vehicle_id'=> $vehicle->id,
                        'images' => $imageUrl
                    ]
                ); 
            }//foreach */




          } //end if





    }//end function
  

}

