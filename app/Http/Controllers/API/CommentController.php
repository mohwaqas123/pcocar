<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Device;
use App\models\Category;
use App\models\Song;
use App\models\Banner;
use App\models\Video;
use App\models\LikeVideo;
use App\models\CommentVideo;
use App\models\commentReply;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;
use Storage;
use Illuminate\Http\File;

class CommentController extends Controller
{
    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
    }

    public function postComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',   
            'video_id' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user_id = JWTAuth::toUser($request->token)->id;

        CommentVideo::create([
            'message' => $request->message,
            'comment_by' => $user_id,
            'video_id' => $request->video_id
        ]);

        $allComments = CommentVideo::where('video_id', $request->video_id)->get();

        foreach($allComments as $comment)
        {
            $commentUser = User::where('id', $comment->comment_by)->first();

            $comment->comment_by = $commentUser;
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'Comment posted successfully.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $allComments
        ]);
    }

    public function listComment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }
        
        $allComments = CommentVideo::where('video_id', $request->video_id)->get();

        foreach($allComments as $comment)
        {
            $commentUser = User::where('id', $comment->comment_by)->first();

            $comment->comment_by = $commentUser;
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'Comment posted successfully.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $allComments
        ]);
    }
}
