<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\CarModel;
use App\models\Make;
use Validator;
use Redirect;
use Input;

class carmodelController extends Controller
{
    public function index()
    {
        $carmodels = CarModel::select('name', 'id', 'make_id', 'status')->orderBy('id', 'DESC')->paginate(10);
        $makes = Make::all();
        $title = 'Car Model List';
          $url = "/admin/dashboard";
          
        $data = [
               'carmodels'=> $carmodels,
               'makes'=> $makes,
               'title'=> $title,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Model'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Model List' ,
                



                
        ];  
        return view('admin.carmodel.list')->with($data);
    }

    // create new state
    public function create()
    {
        $makes = Make::select('name', 'id')->orderBy('name', 'ASC')->get();

        $url = "/admin/dashboard";
        $title = 'Add Car Model';
          $data = [
               'makes'=> $makes,
               'title' =>$title,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Model'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Add new Model' ,
         
                
        ];  
        return view('admin.carmodel.create')->with($data);
    }

    // store new state
    public function store(Request $request)
    {



         $validator = $request->validate([
            'name' => 'required|max:150|unique:car_models',
            'make_id' => 'required',
            'status' => 'required',
            
        ]); 
     /*   $vehicleData = [
            'name' => $request->name, 
        ];
 
       // $vehicle = make::create($vehicleData);



        $validator = Validator::make($request->all(), [
            'name' => 'required|max:150|unique:car_models',
            'make_id' => 'required',
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }*/

        $carmodelData = [
            'name' => $request->name,
            'make_id' => $request->make_id,
            'status' => $request->status,
        ];

        CarModel::create($carmodelData);

        return redirect()->route('listcarmodels')->with('success', 'Car Model Added Successfully');
    }


    // edit state
    public function edit($id)
    {
        $carmodel = CarModel::find($id);
        
        $makes = Make::orderBy('name', 'ASC')->get();
   
        $makeName = Make::select('id', 'name')->where('id', $carmodel->make_id)->first();
  
        $url = "/admin/dashboard";
        $title = 'Edit Car Model';
        $data = [
            'carmodel' =>$carmodel,
            'makes' => $makes,
            'makeName' => $makeName,
            'title' =>$title,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Model'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Edit Car Model' ,
            


        ];
        return view('admin.carmodel.edit')->with($data);
    }

 public function destory($id)
    {
       
          
       $carmodel = CarModel::findOrFail($id);
      $carmodel->delete();
      return redirect()->route('listcarmodels')->with('success', 'Record delete Successfully');  
    }
    // update State
    public function update(Request $request)
    {
        $carmodel = CarModel::find($request->id);

        $request->validate( [
            'name' => 'required|max:150',
            'make_id' => 'required',
            'status' => 'required',
        ]);

        $carmodelData = [
            'name' => $request->name,
            'make_id' => $request->make_id,
            'status' => $request->status,
        ];

        $carmodel->update($carmodelData);

        return redirect()->route('listcarmodels')->with('success', 'Car Model Updated Successfully');
    }

}
