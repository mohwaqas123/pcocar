@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            
                            
                            
                            <!-- Datatable -->
                            
                            <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
                            <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
                            <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
                            <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width: 10px">No.</th>
                                        <th>Customer Detail</th>
                                        <th>Vehicle</th>
                                        <th>Booking</th>
                                        <th>Price</th>
                                        <th style="width:100px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($booking as $bookings)
                                    <tr>
                                        <td>{{$bookings->id}}</td>
                                        <td>{{@$bookings->user->name}} <br />
                                            {{@$bookings->user->email}} <br />
                                            {{@$bookings->user->phone}}
                                        </td>
                                        <td>{{@$bookings->vehicle->car_make->name}}<br>
                                            {{@$bookings->vehicle->year}} <br>
                                            {{@$bookings->vehicle->name}}
                                            {{@$bookings->vehicle->model->name}}
                                            {{@$bookings->vehicle->licence_plate_number}}
                                        </td>
                                        <td>
                                            Created: {{$bookings->created_at}}<br>
                                             Expire:  {{$bookings->booking_drop_off_date}} <br>
                                            Duration:  {{$bookings->duration}} Weeks
                                        </td>
                                                @if($bookings->insurance_type == 1)
                                       <?php
                                        $price = $bookings->price;
                                       ?>
                                       @else
                                       <?php
                                        $price = $bookings->price + 70;
                                       ?>
                                       @endif
 <td>{{$price}} Per week</td>
                                       

                                        <td style="width: 20%;">  
                                            <a href="{{route('customerview', $bookings->id)}}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"
                                                data-toggle="tooltip" data-placement="Top" data-original-title="View Booking" ><i class="fa fa-eye"></i>
                                            </a>
                                            Booking Completed
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>             
<div class="modal fade" id="new" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Why do you want to cancel It? <span id='placer'></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            @foreach ($booking as $bookings)
        <form  action="{{ route('adminremovebooking' , $bookings->id) }}"     method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <input type="hidden" name="id" id="id" value="{{$bookings->id}}">
                
                  <label for="make" class="col-form-label">Please Enter Reasons<span class="text-danger">*</span></label>
                <div class="col-md-12">
                    <textarea  type="text" class="form-control" rows="10" cols="70" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                </div>
                <center> <button type="submit" class="btn btn-primary">Submit</button></center>
          </form>
            @endforeach
        </div>

      </div>
    </div>
  </div>
@endsection
@section('scripts')
 
<script>
$(document).ready(function() {
$('#datatable_tbl').DataTable();
} );
</script>
<script type="text/javascript">
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
"columnDefs": [
{ "orderable": false, "targets": [4,5,6] },
],
"bPaginate": false,
});
} );
</script>
@endsection