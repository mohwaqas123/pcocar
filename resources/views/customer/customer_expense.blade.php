@extends('layouts.customer')
@section('styles')
@endsection
@section('content')



<div class="content-page">
    <div class="content">
      @if(session()->has('message'))
 
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
 
    @endif
     @if(session()->has('error'))
     
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    

    @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <h4>Customer</h4>
                        Name: {{@$booking->user->name}} <br>
                        Email: {{@$booking->user->email}} <br>
                         Phone: {{@$booking->user->phone}} <br>
                         
                        
                    </div>
                </div>
                 <div class="col-md-6">
                    <div class="card-box">
                        <h4>Vehicle</h4>
                         Vehicle Name:  {{@$booking->vehicle->name}} <br>
                        Model:  {{@$booking->vehicle->year}} <br>
                        Price: {{@$booking->vehicle->price}} <br>
                        Vehicle Licence Plate Number  {{@$booking->vehicle->licence_plate_number}} <br>
                        Postal pickup dropoff: {{@$booking->vehicle->postal_pickup_dropoff }}
                        <!-- end row -->
                    </div>
                </div>
                  
                
                   
                
            </div>
                   <form method="POST" enctype="multipart/form-data"
                  action="{{ route('customer_expense_add') }}" >
                  <input type="hidden" value="{{@$booking->id}}" name="id" />
                  {{csrf_field()}}
                  
                  <div class="row">
                    	 <div class="col-md-12 p-5" >
                      <div class="card-box">
                           <div class="row" >
					                    	<div class="form-group col-md-6">
					                        <label for="song name" class="col-form-label">Supplier<span class="text-danger">*</span></label>
                                            <input   type="text" name="name" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the Shop name" required>
                                            </div>

										            <div class="form-group col-md-6">
			                            <label for="song name" class="col-form-label">Price
                                    <span class="text-danger">*</span></label>
                                            <input max="10" min="3" type="text" pattern="^[0-9-+\s()]*$" name="price" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the Price" required>
                                       </div>
                                     </div>
                                     <div class="row">
                                     <div class="form-group col-md-6">
                                  <label for="song name" class="col-form-label">Vehicle Part
                                    <span class="text-danger">*</span></label>
                                            <input  type="text" name="part" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the vehicle part" required>
                                       </div>

                                       <div class="form-group col-md-6">
                                        <input type="radio" class="radioBtn" name="Radio" id="Radio" value="ABC" required>Excluding VAT
                                            <input class="radioBtn" type="radio" name="Radio" id="Radio" value="PQR" required >Including VAT
                                          </div>
                                     </div>
                                       <div class="form-group col-md-6" id="box">
                                  <label for="song name" class="col-form-label">VAT Number 
                                    <span class="text-danger">*</span></label>
                                             <input max="10" min="3" type="text" name="vat_number" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the VAT Number" >
                                       </div>
                                         
                                     <div class="form-group col-md-6">
                                  <label for="song name" class="col-form-label"> Invoice Date
                                    <span class="text-danger">*</span></label>
                                           <input  type="date"  required="required"  class="form-control" value="{{@session()->get('dob')}}" autocomplete="off" placeholder="Date of Birth" name="dob" id="datepicker" />
                                       </div>
                                        <div class="form-group col-md-12">
                                  <label for="make" class="col-form-label">Please Enter Details<span class="text-danger">*</span></label> 
                      			       <textarea  type="text" class="form-control w-50" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                      		        </div>

                                        <p class="col-form-label">Upload Damage part/s Image
                                          <br>
                                          <input type="file" id="images" name="images[]" multiple>
                                          </p>
                                          @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                                @endif
                                           <br>
                                            <p class="col-form-label">Upload Invoice Image
                                            <br>
                                              <input id="file-input"  name="doc_logback" type="file" />
                                           
                                                </p>
                                                 @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                                @endif
                                             <br>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                         </div>
                      </div>
                  </div>
         
                </form>
         
                
            </div>   </div> 
            <!-- end row -->
            </div> <!-- container -->

 <style>
 	
 
 	</style>

@endsection
@section('scripts')
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
  var fileName = $(this).val().split("\\").pop();
  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});




$('input[type="radio"]').click(function(){
        if($(this).attr("value")=="ABC"){
            $("#box").hide('slow');
        }
        if($(this).attr("value")=="PQR"){
            $("#box").show('slow');

        }        
    });
$('input[type="radio"]').trigger('click');
</script>

@endsection
