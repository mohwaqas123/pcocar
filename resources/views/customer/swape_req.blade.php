@extends('layouts.customer')
@section('styles')
@endsection
@section('content')



<div class="content-page">
    <div class="content">
      @if(session()->has('message'))
 
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
 
    @endif
     @if(session()->has('error'))
     
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    

    @endif
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                        <h4>Customer</h4>
                        Name: {{@$booking->user->name}} <br>
                        Email: {{@$booking->user->email}} <br>
                         Phone: {{@$booking->user->phone}} <br>
                         
                        
                    </div>
                </div>
                 
                  <div class="col-md-6">
                    <div class="card-box">
                        <h4>Current Booking</h4>
                         Date:  {{@$booking->booking_start_date}} to {{@$booking->booking_drop_off_date}}  <br>
                        Duration: {{@$booking->duration}} <br>
                        Price : {{@$booking->price}} <br>
                        DVLA : {{@$booking->dvla}} <br>

                        <!-- end row -->
                    </div>
                </div>
            </div>
                   <form method="POST" enctype="multipart/form-data"
                  action="{{ route('customer_swap_req_send') }}" >
                  <input type="hidden" value="{{@$booking->id}}" name="id" />
                  {{csrf_field()}}
                  
                  <div class="row">
                    	 <div class="col-md-12 p-5" >
                      <div class="card-box">
                           <div class="row" >
					                    	<div class="form-group col-md-6">
					                        <label for="song name" class="col-form-label">Why do you want to swap the car?<span class="text-danger">*</span></label>
                                   <textarea  type="text" class="form-control " name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                                  </div>
                                    <div class="form-group col-md-6">
                                  <label for="song name" class="col-form-label">When do you want to swap the car?<span class="text-danger">*</span></label>
                                   <input  type="date"     class="form-control" autocomplete="off" value="{{@session()->get('dob')}}" placeholder="Date of Birth" name="dob"   />
                                  </div>
										              <p><b>Note</b><span class="text-danger">*</span>Swapping depends upon the availability of fleet.Swapping with upper model plate may cause additionals charges</p>
                                     </div>
                                    
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                         </div>
                      </div>
                  </div>
         
                </form>
         
                
            </div>   </div> 
            <!-- end row -->
            </div> <!-- container -->

 <style>
 	
 
 	</style>

@endsection
@section('scripts')

<script>

$( function() {
    $( "#datepicker" ).datepicker(
      {
       dateFormat:'mm/dd/yy', 
       changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0", // last hundred years
       minDate: new Date(1950,1,31), 
       maxDate: new Date(1997,11,30)
     });
  });


</script>
@endsection
