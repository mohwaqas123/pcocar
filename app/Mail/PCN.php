<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PCN extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pcn_image,$user_name,$vehicle,$ref_number)
    {
           $this->pcn_image = $pcn_image; 
           $this->user_name = $user_name; 
           $this->vehicle = $vehicle; 
           $this->ref_number = $ref_number; 

     
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('mail.pcn',['pcn_image' => $this->pcn_image,
            'user_name' => $this->user_name,
            'ref_number' => $this->ref_number,
            'vehicle' => $this->vehicle]);
    }
}
