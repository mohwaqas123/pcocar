@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


    
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                  


                    <div class="card-box">
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            
                            <!-- <table id="songsListTable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Vehincle Detail</th>
                                        <th>description</th>
                                        <th>Thumbnail</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>  -->
                                
                                <!-- Datatable -->
                                
                                <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
                                <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
                                <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
                                <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
   @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif
                                <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Customer Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Booking</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($user as $customer)
                                        <tr>
                                            <td>{{ $customer->id }}</td>
                                            <td>{{ $customer->name }}</td>
                                            <td> {{ @$customer->email}}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td> total {{count(@$customer->booking)}} </td>
                                            <input type="hidden" name="id" value="{{ $customer->id }}">

                                            <td style="width: 20%;">
                                             
                                                <a href="{{route('user_edit', $customer->id)}}"  class="btn btn-sm btn-icon waves-effect waves-light btn-primary" data-toggle="tooltip" data-placement="Top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                                @if (count(@$customer->booking) == 0)
                                               <a href="{{route('bookingview', $customer->id)}}"  onclick="return confirm('You want to remove your selected Customer?')"class="btn btn-sm btn-icon waves-effect waves-light btn-danger " data-toggle="tooltip" data-placement="Top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                                @else
                                                
                                                @endif

                                                
                                                <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Stripe" href="{{ route('chargeamount' , $customer->id) }}">
                                                    <i class='fa fa-cc-stripe'></i> </a>

<!-- 
                                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Customer History" href="{{ route('customer_history' , $customer->id) }}">
                                                    <i class="fa fa-history"></i> </a> -->
                                                
                                                
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')

    <script>
    $(document).ready(function() {
    $('#datatable_tbl').DataTable();
    } );
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
    // Default Datatable
    $('#songsListTable').DataTable({
    "columnDefs": [
    { "orderable": false, "targets": [4,5,6] },
    ],
    "bPaginate": false,
    });
    } );
    </script>
    @endsection