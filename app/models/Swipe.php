<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Swipe extends Model
{
     protected $table = 'swipe';

    protected $fillable = [
        'booking_id', 'old_vehicle_id', 'new_vehicle_id' , 'description', 'swipe_date_time' ,  
    ];


     public function old_vehicles()
    {
        return $this->belongsTo('App\models\Vehicle','old_vehicle_id', 'id');
    }

      public function new_vehicles()
    {
        return $this->belongsTo('App\models\Vehicle','new_vehicle_id', 'id');
    }
     public function booking()
    {
        return $this->belongsTo('App\models\Booking','booking_id', 'id');
    }

/*
    public function count_total_booking()
    {
        //return $this->hasMany('App\models\Vehicle','partner_id', 'id');
        return $this->hasManyThrough('App\models\Booking', 'App\models\Vehicle' , 'id','vehicle_id');
    }


   public function count_total_driver()
    { 
        return $this->hasMany('App\models\Driver', 'partner_id','id');
    }*/


}
