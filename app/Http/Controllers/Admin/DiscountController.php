<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Coupon;

class DiscountController extends Controller
{
	 // $voucher->code = $this->generateRandomString(6);

	public function index(Request $request)
    {

      $coupon = Coupon::get();
         $title = 'Create Discount ';
        $url = "/admin/dashboard";
         $data = [
            'title' =>$title,
            'coupon' =>$coupon,
            'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Promo Code List',
        ]; 
     return view('admin.discount.list')->with($data);
  }
    public function generateRandomString($length = 5)
    {
	
       $characters = '0123456789abcdefghijklmnopqrstuvwxyz@:?<`!")£$%^&*(>:}{';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $url = "/admin/dashboard";
        $title = 'Create Discount ';
          $data = [
            'randomString'=>$randomString,
            'title' =>$title,
            'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Create Promo',
                
        ];  
           return view('admin.discount.create')->with($data);
 		}
		public function store(Request $request)
    {
 

      request()->validate([ 
            'percentdiscount' => 'required|unique:coupons',   
   ]);
  	 $length = 5;
  	 $characters = '0123456789abcdefghijklmnopqrstuvwxyz@:?<`!")£$%^&*(>:}{';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        	 
		 $CouponData = [
            
            'percentdiscount' => $request->percentdiscount,
            'discountcode' => $request->randomString,
            'date_from' => $request->date_from,
            'date_To' => $request->date_to,
            'status' => $request->status,
            'Title' => $request->Title,
          ];

            
         $coupon = Coupon::create($CouponData);
      
    return redirect()->back()->with('message','Discount created Successfully');
    }
     public function destory($id)
    {  

      $coupon = Coupon::find($id);
      $coupon->delete();
     return view('admin.dashboard.dashboard');
           
           
    }
    public function showEditdiscount(Request $request)
    { 
        $coupon = Coupon::find($request->id);
        $title = 'Edit Discount ';
        $url = "/admin/dashboard";
        
        $data = [
            'coupon'=>$coupon,
            'title' =>$title,
            'breadcrum' => '<a style="color:black; &nbsp" href="'. url( $url ).'"  >  Dashboard</a>  > '.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>' .'Promo Code Edit',
                
        ];  

        return view('admin.discount.edit')->with($data);
    }
    public function discount_update(Request $request)
    {

        $coupon = Coupon::find($request->id);
     
  
       $CouponData = [   
            'percentdiscount' => $request->percentdiscount,
            'discountcode' => $request->randomString,
            'date_from' => $request->date_from,
            'date_To' => $request->date_to,
            'status' => $request->status,
            'Title' => $request->Title,
          ];
          $coupon->update($CouponData);


          $data=[
         
            'coupon'=>$coupon,
            'title' => 'Coupon' 

          ];
        return redirect()->route('code/generate/list')->with($data);
       // return view('admin.discount.list')->with($data);
    }
 
}

 
