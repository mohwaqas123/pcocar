@extends('layouts.customer')
@section('styles')
@endsection
@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        {{--  <p class="text-muted font-14 m-b-30">
                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                        </p>  --}}
                        <div class="card-box">
                            
                            @if($errors->any())
                                {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif

                            <form role="form"  action="{{ route('customer_updaet_user') }}"  method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                              
                                    <div class="form-group col-md-6">
                                        <label for="name">Customer Name<span class="text-danger required">*</span></label>
                                          
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Customer Name"   required 
                                        value="{{ old('name', $user->name) }}"
                                         >
                                      
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone">Customer phone <span class="text-danger required">*</span></label>
                                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="phone" placeholder="Enter Customer phone"   required value="{{ old('phone', $user->phone) }}" >
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    
                                    <div class="form-group col-md-6">
                                        <label for="name">Date of Birthday <span class="text-danger required">*</span></label>
                                        <input  type="date"  required="required"  class="form-control"   autocomplete="off" placeholder="Date of Birth" name="dob" id="datepicker" value="{{ old('date_of_birth', $user->date_of_birth) }}"   />
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                      <div class="form-group col-md-6">
                                        <label for="name">Description <span class="text-danger required">*</span></label>
                                        <textarea type="text" class="form-control @error('name') is-invalid @enderror" name="description" placeholder="Enter country name"   required value="{{ old('description', $user->description) }}" >{{$user->description}}</textarea>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                   

                                  <div class="col-md-6">
                                    <button type="submit" class="btn btn-info btn-flat">Update</button>
                                    <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                                  </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')


<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
        });
    } );
</script>
@endsection
