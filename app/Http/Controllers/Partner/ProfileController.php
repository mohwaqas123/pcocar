<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Partner;
use App\models\Booking;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use Illuminate\Support\Facades\Mail;
use Session;
use App\models\Driver;

class ProfileController extends Controller
{
  public function partner_profile(Request $request){

        $partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
        $data= [
                    'partner' => $partner,
                ];
        return view('partner.profile')->with($data);
 
        }
 
 public function partner_profile_view(Request $request){
        
        $partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
        $data= [
                    'partner' => $partner,
                ];
              
        return view('partner.profile_view')->with($data);
 
        }
 
    	public function partner_profile_update(Request $request)
		{
            
   		$partner =   Partner::where('id', $request->session()->get('p_user_id'))->first();
 		$datapartner = [

             'name' => $request->name,
             'email' => $request->email,
             'phone' => $request->phone,
             'address' => $request->address, 
              'business_address' => $request->business_address,
            'pickup_address' => $request->pickup_address,
            'dropoff_address' => $request->dropoff_address,
            'bank_name' => $request->bank_name,
         'account_number' => $request->account_number,
         'account_holder_name' => $request->account_holder_name,
         'sort_code' => $request->sort_code,
            ];  

		$partner->update($datapartner);
	    
             
        $data= [
                    'partner' => $partner,
                 ];
        return view('partner.profile')->with($data);
   }
}
