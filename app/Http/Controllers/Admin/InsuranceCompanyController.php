<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\InsuranceCompany;

class InsuranceCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $url = "/admin/dashboard";
        $title = 'Admin setting';
        // $setting = Setting::where('id',  1)->first(); 

          $data = [
               'msg'=> '',
               'title' =>$title,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Insurance'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Insurance Company' ,
        
            ]; 
         return view('admin.insurance.create')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $url = "/admin/dashboard";
        $title = 'Admin setting';
        $insurance_company = InsuranceCompany::select('insurance_company', 'id','int_status')->orderBy('id', 'DESC')->paginate(10);
        $data = [
                'msg'=> '',
               'title' =>$title,
               'insurance_company' => $insurance_company,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Insurance'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Insurance Company' ,

                
        ]; 
            return view('admin.insurance.list')->with($data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $data = [
            'insurance_company' => $request->name, 
            'int_status' => $request->status, 
        ];
        $insurance_company = InsuranceCompany::create($data);
    return view('admin.insurance.list')->with('message','Data Save Successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $insurance_company = InsuranceCompany::find($id);
        
        $url = "/admin/dashboard";
        $title = 'Edit Driver';
          $data = [
            'insurance_company' => $insurance_company,
            'title'=>   $title,
            'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Insurance'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Insurance Company Edit' ,

           ]; 

    return view('admin.insurance.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $InsuranceCompany = InsuranceCompany::find($request->id); 
        $url = "/admin/dashboard";
        $title = 'Driver List';
          $data = [
             'insurance_company' => $request->name,
             'int_status' => $request->status,
             
              'InsuranceCompany'=> $InsuranceCompany,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Insurance'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Insurance Company Edit' ,
                    
                    ];   

       $InsuranceCompany->update($data);


      return redirect()->back()->with('message','Data Update Successfully.');




   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
