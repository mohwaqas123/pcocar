<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class partner extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
           public $name;
           public $business_name;
           public $email;
           public $number;
           public $business_web_site;
           public $number_of_cars;
           public $business_details;

          
            

    public function __construct($name,$email,$number,$business_name,$business_web_site,$number_of_cars,$business_details)
    {
        $this->name = $name;
        $this->email = $email;
        $this->number = $number;
        $this->business_name = $business_name;
        $this->business_web_site = $business_web_site;
        $this->number_of_cars = $number_of_cars;
        $this->business_details = $business_details;

  
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.partner', ['name' => $this->name, 'email' => $this->email ,'number' => $this->number,'business_name' => $this->business_name, 'business_web_site' => $this->business_web_site ,'number_of_cars' => $this->number_of_cars,'business_details' => $this->business_details,   ]);
    }
}
