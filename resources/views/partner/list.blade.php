@extends('layouts.admin')

@section('styles')

@endsection

@section('content')


<div class="content-page">
    <div class="content">


        <!---start code 
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.js"></script>

<select class="select2" style="width:200px" >
  <option value="1">Apple</option>
  <option value="2">Banana</option>
  <option value="3">Carrot</option>
  <option value="4">Donut</option>
</select>


<script>
    $('.select2').select2({});

// on first focus (bubbles up to document), open the menu
$(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
  $(this).closest(".select2-container").siblings('select:enabled').select2('open');
});

// steal focus during close - only capture once and stop propogation
$('select.select2').on('select2:closing', function (e) {
  $(e.target).data("select2").$selection.one('focus focusin', function (e) {
    e.stopPropagation();
  });
});
</script> -->
<!--- end code  -->



        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">











                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                            
<table id="datatable_tbl" class="table table-striped table-bordered"  >
      <thead>
        <tr>
        <th >Sr.#</th>
        <th>Name</th>
        <th style="width:5px;">Email</th>
        <th>Commission</th>
        <th>Total Vehicles</th>
        <th>Total Booking</th>
        <th style="width:95px;">Action</th>
        </tr>
        </thead>
        <tbody>
        	   @foreach($partner as $partners)
        	   <tr>
        	   <td>{{ $partners->id }}</td>
        	   <td>{{ $partners->name}}</td>
        	   <td>{{ $partners->email}}</td>
              <td>{{ $partners->commission}}%</td>
              <td> {{count($partners->count_vehicles)}}</td>
              <td> {{count($partners->count_total_booking)}} </td>
 				  <td style="width:95px;">
<!-- <a href="{{ route('adminpartnerEdit', $partners->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="fa fa-pencil"></i></a> 

<a href="{{ route('adminpartnerdestory', $partners->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></a> 
<a href="{{ route('adminpartnerdetail', $partners->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>
 -->


<div class="dropdown show">
  <a class="btn-ligh btn btn-s dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Action
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a href="{{ route('adminpartnerEdit', $partners->id) }}" class="dropdown-item"><i class="fa fa-pencil"></i> Edit</a> 
<a href="{{ route('adminpartnerdestory', $partners->id) }}" class="dropdown-item"><i class="fa fa-trash"></i> Remove</a> 
<a href="{{ route('adminpartnerdetail', $partners->id) }}" class="dropdown-item"><i class="fa fa-eye"></i> View</a>

     
  </div>
</div>

 </td>

      </tr>

 				 @endforeach
 </tbody>
 
</table>
 
<input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
 
   @endsection

@section('scripts')
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
