<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\models\Booking;
use App\models\Swipevehiclehistory;
use App\models\Customerinsurance;
use App\models\Vehicle;
use Illuminate\Http\File;
use App\models\User;
use Stripe;
use Imagick;
use Carbon\Carbon;
use App\Helper\Helper;
use App\models\PaymentHistory;
use Mail;


class RenewBookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        
       $booking = Booking::find($request->id);
         
       $vehicle = Vehicle::where('id', $booking->vehicle_id)->first();
      
        //if ($vehicle->bookingStatus == 0 )   {
               //create new booking

        if($request->vehicle== 2){ 
        
          $booking->insurance_type = 0;
       
       
        }
        if ($booking->insurance_type == 1   || $request->vehicle2== 1)  
        {
        $booking_start_date = date('Y-m-d H:i:s',strtotime($booking->booking_drop_off_date.' + 1 minute')); //$booking->booking_drop_off_date;   //Helper::get_start_date($booking->booking_drop_off_date); //get starting date of booking
          $exp_time = date("H:i:s",strtotime($booking_start_date));
          $expire_date = Helper::get_expire_date_from_any_date($request->total_duration,$booking_start_date) . ' '.$exp_time;
          // $expire_date = date('Y-m-d H:i:s',strtotime(Helper::get_expire_date_from_any_date($request->total_duration,$booking_start_date).' + 1 minute'));
        
 //dd($expire_date);
$per_week_rent = $request->booking_cost;
$per_week_rent_included_insuranance_cost = $request->renew_price_per_week;
          //$price = Helper::get_discount_duration_price($request->duration,Helper::get_price_with_check_discount($booking->vehicle->price));
$price = $request->renew_price_per_week;//Helper::get_discount_duration_price($request->duration,Helper::get_price_with_check_discount($request->renew_price_per_week));
     //dd($price);
          $amount = $per_week_rent_included_insuranance_cost * 100 ;
          $discount = Helper::get_discount_percent_func($request->total_duration);

                     
                            $dataBooking = [
                                'name' => $request->name,
                                'user_id' => $booking->user_id, 
                                'vehicle_id' => $booking->vehicle_id,
                                'duration' => $request->total_duration,
                                'miles' => 1000,
                                'address' => $booking->address,
                                'dob' => $booking->dob,
                                'dvla' => $booking->dvla,
                                'national_insu_numb' => $booking->national_insu_numb,
                                'pco_licence_no' => $booking->pco_licence_no,
                                'doc_dvla' => $booking->doc_dvla,
                                'doc_cnic' => $booking->doc_cnic,
                                'expiredate' => $booking->expiredate,
                                'expiry_date' =>   $expire_date, //Helper::get_expire_date($request->duration),
                                'price'=>  $per_week_rent,
                                'discount'=>  $discount,
                                'booking_status'=>1,
                                'pco_paper_licence'=>$booking->pco_paper_licence,
                                'promo' => $booking->promo,
                                'doc_other' => $booking->doc_other,
                                'payment_status'=>0,   //later we change it 0, because if we now send it 0 user can do multiple time booking with same id. 0 will be changed when we confirm payment.expiredate
                                'first_payment'=>  $per_week_rent_included_insuranance_cost,
                                 'booking_start_date' => $booking_start_date,  
                                 'booking_drop_off_date' =>  $expire_date,
                                 'agreement_status'=>1,
                                 'reference_booking'=> $booking->id,
                                 'insurance_type'=> 1,
                            ]; 
                              $data_new_booking = Booking::create($dataBooking);
              
               
          $vehicleImagesFolder = 'documents_uploads'; 
            if (!Storage::exists($vehicleImagesFolder)) 
            {
                Storage::makeDirectory($vehicleImagesFolder);
            }
        if($data_new_booking->insurance_type == 1 || $request->value== 1) 
         { 
         
          $file3 = Storage::putFile($vehicleImagesFolder, new File($request->customer_private_insurance));
        
        $im = new Imagick();
       
        $im->readImage(base_path()."/storage/app/".$file3);
         $filename = rand().'.png';
        $im->writeImages(base_path()."/storage/app/documents_uploads/pdf/" .$filename, true);
          } 
        else
        {
         
         $file3 = '';
         } 
 
                         

    $sh =  $request->hr;
    $sm =  $request->min;
    $esh =  $request->ehr;
    $esm =  $request->emin;
    $Insurance_started_date = date("$request->Insurance_started_date $sh:$sm:s");

    $Insurance_end_date = date("$request->Insurance_end_date $esh:$esm:s");
    

$datacustomerinsurance = [
   'booking_id' =>$data_new_booking->id,
    'user_id' => $booking->user_id,
    'policy_number' => $request->policy_number,
    'cover' => $request->cover,
    'company_name' => $request->Insurance_company_name,
    'email' => $request->Insurance_company_email,
    'contact_number' => $request->Insurance_company_contact_number,
    'start_date' => $Insurance_started_date,
    'end_date' => $Insurance_end_date,
     'customer_private_insurance' =>$filename,
     // 'file3' => $file3,


];
 


 
        $data = Customerinsurance::create($datacustomerinsurance);

                                $product_id = env('PRODUCT_ID');// "prod_IO76QwwM1ZkCI0";
                                $currency = 'GBP';  
                                
              $nickname = $vehicle->licence_plate_number.'-'.$data_new_booking->id;
                    $subscription_amount_charge_every_week =$amount ;
                                $booking_duration =  $request->total_duration;
                                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                               

                              //Step1: Create Price for specific Product
                                            $price = Stripe\Price::create([
                                            'product' => $product_id,
                                            'unit_amount' => $amount,
                                            'currency' => $currency,
                                            'nickname'=>$nickname,
                                            'recurring' => [
                                              'interval' => 'week',
                                            ],
                                          ]);
                                            $price_id = $price->id;

                              //step2 getting custtom strip id
                               $stripe_customer_id = $booking->user->stripe_customer_id; 


                                    //step3 charge customer first week booking amount
                                      $return = Stripe\Charge::create ([
                                                    "amount" => $amount,
                                                    "currency" =>  $currency, 
                                                    "description" => "PCOCAR Booking Vehicle ".$nickname,
                                                    'customer' =>  $stripe_customer_id,
                                            ]);


                                     
                                     //Step4: Assign Subsribed Plan
                                     if($return->status == 'succeeded'){ 
   
                                                   // $price_id = $price->id; 
                                                    $booking_start_date =$booking_start_date;
                                                   // $todayDate = Carbon::now()->format('Y-m-d');
                                                    $startDate =strtotime($booking_start_date);   
                                                    $timespn =  strtotime('+1 week', $startDate) ;

                                                    $booking_duration = "+".  $booking_duration ." week";
                                                    $timespn_cancel =  strtotime( $booking_duration, $startDate) ;
                                                  
                                                     $subscription = Stripe\SubscriptionSchedule::create([
                                                        'customer' =>    $stripe_customer_id,
                                                        'start_date' =>  $timespn,
                                                        'end_behavior' => 'release',
                                                        'phases' => [
          [
                'end_date'=>$timespn_cancel,
                   'items' => [  //  ['price' => $price_id ],
                       ['price_data'=>[
                              'currency'=>$currency,
                                          'product'=>$product_id,
                                                                               //'unit_amount'=>$subscription_amount_charge_every_week,
                                    'unit_amount_decimal' =>$subscription_amount_charge_every_week,
                              'recurring'=> ['interval'=>'week' , 'interval_count'=>1 ] ] ],  
                                                                          ],   

                                                           // 'iterations' => 12,
                                                          ],
                                                        ],
                                                      ]);
 

                                    //after success payment and generated new booking actions
                                            $transaction_id = $return->id;
                                            $date = $data_new_booking->booking_start_date;//date('Y-m-d');
                                            $package_days =30;// '+ '.$request->package_days. ' days'; 
                                            $expire_date=date('Y-m-d', strtotime($date.  $package_days));


                                    $date_time = strtotime($date);
                                    $added_months = "+".$request->expire_month_after." month";
                                    $final_date = date("Y-m-d", strtotime($added_months, $date_time));

                                    //refernce booking status change in previous booking record
                                      $booking->reference_booking_status =1; //if payment success update payment status
                                      $booking->booking_status = 4;
                                      $booking->save(); 
         $name =  $booking->user->name;
      $title = "Booking Status"; 
      $email_to = 'payments@pcocar.com';// 'payments@pcocar.com';//$user->email;//'waqas.ger@gmail.com';
        $booking = Booking::where('id', $data_new_booking->id)->with('vehicle')->with('user')->first();
        $vehicle = Vehicle::where('id', $data_new_booking->vehicle_id)->with('car_make')->with('car_model')->first();
       // $ses_values = $request->session(); 
         $price_per_week = $data_new_booking->price;
        $price_total = $data_new_booking->price  + env('INSURANCE_CODE');//$amount;//$data_new_booking->price_final_total_book;   
        $booking_start_date =  $data_new_booking->booking_start_date;         
 


    $data_new_booking->payment_status =1; //if payment success update payment status
    $data_new_booking->save(); 


 

     $data = [
            'user_id' => $data_new_booking->user_id,
            'amount' => $data_new_booking->price,
            'transaction_id' =>$return->id,
            'expire_date' => $final_date,
            'month_duration' => 1,//$request->expire_month_after,
            'per_month_amount'=>  $data_new_booking->price,
            'vehicle_id' =>  $data_new_booking->vehicle_id,  
            'booking_id' => $data_new_booking->id,
            'payment_type'=>0,
        ];
        $user = PaymentHistory::create($data);



$payment_mode=1;
 
$insurance=0;
Mail::to([$email_to,'bookings@pcocar.com'])->cc($booking->user->email)->send(new \App\Mail\Booking(
    $payment_mode,
    $name, 
    $title,
     $booking , 
     $vehicle , 
     $request->total_duration,  
     $data_new_booking->discount,
     $price_per_week,
     $price_total,
     $booking_start_date,
     $insurance

   )); 

   //$veh = Vehicle::findOrFail($request->id);     
        $vehicle->bookingStatus =1; 
        $vehicle->save();
   
    $this->payment_history_generate($data_new_booking , $data_new_booking->booking_start_date);   

     //$this->booking_payment_relation($booking);   
 

 }
  return redirect()->route('vehicle_booking')->with('message','Renew booking is Done');
     
        }
        else
        {

         
               
          $booking_start_date = date('Y-m-d H:i:s',strtotime($booking->booking_drop_off_date.' + 1 minute')); //$booking->booking_drop_off_date;   //Helper::get_start_date($booking->booking_drop_off_date); //get starting date of booking
          $exp_time = date("H:i:s",strtotime($booking_start_date));
 
          $expire_date = Helper::get_expire_date_from_any_date($request->total_duration,$booking_start_date) . ' '.$exp_time;
          // $expire_date = date('Y-m-d H:i:s',strtotime(Helper::get_expire_date_from_any_date($request->total_duration,$booking_start_date).' + 1 minute'));
        
 //dd($expire_date);



$per_week_rent = $request->booking_cost;
$per_week_rent_included_insuranance_cost = $request->renew_price_per_week;



          //$price = Helper::get_discount_duration_price($request->duration,Helper::get_price_with_check_discount($booking->vehicle->price));
$price = $request->renew_price_per_week;//Helper::get_discount_duration_price($request->duration,Helper::get_price_with_check_discount($request->renew_price_per_week));

     //dd($price);
          $amount = $per_week_rent_included_insuranance_cost * 100 ;
          $discount = Helper::get_discount_percent_func($request->total_duration);

         
                            $dataBooking = [
                                'name' => $request->name,
                                'user_id' => $booking->user_id, 
                                'vehicle_id' => $booking->vehicle_id,
                                'duration' => $request->total_duration,
                                'miles' => 1000,
                                'address' => $booking->address,
                                'expiredate' => $booking->expiredate,
                                'dob' => $booking->dob,
                                'dvla' => $booking->dvla,
                                'national_insu_numb' => $booking->national_insu_numb,
                                'pco_licence_no' => $booking->pco_licence_no,
                                'doc_dvla' => $booking->doc_dvla,
                                'doc_cnic' => $booking->doc_cnic,
                                'expiry_date' =>   $expire_date, //Helper::get_expire_date($request->duration),
                                'price'=>  $per_week_rent,
                                'discount'=>  $discount,
                                'booking_status'=>1,
                                
                                'pco_paper_licence'=>$booking->pco_paper_licence,
                                'promo' => $booking->promo,
                                'doc_other' => $booking->doc_other,
                                'payment_status'=>0,   //later we change it 0, because if we now send it 0 user can do multiple time booking with same id. 0 will be changed when we confirm payment.
                                'first_payment'=>  $per_week_rent_included_insuranance_cost,
                                 'booking_start_date' => $booking_start_date,  
                                 'booking_drop_off_date' =>  $expire_date,
                                 'agreement_status'=>1,
                                 'reference_booking'=> $booking->id,

                            ]; 
                                 $data_new_booking = Booking::create($dataBooking);

                                $product_id = env('PRODUCT_ID');// "prod_IO76QwwM1ZkCI0";
                                $currency = 'GBP';  
                                
                                $nickname = $vehicle->licence_plate_number.'-'.$data_new_booking->id;
                                $subscription_amount_charge_every_week =$amount ;
                                $booking_duration =  $request->total_duration;
                                \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                               

                              //Step1: Create Price for specific Product
                                            $price = Stripe\Price::create([
                                            'product' => $product_id,
                                            'unit_amount' => $amount,
                                            'currency' => $currency,
                                            'nickname'=>$nickname,
                                            'recurring' => [
                                              'interval' => 'week',
                                            ],
                                          ]);
                                            $price_id = $price->id;

                              //step2 getting custtom strip id
                               $stripe_customer_id = $booking->user->stripe_customer_id;              
                              


                                    //step3 charge customer first week booking amount
                                      $return = Stripe\Charge::create ([
                                                    "amount" => $amount,
                                                    "currency" =>  $currency, 
                                                    "description" => "PCOCAR Booking Vehicle ".$nickname,
                                                    'customer' =>  $stripe_customer_id,
                                            ]);


                                     
                                     //Step4: Assign Subsribed Plan
                                     if($return->status == 'succeeded'){ 
   
                                                   // $price_id = $price->id; 
                                                    $booking_start_date =$booking_start_date;
                                                   // $todayDate = Carbon::now()->format('Y-m-d');
                                                    $startDate =strtotime($booking_start_date);   
                                                    $timespn =  strtotime('+1 week', $startDate) ;

                                                    $booking_duration = "+".  $booking_duration ." week";
                                                    $timespn_cancel =  strtotime( $booking_duration, $startDate) ;
                                                  
                                                     $subscription = Stripe\SubscriptionSchedule::create([
                                                        'customer' =>    $stripe_customer_id,
                                                        'start_date' =>  $timespn,
                                                        'end_behavior' => 'release',
                                                        'phases' => [
                                                          [
                                                               'end_date'=>$timespn_cancel,
                                                              'items' => [
                                                                         //  ['price' => $price_id ],
                                                                            ['price_data'=>[
                                                                                'currency'=>$currency,
                                                                                'product'=>$product_id,
                                                                               //'unit_amount'=>$subscription_amount_charge_every_week,
                                                                                'unit_amount_decimal' =>$subscription_amount_charge_every_week,
                                                                                 'recurring'=> ['interval'=>'week' , 'interval_count'=>1 ] ] ],  
                                                                          ],   

                                                           // 'iterations' => 12,
                                                          ],
                                                        ],
                                                      ]);
 

                                    //after success payment and generated new booking actions
                                            $transaction_id = $return->id;
                                            $date = $data_new_booking->booking_start_date;//date('Y-m-d');
                                            $package_days =30;// '+ '.$request->package_days. ' days'; 
                                            $expire_date=date('Y-m-d', strtotime($date.  $package_days));


                                    $date_time = strtotime($date);
                                    $added_months = "+".$request->expire_month_after." month";
                                    $final_date = date("Y-m-d", strtotime($added_months, $date_time));

                                    //refernce booking status change in previous booking record
                                      $booking->reference_booking_status =1; //if payment success update payment status
                                      $booking->booking_status = 4;
                                      $booking->save(); 
                                    



                                            
                                       

      $name =  $booking->user->name;
      $title = "Booking Status"; 
      $email_to = 'payments@pcocar.com';// 'payments@pcocar.com';//$user->email;//'waqas.ger@gmail.com';
        $booking = Booking::where('id', $data_new_booking->id)->with('vehicle')->with('user')->first();
        $vehicle = Vehicle::where('id', $data_new_booking->vehicle_id)->with('car_make')->with('car_model')->first();
       // $ses_values = $request->session(); 
         $price_per_week = $data_new_booking->price;
        $price_total = $data_new_booking->price  + env('INSURANCE_CODE');//$amount;//$data_new_booking->price_final_total_book;   
        $booking_start_date =  $data_new_booking->booking_start_date;         
 


    $data_new_booking->payment_status =1; //if payment success update payment status
    $data_new_booking->save(); 


 

     $data = [
            'user_id' => $data_new_booking->user_id,
            'amount' => $data_new_booking->price,
            'transaction_id' =>$return->id,
            'expire_date' => $final_date,
            'month_duration' => 1,//$request->expire_month_after,
            'per_month_amount'=>  $data_new_booking->price,
            'vehicle_id' =>  $data_new_booking->vehicle_id,  
            'booking_id' => $data_new_booking->id,
            'payment_type'=>0,
        ];
     
        $user = PaymentHistory::create($data);



$payment_mode=1;
 
$insurance=0;

 

   Mail::to([$email_to,'bookings@pcocar.com'])->cc($booking->user->email)->send(new \App\Mail\Booking(
    $payment_mode,
    $name, 
    $title,
     $booking , 
     $vehicle , 
     $request->total_duration,  
     $data_new_booking->discount,
     $price_per_week,
     $price_total,
     $booking_start_date,
     $insurance

   )); 

   //$veh = Vehicle::findOrFail($request->id);     
        $vehicle->bookingStatus =1; 
        $vehicle->save();
   
    $this->payment_history_generate($data_new_booking , $data_new_booking->booking_start_date);   

     //$this->booking_payment_relation($booking);   
 

 } 
 else{
     return redirect()->back()->with('error','Strip payment issue');
 }

        //return redirect()->back()->with('message','Renew booking is Done');
        return redirect()->route('vehicle_booking')->with('message','Renew booking is Done');
        }
        


        }
        
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function payment_history_generate($booking, $r){
      for($i=1;$i<= ($booking->duration-1); $i++){ 

        
         
        /* if($i==1){   
          $price = ($booking->price / 100) + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY') ;
          $payment_status =1;  
        }
         else{ 
          $price =($booking->price / 100) + env('INSURANCE_CODE') ;
            $payment_status =0;  
         }
*/
          $price = $booking->price   + env('INSURANCE_CODE') ;
            $payment_status =0;  


           $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 
  $date = strtotime(  $r   ); //starting date
  $date = strtotime($days_add, $date);
  $show_date = date('Y-m-d', $date);


  
          $data = [
        'booking_id' => $booking->id,
        'user_id' => $booking->user_id,
        'amount' => $price,
        'vehicle_id' =>$booking->vehicle_id,
        'payment_status'=> $payment_status ,
        'payment_mode' =>2,
        'expire_date' => $show_date, 
        'payment_type'=>1,
        
    ];
     PaymentHistory::create($data);



//BookingPaymentRelation

      }
}



}
