<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingPermissionLet extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
   public $booking; 
 
    public function __construct($booking )
    {

         
        $this->booking = $booking; 
        
 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail.booking_permission', [
            'booking'=>$this->booking, 
          

          ]);
         //   return view('pages_merchant_admin.customer_list', compact('customer_list','title'));
    }
}
