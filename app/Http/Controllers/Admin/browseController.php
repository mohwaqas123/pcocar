<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\Vehicle;
 use Session;
use Auth;

class browseController extends Controller
{
    public function index()
    {    
        $vehicles = Vehicle::with('car_make')->orderby('id', 'Desc')->get();
        //return view('frontend.browse', ['vehicles' => $vehicles]);
        return view('frontend.browse', ['vehicles' => $vehicles]);
    }

    

     public function car_detail(Request $request)
    { 
       //$vehicles = Vehicle::where('id', $request->id)->with('car_make')->with('car_model')->orderby('id', 'Desc')->get();
        $vehicles = Vehicle::where('id', $request->id)->with('car_make')->with('car_model')->with('vehicle_images')->first(); 
         //dd($vehicles);
        // $vehicles = Vehicle::findOrFail($request->id);  
        //return view('frontend.browse', ['vehicles' => $vehicles]);
        $discount=0;
        if(session()->get('duration') > 1 ){ 
 
        $week = session()->get('duration') ;
        $price = $vehicles->price;
       

      if($week >= 11 && $week < 24 ){
            $discount = ($price * 5 ) /100;
            $disc = 5;
       }

       if($week >= 24 && $week < 48 ){
            $discount = ($price * 10 ) /100;
            $disc = 10;
       }

       if($week >= 48 ){
            $discount = ($price * 10 ) /100;
            $disc = 10;
       }
        if($week < 12 ){
           $discounted_price = $price  * session()->get('duration') ;
           $discount_text = $discounted_price ;
      
        }
        else
        {
           $discounted_price = $price  * session()->get('duration') ;
           $discount_text =  $discounted_price; 
        } 
}
else
{
      $discounted_price =  $vehicles->price;//* $request->duration;
      $discount_text =$discounted_price;
      $disc = 0;
}
//=========================================
      return view('frontend.car_detail', ['vehicles' => $vehicles,  'duration'=>session()->get('duration'), 'discount_text_price'=> $discount_text, 'discount'=>$discount,'disc_percent'=> $disc ]);
    }

}

