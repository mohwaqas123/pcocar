<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>{{ $title }}</title>
		<style type="text/css">
			@import url('https://fonts.googleapis.com/css?family=Tangerine');
			/* 	@font-face {
		font-family: 'dancingscript';
		font-style: normal;
		font-weight: bold;
		src: url(https://fonts.googleapis.com/css?family=Rancho&effect=shadow-multiple) format('truetype');
		}
		*/
		</style>
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
	</head>
	<body>
		
		
		 
		<div class="pos" id="_0:0" style="top:0"></div>
		<div align="center">
			<div class="pos" id="_311:54" style="top:54;left:311">
				<span id="_29.0" style="font-weight:bold; font-family:Verdana; font-size:29.0px; color:#006dc0">
				Intercity Hire</span>
			</div>
			<div class="pos" id="_328:89" style="top:89;left:328">
				<span id="_13.5" style=" font-family:Arial; font-size:13.5px; color:#001f5f">
				23 Stanley Road, London E47DB</span>
			</div>
			<div class="pos" id="_371:112" style="top:112;left:371">
				<span id="_10.7" style="font-weight:bold; font-family:Verdana; font-size:10.7px; color:#001f5f">
				<U>T</U><U>e</U><U>l</U><U>:</U><U>0</U><U>2</U><U>0</U><U>8</U><U>1</U><U>3</U><U>3</U><U>3</U><U>1</U><U>3</U><U>8</U></span>
			</div>
			<div class="pos" id="_378:127" style="top:127;left:378">
				<span id="_10.7" style=" font-family:Verdana; font-size:10.7px; color:#006dc0">
				<a href="http://www.pcocar.com/">www.pcocar.com</span>
			</div>
		</div><br>
		<table >
			<tr>
				<td style="width:200px;" valign="top"  >
					<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
					</span>
					<p>
					PCN Reference: {{$pcn->ref_number}}<br></p>
					<p>VRM: {{$pcn->vehicle->licence_plate_number}}<br></p>
					<p>Date: {{$date}}<br></p>
					<p>To Whom It May Concern<br></p>
					<p>Dear Sir/Madam,<br></p>
					<p>{{$pcn->letter_txt}}<br></p>
				</td>
				
				
				<div class="pos" id="_100:459" style="top:459;left:100">
					<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
						<img src="https://pcocar.com/public/theme3/assets/img/pcn2.png" style="width: 70px; height: 70px;"/>
						<br>
					Muhammad Zuhair 
					<br>
					(PCN Manager)
				</span>
				</div>
				<div class="pos" id="_100:459" style="top:459;left:100">
					<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
					A:23 Stanley Road, London. E47DB  <br><br></span>
				</div>
				<div class="pos" id="_100:459" style="top:459;left:100">
					<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
					T: +44 (0) 7812214589</span>
				</div>
				<div class="pos" id="_100:459" style="top:459;left:100">
					<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
					E:info@toprangemotors.com </span>
				</div>
				<div class="pos" id="_100:459" style="top:459;left:100">
					<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
					E:pcn@pcocar.com</span>
				</div>
				<div class="pos" id="_99:478" style="top:478;left:99">
					<span id="_15.7" style=" font-family:Arial; font-size:15.7px; color:#000000">
					Should there be any question Please ask us at below Thanks & Regards, </span>
				</div>
				<div class="pos" id="_100:459" style="top:459;left:100">
					<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
					<b> Address: {{$records->address}} </b></span>
				</div>
				<div>
					<span id="_15.8" style=" font-family:Arial; font-size:15.8px; color:#000000">
						<b>Phone number:{{$pcn->user->phone}}</b>
					</span>
				</div>
				<div class="pos" id="_99:697" style="top:697;left:99">
					<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
					<b>DOB:{{$records->dob}}</b></span>
				</div>
				<div class="pos" id="_453:697" style="top:697;left:453">
					<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
					<b>Name:{{$pcn->user->name}}</b> </span>
				</div>
				
				
				
	 
				
				
			</body>
			
		</html>