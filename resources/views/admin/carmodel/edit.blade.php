@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                       <h4 class="breadcrumb-item active"   > 
                        
                    </h4>
                        {{--  <p class="text-muted font-14 m-b-30">
                            DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>.
                        </p>  --}}
                        <div class="card-box">
                          <h4> 
                        
                    </h4>
                            @if($errors->any())
                                {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif

                            <form role="form" action="{{ route('updatecarmodel') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Vehicle Name <span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" placeholder="Enter vehicle Name" value="{{ $carmodel->name }}" required>
                                            <input type="hidden" name="id" value="{{ $carmodel->id }}">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                           <div class="form-group col-md-6">
                                            <label for="make_id">Company Name <span class="text-danger required">*</span></label>
                                            <select name="make_id" id="make_id" class="form-control select2 @error('make_id') is-invalid @enderror" required="">
                                               @foreach ($makes as $make)
                                                    <option value="{{$make->id}}">{{$make->name}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('make_id'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('make_id') }}</strong>
                                                </span>
                                           @endif
                                          </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="status">Status <span class="text-danger required">*</span></label>
                                            <select name="status" id="status" class="form-control @error('status') is-invalid @enderror" required="">
                                                <option value="1" {{ $carmodel->status == 1 ? 'selected' : '' }}>Active</option>
                                                <option value="0" {{ $carmodel->status == 0 ? 'selected' : '' }}>InActive</option>
                                            </select>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                           @endif
                                          </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Update</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                          </div>
                                    </div>
                                </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
