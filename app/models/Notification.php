<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    protected $fillable = [
        'user_id','vehicle_id','booking_id','date','details','int_status',
      ];

public function vehicle()
    {
        return $this->belongsTo('App\models\Vehicle','vehicle_id','id');
    }

public function user()
    {
        return $this->belongsTo('App\models\User','user_id','id');
    }
    public function booking()
    {
        return $this->belongsTo('App\models\Booking','booking_id','id');
    }
}
