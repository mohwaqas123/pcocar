<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\Vehicle;
use App\models\Booking;
use Session;
//use Auth;

class bookingController extends Controller
{ 
    public function index(Request $request)
    {  
      
         if(request()->after_month) //if first time booking selected gnerate its session to use in further
          session([
                'months' => request()->after_month, 
                'miles'=> request()->mile, 
                'price'=> $request()->price
            ]);
        //$value = $request->session()->get('key');
        //dd($value);
        $vehicles = Vehicle::where('id', $request->id)->with('car_make')->with('car_model')->first(); 
        //return view('frontend.browse', ['vehicles' => $vehicles]);
        return view('frontend.booking', [
  
            'vehicles' => $vehicles, 
            'months'=> $request->session()->get('months'), 
            'miles'=> $request->session()->get('miles'), 
            'price'=> $request->session()->get('price'),

        ]);
    }
 

    public function upload_information(Request $request){   
       /* $this->validate($request, [
                'name' => 'required',
                'email'=>'required|email',
           ]);
*/

        if($request->hasFile('file_dvla'))
          { 
                $allowedfileExtension=['pdf','jpg','png','docx'];
               // $files = $request->file('file');

 

            $vehicleImagesFolder = 'documents_uploads'; 
            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }


            $file1 =Storage::putFile($vehicleImagesFolder, new File($request->file_dvla));
            $file2 =Storage::putFile($vehicleImagesFolder, new File($request->file_cnic)); 
            $file3 =Storage::putFile($vehicleImagesFolder, new File($request->file_utility));
            $file4 =Storage::putFile($vehicleImagesFolder, new File($request->file_other));

$dataBooking = [
    'name' => $request->name,
    'user_id' => 1,//$request->name,
    'vehicle_id' => $request->id,
    'duration' => $request->duration,
    'miles' => $request->mile,
    'address' => $request->address,
    'dob' => $request->dob,
    'dvla' => $request->dvla,
    'national_insu_numb' => $request->national_insu_numb,
    'pco_licence_no' => $request->pco_licence_no,
    'doc_dvla' => $file1,
    'doc_cnic' => $file2,
    'doc_utility' => $file3,
    'doc_other' => $file4,
    'payment_status'=>0,
];

        $data = Booking::create($dataBooking);
        $booking_id = $data->id;
        return redirect()->route('booking',
            [
                'id'=>$request->id, 
                'step'=>3,
                'mile'=>$request->mile,
                'after_month'=>$request->duration, 
                'booking_id'=>$data->id,
                'price' => $request->price, 
                'booking_id' => $booking_id,


            ]
        )->with('success','Documents Added Successfully');


/*
        foreach($request->file as $documents)
            {
                $imageUrl = Storage::putFile($vehicleImagesFolder, new File($documents));
                 VehicleImage::create(
                    [
                        'vehicle_id'=> $vehicle->id,
                        'images' => $imageUrl
                    ]
                ); 
            }//foreach */




          } //end if





    }//end function
 
public function done(Request $request){
       $booking = Booking::where('id', $request->booking_id)->with('vehicle')->first(); 

       return view('frontend.booking_done',['booking'=>$booking]);
} 



public function booking_list_admin(){
    $bookings = Booking::all();
    return view('admin.booking.index',['bookings'=>$bookings]);

}


}

