@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<section class="slice slice-lg pt-lg-12 pb-0 pb-lg-12 bg-section-secondary">
  <div class="container">
    <div class="row">
      <div class="col-md-2"> 
        <form action="{{ route('browse') }}" method="get" ifor 1 Monthd="frm_duration">
          {{csrf_field()}}

     <div class="search_box"> 
     <h3>Search</h3>    
          <div class="form-group">
            <label for="inputEmail">Booking Duration</label>
            
            <select name="duration" id="duration" class="form-group select_custom form-control">
              <option @if($duration && $duration==1) selected="selected" @endif value="">Any</option>
              <option @if($duration && $duration==4) selected="selected" @endif value="4">4 Weeks</option>
              <option @if($duration && $duration==8) selected="selected" @endif value="8">8 Weeks</option>
              <option @if($duration && $duration==12) selected="selected" @endif value="12">12 Weeks</option>
              <option @if($duration && $duration==16) selected="selected" @endif value="16">16 Weeks</option>
              <option @if($duration && $duration==20) selected="selected" @endif value="20">20 Weeks</option>
              <option @if($duration && $duration==24) selected="selected" @endif value="24">24 Weeks</option>
              <option @if($duration && $duration==26) selected="selected" @endif value="28">28 Weeks</option>
              <option @if($duration && $duration==28) selected="selected" @endif value="32">32 Weeks</option>
              <option @if($duration && $duration==32) selected="selected" @endif value="36">36 Weeks</option>
              <option @if($duration && $duration==40) selected="selected" @endif value="40">40 Weeks</option>
              <option @if($duration && $duration==44) selected="selected" @endif value="44">44 Weeks</option>
              <option @if($duration && $duration==48) selected="selected" @endif value="48">48 Weeks</option>
              <option @if($duration && $duration==52) selected="selected" @endif value="52">52 Weeks</option>
            </select>
          </div>
          <div class="form-group">
            <label for="inputEmail">Vehicle Make</label>
             <select name="car_make" id="car_make" onchange="get_model(this.value)" class="form-group form-control select_custom">
                <option value="">Any</option>
                @foreach($car_make as $make)
                <option value="{{$make->id}}">{{$make->name}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="inputEmail">Vehicle Model</label>
             <select name="car_model" id="car_model" class="form-group select_custom form-control">
                <option value="">Any</option>
                @foreach($car_model as $model)
                <option value="{{$model->id}}">{{$model->name}}</option>
                @endforeach
              </select>
          </div>
          <div class="form-group">
            <label for="inputEmail">Vehicle Year</label>
            <select name="year" id="year" class="form-group select_custom form-control">
                <option value="">Any</option>
                <option value="2020">2020</option>
                <option value="2019">2019</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
                <option value="2013">2013</option>
                <option value="2012">2012</option>
                <option value="2011">2011</option>
              </select>
          </div>
          <!--   <select name="duration" id="duration" onchange="this.form.submit()" class="form-group select_custom"> -->
          
          <!--  <form action="{{ route('browse') }}" method="get" id="frm_duration">
            {{csrf_field()}}
            -->
            <!--     Weekly Budget
            <input type="range" step="50" class="border-0 range" min="150" max="1500">
            <div class="code">
              £<code></code>
            </div> -->
             
         <!--    <input name="btn_submit" type="submit" class="btn-primary btn-sm" value="Find" /> -->
            <input  name="btn_submit" type="button" onclick="window.location='{{ url('/')}}/browse'" class="btn-primary btn-sm" value="Reset" />
          </form></div>
        </div>
        <div class="col-md-10">
          <div class="row">
            <div class="col-md-6"> <h2>{{count($Vehicle)}} Vehicles  </h2></div>
            <div class="col-md-6" align="right">
              <!--  <select name="" id="" class="form-group">
                <option value="">Filter</option>
                <option value="1">Price High to Low</option>
                <option value="2">Price Low to High</option>
                <option value="3">Best Value</option>
              </select> -->
            </div>
          </div>
       
        
        
        
        <div class="" id="vehicle_list">
          
        </div>
        
        
      </div>
    </div>
    
  </div>
</section>
@include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
<style>
.img-thumbnail{height: 230px!important;}
.inner{font-size: 10px;}
.custom_font{    font-family: inherit;}
.car-make{font-size: 16px; font-weight: bold;}
.small{font-size:8px;}
label{margin-bottom:0px!important;}
.search_box{  /*  border: 1px solid #eee;*/
    padding: 6px;
    box-shadow: brown;
    font-size: 12px;
     }
</style>
<script>


function get_model(id){
// e.preventDefault();
var url_call = "{!! url('/') !!}/get_models";
$.ajax({
type : 'get',
url  : url_call,
data: { id:id  },
//    data: { ticket_id: ticket_id, status_value: status_value },
contentType: 'application/json; charset=utf-8',
success :  function(data){
$("#car_model").empty();
$("#car_model").append($("<option value=''>Any</option>"));
$.each(data, function () {
$("#car_model").append($("<option></option>").val(this['id']).html(this['name']));
});
}
});//ajax
}
</script>