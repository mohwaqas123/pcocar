@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


    
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                  


                    <div class="card-box">
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            
                            <!-- <table id="songsListTable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Vehincle Detail</th>
                                        <th>description</th>
                                        <th>Thumbnail</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>  -->
                                
                                <!-- Datatable -->
                                
                                <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
                                <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
                                <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
                                <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
   @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif
                                <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Customer Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Booking</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($booking as $customer_history)
                                        <tr>
                                            <td>{{ $customer_history->id }}</td>
                                            <td>{{ $customer_history->name }}</td>
                                            <td> {{ @$customer_history->email}}</td>
                                            <td>{{ $customer_history->phone }}</td>
                                             
                                            <input type="hidden" name="id" value="{{ $customer_history->id }}">

                                            <td style="width: 20%;">
                                             
                                                
                                                
                                                
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')

    <script>
    $(document).ready(function() {
    $('#datatable_tbl').DataTable();
    } );
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
    // Default Datatable
    $('#songsListTable').DataTable({
    "columnDefs": [
    { "orderable": false, "targets": [4,5,6] },
    ],
    "bPaginate": false,
    });
    } );
    </script>
    @endsection