@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<div class="container">
  
  <div class="row">
    <div class="col-md-3 text-center">
      
      
   
    </div>
    <div class="col-md-9">
      
      
      <div class="container py-5">
        <!-- For demo purpose -->
        <div class="row mb-4">
          <div class="col-lg-8 mx-auto text-center">
            
          </div>
          </div> <!-- End -->
          <div class="row">
            <div class="col-lg-12 mx-auto">
              <div class="card ">
                <div class="card-header">
                 
                  <div class="tab-content">
                   <h3>Booking Submitted Successfully!!!</h3>
       <p>Thank you for select PCOCAR service, your booking is in under review and we will reply back to you soon.</p>
       <p>if you have any inquiry please <a href="{{ url('/')}}/contactus">Contact us</a> or login customer section <a href="{{url('login')}}"> Click Here</a></p>
                       
                          <!-- End -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                
                
                
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    @include('layouts.footer')
  </body>
  @endsection
  @section('scripts')
  @endsection