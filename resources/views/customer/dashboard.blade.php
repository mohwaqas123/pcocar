
@extends('layouts.customer')
@section('styles')
@endsection
@section('content')
<div class="content-page">
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
            <!-- <ul class="list-unstyled topbar-right-menu float-right mb-0">
                <li class="hide-phone app-search">
                    
                    <form>
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
                
            </ul> -->
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    <!-- <div class="page-title-box">
                        <h4 class="page-title">Dashboard </h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Welcome to PCOCAR Admin!</li>
                        </ol>
                    </div> -->
                </li>
            </ul>
        </nav>
    </div>
    <!-- Top Bar End -->
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
            
            <div class="row">
                <div class="col-md-12">
                    <div class="card-box">
                    <!-- <input type="text" value="{{Carbon\Carbon::now()->format('Y-m-d')."T".Carbon\Carbon::now()->format('H:i')}}" /> -->
                        <h5>Pending Booking </h5>

                         @foreach($booking_pending as $p_booking)
                         <?php
                $date = $p_booking->booking_start_date;//date('Y-m-d'); 
                $ldate = date('Y-m-d H:i:s');
                $ldate2 = date('Y-m-d H:i:s', strtotime("$date". " +1 hours"))
                ?>
                         
                            @if($p_booking->agreement_status == 1  && $p_booking->payment_status == 0)
                            @if($ldate <  $ldate2 )
                           <table class="table table-hover table-centered m-0">
                            <tr>
                                <th>
                                   Booking Id
                                </th>
                              
                                <th>
                                  Created Date
                                </th>
                                <th>
                                  Action
                                </th></tr>
                              <tr>
                                <td>{{$p_booking->id}}</td>
                                   
                  <td>{{$p_booking->booking_start_date}}</td>
                
                        <td><a target="_blank" href="{{ route('upload_information_second', $p_booking->id) }}">Process</a></td>
                
                @else
                @endif

                            </tr>
                        
                               @else 
                        <table class="table table-hover table-centered m-0">
                            <tr>
                                <th>
                                   Booking Id
                                </th>
                              
                                <th>
                                  Created Date
                                </th>
                                <th>
                                  Action
                                </th></tr>
                                 
                         
                       
                           
                            <tr>
                                <td>{{$p_booking->id}}</td>
                                 
                                  <td>{{$p_booking->created_at}}</td>

 <td><a target="_blank" href="{{url('/')}}/booking4">Process</a></td>
                            </tr>
                         
                            @endif
                            
                        @endforeach
                           
                        </table>
                    </div></div>
                </div>


                <div class="row">
                    <div class="col-4">
                        <div class="card-box">
                            @php
$booking=\App\models\Booking::where('user_id', session()->get('user_id') )->where('payment_status',1)->orderby('id', 'Desc')->count();
                            @endphp
                            <h4>Total Booking</h4>
                            <p>Booking:{{$booking}}</p>
                            <input onclick="window.location='{{ url("customer_booking_list") }}'"  type="button" class="btn btn-primary" value="View List">&nbsp;
                            
                            <!-- end row -->
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card-box">
                            <h4>Current  Booking</h4>
                            @foreach ($duration as $cu)
                            @if($cu->payment_status == 1 )
                            Weeks:{{$cu->duration}} <br>
                            started :{{$cu->created_at}} <br>
                            Expire :{{$cu->booking_drop_off_date}} <br>
                            @else
                             @endif 
                            @endforeach
                            <input onclick="window.location='{{ url("customer_booking_list") }}'"  type="button" class="btn btn-primary" value="View Detail">&nbsp;
                            
                            
                            <!-- end row -->
                        </div>
                    </div>
                </div>
                <!-- end row -->
              <!--    <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <h4 class="header-title mb-3">Payment History</h4>
                            <div class="table-responsive">
                                <table class="table table-hover table-centered m-0">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Detail</th>
                                            <th>Price</th>
                                            <th>Payment Reference</th>
                                            <th>Status</th>
                                              <th>Reserved in orders</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($booking_history as $p_history)
                                        <tr>
                                            <td>
                                                <h5 class="m-0 font-weight-normal">{{$p_history->created_at}}</h5>
                                                <p class="mb-0 text-muted"><small>{{$p_history->description}}</small></p>
                                            </td>
                                            <td>
                                                Payment receive against booking# {{$p_history->booking_id}}
                                            </td>
                                            <td>
                                                {{$p_history->amount}}GBP
                                            </td>
                                            <td>
                                                @if($p_history->payment_type==1)
                                                <span class="badge badge-info">Weekly booking payment</span>
                                                @elseif($p_history->payment_type==2)
                                                <span class="badge badge-primary">Other payment</span>
                                                @elseif($p_history->payment_type==4)
                                                <span class="badge badge-primary">Expenses Charges</span>
                                                @elseif($p_history->payment_type==3)
                                                <span class="badge badge-primary">PCN payment</span>
                                                @elseif($p_history->payment_type==5)
                                                <span class="badge badge-primary">Toll Charges</span>
                                                @endif
                                                
                                            </td>
                                            <td>
                                            <span class="badge badge-success">Success</span></td>
                                            
                                        </tr>
                                        @endforeach
                                        
                                        
                                        
                                        
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div> -->
                
                <!-- end row -->
                </div> <!-- container -->
                </div> <!-- content -->
                
            </div>
            @endsection
            @section('scripts')
            @endsection