@extends('layouts.customer')



@section('styles')



@endsection



@section('content')

<div class="content-page">

    <div class="content">

        <div class="container-fluid">



            <div class="row">

                <div class="col-12">

                    <div class="card-box">
 
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

                            



 

                                

<!-- Datatable -->

 

 <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

 

 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />

 <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />





@if(session()->has('message'))

    <div class="alert alert-success">

        {{ session()->get('message') }}

    </div>

@endif









<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

        <thead>

            <tr>

                                    <th>Booking No</th>

                                    <th>Vehicle Detail</th>

                                    <th>Date</th>

                                    <th>Expire</th>

                          

                                    <th>Contact Detail</th>

                                    <th>Action</th>

            </tr>

        </thead> 





     

                                <tbody>

                                     @foreach ($booking as $bookings)

                                    <tr>

                                        <td>{{$bookings->id}}</td>

                                        <td>{{@$bookings->vehicle->name}},

                                            {{@$bookings->vehicle->price}},

                                            {{@$bookings->vehicle->year}},

                                        </td>



                                        <td>{{$bookings->created_at}}</td>

                                        <td>{{$bookings->created_at}}</td>

                                     <!--    <td>{{$bookings->doc_dvla}},

                                            {{$bookings->doc_cnic}},

                                            {{$bookings->doc_utility}}

                                        </td> -->



                                        <td>{{@$bookings->user->name}},

                                            {{@$bookings->user->email}},

                                            {{@$bookings->user->phone}}</td>

                                              <td style="width: 15%;">

                                               <a href="{{ route('bookingView', $bookings->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>  

                                                

                                            </td>

                                    </tr>   

                                    @endforeach

                                </tbody>

                            </table>

                       

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection



@section('scripts')

<script>

     $(document).ready(function() {

    $('#datatable_tbl').DataTable();

} );

 </script>

<script type="text/javascript">

    $(document).ready(function() {

        // Default Datatable

        $('#songsListTable').DataTable({

            "columnDefs": [

            { "orderable": false, "targets": [4,5,6] },

            ],

            "bPaginate": false,

        });

    } );

</script>

@endsection

