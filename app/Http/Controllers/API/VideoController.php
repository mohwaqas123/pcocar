<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Video;
use App\models\Device;
use App\models\LikeVideo;
use App\models\CommentsVideo;
use App\models\ShareVideo;
use App\models\Tag;
use App\models\VideoTag;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;
use Storage;
use Illuminate\Http\File;

class VideoController extends Controller
{
    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
    }

    public function uploadVideo(Request $request)
    {
        // $data = json_decode($request->tags);
        // dd($data);
        $validator = Validator::make($request->all(), [
            'description' => 'required|string|max:255|unique:users',
            'privacy' => 'required',
            // 'song_id'=> 'required',
            'allow_comments' => 'required',
            'allow_duet_and_read' => 'required',
            'video' => 'required'
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $userID = JWTAuth::toUser($request->token)->id;
        
        $videoData = [
            'user_id' => $userID,
            'song_id' => $request->song_id,
            'description' => $request->description,
            'privacy' => $request->privacy,
            'allow_comments' => $request->allow_comments,
            'allow_duet_and_read' => $request->allow_duet_and_read,
        ];

        $video = Video::create($videoData);

        if ($request->hasFile('video')) {
            $videosFolder = 'videos';

            if (!Storage::exists($videosFolder)) {
                Storage::makeDirectory($videosFolder);
            }

            $videoUrl = Storage::putFile($videosFolder, new File($request->video));
            $video->update(['video_url'=> $videoUrl]);
        } 
        
        if ($request->hasFile('video_thumbnail')) {
            $videosThumbnailFolder = 'videosThumbnail';

            if (!Storage::exists($videosThumbnailFolder)) {
                Storage::makeDirectory($videosThumbnailFolder);
            }

            $videoThumbnailUrl = Storage::putFile($videosThumbnailFolder, new File($request->video_thumbnail));
            $video->update(['video_thumbnail'=> $videoThumbnailUrl]);
        } 

        
        if(isset($request->tags))
        {
            $tags = $request->tags;

            foreach($tags as $tag)
            {
                $checkTag = Tag::where('name', $tag)->first();
                if($checkTag == null)
                {
                    $tagData = [
                        'name' => $tag,
                    ];

                    $newTag = Tag::create($tagData);

                    VideoTag::create([
                        'tag_id' => $newTag->id,
                        'video_id' => $video->id 
                    ]);
                }
                else
                {
                    VideoTag::create([
                        'tag_id' => $checkTag->id,
                        'video_id' => $video->id 
                    ]);
                }

            }
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'Video uploaded successfully.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
        ]); 
    }

    public function likeUnlikeVideo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required',
            'type' => 'required'
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $userID = JWTAuth::toUser($request->token)->id;

        $old_like = LikeVideo::where('liked_by', $userID)->where('video_id', $request->video_id)->first();
      
        if($old_like != null)
        {
            $old_like->update([
                'type' => $request->type,
            ]);
        }
        else
        {
            $likeData = [
                'video_id' => $request->video_id,
                'liked_by' => $userID,
                'type' => $request->type
            ];
    
            $like = LikeVideo::create($likeData);
        }
        
        if($request->type)
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Video liked successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]); 
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Video unliked successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]); 
        }
    }

    public function shareVideo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_id' => 'required|numeric',
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $video = Video::where('id', $request->video_id)->first();

        if($video == null)
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Video does not exist against this reference video.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]); 
        }

        $userID = JWTAuth::toUser($request->token)->id;

        $oldShareVideo = ShareVideo::where('user_id', $userID)->where('video_id', $request->video_id)->first();
        
        if($oldShareVideo == null)
        {
            $sharevideo = ShareVideo::create([
                'video_id' => $request->video_id,
                'user_id' => $userID,
            ]);
    
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Video shared successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]); 
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Video shared already exists.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]); 
        }

        
    }
}
