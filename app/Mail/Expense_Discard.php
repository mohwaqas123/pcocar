<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Expense_Discard extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($vehicle_name,$vehicle_year,$user_name,$user_Email,$user_Phone,$Expense_price,$detail,$shop_name,$reasons)
    {
        $this->vehicle_name = $vehicle_name; 
        $this->vehicle_year = $vehicle_year; 
        $this->user_name = $user_name; 
        $this->user_Email = $user_Email; 
        $this->user_Phone = $user_Phone; 
        $this->Expense_price = $Expense_price; 
        $this->detail = $detail; 
        $this->shop_name = $shop_name; 
        $this->reasons = $reasons; 

     }


    /**
     * Build the message.
     *
     * @return $this
     */
   public function build()
    {
        return $this->view('mail.expence_discard', [ 'vehicle_name' => $this->vehicle_name , 'vehicle_year' => $this->vehicle_year, 'user_name' => $this->user_name , 'user_Email' => $this->user_Email,'user_Phone' => $this->user_Phone,
            'Expense_price' => $this->Expense_price , 'detail' => $this->detail,'shop_name' => $this->shop_name,'reasons' => $this->reasons]);
    }
}
