@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">	
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif

						<table id="datatable_tbl" class="table table-striped table-bordered"  style="width:100%;  " >
							 	<thead>
                                <tr>
                                <th>PCN Ref</th>
                                 <th>VRM</th>
                                 <th>Status</th>
                                  <th>PCN Con Date</th>
                                  <th>Date Of Notice</th>
                                  <th>Date Of Received</th>
                                  <th>Type of Action</th>
                                  <th>Action Date</th>
                                  <th>Driver Name</th>
                                  <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@foreach($pcn as $pcn_var)
                                <tr>
                                  <!--   @if($pcn_var->ref_number == $pcn_var->ref_number)
                                      <td style="color: ">{{ $pcn_var->ref_number }}</td>
                                    @else
                                    <td>{{ $pcn_var->ref_number }}</td>
                                    @endif -->

                                     <td>{{ $pcn_var->ref_number }}</td>
                                    <td>{{ $pcn_var->vehicle->licence_plate_number }}</td>
                                    <td>{{ $pcn_var->status }}</td>


 <td>{{date('d-M-Y', strtotime($pcn_var->pcn_contravention))}}</td>
                                    <td>{{date('d-M-Y', strtotime($pcn_var->notice_date))}}</td>
                                    <td>{{date('d-M-Y', strtotime($pcn_var->received_date))}}</td>
                                    <td>{{ $pcn_var->type_of_action }}</td>
                                    <td>{{date('d-M-Y', strtotime($pcn_var->action_date))}}</td>
                                    
                                     <td>{{@$pcn_var->user->name }}<br>
                                        {{@$pcn_var->user->email}},<br>
                                         {{@$pcn_var->user->phone }}</td>
                                      <td>
                                        <div class="dropdown show">
                                          <a class="btn-ligh btn btn-s dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                          </a>

                                          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a href="{{ route('adminpcnedit', $pcn_var->id) }}" class="dropdown-item"><i class="fa fa-pencil"></i> Edit</a> 
                                        <a href="{{ route('adminpcnview', $pcn_var->id) }}" class="dropdown-item"><i class="fa fa-eye"></i> View</a> 
                                         <a data-toggle="modal" data-toggle="tooltip" data-placement="Top" data-original-title="Booking Cancel" data-target="#new"  onclick="commentedit({{$pcn_var->id}})"
                                                data-target="#new"  class="dropdown-item"><i class="far fa-edit"></i> Edit Letter</a> 
                                                <a href="{{url('/')}}/generate_pdf_pcn_letter/{{$pcn_var->id}}" class="dropdown-item"><i class="fa fa-download"></i> Download</a>

 

                                                
                                        </div>
                                      </div>
                                       </td>                                          
                                     </tr>

                                @endforeach
                                </tbody>
                               
                               
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="modal fade" id="new" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">State a reason/s for transfer of Liability <span id='placer'></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        
        <form  action="{{ route('admineditpcnletter') }}"       method="post" enctype="multipart/form-data">
              {{csrf_field()}}
              <input type="hidden" name="id" id="id"  >
                
                  <label for="make" class="col-form-label">Please Enter Reasons<span class="text-danger">*</span></label>
                <div class="col-md-12">
                    <textarea  type="text" class="form-control" rows="10" cols="70" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                </div>
                <center> <button type="submit" class="btn btn-primary">Submit</button></center>
          </form>
         
        </div>

      </div>
    </div>
  </div>




@endsection
@section('scripts')
<script>

  function commentedit(id){  
    
 $('#id').val(id);
  }

     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection