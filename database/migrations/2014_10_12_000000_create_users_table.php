<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('username')->unique();
            $table->string('user_firebase_id')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('profile_image')->nullable();
            $table->string('cover_video')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('OTP', 4)->nullable();
            $table->tinyInteger('is_verified')->default(0);
            $table->tinyInteger('is_registered')->default(0);
            $table->string('device_id');
            $table->tinyInteger('device_type')->nullable()->comment('1=Android,2=IOS,3=Admin');
            $table->longText('description')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->tinyInteger('user_type')->comment('1=Admin,2=enduser');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
