<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;

class RegisterController extends Controller
{
    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'is_email' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->errors()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        if($request->get('is_email') == 1)
        {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                // 'name' => 'required',
                // 'phone' => 'required|string|max:255|unique:users',
                'date_of_birth' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
                'password'=> 'required',
            ]);
    
            $message = $validator->errors()->first();
    
            if ($validator->fails()) {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => $message,
                    'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
                ]);
            }
            
    
            $user = User::find($request->get('user_id'));
            
            if(empty($user))
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user does not exists',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }

            if($user->phone != null) 
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user already registered with phone.',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }
            
            $updatedUser = User::find($request->get('user_id'));
               
            $userData1 = [
                // 'name' => $request->get('name'),
                // 'phone' => $request->get('phone'),
                'date_of_birth' => $request->get('date_of_birth'),
                'device_type' => $request->get('device_type'),
                'OTP' => null,
                'device_id' => $request->get('device_id'),
                'user_type' => 2,
                'password' => bcrypt($request->get('password')),
                'is_verified' => 1,
                'is_registered' => 1,
            ];
    
            $updatedUser->update($userData1);
            $finalUser = User::find($request->get('user_id'));
            $token = JWTAuth::fromUser($finalUser);
            
            $finalUser->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at', 'profile_image', 'cover_video', 'OTP'
                , 'device_id', 'device_type', 'description', 'website', 'facebook', 'youtube', 'twitter', 'instagram', 'is_verified', 'is_registered'
            ]);
    
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => $this->responseConstants['SIGN_UP_SUCCESS_MESSAGE'],
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'token' => $token,
                'data' => $finalUser,
                
            ]);
        }

        else
        {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'user_firebase_id' => 'required|string|max:255|unique:users',
                'phone' => 'required|string|max:255|unique:users',
                'date_of_birth' => 'required',
                'device_id' => 'required',
                'device_type' => 'required',
                'password'=> 'required',
            ]);
    
            $message = $validator->errors()->first();
    
            if ($validator->fails()) {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => $message,
                    'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
                ]);
            }
            
    
            $user = User::find($request->get('user_id'));
            
            if(empty($user))
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user does not exists',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }

            if($user->email != null) 
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user already registered with email.',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }
            
            $updatedUser = User::find($request->get('user_id'));
               
            $userData1 = [
                'user_firebase_id' => $request->get('user_firebase_id'),
                'phone' => $request->get('phone'),
                'date_of_birth' => $request->get('date_of_birth'),
                'device_type' => $request->get('device_type'),
                'OTP' => null,
                'device_id' => $request->get('device_id'),
                'user_type' => 2,
                'password' => bcrypt($request->get('password')),
                'is_verified' => 1,
                'is_registered' => 1,
            ];
    
            $updatedUser->update($userData1);
            $finalUser = User::find($request->get('user_id'));
            $token = JWTAuth::fromUser($finalUser);
            
            $finalUser->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at', 'profile_image', 'cover_video', 'OTP'
                , 'device_id', 'device_type', 'description', 'website', 'facebook', 'youtube', 'twitter', 'instagram', 'is_verified', 'is_registered'
            ]);
    
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => $this->responseConstants['SIGN_UP_SUCCESS_MESSAGE'],
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'token' => $token,
                'data' => $finalUser,
                
            ]);
        }

    }
}
