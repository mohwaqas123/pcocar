<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use JWTFactory;
use JWTAuth;
use App\models\User;
use App\models\ResetPassword;
use Illuminate\Support\Facades\Auth;
use Config;
use Str;
use App\Mail\ForgetPassword;

class LoginController extends Controller
{
    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
    }

    function checkEmail($email) {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false && $find2 > $find1);
     }

    //user login
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|max:255',
            'password'=> 'required'
        ]);

        $message = $validator->errors()->first();
        
        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }
        
        $email = $request->get('email');
        if ( LoginController::checkEmail($email)) 
        {
            $credentials = $request->only('email', 'password');
        }
        else
        {
            $credentials['phone'] = $request->get('email');
            $credentials['password'] = $request->get('password');
            // dd($credentials);
            // $credentials = $request->only('email', 'password');
            $user = User::where('phone', $request->get('email'))->first();
            if(empty($user))
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This phone number does not exist.',
                    'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
                ]);
            }
            // $credentials['email'] = $user->email;
            // dd($credentials);
        }

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                // return response()->json(['error' => 'invalid_credentials'], 401);
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => $this->responseConstants['ERROR_INVALID_CREDENTIALS'],
                    'response_code' => 401,
                ]);
            }
        } catch (JWTException $e) {
            // return response()->json(['error' => 'could_not_create_token'], 500);
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'could_not_create_token',
                'response_code' => 500,
            ]);
        }

        $user = User::find(JWTAuth::user()->id);
        $token = JWTAuth::fromUser($user);

        if($user != null)
        {
            $user->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at'
            ]);
        }

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => $this->responseConstants['MSG_LOGGED_IN'],
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'token' => $token,
            'data' => $user,
        ]);
    }

    public function forgetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
        ]);

        $message = $validator->errors();
        
        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user = User::where('email', $request->get('email'))->first();

        if(empty($user))
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'This email does not exist.',
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }
        
        $token = Str::random(4);

        ResetPassword::create([
                'email' => $request->get('email'),
                'token' => $token,
        ]);

        \Mail::to($request->get('email'))->send(new \App\Mail\ForgetPassword($user));

        $user->makeHidden([
            'created_at', 'updated_at', 'user_type', 'email_verified_at', 'name', 'phone', 'profile_image', 'cover_video', 'date_of_birth', 'OTP', 
            'is_verified', 'is_registered', 'device_id', 'device_type', 'description', 'website', 'facebook', 'youtube', 'twitter', 'instagram'
        ]);
        
        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'Forget Password Email has been sent to you.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $user,
        ]);
    }

    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'otp' => 'required',
            // 'email' => 'required|string|email|max:255',
            'user_firebase_id' => 'required',
            'password' => 'required'
        ]);

        $message = $validator->errors()->first();
        
        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        // $forgetUser = ResetPassword::where('email', $request->get('email'))->first();

        // if(empty($forgetUser))
        // {
        //     return response()->json([
        //         'status' => $this->responseConstants['STATUS_ERROR'],
        //         'message' => 'This email does not exist for forget password.',
        //         'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
        //     ]);
        // }

        // if($forgetUser->token == $request->get('otp'))
        // {
            $user = User::where('user_firebase_id', $request->get('user_firebase_id'))->first();
            
            if(empty($user))
            {
                return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user does not exist.',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
            }
            dd($request->get('password'));
            $userData = [
                'password' => bcrypt($request->get('password'))
            ];

            $user->update($userData);

            // $forgetUser->delete();

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Your Password changed successfully.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        // }
    }
}
