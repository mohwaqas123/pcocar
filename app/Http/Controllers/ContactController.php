<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       
        $data = [
                'name' => $request->post('name'),
                'business_name' => $request->post('business_name'),
                'email' => $request->post('email'), 
                 
                'number' => $request->post('number'), 
                'Business_web_site' => $request->post('Business_web_site'), 
                'number_of_cars' => $request->post('number_of_cars'), 
                'business_details' => $request->post('business_details'), 
                ];
              
                $name = $request->post('name');
                $business_name = $request->post('business_name');
                $email = $request->post('email'); 
                $number = $request->post('number');
                $business_web_site = $request->post('Business_web_site'); 
                $number_of_cars = $request->post('number_of_cars'); 
                $business_details = $request->post('business_details'); 

    Mail::to('partners@pcocar.com')->send(new \App\Mail\partner
    ($name, $number, $email,$business_name, $business_web_site, $number_of_cars,
     $business_details)); 

    return redirect('partner_inquiry')->with('error', 'Information  is send successfully.');



    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function contact(Request $request)
    {
            $data = [
                'name' => $request->post('name'),
                'email' => $request->post('email'),
                'phone' => $request->post('phone'), 
              
            ]; 
   
        
        //send email to umer sab
        $name =   $request->post('name');
        $email =   $request->post('email');
        $phone =   $request->post('phone');
    Mail::to('info@pcocar.com')->send(new \App\Mail\Contact
    ($name, $phone, $email ));  
     
            
             

             return redirect('contactus')->with('message', 'Information  is send successfully.');
            
    }



}
