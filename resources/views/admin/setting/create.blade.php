@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                         
                        <div class="card-box">
                            
                            @if($errors->any())
                            {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif



                            @if($msg)
                                    <div class="alert alert-success" role="alert">
                                      {{$msg}}
                                    </div>
                            @endif


                            <form role="form" action="{{ route('setting_admin_update') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="form-group col-md-6">
                                        <label for="name">Payment Mode</label>
                                        <input type="radio" value="0" id="live" @if($setting->stripe_mode==0) checked @endif name="stripe_mode" value="live">
                                        <label for="live">live</label>
                                        <input type="radio" value="1" id="test" name="stripe_mode"  @if($setting->stripe_mode==0) checked @endif value="test">
                                        <label for="test">Test</label>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name">Stripe live published key</label>
                                        <input  value="{{$setting->stripe_live_published_key}}" type="text" class="form-control @error('name') is-invalid @enderror" name="stripe_live_published_key" placeholder=""   >
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name">Stripe live secret key</label>
                                        <input  value="{{$setting->stripe_live_secret_key}}" type="text" class="form-control @error('name') is-invalid @enderror" name="stripe_live_secret_key" placeholder=""     >
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name">Stripe live prod key </label>
                                        <input  value="{{$setting->stripe_live_prod_key}}" type="text" class="form-control @error('name') is-invalid @enderror" name="stripe_live_prod_key" placeholder=""     >
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name">Stripe test published key </label>
                                        <input  value="{{$setting->stripe_test_published_key}}" type="text" class="form-control @error('name') is-invalid @enderror" name="stripe_test_published_key" placeholder=""   >
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name">Stripe test secret key</label>
                                        <input type="text"  value="{{$setting->stripe_test_secret_key}}" class="form-control @error('name') is-invalid @enderror" name="stripe_test_secret_key" placeholder=""     >
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name">Stripe test prod key</label> 
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"  value="{{$setting->stripe_test_prod_key}}" name="stripe_test_prod_key" placeholder=""    >
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>


                                        <div class="form-group col-md-6">
                                        <label>Promotion Promo Option:</label>
                                        
                                    
                                        <input type="radio"  value="0" name="promotion_type" @if($setting->promotion_type==0) checked @endif >Disable &nbsp;
                                        <input type="radio"  value="1" name="promotion_type" @if($setting->promotion_type==1) checked @endif >Enable &nbsp;


                                        
<!-- 
                                        <input type="checkbox" id="discount" value="discount" name="promotion_type"  @if($setting->promotion_type==1) checked @endif >% Discount &nbsp;
                                
                                        <input type="checkbox" id="flat_rate" name="promotion_type"  @if($setting->promotion_type==2) checked @endif value="test">Flat Rate -->
                                    </div> 

                                    <div class="form-group col-md-6">
                                        <label>Flate Rate Discount Option:</label>
                                        <!-- <input type="text" value="{{$setting->discount_rate}}" class="form-control @error('name') is-invalid @enderror" name="discount_rate" placeholder=""   >    -->
                                        <input type="radio"  value="0" name="flat_discount_type" @if($setting->flat_discount_type==0) checked @endif >Disable &nbsp;
                                        <input type="radio"  value="1" name="flat_discount_type" @if($setting->flat_discount_type==1) checked @endif >Enable &nbsp;
                                    </div>

<div class="form-group col-md-6">
                                        <label>Flate Rate disocunt Amount (GBP):</label>
                                        <input type="text" value="{{$setting->flat_discount_rate}}" class="form-control @error('name') is-invalid @enderror" name="flat_discount_rate" placeholder=""   > 
                                    </div>

                                
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-info btn-flat">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
});
} );
</script>
@endsection