<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Booking;
use App\models\Swipevehiclehistory;
use App\models\Vehicle;
use App\models\User;
use App\models\Customerinsurance;
use DB;
use App\models\PaymentHistory;
use App\Helper\Helper;
use App\models\Swipe;

use Illuminate\Support\Facades\Mail;
class bookingvehicleController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{    
// $booking = Booking::where('user_id', session()->get('user_id') )->orderby('id', 'Desc')->get();
// $booking = Booking::where('booking_status','3')->orderby('id', 'Desc')->get();
// dd($booking);

// $booking = DB::table('Booking')->where('booking_status', '=', 1)->where('booking_status', '=', 3)->get();


$booking = Booking::where('payment_status', !null)->whereRaw('booking_status = 1  or booking_status = 3')->where('is_deleted',0)->orderby('id', 'Desc')->get();
 
  

// $booking = Booking::orderby('id', 'asc')->get();
 
$url = "/admin/dashboard";
$title = 'Add Partner';
          $data = [

             	'booking'=>$booking, 
               'title' =>$title,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Active Bookings' ,


     
           ]; 




// return redirect()->route('vehicle_booking_pending')->with($data);
return view('admin.vichealbooking.index')->with($data);

}
public function vehicle_booking_old()
{   
 
$booking = Booking::where('booking_status',4)->orderby('id', 'Desc')->get();
$url = "/admin/dashboard";
 $title = 'Add Partner';
          $data = [

             	'booking'=>$booking, 
               'title' =>$title,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Completed' ,
           
           ]; 
          return view('admin.vichealbooking.completed')->with($data);

}
 
public function pending_vehicle_booking_here()
{   

// $booking = Booking::where('agreement_status',1)->where('payment_status',null)->
// where('booking_status' , '!2' )->where('reference_booking',null)->orderBy('id', 'DESC')->get();


$booking = Booking::where('payment_status',null)->where('reference_booking',0)->orderBy('id', 'DESC')->get();

$url = "/admin/dashboard";
 $title = 'Add Partner';
          $data = [

              'booking'=>$booking, 
               'title' =>$title,
                 'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Pending Bookings' ,
           
           ]; 
          return view('admin.vichealbooking.pending')->with($data);

}
public function vehicle_booking_cancel()
{   
 
$booking_cancel = Booking::where('booking_status',2)->orderBy('id', 'DESC')->get();
 

$url = "/admin/dashboard";
 $title = 'Add Partner';
          $data = [

              'booking_cancel'=>$booking_cancel, 
               'title' =>$title,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Cancelled' ,
           
           ]; 
          return view('admin.vichealbooking.cancel')->with($data);

}


/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
//
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
//
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
//
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
//
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
//
}
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy(Request $request)
{ 


      $booking = Booking::find($request->id);
      $vehicle = Vehicle::where('id', $booking->vehicle_id)->first();
 
      $vehicle->bookingStatus =0; 
      $vehicle->insurance_type =0; 
      $data = [  
       'detail' => $request->detail,
       $booking->booking_status =2, 
      ];
      $booking->update($data); 
      $booking->save();
      $vehicle->save();
       
   
    return redirect()->back()->with('message','Booking is cancel Successfully.');


}



public function expire(Request $request)
{ 

      $booking = Booking::find($request->id);
      $vehicle = Vehicle::where('id', $booking->vehicle_id)->first();
      
      // $vehicle->bookingStatus =0; 
      $booking->booking_status =4; 
      // $vehicle->insurance_type =0; 
      $vehicle->save();
      $booking->save();

      return redirect()->back()->with('message','Booking completed Successfully');

}






public function adminAproveBooking(Request $request)
{  
      $booking = Booking::find($request->id);
 
      $booking->booking_status =3; 
      $booking->save();

 
      $email_to = $booking->user->email;
      
       Mail::to([$email_to])->send(new \App\Mail\BookingPermissionLet(
          $booking
        
           )); 
 
       
       return redirect()->route('vehicle_booking')->with('message','Booking Approved & Permission Letter sent via email to customer');

}
public function adminCancelBookingNew(Request $request)
{  

      $booking = Booking::find($request->id);

      $vehicle = Vehicle::where('id', $booking->vehicle_id)->first();
      
      $vehicle->bookingStatus =0; 
      $booking->booking_status =2;
      $vehicle->insurance_type = 0; 
      $vehicle->save();
      $booking->save();

 
     
       
       return redirect()->route('vehicle_booking')->with('message','Booking cancel becouse payment are not done');

}




public function customerview(Request $request)
{

 
$booking = Booking::where('id', $request->id)->with('vehicle')->first();
 
$vehicles = Vehicle::where('id',  $booking->vehicle_id)->first();
$customer_insurance = 
Customerinsurance::where('booking_id', $request->id)->first();
 
$swipe = Swipe::with('old_vehicles')->with('new_vehicles')->where('booking_id', $request->id)->orderBy('created_at', 'desc')->get();
 
$vehicles_all = Vehicle::where('bookingStatus',  '0')->get();
 $url = "/admin/dashboard";
 $title = 'Add Partner';
          $data = [

                   'vehicle' => $vehicles, 
                  'booking' => $booking,
                  'customer_insurance' => $customer_insurance,
                  'swipe' => $swipe,
                  'vehicles_all' => $vehicles_all,
                  'booking_id' => $request->id,
'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Booking Details' ,

                ]; 

 
return view('admin.booking.booking_detail')->with($data);
}

public function swipevehicle(Request $request){  

    $booking = Booking::where('id', $request->id)->with('vehicle')->first();
    $vehicle = Vehicle::where('id',  $request->vehicle_id)->first();
  

    $booking_id = $booking->id; 
    $user_id = $booking->user->id; 
    $vehicle_id = $vehicle->id;
 
    $booking->vehicle_id = $vehicle->id; 
    $vehicle->bookingStatus =1; 

    $data = 
    [
      'booking_id' => $booking_id, 
      'user_id' => $user_id, 
      'vehicle_id' => $vehicle_id, 
    ];
 
    $swipevehiclehistory = Swipevehiclehistory::create($data);
 





    $vehicle->save();
    $booking->save(); 

    return back()->with('message','Vehicle for this booking is updated');
}
public function clearpayment(Request $request){  

      $booking = Booking::find($request->id);
      $vehicle = Vehicle::where('id', $booking->vehicle_id)->first();
      $user = PaymentHistory::where('booking_id',$request->id )->first();
        $booking_id = $booking->id;
        $user_id = $booking->user->id;
        $vehicle_id = $vehicle->id;
        $month_duration = $booking->duration;

            $data = [
            'booking_id' => $booking_id,
            'payment_type' => '1',
            'user_id' => $user_id,
            'amount' => $request->amount,
            'vehicle_id' => $vehicle_id,
            'month_duration' => $month_duration,
            'per_month_amount' => $request->amount,
            'payment_mode' => '2',
            'payment_status' => '1',
            'description' => $request->detail,
            ];
             PaymentHistory::create($data);


             return redirect()->route('vehicle_booking')->with('message','payment are recived');

       
}


public function renew(Request $request)
{         

  
              $booking = Booking::find($request->id);
              $vehicle = Vehicle::where('id', $booking->vehicle_id)->with('car_model')->with('car_make')->first();  
              $price = Helper::get_price_with_check_discount($vehicle->price);
              $url = "/admin/dashboard";
              $title = 'Add Renew Booking';
               $data =  [
               'booking' => $booking, 
               'vehicle' => $vehicle, 
               'title' =>$title,
               'price' =>$price,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Renew Booking' ,

                ];

          return view('admin.booking.renew')->with($data);
 }
      public function renew_permission(Request $request)
      {         
  
                
                $booking = Booking::find($request->id);
                $booking->renew_permission =1; 
                $booking->save();
                return redirect()->back()->with('message','Permission are send.');
       }

       public function swipevecicle(Request $request)
      {         
      $vehicle_available = Vehicle::where('bookingStatus',0)->orderby('id', 'Desc')->get();
      $booking = Booking::find($request->id); 
      $vehicle = Vehicle::where('id', $booking->vehicle_id)->with('car_model')->with('car_make')->first(); 
              $url = "/admin/dashboard";
              $title = 'Add Renew Booking';
               $data =  [
               'vehicle_available' => $vehicle_available, 
               'booking' => $booking, 
                'vehicle' => $vehicle, 
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Swap Request' ,

                ];
          return view('admin.booking.swipe')->with($data);

     
                
       }
public function swipebooking_add(Request $request){

             $booking_id = $request->id;
            
             $old_vehicle_id = $request->old_vehicle_id;
             $new_vehicle_id = $request->new_vehicle_id;
             $detail = $request->detail;
             $date_time = date("Y-m-d H:i:s");//$request->booking_swipe_date .' '.$request->hr.':'.$request->min.':00';
            
             $booking = Booking::where('id', $request->id)->with('user')->first(); 
             $email_to = $booking->user->email;
             $name = $booking->user->name;

             $data = 
              [
                'booking_id' => $booking_id, 
                'old_vehicle_id' => $old_vehicle_id, 
                'new_vehicle_id' => $new_vehicle_id,
                'description' => $detail, 
                'swipe_date_time' => $date_time,
              ];
 
              $swipevehicle  = Swipe::create($data);

              //booking id changed in booking table
              $booking->vehicle_id =$request->new_vehicle_id;
              $booking->save();
 

              //old_vehicle change status (out from booking & show at browse page)  
              $old_vehicle = Vehicle::with('car_model')->with('car_make')->where('id',  $request->old_vehicle_id)->first();
              $old_vehicle->bookingStatus =0;
              $old_vehicle->save(); 


              //new_vehicle change status (book status)  
              $new_vehicle = Vehicle::with('car_model')->with('car_make')->where('id',  $request->new_vehicle_id)->first();
              $new_vehicle->bookingStatus =1;
              $new_vehicle->save(); 
 
          /*  Send agreement email (Process) */
          //cancel old agreement email notification
           Mail::to([$email_to,'bookings@pcocar.com'])->send(new \App\Mail\SwipeCancelOldAgreement(
          $swipevehicle,
          $new_vehicle,
          $old_vehicle,
          $name,
          $booking
           )); 
        //renew agreement email
         /* Mail::to([$email_to,'bookings@pcocar.com'])->cc($request->session()->get('email'))->send(new \App\Mail\SwipeRenewal(
          $swipevehicle,
          $new_vehicle,
           )); */

              return redirect()->back()->with('message','Swap done Successfully.');
 }
public function vehicle_booking_all(Request $request)
      {         
        
      $booking = Booking::orderby('id', 'asc')->get();

        $url = "/admin/dashboard";
        $title = 'Add Partner';
                  $data = [
              'booking'=>$booking, 
               'title' =>$title,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'All Bookings' ,


               
           ]; 
            return view('admin.vichealbooking.all')->with($data);
       }
public function vehicle_details(Request $request)
      {         
        
         $records = Vehicle::with('car_model')->with('car_make')->where('vehicle_id',$request->vehicle_id)->get(); 
         //dd( $records->user->name);
            // return json_encode($records);


          $customer = ' <label for="make" class="col-form-label">Information</label>
                  <select    name="name" placeholder="Information Drop Down"
                    class="form-control select2">
                     ';
               
            foreach($records as $rec){
               // if($rec->user->name)
                $customer .= "<option value='".@$rec->price ."'>".@$rec->price ." (".@$$rec->price .")</option>";
            }

 $customer .= "</select>";
 return $customer;


       }
       public function bookingdestory(Request $request)
      {         
        $booking = Booking::where('id', $request->id)->first();
        $booking->update([
            'is_deleted' => 1
          ]);
        $booking->save();
        return redirect('admin/vehicle_booking')->with('message', 'Booking is deleted');
        }



public function deleted()
{    

$booking = Booking::where('is_deleted',1)->orderby('id', 'Desc')->get();
 
 
$url = "/admin/dashboard";
$title = 'Add Partner';
          $data = [

              'booking'=>$booking, 
               'title' =>$title,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Bookings'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Deleted Bookings' ,
        ]; 




// return redirect()->route('vehicle_booking_pending')->with($data);
return view('admin.vichealbooking.completed')->with($data);

}


       


}
 






