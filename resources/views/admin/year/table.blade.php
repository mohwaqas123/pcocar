<div id="custom-search-table">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
        <th style="width: 10px">Sr.#</th>
        <th>Name</th>
        <th style="width:90px;">Action</th>
        </tr>
        </thead>
        <tbody>
            @php
                $count = 1;
            @endphp
        @foreach($countries as $country)
        <tr>
            <td>{{ $count }}</td>
            <td>{{ $country->name }}</td>
            <td>
            <a class="btn btn-warning btn-sm" href="{{ route('adminCountryEdit', $country->id) }}" class="mr-5">Edit </a>
        </td>
        </tr>
        @php
            $count++;
        @endphp
        @endforeach
        </tbody>
    </table>
    {{$countries->links()}}
</div>
