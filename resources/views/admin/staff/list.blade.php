@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">	
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif

						<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
							 	<thead>
                                <tr>
                                <th>id</th>
                                <th>Name</th>
                                <th>Email</th>                                
                                <th>Phone</th>
                                <th>Job Description</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                	@foreach($staff as $staff_var)
                                <tr>
                                    <td>{{ $staff_var->id }}</td>
                                    <td>{{ $staff_var->name }}</td>
                                    <td>{{ @$staff_var->email }}</td>
                                    <td>{{ @$staff_var->phone }}</td>
                                    <td>{{ @$staff_var->description }}</td>
                                    <td>
                                    <a href="{{ route('adminstaffedit', $staff_var->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip" 
                                    data-toggle="tooltip" data-placement="Top" data-original-title="Edit"  class="mr-5"><i class="fa fa-pencil"></i> </a>

                                    <a  href="{{ route('adminstaffview', $staff_var->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary metismenu with-tooltip"
                                    data-toggle="tooltip" data-placement="Top" data-original-title="View" ><i class="fa fa-eye"></i>
			                        </a>



                                </td>
                                </tr>

                                @endforeach
                                </tbody>
                                </tbody>
                               
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>




@endsection
@section('scripts')
<script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection