<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;




class VehicleImage extends Model
{


	protected $table = 'vehicle_images';

	
    protected $fillable = [
        'vehicle_id','images'
    ];
}
