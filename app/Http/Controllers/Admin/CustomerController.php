<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Booking;
use App\models\Vehicle;
use App\models\PaymentHistory;
use App\models\Swipevehiclehistory;



use App\models\Customerinsurance;



use PDF; 
use App\Helper\Helper;
use Carbon\Carbon;
use Stripe;
use DB;
use App\models\Swipe;

class CustomerController extends Controller

{

  public function generate_pdf_permission(Request $request){
       $booking = Booking::where('id', $request->id)->with('vehicle')->with('user')->first();
        // dd($booking);
       $vehicles = Vehicle::where('id', $booking->vehicle_id)->with('car_make')->with('car_model')->first();
      $customerinsurance = Customerinsurance::where('booking_id', $booking->id)->first();  
       // dd($customerinsurance->policy_number);

          $user = User::where('id', $request->id)->first(); 
          $swipes_records = Swipe::where('booking_id',$booking->id)->orderBy('id','desc')->first();
          $checker =Swipe::where('booking_id',$booking->id)->orderBy('id','desc')->exists();

        if($checker){
          
        $start_date = $swipes_records->swipe_date_time;
       
        }
       else{     
         
        $start_date = $booking->booking_start_date;
      
        }

if($booking->insurance_type ==  1 )
{
  
      $policy_number   =  $customerinsurance->policy_number;
}

else
{
  
      $policy_number   =   'ZLPA/8QZ7TO9';
    

}
          $data = [
          'title' => 'PCOCAR Permission Letter',
          'heading' => 'PCOCAR Permission Letter',
          'booking_date'=>$start_date,
          'policy_number'=> $policy_number,
          'driver_name'=>$booking->user->name,
          'driving_licence_no'=>$booking->dvla,
          'address' => $booking->address,
          'registeration_no' => $booking->vehicle->licence_plate_number,
          'expire_date' =>  $booking->booking_drop_off_date,
          'make' =>  $vehicles->car_make->name,
          'model' =>  $vehicles->car_model->name, 
            ];
      
        $pdf = PDF::loadView('frontend.pdf_permission', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);
 


  }
  public function chargeamountStripe(Request $request)
  {

          \Stripe\Stripe::setApiKey( env('STRIPE_SECRET') );
            $amount = $request->amount * 100;
          
             $currency = 'GBP';  
            $return = Stripe\Charge::create ([
                            "amount" => $amount,
                            "currency" =>  $currency, 
                            "description" =>  $request->detail,
                            'customer' => $request->stripe_customer_id,
                    ]);
     
            if($return->id !==0 )
            {
      
            $user = User::find($request->id);
            $data = [
            'booking_id' =>$request->booking_id ,
            'payment_type' => '2',
            'user_id' => $request->customer,
            'amount' => $request->amount,
            'transaction_id' => $return->id,
            'vehicle_id' => '0',
            'month_duration' => '0',
            'per_month_amount' => $request->amount,
            'payment_mode' => '2',
            'payment_status' => '1',
            'description' => $request->detail,
            ];

             PaymentHistory::create($data);
                 return redirect()->route('customer_list')->with('message', 'Payment is done Successfully');
            }
            else
            {    
               return redirect()->route('customer_list')->with('error', 'card may be expire May be not enough balance.');
             }

  }

  public function index()
    {
        $user = User::with('booking')->get();
    
        //$booking = Booking::where('id', $request->user_id)->with('user')->first();
        $url = "/admin/dashboard";
        $title = 'Customer List';
        $data = [
                'user'=> $user,
                'total_booking'=>4,
                 'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Customer'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer List' ,

                  ];  
        return view('admin.customer.index')->with($data);
      }
      public function user_edit(Request $request)
      
      {
              $user = User::find($request->id);
              $url = "/admin/dashboard";
              $title = 'Customer List';
              $data = [
                      'user'=> $user,
                      'title' =>$title,
                      'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Customer'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer Edit' ,
                   
                       ]; 
               return view('admin.customer.edit')->with($data);
            }

       public function updaet_user(Request $request)
        {
        $User = User::find($request->id); 
        $url = "/admin/dashboard";
        $title = 'Customer List';
        $data = [
              'name' => $request->name,
              'phone' => $request->phone,
             'address' => $request->address, 
             'address' => $request->dob, 
             'address' => $request->description,  
              'user'=> $User,
              'title' =>$title, 
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Customer'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer Edit' ,
                
        ];   

       $User->update($data);

       

        return view('admin.customer.edit')->with($data);

    

    }

      

             public function customerview($id)

            {

   
 
            $User = User::findOrFail($id);

            $User->delete();

            return redirect()->route('customer_list')->with('success', 'Record delete Successfully');







            $booking = Booking::where('id', $request->user_id)->with('user')->first();

            $User = User::where('id',  $booking->user_id)->first();



            return view('admin.booking.booking_detail',[

             'vehicle' => $vehicles, 

            'booking' => $booking,



            ]) ;



            } 

              public function bookingedit(Request $request , $id)

            {

            $User = User::findOrFail($id);   

         

           

            // $booking = Booking::where('user_id')->get();

            $booking = DB::table('Booking')->select('user_id')->first();

            







// $booking = Booking::where('user_id', $request->User)->first();

// dd($booking);



// $vehicles = Vehicle::where('id',  $booking->vehicle_id)->first();

          



            dd($booking);

            

   return redirect()->route('customer_list')->with('success', 'Record delete Successfully');

          

            

            } 



 public function chargeamount(Request $request) 
            { 

          $booking = Booking::where('user_id', $request->id)->with('user')->orderby('id', 'Desc')->get();
             
          $url = "/admin/dashboard"; 
          $title = 'Customer charge';
          $customer = User::findOrFail($request->id);  
          $data = [ 
               'title' =>$title,
               'booking' =>$booking,
               'customer' => $customer,


                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Customer'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer charge' ,

        ];  

        return view('admin.customer.charge')->with($data);
            }
  public function addamount(Request $request)
      {
            $url = "/admin/dashboard";
            $title = 'Customer charge';
            $booking = Booking::where('user_id', $request->id)->with('user')->get();
            $data = [
              'title' =>$title,

      'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Customer'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer Charge' ,
                 'booking' => $booking,
               'id'=> $request->id,
              ];  
            return view('admin.customer.addcharge')->with($data);
       }
  public function addcharges_payment_history(Request $request)
  {  
       $validator = $request->validate([
            'amount' => 'required', 
        ]);
          $paymentData = [
            'name' => $request->name, 
            'description' => $request->description,
            'amount' => $request->amount,
            'payment_type'=>$request->charges_type,
            'booking_id' =>$request->booking,
            'user_id' =>$request->user_id,
            'payment_mode'=> 1,
            'payment_status'=>1,
        ];
 //dd($paymentData);
        $payment_history = PaymentHistory::create($paymentData);
        return redirect()->route('customer_list')->with('message', 'Payment is added Successfully');
  }
   public function customer_history(Request $request) 
            { 

          $booking = Booking::where('user_id', $request->id)->with('user')->orderby('id', 'asc')->get();
          dd($booking);
          $Swipe = Swipe::where('id', $booking->id)->with('car_make')->with('car_model')->first(); 


          // $swipes_records = Swipe::where('id',$booking->id)->orderBy('id','desc')->get();
      
          $checker =Swipe::where('booking_id',$booking->id)->orderBy('id','desc')->exists();
    dd($checker);



            $User = User::where('id',  $booking->user_id)->first();


              $vehicles = Vehicle::where('id', $booking->vehicle_id)->with('car_make')->with('car_model')->first();

          $url = "/admin/dashboard"; 
          $title = 'Customer charge';
          $customer = User::findOrFail($request->id);

          $data = [ 
               'title' =>$title,
               'booking' =>$booking,
               'customer' => $customer,
              'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Customer'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Customer history' ,

        ];  

        return view('admin.customer.history')->with($data);
            }

      public function generate_pdf_pcn_hire_agreement(Request $request){
 $booking_info = Booking::where('id', $request->id)->with('user')->first();

$vehicles = Vehicle::where('id', $booking_info->vehicle_id)->with('car_make')->with('car_model')->first(); 
       $user = User::where('id', $request->id)->first(); 

     // $customer_insurance_id = Customerinsurance::where('booking_id',  $booking_info->id)->with('user')->with('booking')->first(); 
   
     

         

        $swipes_records = Swipe::where('booking_id',$booking_info->id)->orderBy('id','desc')->first();
        $checker =Swipe::where('booking_id',$booking_info->id)->orderBy('id','desc')->exists();

       if($checker){
        $start_date = $swipes_records->swipe_date_time;
        
       }
       else{  
        $start_date = $booking_info->booking_start_date;
   
      
       }
 

         //           'customer_insurance_id' => $customer_insurance_id, This  $data array will be passed to our PDF blade
       $data = [
          'title' => 'PCOCAR',
          'heading' => 'SUBSCRIPTION BOOKING FORM PCOCAR',
          'content' => '',
          'signature_name' => $booking_info->user->name ,
          'signature'=> $booking_info->user->name ,   
          'booking' => $booking_info, 
 
           'vehicle' => $vehicles,    
             'insurance' => $request->session()->get('insurance'),
             'start_date'=>$start_date,

            ];
       
        $pdf = PDF::loadView('frontend.pcn_hire_agreement', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);


  }













}

