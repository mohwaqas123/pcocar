<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePcnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pcn', function (Blueprint $table) {
            $table->id();
            $table->string('ref_number');
            $table->integer('vrm');
            $table->integer('status');
            $table->string('stage');
            $table->string('pcn_contravention');
            $table->string('notice_date');
            $table->string('received_date');
            $table->integer('type_of_action');
            $table->string('action_date');
            $table->string('charging_authority_name');
            $table->string('driver_name');
            $table->string('driver_email');
            $table->string('driver_phone');
            $table->string('doc_mot')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pcn');
    }
}
