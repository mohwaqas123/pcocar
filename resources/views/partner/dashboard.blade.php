@extends('layouts.partner')
@section('styles')

<style>
    .widget-chart-two { 
    border: 1px solid #eee;
}
 
.color1{color: blueviolet; font-size:69px;}
.color2{color: aquamarine; font-size:69px;}
.color3{color: #78a797; font-size:69px;}
.color4{color: #fda5de; font-size:69px;}

</style>


@endsection
@section('content')
<div>
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
        
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                    </button>
                </li>
               <li>
                    <div class="page-title-box">
                   
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">Welcome to {{$partner->name}}</li>
                        </ol>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
    <!-- Top Bar End -->
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">



            <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <h4 class="header-title mb-4">Statistic Overview</h4>

                                    <div class="row">
                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <i class="mdi color1 mdi-apple-keyboard-shift"></i>
                                                </div>     
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Total Earning</p>
                                                    <h3 class=""> &#163; 35,715</h3>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <i class="mdi color2 mdi mdi-car-connected"></i>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Vehicle Available</p>
                                                    <h3 class="">45</h3>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <i class="mdi color3 mdi mdi-car-connected"></i>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Vehicle Hired</p>
                                                    <h3 class="">954</h3>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-sm-6 col-lg-6 col-xl-3">
                                            <div class="card-box mb-0 widget-chart-two">
                                                <div class="float-right">
                                                    <i class="mdi color4 mdi mdi-pencil-lock"></i>
                                                </div>
                                                <div class="widget-chart-two-content">
                                                    <p class="text-muted mb-0 mt-2">Current Booking</p>
                                                    <h3 class="">67</h3>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>



            <div class="row">
                <div class="col-4">
                    <div class="card-box">
                        <h4>Vehicle</h4>
                        <p>PCO: {{$Total_vehicle_pco}}</p>
                        <p>Private:{{$Total_vehicle_private}}</p>

                         <input onclick="window.location='{{ url("partner/p_vehiclesList") }}'"  type="button" class="btn btn-primary" value="View List">&nbsp;
           <input onclick="window.location='{{ url("partner/p_vehicleCreate") }}'"  type="button" class="btn btn-primary" value="Add New">
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-4">
                    <div class="card-box">
                        <h4>Driver</h4>
                        <p>Total:{{$Total_driver_add_by_partner}}</p>
                        <p>Active:{{$Total_driver_add_by_partner_active}}</p>
                         
            <input type="button" onclick="window.location='{{ url("partner_driver_list") }}'"  class="btn btn-primary" value="View List">&nbsp;
            <input type="button" onclick="window.location='{{ url("partner_driver_add") }}'"  class="btn btn-primary" value="Add New">
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-4">
                    <div class="card-box">
                        <h4>Booking</h4>
                        <p>Old:{{$booking}}</p>
                        <p>New:{{$booking}}  </p>
                         

                         <input type="button" onclick="window.location='{{ url("partner_vehicle_booking") }}'"  class="btn btn-primary" value="View List">&nbsp;
                         <input type="button" onclick="window.location='{{ url("partner_vehicle_booking") }}'"  class="btn btn-primary" value="Add New">
                        <!-- end row -->
                    </div>
                </div>


 
                
            </div>
           
            <!-- end row -->
            </div> <!-- container -->
            </div> <!-- content -->
            
        </div>
        @endsection
        @section('scripts')
        @endsection


