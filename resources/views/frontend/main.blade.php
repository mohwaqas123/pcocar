@extends('layouts.app')

@section('styles')

@endsection

@section('content')
 


  @include('layouts.header')

 

    <!-- Main content -->
      <section class="slice py-7">
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-12 col-md-5 col-lg-6 order-md-2 text-center">
                    <!-- Image -->
                    <figure class="w-100">
                        <img alt="Image placeholder" src="{{ url('/')}}/public/theme3/assets/img/header-car.png" class="img-fluid" />
                    </figure>
                </div>
                <div class="col-12 col-md-7 col-lg-6 order-md-1 pr-md-5">
                    <!-- Heading -->
                    <h1 class="display-4 text-center text-md-left mb-3">
                         
                        Your UK-wide personal <strong class="text-primary">drive Service</strong>
                    </h1>
                    <!-- Text -->
                    <p class="lead text-center text-md-left text-muted">
                        Travel in Comfort and Safety with PCO and Non-Pco car
                    </p>
                    <!-- Buttons -->
<?php

 $ip = $_SERVER['REMOTE_ADDR'];// '168.192.0.1'; // your ip address here

    $query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
    if($query && $query['status'] == 'success')
      $city =   $query['city'];
    else
      $city = '';
    



//$country=file_get_contents('http://api.hostip.info/get_html.php?ip=84.39.153.51');
//$only_country=explode (" ", $country); 
//substr($only_country[3], 0, -3) ;
?>                    

    
        <form method="post" enctype="multipart/form-data" action="{{ route('browse') }}">

                        @csrf


                    <div class="text-center text-md-left mt-5">
                        <input name="city" value="{{$city}}" type="text" id="autocomplet2" onFocus="geolocate()"  placeholder="Enter Your Nearest Area" class="form-control" required="required" > <br>
                        <div> 
                            <!--  <a href="{{ route('browse', 1) }}"  class="btn btn-lg btn-primary btn-font-size" disabled>P C O</a>
                             <a  href="{{ route('browse',2) }}"   class="btn btn-secondary btn-lg btn-font-size" disabled>P R I V A T E</a>
 -->
                             <input type="submit" name="btn_pco" value="P C O" class="btn btn-lg btn-primary btn-font-size">
                             <input type="submit" name="btn_private" value="P R I V A T E" class="btn btn-lg btn-primary btn-font-size">
                             
                    </div>
                    </div></form>
                </div>
            </div>
        </div>
    </section>

    <section class="slice slice-lg pt-lg-6 pb-0 pb-lg-6 bg-section-secondary">
        <div class="container">
            <!-- Title -->
            <!-- Section title -->
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-6">
                    <span class="badge badge-soft-success badge-pill badge-lg">
                        Get started
                    </span>
                    <h2 class=" mt-4">Car Hire Services UK from PCOCAR</h2>
                    <div class="mt-2">
                        <p class="lead lh-180">We want to help you earn a living right away. Private an Uber-ready PCO Car today</p>
                    </div>
                </div>
            </div>
            <!-- Card -->
            <div class="row mt-5">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body pb-5">
                            <div class="pt-4 pb-5">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/illustration-5.svg" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                            <h5 class="h4 lh-130 mb-3">Flexible Friends</h5>
                            <p class="text-muted mb-0">We offer a minimum rental of 4 weeks and optional insurance for our Partners</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body pb-5">
                            <div class="pt-4 pb-5">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/illustration-6.svg" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                            <h5 class="h4 lh-130 mb-3">Excellent Support</h5>
                            <p class="text-muted mb-0">From uploading documents with ease to booking can be omitted your car's servicing needs</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body pb-5">
                            <div class="pt-4 pb-5">
                                <img src="{{ url('/')}}/public/theme3/assets/img/svg/illustrations/illustration-7.svg" class="img-fluid img-center" style="height: 150px;" alt="Illustration" />
                            </div>
                            <h5 class="h4 lh-130 mb-3">Fully Inclusive</h5>
                            <p class="text-muted mb-0">All servicing free. No extra charges even for tyres or brakes</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="slice slice-lg bg-section-dark pt-5 pt-lg-8">
        <!-- SVG separator -->
        <div class="shape-container shape-line shape-position-top shape-orientation-inverse">
            <svg width="2560px" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 2560 100" style="enable-background:new 0 0 2560 100;" xml:space="preserve" class="">
                <polygon points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
        <!-- Container -->
        <div class="container position-relative zindex-100">
            <div class="col">
                <div class="row justify-content-center">
                    <div class="col-md-10 text-center">
                        <div class="mt-4 mb-6">
                            <h2 class="h1 text-white">
                                Start earning now with UBER, BOLT, OLA, KAPTEN, VIVA VAN , AMAZON and so many other businesses by driving away our fully maintained licensed vehicles
                            </h2>
                            <h4 class="text-white mt-3">Create your own experience</h4>
                            <!-- Play button -->
                            <a href="#" class="btn btn-primary btn-icon mt-4">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice pt-0">
        <div class="container position-relative zindex-100">
            <div class="row">
                <div class="col-xl-4 col-sm-6 mt-n7">
                    <div class="card">
                        <div class="d-flex p-5">
                            <div>
                                <span class="badge badge-warning badge-pill">New</span>
                            </div> 
 
                            <div class="pl-4">
                                <h5 class="lh-130">make your journeys safely</h5>
                                <p class="text-muted mb-0">
                                 Our fully maintained & reliable vehicles provide you assurance of safe and sound journey.
                                    Should there be any disturbance we are here to help you on spot.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6 mt-sm-n7">
                    <div class="card">
                        <div class="d-flex p-5">
                            <div>
                                <span class="badge badge-success badge-pill">Tips</span>
                            </div>
                            <div class="pl-4">
                                <h5 class="lh-130">Flexible terms up to 24 months</h5>
                                <p class="text-muted mb-0">
                                        No long terms fixed & tough bindings. Flexible rules and procedures to support your changing requirements. Quick replacements if you are not happy with our vehicle. We take responsibility of all disruptions.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-12 col-sm-6 mt-xl-n7">
                    <div class="card">
                        <div class="d-flex p-5">
                            <div>
                                <span class="badge badge-danger badge-pill">Update</span>
                            </div>
                            <div class="pl-3">
                                <h5 class="lh-130">One flat monthly payment</h5>
                                <p class="text-muted mb-0">
                                    Roll your contact on monthly basis. No stress of long term commitments
Make one monthly payment and drive with peace as simple pay as go service.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-sm-6">
                    <div class="card">
                        <div class="d-flex p-5">
                            <div>
                                <span class="badge badge-warning badge-pill">New</span>
                            </div>
                            <div class="pl-4">
                                <h5 class="lh-130">Completely Online Payment</h5>
                                <p class="text-muted mb-0">
                                    Paypal, Stripe & Credit Card Options.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-6">
                    <div class="card">
                        <div class="d-flex p-5">
                            <div>
                                <span class="badge badge-success badge-pill">Tips</span>
                            </div>
                            <div class="pl-4">
                                <h5 class="lh-130">Online Documentation</h5>
                                <p class="text-muted mb-0">
                                    Online document approval and submitted through Digital Signature.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-12 col-sm-6">
                    <div class="card">
                        <div class="d-flex p-5">
                            <div>
                                <span class="badge badge-danger badge-pill">Update</span>
                            </div>
                            <div class="pl-3">
                                <h5 class="lh-130">Modern car ownership</h5>
                                <p class="text-muted mb-0">
                                    The joy of having your perfect car, without the downpayment.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="slice slice-lg bg-section-secondary">
        <div class="container text-center">
            <div class="row justify-content-center mb-6">
                <div class="col-lg-8">
                    <!-- Title -->
                    <h2 class="h1 strong-600">
                        Complete flexible pricing plan
                    </h2>
                    <!-- Text -->
                    <p class="text-muted">
                       Experience electric & hybrid car ownership for 1-24 months with Drover's flexible deals
                    </p>
                </div>
            </div>
            <!-- Pricing -->
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md">
                    <div class="card card-pricing text-center px-3 hover-scale-110">
                        <div class="card-header py-5 border-0 delimiter-bottom">
                            <!--<div class="h1 text-center mb-0"><span class="price font-weight-bolder">49</span></div>-->
                            <span class="h6 text-muted">Private Car</span>
                        </div>
                        <div class="card-body">
                            <p>Our excellent quality fully maintained private vehicles cover you for:<br>
                            
Delivery Service including Food or any type of Parcel [excluding all flammable or explosive substances</p>
                            <ul class="list-unstyled text-sm mb-4">
                                <li class="py-2">Business or work commutation</li>
                                <li class="py-2">Social, domestic & pleasure</li>
                                <li class="py-2">Fully maintained before & during hire period</li>
                                <li class="py-2">Available at cheapest price in UK</li>
                                <li class="py-2">Comprehensively covered</li>
                                 <li class="py-2">Free road side assistance</li>
                                  <li class="py-2">Best customer service by our experienced staff</li>
                                   <li class="py-2"> Immediate replacement in case or accident or any other matter</li>
                                  
                                  
                                 
                            </ul>
                            <a href="#" class="btn btn-sm btn-warning hover-translate-y-n3 hover-shadow-lg mb-3">Book Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md">
                    <div class="card card-pricing bg-dark text-center px-3 border-0 hover-scale-110">
                        <div class="card-header py-5 border-0 delimiter-bottom">
                        <!--    <div class="h1 text-white text-center mb-0">-->
                        <!--<span class="price font-weight-bolder">590</span></div>-->
                            <span class="h6 text-white">PCO Car</span>
                        </div>
                        <div class="card-body">
                            <ul class="list-unstyled text-white text-sm opacity-8 mb-4">
                                <li class="py-2">Our large range of maintained & fully compliance mini cabs are available on  just a few clicks:</li>
                                <li class="py-2">All fuel types available </li>
                                <li class="py-2">Electric & Hybrid vehicles at best price in UK</li>
                                <li class="py-2">ULEZ & CONGESTION charge free   </li>
                                <li class="py-2">Fully maintained before & during hire period</li>
                                  <li class="py-2">Fully maintained before & during hire period</li> 
                                  <li class="py-2">Comprehensively covered</li> 
                                 <li class="py-2">Free road side assistance</li>
                                 <li class="py-2">Best customer service by our experienced staff</li>
                                  <li class="py-2">Immediate replacement in case or accident or any other matter</li>
                                 
                            </ul>
                            <a href="#" class="btn btn-sm btn-white hover-translate-y-n3 hover-shadow-lg mb-3">Book Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="mt-5 text-center">-->
            <!--    <p class="mb-2">-->
            <!--        Both pricings contains all 6 months free support. Need more?-->
            <!--    </p>-->
            <!--    <a href="#" class="text-primary text-underline--dashed">Contact us<i data-feather="arrow-right" class="ml-2"></i></a>-->
            <!--</div>-->
        </div>
    </section>

     @include('layouts.footer')
   
 

</body>
@endsection
@section('scripts')
@endsection
