<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PCOCAR </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0; background: #cccccc; font-family: arial; ">
    <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tr>
            <td style="padding: 10px 0 30px 0;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					
					 
                     
                    <tr>
                        <td bgcolor="#fff" style="padding: 0px 25px 0px 25px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
							 
								
								<tr>
                                    <td style="padding: 0px 0 10px 0; font-weight: bold; color:#153643;">
                                        Hi  {{$booking->user->name}},  

                                    </td>
                                </tr>
                                 
                                <tr>
                                    <td style="padding: 0px 0 10px 0; color: #153643;   font-size: 14px; line-height: 20px;">
                                        Please have a look on the details below 

                                    </td>
                                </tr>

                                  


 
                            </table>




                            <table width="500" border="1">
  <tbody>
    <tr style="background:#eee; font-weight:bold;">
      <th>Customer Details</th> 
    </tr>
    <tr>
      <td>
        Name: {{$booking->user->name}} <br>
        Email: {{$booking->user->email}} <br>
        Phone: {{$booking->user->phone}} <br>
        DVLA License Number: {{date('d-m-Y H:i:s', strtotime($booking->dvla))}} <br>
       Date:{{date('d-m-Y H:i:s', strtotime($booking->dob))}}<br>
        Address: {{$booking->address}} <br>
        National Insurance Number: {{$booking->national_insu_numb}} <br>
        @if($booking->pco_licence_no) PCO License Number: {{$booking->pco_licence_no}}  @endif
         <br>

      </td>
       
    </tr>
    
     <tr style="background:#eee; font-weight:bold;">
      <th>Vehicle Details</th> 
    </tr>
    <tr>
      <td>
        {{$vehicle->licence_plate_number}} <br>
        {{$vehicle->car_make->name}} <br>
        {{$vehicle->car_model->name}} <br>
        {{$vehicle->year }} <br>
        Postal pickup dropoff: {{$vehicle->postal_pickup_dropoff }}
      </td>
       
    </tr>
    
    <tr style="background:#eee; font-weight:bold;">
      <th>Booking Details</th> 
    </tr>
    <tr>
      <td>
        @php
         $discount_amount = $vehicle->price * ($discount / 100);
        $total = $vehicle->price - $discount_amount;
      
 $promo_text= '';
//if promo code is applied
if(session('permotion_discount')){
              $price =  $total -  (( $total * session('permotion_discount'))/100);
               $promo_text =  ' ('.session('permotion_discount').'% Discount Promo Applied)';
              
   }
 

 if($insurance==1){
$ins_cost = 0;
}
else
{
$ins_cost = env('INSURANCE_CODE');
}




         if($booking->reference_booking > 0)
        $grand_tota = $booking->price + $ins_cost; 
       else
        $grand_tota = $booking->price + $ins_cost + env('DEPOSIT_SECURITY');   





 
$start_date = date('Y-m-d');   
$weeks = '+'.$booking->duration. ' week';
$exp_date = date('d.m.Y', strtotime($weeks, strtotime($start_date)));
if($payment_mode==2)
 {
  $p = "Credit Card Stripe"; 
  $first_payment = "Paid";
}
else 
{
  $p ="Credit Card Stripe";
  $first_payment = "paid";
}



if($months==1)
   $m = '4 weeks';
else
  $m = $months.' weeks';


@endphp
 


        Booking Generated: {{date('d-m-Y H:i:s', strtotime($booking->created_at))}}<br>
        Booking Start From: {{date('d-m-Y H:i:s', strtotime($booking_start_date))}}<br>
        Expire Date: {{date('d-m-Y H:i:s', strtotime($booking->booking_drop_off_date))}}<br>
      
        Price per week: £{{$price_per_week}}  {{ $promo_text}} <br>
        Duration: {{$m}}  <br>
         @if ($booking->discount == 0)
         no discount are apply <br>
        @else
       Discount: {{$booking->discount}}% <br>
        @endif
       
        @if ($booking->discount == 0)
        Insurance: £{{$ins_cost}} <br>
        @else
        @endif
        @if ($booking->deposit == 2)
       Security Deposit(First time only): £{{env('DEPOSIT_SECURITY')}}<br>
        @else
         No Security Deposit   <br>
        @endif
        
 

        
       


        Total: <b>£{{$price_total}}</b>  <br> 
        Payment Mode:  {{$p}}
        
      </td>
       
    </tr>


    <tr style="background:#eee; font-weight:bold;">
      <th>Attached Documents</th> 
    </tr>
    <tr>
      <td> 
        <a href="{{url('/')}}/storage/app/{{$booking->doc_dvla}}">DVLA Licence(Front)</a> <br>
        <a href="{{url('/')}}/storage/app/{{$booking->doc_cnic}}">DVLA Licence(Back)</a><br>
       @if($vehicle->vehicle_category==1) 
       <a href="{{url('/')}}/storage/app/{{$booking->doc_other}}">PCO Driver Licence Front</a><br>
      <!--  <a href="{{url('/')}}/storage/app/{{$booking->pco_driver_back}}">PCO Driver Licence Back</a><br>        -->
       <a href="{{url('/')}}/storage/app/{{$booking->pco_paper_licence}}">PCO Paper Licence</a><br> 
        @endif
        <a href="{{url('/')}}/generate_pdf_email/{{$booking->id}}">Hire Agreement </a> <br>
      </td>
    </tr>


 
     <tr style="background:#eee; font-weight:bold;">
      <th>Payment & Billing Details</th> 
    </tr>
    <tr>
      <td> 
        <table style="width:100%">
            <tr>
                <td>Date</td>
                <td>Amount</td>
                <td>Status</td>
            </tr>
            <tr>
                <td>
                    {{$booking->created_at}}
                </td>    
                <td>
                    £{{$grand_tota}}
                </td>
                <td>
                    {{ $first_payment}}
                </td>
            </tr>
  <?php
  if($months==1) $months=4;
$total_week = $months - 1;

/*if($discount)
  $price =  $booking->price - $discount +  env('INSURANCE_CODE')  ;
else*/
  $price =  $booking->price +  $ins_cost  ;


 


for($i=1;$i<=$total_week;$i++){
 
  $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 
  $date = strtotime( $booking->booking_start_date );
  $date = strtotime($days_add, $date);
  $show_date = date('d/m/Y', $date);

  echo "<tr>
          <td>".$show_date."</td>
          <td>£".$price."</td>
          <td>Unpaid</td>
  </tr>";


    //  echo "£".$price.' @ '. $show_date.'<br>';
}
?>    

       
        </table>
      </td>
    </tr>


  </tbody>
</table>
 


                        </td>
                    </tr>
                    <tr>

                        <td bgcolor="#0b5578" style="padding: 30px 30px 30px 30px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td style="color: #ffffff;   font-size: 12px; margin-left: 17px; " width="50%">
                                          Copyright © 2020 PCOCAR. All rights reserved.<br/>
                                    </td>
                                    <td align="right" width="25%">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-family: Arial, sans-serif; font-size: 12px;  ">
                                				<a href="https://twitter.com/pcocar"><i class="fab fa-twitter-square"></i></a>
                                                   
                                                </td>
                                                <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                                <td style="  font-size: 10px;  ">
                                                    <a href="https://www.facebook.com/pcocar/"><i class="fab fa-facebook"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>



