@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                         @if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                                <div class="card">

                                    <div class="card-body"> 
                                        
                                       <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a href="#home" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                                   <i class="fe-monitor"></i><span class="d-none d-sm-inline-block ml-2">Received</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#profile" data-toggle="tab" aria-expanded="true" class="nav-link ">
                                                    <i class="fe-user"></i> <span class="d-none d-sm-inline-block ml-2">In Progress</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#onhold" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="fe-mail"></i> <span class="d-none d-sm-inline-block ml-2">On Hold</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#messages" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="fe-mail"></i> <span class="d-none d-sm-inline-block ml-2">Further Evidence</span>
                                                </a>
                                            </li>
                                            
                                            <li class="nav-item">
                                                <a href="#setting" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="fe-mail"></i> <span class="d-none d-sm-inline-block ml-2">Approved</span>
                                                </a>
                                            </li><li class="nav-item">
                                                <a href="#awais" data-toggle="tab" aria-expanded="false" class="nav-link">
                                                    <i class="fe-mail"></i> <span class="d-none d-sm-inline-block ml-2">Disapproved</span>
                                                </a>
                                            </li>
                                             
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane show active" id="home">
                         <table id="datatable_tbl" class="table table-striped table-bordered"      style="width:100%">
                            <thead>
                                <tr>
                                <th  >Id</th>
                                <th >Customer</th>
                                <th >Vehicle</th>
                                <th  >Expense </th>
                                <th >Created  </th>
                                <th>Status  </th>
                                <th>Image</th>
                                <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    
                                @foreach($expence_submitted as $make)
                                <tr>
                                    <td>{{ $make->id}}</td> 
                                    <td>{{ @$make->booking->user->name}} 

                                        {{ @$make->booking->user->email}},
                                        {{ @$make->booking->user->phone}},</td> 
                                    <td>{{ @$make->vehicle->name}},{{ @$make->vehicle->year}},
                                      {{ @$make->vehicle->licence_plate_number}}</td> 
                                     <td>£{{ @$make->price}}</td>
                                      <td>{{ @$make->dob }} </td>
                                      <td>
                                      @if(@$make->int_status == 1)
                                      Received
                                      @elseif(@$make->int_status == 2)
                                      In Progress
                                      @elseif(@$make->int_status == 4)
                                      Approved
                                      @elseif(@$make->int_status == 5)
                                      Disapproved
                                      @elseif(@$make->int_status == 3)
                                      Further Evidence
                                      @else
                                      Onhold
                                      @endif</td>
                                    

                                      @if($make->attachment == Null)
                                     <td>
                                        </td>
                                      @else
                                      <td>
                                     <img src="{{ asset('storage/app/'.$make->attachment) }}"  height="50" width="50">
                                      </td>
                                      @endif
                                    <td>
                                  
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminexpensesview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>


                                     <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Click to Process"   href="{{route('adminexpensesprogress', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-refresh fa-spin"></i> </a>

                                   

                                  
                                    </td>
                                </tr>
                                
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                                            <!-- In Prove table are started here -->
                                            <div class="tab-pane " id="profile">
                   <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                <tr>
                                <th  >Id</th>
                                <th style="width: 20px" >Customer</th>
                                <th >Vehicle</th>
                                <th  >Expense </th>
                                <th  >Created  </th>
                                <th>Image</th>
                                <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    
                                @foreach($expence_in_progress as $make)
                                <tr>
                                    <td>{{ $make->id}}</td> 
                                    <td>{{ @$make->booking->user->name}}, 

                                        {{ @$make->booking->user->email}},
                                        {{ @$make->booking->user->phone}},</td> 
                                    <td>{{ @$make->vehicle->name}},{{ @$make->vehicle->year}},{{ @$make->vehicle->licence_plate_number}}</td> 
                                     <td>£{{ @$make->price}}</td>
                                     <td>{{ @$make->dob }} </td>

                                    
                                     @if($make->attachment == Null)
                                        <td>
                                        </td>
                                      @else
                                      <td>
                                     <img src="{{ asset('storage/app/'.$make->attachment) }}"  height="50" width="50">
                                      </td>
                                      @endif

                                      <td>
                                   
                                    <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminexpensesview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>



                                     <a class="btn btn-danger btn-sm metismenu with-tooltip"
                                    data-toggle="modal"  ata-toggle="tooltip" data-placement="Top" data-original-title="Disapprove" data-target="#new" 
                                     onclick="commentedit2({{$make->id}})" >
                                      <i class="fa fa-remove" ></i> </a>


                                      <a class="btn btn-danger btn-sm metismenu with-tooltip"
                                    data-toggle="modal"  ata-toggle="tooltip" data-placement="Top" data-original-title="further evidence" data-target="#further" 
                                      onclick="commentedit({{$make->id}})" >
                                      <i class="  fa fa-thumbs-o-down" ></i> </a>



                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Click To approve"
                                     href="{{route('adminexpensesprove', $make->id)}}"        
                                    class="mr-5"><i class="fa fa-check-circle"  ></i></a>
                                     
                                    </td>
                                </tr>
                               
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                                     <div class="tab-pane " id="onhold">
                      <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

                           <thead>
                                <tr>
                                <th  >Id</th>
                                <th style="width: 20px" >Customer</th>
                                <th >Vehicle</th>
                                <th  >Expense </th>
                                <th  >Created  </th>
                                <th>Image</th>
                                <th >Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recorded_on_hold as $make)
                                <tr>
                                      <td>{{ @$make->id}}</td> 
                                        <td>{{ @$make->booking->user->name}}, 

                                        {{ @$make->booking->user->email}},
                                        {{ @$make->booking->user->phone}},</td> 
                                    <td>{{ @$make->vehicle->name}},
                                        {{ @$make->vehicle->year}},
                                    {{ @$make->vehicle->licence_plate_number}}</td> 
                                     <td>£{{ $make->price}}</td>
                                     <td>{{ $make->dob }} </td>

                                    
                                     @if($make->attachment == Null)
                                        <td>
                                        </td>
                                      @else
                                      <td>
                                     <img src="{{ asset('storage/app/'.$make->attachment) }}"  height="50" width="50">
                                      </td>
                                      @endif
                                    <td>
                                    
                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminexpensesview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                     <a    class="btn btn-danger btn-sm metismenu with-tooltip"
                                    data-toggle="modal"  ata-toggle="tooltip" data-placement="Top" data-original-title="Rejected" data-target="#new" 
                                         >
                                      <i class="fa fa-remove" ></i> </a>



                                      <a class="btn btn-danger btn-sm metismenu with-tooltip"
                                    data-toggle="modal"  ata-toggle="tooltip" data-placement="Top" data-original-title="Rejected" data-target="#further" 
                                     >
                                      <i class="  fa fa-thumbs-o-down" ></i> </a>



                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Approve"
                                     href="{{route('adminexpensesprove', $make->id)}}"        
                                    class="mr-5"><i class="fa fa-check-circle"  ></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                       <div class="tab-pane " id="messages">
                      <table id="datatable_tbl" class="table table-striped table-bordered"  >

                           <thead>
                                <tr>
                                <th  >Id</th>
                                <th >Customer</th>
                                <th >Vehicle</th>
                                <th  >Expense </th>
                                <th  >Created  </th>
                                <th>Image</th>
                                <th style="width: 90px">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recorded_further_evidence as $make)
                                <tr>
                                      <td>{{ @$make->id}}</td> 
                                        <td>{{ @$make->booking->user->name}}, 

                                        {{ @$make->booking->user->email}},
                                        {{ @$make->booking->user->phone}},</td> 
                                    <td>{{ @$make->vehicle->name}},
                                        {{ @$make->vehicle->year}},
                                    {{ @$make->vehicle->licence_plate_number}}</td> 
                                     <td>£{{ $make->price}}</td>
                                     <td>{{ $make->dob }} </td>

                                    
                                     @if($make->attachment == Null)
                                        <td>
                                        </td>
                                      @else
                                      <td>
                                     <img src="{{ asset('storage/app/'.$make->attachment) }}"  height="50" width="50">
                                      </td>
                                      @endif
                                    <td>
                                    
                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminexpensesview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    <a class="btn btn-danger btn-sm metismenu with-tooltip"
                                    data-toggle="modal"  ata-toggle="tooltip" data-placement="Top" data-original-title="Disapprove" data-target="#new" 
                                     onclick="commentedit2({{$make->id}})" >
                                      <i class="fa fa-remove" ></i> </a>
                                      <a class="btn btn-danger btn-sm metismenu with-tooltip"
                                    data-toggle="modal"  ata-toggle="tooltip" data-placement="Top" data-original-title="further evidence" data-target="#further" 
                                      onclick="commentedit({{$make->id}})" >
                                      <i class="  fa fa-thumbs-o-down" ></i> </a>
                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Click To approve"
                                     href="{{route('adminexpensesprove', $make->id)}}"        
                                    class="mr-5"><i class="fa fa-check-circle"  ></i></a>
                                      <!-- <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="Click To approve"
                                     href="{{route('adminexpensesprove', $make->id)}}"        
                                    class="mr-5"><i class="fa fa-check-circle"  ></i></a> -->

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                            </div>






                               
                                            <div class="tab-pane " id="setting">
                      <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

                           <thead>
                                <tr>
                                <th  >Id</th>
                                <th >Customer</th>
                                <th >Vehicle</th>
                                <th  >Expense </th>
                                <th  >Created  </th>
                                <th>Image</th>
                                <th style="width: 90px">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recorded_approved as $make)
                                <tr>
                                      <td>{{ $make->id}}</td> 
                                        <td>{{ $make->booking->user->name}}, 

                                        {{ @$make->booking->user->email}},
                                        {{ @$make->booking->user->phone}},</td> 
                                    <td>{{ @$make->vehicle->name}},
                                        {{ @$make->vehicle->year}},
                                    {{ @$make->vehicle->licence_plate_number}}</td> 
                                     <td>£{{ $make->price}}</td>
                                     <td>{{ $make->dob }} </td>

                                    
                                    @if($make->attachment == Null)
                                        <td>
                                        </td>
                                      @else
                                      <td>
                                     <img src="{{ asset('storage/app/'.$make->attachment) }}"  height="50" width="50">
                                      </td>
                                      @endif
                                    <td>
                                    
                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminrecordsview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                                            <div class="tab-pane " id="awais">
                      <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

                           <thead>
                                <tr>
                                <th  >Id</th>
                                <th >Customer</th>
                                <th >Vehicle</th>
                                <th  >Expense </th>
                                <th  >Created  </th>
                                <th>Image</th>
                                <th style="width: 90px">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recorded_rejected as $make)
                                <tr>
                                      <td>{{ $make->id}}</td> 
                                        <td>{{ @$make->booking->user->name}}, 

                                        {{ @$make->booking->user->email}},
                                        {{ @$make->booking->user->phone}},</td> 
                                    <td>{{ @$make->vehicle->name}},
                                        {{ @$make->vehicle->year}},
                                    {{ @$make->vehicle->licence_plate_number}}</td> 
                                     <td>£{{ $make->price}}</td>
                                     <td>{{ $make->dob }} </td>

                                    @if($make->attachment == Null)
                                        <td>
                                        </td>
                                      @else
                                      <td>
                                     <img src="{{ asset('storage/app/'.$make->attachment) }}"  height="50" width="50">
                                      </td>
                                      @endif
                                    <td>
                                    
                                      <a class="btn btn-warning btn-sm metismenu with-tooltip" data-toggle="tooltip" data-placement="Top" data-original-title="View"   href="{{route('adminrecordsview', $make->id)}}"    
                                    class="mr-5"><i class="fa fa-eye"></i> </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                                            </div>
                                             <!-- In dispute table are started here -->

                                            
                                        </div>
                                    </div>
                                </div>
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                   
                        </div>
                    </div>
                    <div class="modal fade" id="new" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Why do you want to reject It? <span id='placer'></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            
        <form  action="{{ route('adminexpensesreject') }}"   method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="id" id="id" value="{{$make->id}}">
                  <label for="make" class="col-form-label">Please Enter Reasons<span class="text-danger">*</span></label>
                <div class="col-md-12">
                    <textarea  type="text" class="form-control" rows="10" cols="70" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                </div>
                <center> <button type="submit" class="btn btn-primary">Submit</button></center>
          </form>
        </div>

      </div>
    </div>
  </div>



  <div class="modal fade" id="further" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Enter Further Evidence Details: <span id='placer'></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            
        <form  action="{{ route('adminexpensesfurtherevidence') }}"   method="post" enctype="multipart/form-data">
              @csrf
            
      
                 <input type="hidden" name="id" id="id2" value="{{$make->id}}">

 
                  <label for="make" class="col-form-label">Please Enter Reasons<span class="text-danger">*</span></label>
                <div class="col-md-12">
                    <textarea  type="text"class="form-control" rows="10" cols="70" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                   
                </div>


             
              <center> <button type="submit" class="btn btn-primary">Submit</button></center>
          </form>
        </div>

      </div>
    </div>
  </div>

                </div>
            </div>
        </div>
    </div>
</div>





@endsection
@section('scripts')
 <script>

  function commentedit(id){  
    // alert(id);
 $('#id2').val(id);
  }

 function commentedit2(id){  
  
 $('#id').val(id);
  }
         
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
