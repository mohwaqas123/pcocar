<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>{{ $title }}</title>
<style type="text/css">

	@import url('https://fonts.googleapis.com/css?family=Tangerine');

/* 	@font-face {
  font-family: 'dancingscript';
  font-style: normal;
  font-weight: bold;
  src: url(https://fonts.googleapis.com/css?family=Rancho&effect=shadow-multiple) format('truetype');
}
 */

 </style>  
 <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
</head>
<body>
  
  
<nobr><nowrap>
<div class="pos" id="_0:0" style="top:0"></div>

<div align="center">
<div class="pos" id="_311:54" style="top:54;left:311">
<span id="_29.0" style="font-weight:bold; font-family:Verdana; font-size:29.0px; color:#006dc0">
Intercity Hire</span>
</div>
<div class="pos" id="_328:89" style="top:89;left:328">
<span id="_13.5" style=" font-family:Arial; font-size:13.5px; color:#001f5f">
23 Stanley Road, London E47DB</span>
</div>
<div class="pos" id="_371:112" style="top:112;left:371">
<span id="_10.7" style="font-weight:bold; font-family:Verdana; font-size:10.7px; color:#001f5f">
<U>T</U><U>e</U><U>l</U><U>:</U><U>0</U><U>2</U><U>0</U><U>8</U><U>1</U><U>3</U><U>3</U><U>3</U><U>1</U><U>3</U><U>8</U></span>
</div>
<div class="pos" id="_378:127" style="top:127;left:378">
<span id="_10.7" style=" font-family:Verdana; font-size:10.7px; color:#006dc0">
<a href="http://www.pcocar.com/">www.pcocar.com</span>
</div>
</div><br>


<table>
<tr>
<td valign="top" width="300px"><div class="pos" id="_100:201>
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
</a>Date: {{$booking_date}}</span>
</div></td>
<td><div class="pos" id="_210:201" style="top:201;left:210">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
</span>
</div>
<div class="pos" id="_454:200" style="top:200;left:454">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
Company Intercity Hire Ltd</span>
</div>
<div class="pos" id="_454:221" style="top:221;left:454">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
Address: 23 Stanley Road, Chingford, </span>
</div>
<div class="pos" id="_540:240" style="top:240;left:540">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
E4<span style=" font-family:Times New Roman"> </span> 7DB</span>
</div>
<div class="pos" id="_454:259" style="top:259;left:454">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
Email:</span>
</div>
<div class="pos" id="_540:260" style="top:260;left:540">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
<a href="mailto:info@toprangemotors.com">info@toprangemotors.com</span>
</div>
<div class="pos" id="_454:279" style="top:279;left:454">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
</a>Tel.<span style=" font-family:Times New Roman"> </span></span>
</div>
<div class="pos" id="_540:279" style="top:279;left:540">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
0208 1333 138</span>
</div>
<div class="pos" id="_454:298" style="top:298;left:454">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
Number.</span>
</div>
<div class="pos" id="_529:312" style="top:312;left:529">
<span id="_13.4" style=" font-family:Times New Roman; font-size:13.4px; color:#000000">
 </span>
</div>


</td>
</tr>
</table>








<div class="pos" id="_100:338" style="top:338;left:100">
<span id="_16.1" style=" font-family:Arial; font-size:16.1px; color:#000000">
To whom it may Concern,<br><br></span> 
</div>
<div class="pos" id="_100:379" style="top:379;left:100">
<span id="_16.6" style=" font-family:Arial; font-size:16.6px; color:#000000">
We confirm that below vehicle can be used for the carriage of passengers for hire and<span style=" font-family:Times New Roman"> </span></span>
</div>
<div class="pos" id="_100:398" style="top:398;left:100">
<span id="_16.6" style=" font-family:Arial; font-size:16.6px; color:#000000">
reward by prior appointment (private hire) as specified on our insurance policy with policy<br>
<span style=" font-family:Times New Roman"> </span></span> </div>
<div> 
<span id="_15.8" style=" font-family:Arial; font-size:15.8px; color:#000000">
number: {{$policy_number}}

 



</span>





</div>
<div class="pos" id="_100:459" style="top:459;left:100">
<span id="_16.5" style=" font-family:Arial; font-size:16.5px; color:#000000">
We authorize and give permission to the following individual to use the vehicle for <span style="font-style:italic"> all</span></span>
</div>
<div class="pos" id="_99:478" style="top:478;left:99">
<span id="_15.7" style=" font-family:Arial; font-size:15.7px; color:#000000">
private hire appointments, including any trips taken through the Uber, uber eats,bolt and Ola platforms :<br></span>
</div>
<div class="pos" id="_99:517" style="top:517;left:99">
<span id="_15.7" style=" font-family:Arial; font-size:15.7px; color:#000000">
<b>Vehicle Registration:</b> {{$registeration_no}}</span>
</div>
<div class="pos" id="_311:519" style="top:519;left:311">
<span id="_13.6" style=" font-family:Verdana; font-size:13.6px; color:#000000">
  </span>
</div>
<div class="pos" id="_99:537" style="top:537;left:99">
<span id="_16.3" style=" font-family:Arial; font-size:16.3px; color:#000000">
<b>Make and Model:</b> {{$make}} {{$model}}</span>
</div>
<div class="pos" id="_318:537" style="top:537;left:318">
<span id="_15.8" style=" font-family:Arial; font-size:15.8px; color:#000000">
</span>
</div>
<div class="pos" id="_99:577" style="top:577;left:99">
<span id="_15.8" style=" font-family:Arial; font-size:15.8px; color:#000000">
<b>Driver Name:</b> {{$driver_name}}</span>
</div>
<div class="pos" id="_318:577" style="top:577;left:318">
<span id="_16.3" style=" font-family:Arial; font-size:16.3px; color:#000000">
</span>
</div>
<div class="pos" id="_99:597" style="top:597;left:99">
<span id="_16.3" style=" font-family:Arial; font-size:16.3px; color:#000000">
<b>Address:</b> {{$address}}</span>
</div>
<div class="pos" id="_311:597" style="top:597;left:311">
<span id="_16.3" style=" font-family:Arial; font-size:16.3px; color:#000000">
</span>
</div>
<div class="pos" id="_99:658" style="top:658;left:99">
<span id="_16.3" style=" font-family:Arial; font-size:16.3px; color:#000000">
<b>Driving License Number:</b> {{$driving_licence_no}}</span>
</div>
<div class="pos" id="_318:658" style="top:658;left:318">
<span id="_13.8" style=" font-family:Verdana; font-size:13.8px; color:#000000">
</span>
</div>

<div class="pos" id="_99:697" style="top:697;left:99">
<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
Hire start date:{{$booking_date}}</span>
</div>
<div class="pos" id="_453:697" style="top:697;left:453">
<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
Hire end date: {{$expire_date}}</span>
</div>
<div class="pos" id="_100:737" style="top:737;left:100"> <br><br>
<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
Regards,<span style=" font-family:Times New Roman"> </span></span>
</div>
 
<div class="pos" id="_100:775" style="top:775;left:100">
<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
Signature:</span><br />
<img src="https://pcocar.com/public/theme3/assets/img/sign.png" /><p  id="_15.9" style="bottom:775;left:100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://pcocar.com/public/theme3/assets/img/awa.png" /></p>
</div>
<div class="pos" id="_100:863" style="top:863;left:100">
<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
Print Name:</span>
</div>


<div class="pos" id="_210:863" style="top:863;left:210">
<span id="_15.9" style=" font-family:Arial; font-size:15.9px; color:#000000">
UMER FAROOQ</span>
</div>
<div class="pos" id="_111:1035" style="top:1035;left:111">
<span id="_10.3" style="font-weight:bold; font-family:Verdana; font-size:10.3px; color:#000000">
Company registered in England & Wales number 11671887<span style=" font-family:Times New Roman"> </span></span>
</div>
<div class="pos" id="_111:1047" style="top:1047;left:111">
<span id="_10.3" style="font-weight:bold; font-family:Verdana; font-size:10.3px; color:#000000">
Registered office:23 Stanley Road, London, E4 7DB</span>
</div>
 

 
 
 
</nowrap></nobr>
 
     
</body>
</body>
</html>