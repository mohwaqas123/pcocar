@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
 
<div class="content-page" >
  <div class="content">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
          <div class="card-box">
            <h4>Previous Booking</h4>
            <p>Started: {{$booking->booking_start_date}} to {{$booking->booking_drop_off_date}} </p>
            <p>Duration: {{$booking->duration}}</p>
            <p>Rent:£{{$booking->price}} </p>
            <p>Registration:{{$vehicle->licence_plate_number}}  {{$vehicle->car_make->name}} {{$vehicle->car_model->name}} {{$vehicle->year}}</p>
             <!-- end row -->
          </div>
        </div>
        <!-- Second col of the dev are started -->
           <div class="col-md-6">
          <div class="card-box">
            <h4>Customer Detail</h4>
            <p>Name:  {{$booking->user->name}} </p>
            <p>Phone no:{{$booking->user->phone}} </p>
            <p>Address: {{$booking->address}}</p>
            <p>Licence: {{$booking->dvla}}</p>
        </div>
      </div>
        </div><!---upper two col are closed -->
      <!-- Form are started -->
      <div class="col-md-12">
        <div class="card-box" >
          <form action="{{route('renewbooking')}}"  method="post"  enctype="multipart/form-data" onsubmit='disableButton()'>
             <input type="hidden" name="p_price" id="p_price" value="{{$price}}" />
             @csrf
            <input type="hidden" name="id" value="{{$booking->id }}">
            @if($booking->insurance_type == 1)
            <input type="checkbox" name="vehicle" class="company_insurance" id="company_insurance" onclick="myFunction()" value="2"> Standard Fleet insurance &nbsp; 
            @else
            <input type="checkbox" id="company_insurance"  class="company_insurance_company" name="vehicle2" value="1"> Private insurance &nbsp; 
             @endif
             @if($booking->insurance_type == 0)
              <div id="company_insurance_private">
              <div class="row">
              <div class="col-md-6">Policy Number<input maxlength="50"    minlength="6" type="text" name="policy_number" id="pco_licence_no" class="form-control"     placeholder="Policy Number" />
                        </div> <div class="col-md-6">Cover Type<input maxlength="20"    minlength="6"   type="text" name="cover" id="pco_licence_no" class="form-control"     placeholder="Cover" />
                        </div>
                        <div class="col-md-6">Insurance Company Name <input        type="num" name="Insurance_company_name" id="pco_licence_no" class="form-control"     placeholder="Insurance Company Name" />
                        </div><div class="col-md-6">Insurance Company Email<input   type="email" name="Insurance_company_email" id="pco_licence_no" class="form-control"    placeholder="Insurance Company Email" />
                        </div><div class="col-md-6">Insurance Company Contact Number <input maxlength="20"    minlength="6"   type="number" name="Insurance_company_contact_number" id="pco_licence_no" class="form-control"   value="{{@session()->get('pco_licence_no')}}" placeholder="Insurance Company Contact" />
                        </div>
                        <div class="col-md-6">Customer's Private Insurance Certificate<input name="customer_private_insurance" id="pco_paper_licence" class="form-control form-control-lg" type="file" placeholder="PCO Licence No"  > <p class="driver-badge">Your image must be clearly shown</p>
                         </div>
              
            </div>


            <div class="row">
            <div class="col-md-3">
              Insurance Start Date <input maxlength="20" onchange="change_date()"  minlength="6"   type="date" name="Insurance_started_date" id="insurance_started_date" class="form-control" required="required" value="{{date('Y-m-d')}}"   />
              </div>
              <div class="col-md-2">
               <br>
               <select name="hr"  class="form-control" required="required" >
                               <option value="0">Hours</option>
                              <?php
                              for($i=0;$i<=23;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                          <br>
                          <select name="min"  class="form-control" required="required">
                            <option value="0">Minutes</option>
                              <?php
                              for($i=0;$i<=59;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="row">
              <div class="col-md-3">Insurance End Date <input maxlength="20" min="2021-04-27"    minlength="6"   type="date" name="Insurance_end_date"  value="{{date('Y-m-d')}}" id="insurance_end_date" class="form-control" required="required"  placeholder="Insurance End Date"  />
                        </div>
                        <div class="col-md-2">
                            <br>
                          <select name="ehr"  class="form-control" required="required" >
                               <option value="0">Hours</option>
                              <?php
                              for($i=0;$i<=23;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                          <br>
                          <select name="emin"  class="form-control" required="required">
                            <option value="0">Minutes</option>
                              <?php
                              for($i=0;$i<=59;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                


            </div>
            </div>
            @else
            @endif
            @if($booking->insurance_type == 1)
            <div id="insurance_hide">
              <div class="row">
              <div class="col-md-6">Policy Number<input maxlength="20"    minlength="6" type="text" name="policy_number" id="pco_licence_no" class="form-control"     placeholder="Policy Number" />
                        </div> <div class="col-md-6">Cover Type<input maxlength="20"    minlength="6"   type="text" name="cover" id="pco_licence_no" class="form-control"     placeholder="Cover" />
                        </div>
                        <div class="col-md-6">Insurance Company Name <input        type="num" name="Insurance_company_name" id="pco_licence_no" class="form-control"     placeholder="Insurance Company Name" />
                        </div><div class="col-md-6">Insurance Company Email<input   type="email" name="Insurance_company_email" id="pco_licence_no" class="form-control"    placeholder="Insurance Company Email" />
                        </div><div class="col-md-6">Insurance Company Contact Number <input maxlength="20"    minlength="6"   type="number" name="Insurance_company_contact_number" id="pco_licence_no" class="form-control"   value="{{@session()->get('pco_licence_no')}}" placeholder="Insurance Company Contact" />
                        </div>
                        <div class="col-md-6">Customer's Private Insurance Certificate<input name="customer_private_insurance" id="pco_paper_licence" class="form-control form-control-lg" type="file" placeholder="PCO Licence No"  > <p class="driver-badge">Your image must be clearly shown</p>
                         </div>
              
            </div>


            <div class="row">
            <div class="col-md-3">
              Insurance Start Date <input maxlength="20" onchange="change_date()"  minlength="6"   type="date" name="Insurance_started_date" id="insurance_started_date" class="form-control" required="required" value="{{date('Y-m-d')}}"   />
              </div>
              <div class="col-md-2">
               <br>
               <select name="hr"  class="form-control" required="required" >
                               <option value="0">Hours</option>
                              <?php
                              for($i=0;$i<=23;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                          <br>
                          <select name="min"  class="form-control" required="required">
                            <option value="0">Minutes</option>
                             <?php
                              for($i=0;$i<=59;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="row">
              <div class="col-md-3">Insurance End Date <input maxlength="20" min="2021-04-27"    minlength="6"   type="date" name="Insurance_end_date"  value="{{date('Y-m-d')}}" id="insurance_end_date" class="form-control" required="required"  placeholder="Insurance End Date"  />
                        </div>
                        <div class="col-md-2">
                            <br>
                          <select name="ehr"  class="form-control" required="required" >
                               <option value="0">Hours</option>
                              <?php
                              for($i=0;$i<=23;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                          <br>
                          <select name="emin"  class="form-control" required="required">
                            <option value="0">Minutes</option>
                              <?php
                              for($i=0;$i<=59;$i++){
                              echo "<option value='".$i."'>".$i."</option>";
                              }
                              ?>
                            </select>
                          </div>
                


            </div>
            </div>
            @else
            @endif
            
            <br>
              <label for="inputEmail">Duration</label>
            <select name="duration" id="duration" class="form-control" style="width: 311px;">
              <option   value="4">4 Weeks</option>
              <option   value="5">5 Weeks</option>
              <option   value="6">6 Weeks</option>
              <option   value="7">7 Weeks</option>
              <option   value="8">8 Weeks</option>
              <option   value="9">9 Weeks</option>
              <option   value="10">10 Weeks</option>
              <option   value="11">11 Weeks</option>
              <option   value="12">12 Weeks</option>
              <option   value="16">16 Weeks (5% Discount) </option>
              <option   value="20">20 Weeks (5% Discount)  </option>
              <option value="24">24 Weeks (10% Discount) </option>
            </select>
            <br>
            <label>Rent (Per Week)</label>
           <p>£<span id="final_price">{{$price}}</span>
             <span id="final_price_insurance" style="display: none;">+70</span></p>
          <div class="alert alert-success" role="alert">
              Thanks! Promo code  is applied
            </div>
            <div class="alert alert-danger" role="alert">
              Sorry! Promo Code is not valid, please try again
            </div>
            
            <div class="row" id="frm_discount">
              <div class="col-md-2">
                <input maxlength="6"   type="text" value="" placeholder="Promo Code" class="form-control " name="discountcode" id="discountcode"   />
              </div>
              <div class="col-md-2">
                <input type="button"  id="btn_app_discount" class="btn-sm btn-primary" value="Apply" />
                
              </div>
            </div>  <br>
              
             <!--  <input type="checkbox" name="insurance" id="insurance"> Private insurance &nbsp; 
              <br> -->
            <label>Total Per Week</label>
               @if($booking->insurance_type == 1)
               <?php 
               $total_insurance =  0;
               ?>
                @else
                <?php
                 $total_insurance =  70;
                ?>
                @endif
            <?php $total =   $price + $total_insurance ?>
            <p>£<span id="price_final_total">{{$total}}</span> </p>
            <button type="submit" id="btn" class="btn btn-primary">Submit Renew Booking</button>

            <input type="hidden" name="booking_cost" id="booking_cost" value="{{$price}}" />
            <input type="hidden" name="renew_price_per_week" id="renew_price_per_week" value="{{$total}}" />
            <input type="hidden" name="total_duration" value="4" id="total_duration" value="" />
          </form>
        </div>
      </div>

    </div>
  </div>
</div>







@endsection
@section('scripts')

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>



 
function myFunction() {

  var x = document.getElementById("#final_price_insurance");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

$(".company_insurance").change(function() {
    var insurance = {{env('INSURANCE_CODE') }};
    if(this.checked) {
         
         final_price_tot_insurance = parseInt($('#final_price').text()) + parseInt(insurance);
         $("#renew_price_per_week").val(final_price_tot_insurance);
         $("#price_final_total").text(final_price_tot_insurance);
           // document.getElementById("btn").disabled = false;

          // $("#btn").enable();

    }
    else{
     final_price_tot_insurance = parseInt($('#final_price').text())  ;
         $("#renew_price_per_week").val(final_price_tot_insurance);
         $("#price_final_total").text(final_price_tot_insurance);
    }
});
$(".company_insurance_company").change(function() {
    var insurance = {{env('INSURANCE_CODE') }};
    if(this.checked) {
       
       final_price_tot_insurance = parseInt($('#final_price').text())  ;
         $("#renew_price_per_week").val(final_price_tot_insurance);
         $("#price_final_total").text(final_price_tot_insurance);
    

    }
    else{
      final_price_tot_insurance = parseInt($('#final_price').text()) + parseInt(insurance);
         $("#renew_price_per_week").val(final_price_tot_insurance);
         $("#price_final_total").text(final_price_tot_insurance);
    }
});



$(function(){

  var insurance_cost = {{env('INSURANCE_CODE') }}
// $(".insurance_hide").hide();
$('input[name=vehicle]').on('change', function(){
  // $('#final_price_insurance').hide();
 
if(this.checked){

  $('#insurance_hide').hide();
  $('#final_price_insurance').show();
  // $('#company_insurance_private').show(); 

}
else
{
  $('#insurance_hide').show();
  $('#final_price_insurance').hide();
  // $('#company_insurance_private').hide();  
 }
});
});
$(function(){

  var insurance_cost = {{env('INSURANCE_CODE') }}
  $('#company_insurance_private').hide(); 

$('input[name=vehicle2]').on('change', function(){
  // $('#final_price_insurance').hide();
 
if(this.checked){
$('#company_insurance_private').show(); 
}
else
{
  $('#company_insurance_private').hide(); 
 }
});
});

  function disableButton() {
        var btn = document.getElementById('btn');
        btn.disabled = true;
        btn.innerText = 'Posting...'
    }
$(document).ready(function() {
$('#btn_app_discount').click(function(event) {
if($('#discountcode').val()=='')
$('.alert-danger').show();

var url_call = "{!! url('/') !!}/promotion_apply";
var discountcode = $('#discountcode').val();
$.ajax({
type : 'get',
url  : url_call,
data: { discountcode:discountcode },
//    data: { ticket_id: ticket_id, status_value: status_value },
contentType: 'application/json; charset=utf-8',
success :  function(data){
if(data==2){
$('.alert-danger').show();
$('.alert-success').hide();
$('#frm_discount').show();
} else {
$('.alert-success').show();
$('.alert-danger').hide();
$('#frm_discount').hide();
$('#btn_app_discount').hide();
$("#duration").prop("disabled", true);
// $("#company_insurance").prop("disabled", true);
final_price_tot = $('#final_price').text();
var  disct  = final_price_tot -  ((final_price_tot * data) / 100);
var  insurance  = 70;
$("#final_price").text(parseFloat(disct).toFixed(2));
$("#booking_cost").val(parseFloat(disct).toFixed(2));
 
 
  if($('input[name="vehicle"]').is(':checked') )  //private insurance
  {
   
    final_price_tot_insurance = parseInt($('#final_price').text()) + insurance;
  }
  else
  {
   

  final_price_tot_insurance = parseInt($('#final_price').text()) ;
  }
if ($('#company_insurance').val() =='1') {
 
 if($('input[name="vehicle2"]').is(':checked') )  //company insurance
  {
 

    final_price_tot_insurance = parseInt($('#final_price').text()) ;
  }
  else
  {
  

    final_price_tot_insurance = parseInt($('#final_price').text()) + insurance ;
  }

}
else
{

}


//==============================================================================================
 

// alert(final_price_tot_insurance);
$("#price_final_total").text(final_price_tot_insurance);
$("#renew_price_per_week").val(final_price_tot_insurance);
}






}



});//ajax

});
//============detail page on change of duration booking
$('#duration').change(
function(){
calculate_booking();

});
function calculate_booking(){
var url_call = "{!! url('/') !!}/calculate_renewbooking";
var duration = $('#duration').val();
var vehicle_id =3;// $('#vehicle_id').val();
var discount =2;// $('#discount').val();
//$('#weeks').val( $('#month').val() );
var price =  $('#p_price').val();
$('#total_duration').val($('#duration').val() );

$.ajax({
type : 'get',
url  : url_call,
data: {duration:duration, vehicle_id:vehicle_id ,price:price, discount:discount},
contentType: 'application/json; charset=utf-8',
success :  function(data){
$('#final_price').text(data);
$("#booking_cost").val(data);
//  $("#booking_cal").empty();
//  $("#booking_cal").append(data);
final_price_tot = $('#final_price').text();

final_price_tot_insurance = parseInt(final_price_tot) + parseInt({{env('INSURANCE_CODE')}} );
//alert(final_price_tot_insurance);
$("#price_final_total").text(final_price_tot_insurance);
$("#renew_price_per_week").val(final_price_tot_insurance);
},
error: function() {
alert(data);
}
});//ajax
}
//=================================================================
});

</script>
<style>

.alert-success{display: none;}
.alert-danger{display: none;}
body{padding:0px!important;}
</style>
@endsection
