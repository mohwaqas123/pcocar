<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Accidental_records_comments extends Model
{
	protected $table = 'accidental_records_comments';
 	protected $fillable = [
        'vehicle_id','booking_id','accidental_id','user_id','comments','user_type','attachment'
 	 ];



 	  public function vehicle()
    {
        return $this->belongsTo('App\models\Vehicle','vehicle_id','id');
    }

public function user()
    {
        return $this->belongsTo('App\models\User','user_id','id');
    }
    public function accidental()
    {
        return $this->belongsTo('App\models\Accedental_records','accidental_id','id');
    }
    public function make()
{
    return $this->hasManyThrough('App\models\Make', 'App\models\Vehicle' , 'vehicle_id','id');
}  



}
