-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 01, 2020 at 02:14 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_c`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$E8kbSGHAbxtuEr7OLYxX8emgRD6m/KQrh0KeqZ6ZzKeSp5Wy2iV4a', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `miles` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dvla` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `national_insu_numb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pco_licence_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_dvla` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_cnic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_utility` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_other` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` tinyint(1) DEFAULT NULL COMMENT '0=not,1=done',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `created_at`, `updated_at`, `user_id`, `vehicle_id`, `duration`, `miles`, `address`, `dob`, `dvla`, `national_insu_numb`, `pco_licence_no`, `doc_dvla`, `doc_cnic`, `doc_utility`, `doc_other`, `payment_status`, `deleted_at`) VALUES
(1, '2020-08-30 10:57:49', '2020-08-30 10:57:49', 1, 2, 1, 800, 'test', '23423', 'asdfa', 'sdfa', 'sdfasdfa', 'D:\\xampp\\tmp\\php5044.tmp', 'D:\\xampp\\tmp\\php5055.tmp', 'D:\\xampp\\tmp\\php5065.tmp', 'D:\\xampp\\tmp\\php5076.tmp', NULL, NULL),
(2, '2020-08-30 11:05:33', '2020-08-30 11:05:33', 1, 2, 1, 800, 'test', '23423', 'asdfa', 'sdfa', 'sdfasdfa', 'D:\\xampp\\tmp\\php630D.tmp', 'D:\\xampp\\tmp\\php632D.tmp', 'D:\\xampp\\tmp\\php632E.tmp', 'D:\\xampp\\tmp\\php632F.tmp', NULL, NULL),
(3, '2020-09-01 02:09:08', '2020-09-01 02:09:08', 1, 2, 1, 800, 'asdf', 'asdfas', 'dfasd', 'sdfa', 'fasdfa', 'D:\\new2\\tmp\\php5EF4.tmp', 'D:\\new2\\tmp\\php5EF5.tmp', 'D:\\new2\\tmp\\php5F16.tmp', 'D:\\new2\\tmp\\php5F17.tmp', NULL, NULL),
(4, '2020-09-01 02:54:18', '2020-09-01 02:54:18', 1, 2, 3, 800, 'asdf', 'asdfasdf', 'asdfasdfas', 'dfasdfasdfasd', 'fasdf', 'documents_uploads/xREfsf4Y0a3vRqNUOicME3qnm9ntX3WgrcV3toRU.jpeg', 'documents_uploads/pglqn5rl6kVdY4tFAcG7DT11Em39v28fhRWRx5ht.jpeg', 'documents_uploads/KkHQg5s70J3CMshbc5WDhN6Yw0VGVIV6kp4HcO7j.jpeg', 'documents_uploads/d2UBsgaOGY6YIIS1iNJvTai5EJlCEpWSPGxCZM3T.jpeg', NULL, NULL),
(5, '2020-09-01 03:52:31', '2020-09-01 03:52:31', 1, 2, 4, 1000, 'asdf', 'asdfasdfa', 'sdfasdfa', 'sdfa', 'sdfa', 'documents_uploads/klgzx7qtruyb50OKdoxjb9PmcZKiPzAv7brAGw1m.jpeg', 'documents_uploads/a2cpg53nd0mQ4DJT6ICU2jCnMtI01mZ605B3n1lM.jpeg', 'documents_uploads/aEuiVylstE4ayjWctBDTxem6Cf3M19khcXWoEgTE.jpeg', 'documents_uploads/Yt0chQD6sK2fTnCXj1UCnY5eqYQ9qYI8LWmZNY41.jpeg', NULL, NULL),
(6, '2020-09-01 03:54:13', '2020-09-01 03:54:13', 1, 2, 4, 1000, 'asd', 'fasdf', 'asdfa', 'dfas', 'sdfas', 'documents_uploads/gyVR9XivVVuB7yQYAQdxUSCanx0ZyQtgmQzAsGO8.jpeg', 'documents_uploads/oPMj6yLQO1q2tQqCsDMPJTEHMS3ZjiSie4gjbVAA.jpeg', 'documents_uploads/RXPzU3AhuIA6mNyPFE6XUwZv7DaxUghrNGq219Ax.jpeg', 'documents_uploads/JmRdX0OLkyINUwocEhBlQvOwQ03NkKvFhskMLBXj.jpeg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_models`
--

CREATE TABLE `car_models` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `make_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `car_models`
--

INSERT INTO `car_models` (`id`, `name`, `status`, `make_id`, `created_at`, `updated_at`) VALUES
(1, 'A4', 1, 3, '2020-08-30 08:58:16', '2020-08-30 08:58:16'),
(2, '4 Series', 1, 1, '2020-08-30 08:58:53', '2020-08-30 08:58:53'),
(3, '5 Series', 1, 1, '2020-08-30 08:59:05', '2020-08-30 08:59:05'),
(4, 'Leaf', 1, 2, '2020-08-30 08:59:29', '2020-08-30 08:59:29'),
(5, 'ProCeed', 1, 4, '2020-08-30 09:00:03', '2020-08-30 09:00:03'),
(6, 'xCeed', 1, 4, '2020-08-30 09:00:23', '2020-08-30 09:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `makes`
--

CREATE TABLE `makes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `makes`
--

INSERT INTO `makes` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'BMW', '2020-08-30 08:56:49', '2020-08-30 08:56:49'),
(2, 'Nissan', '2020-08-30 08:57:00', '2020-08-30 08:57:00'),
(3, 'Audi', '2020-08-30 08:57:45', '2020-08-30 08:57:45'),
(4, 'Kia', '2020-08-30 08:57:57', '2020-08-30 08:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2014_10_12_000000_create_users_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2020_06_10_043839_create_reset_passwords_table', 1),
(15, '2020_07_13_095112_create_admins_table', 1),
(16, '2020_07_15_091914_create_categories_table', 1),
(17, '2020_07_15_092036_create_banners_table', 1),
(18, '2020_08_18_170208_create_vehicles_table', 1),
(19, '2020_08_18_170655_create_vehicle_images_table', 1),
(20, '2020_08_23_093820_create_makes_table', 1),
(21, '2020_08_23_094741_create_car_models_table', 1),
(22, '2020_08_23_095446_create_years_table', 1),
(23, '2020_08_30_134943_create_todos_table', 1),
(24, '2020_08_30_135026_create_bookings_table', 1),
(25, '2020_04_30_052008_create_payment_histories_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `payment_histories`
--

CREATE TABLE `payment_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` decimal(9,2) NOT NULL,
  `transaction_id` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_id` int(11) NOT NULL,
  `month_duration` int(11) NOT NULL,
  `per_month_amount` decimal(9,2) NOT NULL,
  `expire_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_histories`
--

INSERT INTO `payment_histories` (`id`, `user_id`, `amount`, `transaction_id`, `vehicle_id`, `month_duration`, `per_month_amount`, `expire_date`, `created_at`, `updated_at`) VALUES
(1, 1, '50.00', 'ch_1HMVIPHONfFiV5aLp86X5Mqb', 2, 4, '50.00', '2021-01-01', '2020-09-01 03:54:29', '2020-09-01 03:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `reset_passwords`
--

CREATE TABLE `reset_passwords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE `todos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_firebase_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `OTP` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` tinyint(4) NOT NULL DEFAULT 0,
  `is_registered` tinyint(4) NOT NULL DEFAULT 0,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` tinyint(4) DEFAULT NULL COMMENT '1=Android,2=IOS,3=Admin',
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` tinyint(4) NOT NULL COMMENT '1=Admin,2=enduser',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `make` int(11) NOT NULL,
  `model` int(11) NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fuel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `offer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_Passengers` int(11) NOT NULL,
  `engine_Capacity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `toll_Charges` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `garebox` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body_type` tinyint(4) DEFAULT NULL COMMENT '1=MPV',
  `vehicle_category` tinyint(1) NOT NULL COMMENT '0=pco,1=private',
  `vehicle_type` tinyint(1) NOT NULL COMMENT '0=family,1=premium',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `name`, `make`, `model`, `year`, `fuel`, `price`, `offer`, `no_of_Passengers`, `engine_Capacity`, `toll_Charges`, `description`, `garebox`, `body_type`, `vehicle_category`, `vehicle_type`, `image`, `created_at`, `updated_at`) VALUES
(1, 'ProCeed GT Line - 1.4', 4, 5, '2015', '0', 5600.00, '0', 1, '1500cc', 20, 'asdf asdfasd fasdf asdf asdfasd', NULL, NULL, 0, 1, 'vehicleImages/5JJy1dndtjplNQJF7o3ajjjcPVHdh37mZU1BzB83.png', '2020-08-30 09:13:55', '2020-08-30 09:13:55'),
(2, 'Test', 1, 1, '2015', '1', 2000.00, '0', 2, '1500cc', 20, 'asd fasd fasdfa', NULL, NULL, 0, 1, 'vehicleImages/a7qlOzAznJaB50INFowP9Yj46vhcwAhbKx0VBJDI.png', '2020-08-30 09:14:27', '2020-08-30 09:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_images`
--

CREATE TABLE `vehicle_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `vehicle_id` bigint(20) UNSIGNED NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `year` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `banners_name_unique` (`name`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_models`
--
ALTER TABLE `car_models`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `car_models_name_unique` (`name`),
  ADD KEY `car_models_make_id_foreign` (`make_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `makes`
--
ALTER TABLE `makes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `makes_name_unique` (`name`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_histories`
--
ALTER TABLE `payment_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reset_passwords`
--
ALTER TABLE `reset_passwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_user_firebase_id_unique` (`user_firebase_id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_images`
--
ALTER TABLE `vehicle_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicle_images_vehicle_id_index` (`vehicle_id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `years_year_unique` (`year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `car_models`
--
ALTER TABLE `car_models`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `makes`
--
ALTER TABLE `makes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `payment_histories`
--
ALTER TABLE `payment_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reset_passwords`
--
ALTER TABLE `reset_passwords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vehicle_images`
--
ALTER TABLE `vehicle_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `car_models`
--
ALTER TABLE `car_models`
  ADD CONSTRAINT `car_models_make_id_foreign` FOREIGN KEY (`make_id`) REFERENCES `makes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicle_images`
--
ALTER TABLE `vehicle_images`
  ADD CONSTRAINT `vehicle_images_vehicle_id_foreign` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
