@extends('layouts.partner')

@section('styles')

@endsection

@section('content')
<div >
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        

                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            


                            <!-- <table id="songsListTable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Vehincle Detail</th>
                                    <th>description</th>
                                    <th>Thumbnail</th>
                                    <th>Action</th>
                                </tr>
                                </thead>  -->
                                
<!-- Datatable -->
 
 <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />


@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif




<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                                     <th>ID</th>
                                    <th>Vehicle Details</th>
                                    <th>Type</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Discounted Price</th>
                                     
                                    <!-- <th>Created </th> -->
                                    <th>Action</th>
            </tr>
        </thead> 


     
                                <tbody>
                                    @foreach ($vehicles as $vehicle)
                                        <tr>
                                            <td>{{ $vehicle->id }}</td>
                                            <td>{{ $vehicle->licence_plate_number }},  

                                                {{ @$vehicle->car_make->name }}, {{  @$vehicle->car_model->name }}, 

                                                {{ $vehicle->year }}
                                                {{ $vehicle->colour }} </td>
    <td> 
    @if ($vehicle-> fuel_type == 0)
    petrol
    @elseif ($vehicle-> fuel_type == 4)
    Hybrid
    @elseif ($vehicle-> fuel_type == 3)
    Electric     
    @else 
    Electric Hybrid
    @endif
    </td> 
                                            <td>@if($vehicle->vehicle_category==0) PCO @else  Private @endif</td>
 
                                            
                                            <td>£{{ $vehicle->price }}</td>
                                            <?php
                                             $dis_price =  $vehicle->price / 2;
                                            ?>
                                            <td>£{{ $dis_price }}</td>
                                             
                                                 <!-- <td>{{ $vehicle->created_at }}</td> -->
                                            <td style="width: 15%;">
                                               <a href="{{ route('p_vehicleEdit', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="fa fa-pencil"></i></a>  
                                                <a href="{{ route('p_vehicledestory', $vehicle->id) }}" class="btn btn-sm btn-icon waves-effect waves-light btn-danger " onclick="return confirm(' Do You want to remove your selected vehicle?')"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
         {!! $vehicles->links() !!}                     
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
