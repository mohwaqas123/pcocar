<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>{{ $title }}</title>
<style type="text/css">

	@import url('https://fonts.googleapis.com/css?family=Tangerine');

/* 	@font-face {
  font-family: 'dancingscript';
  font-style: normal;
  font-weight: bold;
  src: url(https://fonts.googleapis.com/css?family=Rancho&effect=shadow-multiple) format('truetype');
}
 */

 </style>  
 <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
</head>
<body>
  
 
          <div>
   <h2 align="center">Subscription Hire Agreement</h2>     
<ul>
  
<p style="font-weight: bold;"> Date:{{date('d-m-Y H:i:s', strtotime($start_date))}}</p>
This Short Term Subscription Agreement (which comprises this Subscription Booking Form and Intercity Hire’s Terms of Subscription displayed below) ("<b>Subscription Agreement</b>") is entered into between {{$vehicle->company_name}} (the "Company") of&nbsp; <b>{{$vehicle->company_address}}</b> and {{$signature_name}} (the "Driver" ) of {{$booking->address}}, (collectively the "<b>Parties</b>" ). Intercity Hire operates the site www.pcocar.com on which the Vehicle to which the Driver wishes to subscribe is offered for subscription.<br>

The Driver subscribes to use the Vehicle supplied by Intercity Hire for the Subscription Fee during the Subscription Term (as defined below) (which may be amended if agreed between the Parties in writing in accordance with this Subscription Agreement), such arrangement being the "Subscription".<br>

<br>
  <b>1. IDENTIFICATION OF THE VEHICLE AND NAMED DRIVERS</b>
      <p>Intercity Hire agrees to provide to the Driver the following or similar vehicle (the "Vehicle"):</p>
      <p>Make:  {{$vehicle->car_make->name}}</p>
      <p>Model:  {{$vehicle->car_model->name}}</p>
      <p>Registration Plate: {{$vehicle->licence_plate_number}}</p>
 
  <b>2.  Only the Driver and the following individuals can drive the Vehicle during the Subscription: </b>
     <p>&nbsp;&nbsp;&nbsp;&nbsp;2.1.Name: {{$signature_name}}
    <ul>D.O.B: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     {{date('d-m-Y H:i:s', strtotime($booking->dob))}}</ul><br>
    <ul>D.V.L.A:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{$booking->dvla}}</ul><br>
    <ul>Issuing Authority: Driver and Vehicle Licensing Authority England</ul><br>
     @if($vehicle->vehicle_category == 1)
    {
    <ul>PCO Licence No:&nbsp;{{$booking->pco_licence_no}}</ul><br>
    }
    @else
    @endif
    <ul>D.V.L.A Expiry Date:&nbsp;{{$booking->expiredate}}</ul>

       29/01/2025 UK (Driver)</p> 
<br> 
 <b>3. SUBSCRIPTION TERM: </b>
     <p>The term of the Subscription will run for 6 months from the Subscription Start Date (unless cancelled by agreement of the Parties according to the Earliest Subscription End Date or cancelled in accordance with this Subscription Agreement):</p> 
     <p>Subscription Start Date:  Date:{{date('d-m-Y H:i:s', strtotime($start_date))}}</p>
     <p>Earliest Subscription End Date: WEEKLY renewal until 

                            <?php
                            $date_p = $booking->booking_drop_off_date;//date('Y-m-d');
                            $ldate2 = date('d-m-Y', strtotime("$date_p". " +7 months"));
                            ?>


      {{date('d-m-Y H:i:s', strtotime($ldate2))}},



       unless cancelled</p>

 <p>4.  The time from which the Driver collects the Vehicle to the point the Driver returns the Vehicle is the "Subscription Term".</p>
<!-- <br> <b>5.SUBSCRIPTION FEE: </b>
     <p>The Driver agrees to pay Intercity Hire the following fee for the Subscription (the "Subscription Fee"):</p> 

      @if($booking->insurance_type == 1)
      <p>Subscription Price:  £{{$booking->price }} "Weekly Subscription Rate"</p>
     <p>Frequency of payment: WEEKLY</p>
     @else
     <p>Subscription Price [incl insurance]:  £{{$booking->price }} "Weekly Subscription Rate"</p>
     <p>Frequency of payment: WEEKLY</p>
     @endif -->
<p><strong>TERMS OF SUBSCRIPTION</strong></p>
<p>&nbsp;</p>
<ol>
<li><strong>Subscription</strong></li>
</ol>
<p>&nbsp;</p>
<p>1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Hire Agreement is made between Intercity Hire and the Driver is constituted by the front pages (which we refer to as the "Subscription Booking Form") and these Terms of Subscription which form part of and are incorporated within this Hire Agreement made between Intercity Hire and the Driver. Any capitalised terms that are used but not defined in these Terms shall have the meaning given to them in the Subscription Booking Form</p>
<p>&nbsp;</p>
<p>1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver agrees to hire the Vehicle referred to in the Subscription Booking Form for the Subscription Term and any further period as provided for in this Agreement. The Vehicle is provided by Intercity Hire at the relevant Subscription Fee stated in the Subscription Booking Form. If the Driver does not return the Vehicle to Intercity Hire by the Subscription End Date (or such extended date as provided for in this Agreement), the Driver will be in breach of the conditions of this Agreement. For the avoidance of doubt, the Driver will not own the Vehicle.</p>
<p>&nbsp;</p>
<p>1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire may substitute the Vehicle for another vehicle (of a similar age, quality and specification) on at least 7 days&rsquo; notice at any point during this Agreement. Intercity Hire may also substitute the Vehicle for another vehicle in the event of a manufacturer&rsquo;s recall, or for other legitimate mechanical, health or safety purposes. The Driver will cooperate with the reasonable requirements of Intercity Hire as to substitution of the Vehicle and, following substitution, the new Vehicle will be the &ldquo;Vehicle&rdquo; for the purpose of this Agreement.</p>
<p>&nbsp;</p>
<ol start="2">
<li><strong>Responsibilities of the Driver</strong></li>
</ol>
<p>&nbsp;</p>
<p>2.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire, or a third party on behalf of Intercity Hire, will deliver the Vehicle to an address specified by the Driver on the Subscription Start Date and collect it at its end (unless alternative arrangements are agreed by Intercity Hire and the Driver in writing). The Driver must inspect the Vehicle before the start of the Subscription Term. The Driver will make Intercity Hire aware of any defects in the Vehicle at the time when the Vehicle is delivered. In the absence of the Driver notifying Intercity Hire of any issues at the time when the Vehicle is received by the Driver, it shall be deemed that the Driver received the Vehicle in perfect working order (save for any existing damage recorded in the Vehicle Condition Report which the Driver will sign on delivery). The Driver will return the Vehicle to Intercity Hire in the same condition in which the Driver received it, save for the normal wear and tear in relation to distance travelled, including (but not limited to) tyres, fittings, documents and complete equipment outfit. The Driver shall return the Vehicle with no less than the amount of fuel that it had on delivery or pay Intercity Hire for the cost of the shortfall in fuel as compared to the fuel in the Vehicle upon delivery. The Driver must take care of, and keep secure, the Vehicle and its keys. The Driver must always lock the Vehicle when not using it (and will be charged for the replacement cost of any lost keys). The Driver must make sure that they use the correct fuel. The Driver is also responsible for tyre punctures.</p>
<p>&nbsp;</p>
<p>2.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Intercity Hire has the right to request and conduct a Vehicle inspection during the Subscription Term, subject to giving the Driver 7 days&rsquo; prior notice. The Driver shall act in good faith and reasonably comply with such a request.</p>
<p>&nbsp;</p>
<p>2.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must not sell, rent or dispose of the Vehicle or any of its parts. The Driver must not give anyone other than Intercity Hire any legal rights over the Vehicle.</p>
<p>&nbsp;</p>
<p>2.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must not (and must not let anyone else) modify or work on or attach or affix anything to the Vehicle without Intercity Hire&rsquo;s written permission. Any additions, alterations or modified parts fitted without such permission shall become part of the Vehicle and shall belong to Intercity Hire, and the Driver shall be responsible for any costs in returning the Vehicle to its pre-modified condition.</p>
<p>&nbsp;</p>
<p>2.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must let Intercity Hire know as soon as they become aware of any defect(s) in or damage to the Vehicle at any point if or when the Driver discovers any issues or defects during the Subscription Term.</p>
<p>&nbsp;</p>
<p>2.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver shall be liable for damage to, and theft of, the Vehicle from delivery of the Vehicle until Intercity Hire has collected the Vehicle from the Driver at the end of the Subscription Term. The Driver must meet with the collection manager at the Subscription End Date or such extended date as provided for in this Agreement (or as otherwise agreed between the Parties in writing and in accordance with this Agreement). Intercity Hire, or a third party on behalf of Intercity Hire must inspect the Vehicle to check that it is in good condition. Where the Driver has requested a collection of the Vehicle by Intercity Hire at a specified</p>
<p>&nbsp;</p>
 
<p>location, outside of the hours of 09:00 to 18:00 Monday to Saturday, the Subscription Term (and the Driver&rsquo;s liability for damage, theft and parking violations) shall extend to the earlier of: midday of the first working day following the requested collection time or the time of re-inspection by Intercity Hire; or of a third party on behalf of Intercity Hire.</p>
<p>&nbsp;</p>
<p>2.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver will be liable for repairs (which must be carried out by a repair centre approved by Intercity Hire) if the Vehicle needs more than Intercity Hire&rsquo;s standard valeting (cleaning), or if the Vehicle has been damaged either inside or outside (whether or not it is the Driver&rsquo;s fault). Any insurance coverage may cover such repairs and charges but in the event it does not, the Driver will be separately charged by Intercity Hire for any such repairs and charges. The Driver will be charged by Intercity Hire (as payment collection agent for the Company) using the bank details supplied by Driver to Intercity Hire for any such repairs and charges.</p>
<p>&nbsp;</p>
<p>2.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must check before they bring back the Vehicle that they have not left any belongings in the Vehicle. Intercity Hire is not liable for any personal belongings left in the Vehicle after the Vehicle has been returned.</p>
<p>&nbsp;</p>
<p>2.9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver will be liable for any Excess Mileage Charge as set out in the Subscription Booking Form if the Driver exceeds the Mileage Allowance during the Subscription Term. On any substitution of a Vehicle pursuant to condition 1.3, the Mileage Allowance and any Excess Mileage Charge shall be recalculated by Intercity Hire by reference to the distance in mileage travelled by the substitute Vehicle at the date of substitution.</p>
<p>&nbsp;</p>
<p>2.10&nbsp;&nbsp;&nbsp;&nbsp; The Driver shall comply with its insurance obligations, as set out in condition 7 below.</p>
<p>&nbsp;</p>
<p>2.11&nbsp;&nbsp;&nbsp;&nbsp; If the Driver wishes to alter the collection or drop-off arrangements, they must obtain prior consent from Intercity Hire for such alternative arrangements.</p>
<p>&nbsp;</p>

<ol start="4">
<li><strong>Liability</strong></li>
</ol>
<p>&nbsp;</p>
<p>4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Subject to condition 4.2 below, Intercity Hire&rsquo;s maximum aggregate liability to the Driver for any and all claims arising out of or in connection with this Agreement shall in no event exceed &pound;10,000.</p>
<p>&nbsp;</p>
<p>4.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nothing in this Agreement excludes or restricts Intercity Hire&rsquo;s liability to the Driver for death or personal injury caused by Intercity Hire&rsquo;s negligence, for fraud and fraudulent misrepresentation, or for any other liability that cannot be excluded or limited under applicable law.</p>
<p>&nbsp;</p>
<p>4.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Notwithstanding condition 4.1 above, Intercity Hire shall not be liable for:</p>
<p>&nbsp;</p>
<p>4.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; indirect losses which happen as a side effect of the main loss or damage and which are not reasonably foreseeable by Intercity Hire and the Driver at the time of entering into this Agreement (such as loss of profits, income or loss of opportunity);</p>
<p>&nbsp;</p>
<p>4.3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; losses not caused by Intercity Hire&rsquo;s breach; or</p>
<p>&nbsp;</p>
<p>4.3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; failure or delay in performing any or all of its obligations under this Agreement, where such failure is caused, directly or indirectly, by events beyond Intercity Hire&rsquo;s reasonable control (including, but not limited to, network failure, fire, flood, earthquake, acts of God, acts of war, terrorism, riots, civil disorders, blockades, insurrections, pandemics, epidemics, any law or action taken by a government or public authority, any labour or trade disputes, strikes, industrial action or lockouts, or non-performance by suppliers or subcontractors).</p>
<p>&nbsp;</p>

<ol start="6">
<li><strong>Subscription Fees, Fines, Tolls and other charges</strong></li>
</ol>
<p>&nbsp;</p>
<p>6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver must pay the Initial Payment and the Subscription Fees at the rate and on the dates specified in the Subscription Booking Form. The Driver must obtain the prior written consent of Intercity Hire to change the Payment Date to a to a day in the month other than the Payment Date specified in the Subscription Booking Form. The Driver shall be liable for the following charges and pay them on demand during the Subscription Term:</p>
<p>&nbsp;</p>
<p>6.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; all charges which are payable after the discovery of damage following re-inspection of the Vehicle when returned by the Driver to Intercity Hire;</p>
<p>&nbsp;</p>
<p>6.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; all charges, fines and court costs including congestion charges, parking, traffic, speeding or other offences, and any civil penalty payable relating to the Vehicle (&ldquo;Fines&rdquo;) and any tolls, fees or charges including toll road fees, and the London Congestion Charge (&ldquo;Tolls&rdquo;). The Driver must also pay to the appropriate authority any Fines, Tolls and costs if and when the relevant authority demands this payment, and acknowledges that such obligations may be communicated to the Driver directly, through Intercity Hire or the relevant authority;</p>
<p>&nbsp;</p>
<p>6.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; value added tax and all other taxes and levies on any of the Fines, Tolls and charges, as appropriate including an administration fee for each Fine, Toll or charge that the Driver incurs during the term of their subscription;</p>
<p>&nbsp;</p>
<p>6.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver is liable to pay all road penalties which may includes;</p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Penalty Charge Notices.</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Notice to owner/Enforcement Notice</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Charge Certificate</b></p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Order of Recoveries</b></p>
<p>&nbsp;</p>
<p>6.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any reasonably incurred, foreseeable losses, costs and charges resulting from the breach by the Driver of this Agreement (such losses being foreseeable where they are contemplated by</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Intercity Hire and the Driver at the time this Agreement is entered into);</p>
<p>&nbsp;</p>
<p>6.1.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any other charges arising under this Agreement; and</p>
<p>&nbsp;</p>
<p>6.1.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any charges in relation to the repossession of the Vehicle, including, without limitation, third party agent costs, transportation required for the repossession of the Vehicle, legal proceedings in relation to the repossession of the Vehicle and any other costs, charges and expenses in relation to or in connection with the breach of this Agreement by the Driver.</p>
<p>&nbsp;</p>
<p>6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; For the avoidance of doubt, the Driver shall be liable for any Fines, Tolls and other charges, issued by public authorities or private parking companies, incurred during the Subscription Term even if such Fines, Tolls and other charges are not discovered until after the end of the Subscription Term.</p>
<p>&nbsp;</p>
<p>6.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; It is the responsibility of the Driver to pay the relevant authorities directly for any Fines, Tolls and other charges that the Vehicle or the Driver incurs during the Subscription Term. The Driver must provide a written report of any offences committed by him or her to Intercity Hire. In the case of any Fine, the Driver acknowledges and agrees that Intercity Hire may pass on the Driver&rsquo;s details to the police or relevant authority, who may then contact the Driver directly. Intercity Hire is not liable for any escalation in value of a Fine, Toll or charge as a result of it being delivered to an out-of-date address. It is the Driver&rsquo;s responsibility to inform Intercity Hire of any change of address so that Fines may be delivered to them in sufficient time to prevent escalation. In the event that Intercity Hire incurs a fine, charge or admin fee levied by a third party as a result of the Driver&rsquo;s incurring of a Fine, charge or admin fee. Intercity Hire retains the right to charge the cost of such a fine, charge or admin fee to the Driver. In the event that the Driver does not pay the Fines, Tolls or other charges, Intercity Hire may pay such Fines, Tolls or other charges and then reclaim such Fines, Tolls or other charges from the Driver if it would be in Intercity Hire&rsquo;s interests to do so, including, without limitation, where the Vehicle may be at risk and/or there may be other enforcement in relation to unpaid Fines, Tolls or other charges.</p>
<p>&nbsp;</p>
<p>6.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Driver shall keep the Vehicle in good and roadworthy condition (fair wear and tear excepted) throughout the Subscription Term and return the Vehicle in good and roadworthy condition to Intercity Hire at the end of the Subscription Term.</p>
<p>&nbsp;</p>
<p>6.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Driver or any other person smoke inside the Vehicle in contravention of condition 5.3.4 above, the Driver shall be liable to pay Intercity Hire a penalty of &pound;150.</p>
<p>&nbsp;</p>
<p>6.6&nbsp;&nbsp;&nbsp;&nbsp; All charges including rental, insurance, congestion&nbsp; and all other toll charges must be paid on time.</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rental plus insurance must be paid within 24 hours of rental date unless pre-notified for extension or any missing payment. Congestion or any toll charges must be cleared in 7 working days from day of notification. Any delay will be considered towards breach of contract &amp; could incur any extra charges against all late payments as late processing fee. Missing 2 rental payments without pre-notification, would allow company to repossess vehicle and recover all outstanding payment through debt collection agency with an additional charge of &pound;500.00 + VAT as vehicle repossession plus recovery charges.</p>
<!-- <p>&nbsp;</p>

<ol start="7">
<li><strong>Insurance</strong></li>
</ol>
<p>&nbsp;</p>
<p>7.1&nbsp;</p>
<p>Intercity Hire has arranged its motor insurance for PCO vehicles through Zego, an insurance intermediary who introduces business customers to Zego policies underwritten by La Parisienne. The policy provides coverage for accidental damage, fire &amp; theft and third-party liability claims. The details of any such insurance shall be included in the Subscription Booking Form. The insurance key facts document can be found in the Drivers My PCO CAR account and Intercity Hire website. If insurance is not arranged for the Driver through Intercity Hire, insurance must be obtained prior to the Driver using the Vehicle.</p>
<p>&nbsp;</p>
<p>7.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Driver does not buy comprehensive motor insurance through Intercity Hire by selecting the insurance option on the Subscription Booking Form, it is the Driver&rsquo;s responsibility to obtain comprehensive motor insurance for themselves. If the driver chooses to use an alternative comprehensive policy, this must be pre-approved by Intercity Hire in writing. Intercity Hire will require the Driver to advise the insurers that the Vehicle is on hire and the policy of insurance must name Intercity Hire as loss payee in respect of all claims other than third party claims. The Driver irrevocably authorises Intercity Hire to negotiate with the insurers to settle any insurance claim and to receive the insurance monies as appropriate. The Driver will accept any reasonable settlement we agree with the insurers.</p>
<p>&nbsp;</p>
<p>7.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Where the Driver has elected to receive their insurance through Intercity Hire, Intercity Hire reserves the right to amend the Driver&rsquo;s comprehensive motor insurance price in cases where modifications in the Driver&rsquo;s risk profile change and give rise to additional costs. In the event of an insurance price change, the Driver will receive fair notice of the changes and new price.</p>
 <p>&nbsp;</p>
<p>&nbsp;</p>-->
<ol start="14">
<li><strong>General</strong></li>
</ol>
<p>14.8&nbsp;&nbsp;&nbsp;&nbsp; The Driver agrees this Agreement may be entered into and signed by way of electronic signature or advanced electronic signature as defined in the Electronic Communications Act 2000 or articles 3(10) or 3(11) of Regulation EU N09012014. Any signature made by the Driver in a way which complies with the Electronic Communications Act 2000 will be effective and binding on the Driver.</p>
<p>&nbsp;</p>
<p>14.9&nbsp;&nbsp;&nbsp;&nbsp; By electronically signing the Subscription Agreement the Driver agrees that:</p>
<p>&nbsp;</p>
<p>14.10&nbsp;&nbsp; The Driver is entering this agreement wholly or predominantly for the purposes of a business carried on by themselves or intended to be carried on by themselves.</p>
<p>&nbsp;</p>
<p>14.11&nbsp;&nbsp; The Driver will not have the benefit of the protection and remedies that would be available under the Financial Services and Markets Act 2000 or under the Consumer Credit Act 1974 if the agreement were a regulated agreement under those Acts. The Driver is aware that if in any doubts as to the consequences of the agreement not being regulated by the Financial Services and Markets Act 2000 or the Consumer Credit Act 1974, then they should seek independent legal advice.</p>
<p>&nbsp;</p>

<br />
<p style="font-weight: bold;">By signing electronically, the Parties confirm that they have reviewed and agree to the Subscription Agreement.</p>
<br />

Company Name: <span style="margin-left: 50px;">Intercity Hire LTD</span><br />
Signature: <span style="font-family:Tangerine; font-size: 40px; margin-left: 50px;">Intercity Hire LTD </span><br />
<br />
<br />

Driver Name: <span>{{$signature_name}}</span><br />
Signature: <span style="font-family:Tangerine; font-size: 40px">{{$signature_name}}</span><br />





</ul>

  </div>
     
</body>
</body>
</html>