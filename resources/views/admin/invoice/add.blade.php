@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


 	<div class="content-page"
    	<div class="content">
        	<div class="container-fluid">
            	<div class="row">
                	<div class="col-md-12">
						<div class="card-box"> 
  							    <form role="form" action="{{ route('add_invoice') }}" enctype="multipart/form-data" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                     <!-- clone are started  -->
                                     <div id="car_parent">
                                    <div class="row">
                                      <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">Vehicle<span class="text-danger required">*</span></label>
                                      <select    name="vehicle" placeholder="Staff Drop Down"
                                        class="form-control"  id="select2"   >    
                                        @foreach($vehicle as $vehicle_us)
                                        <option value="{{$vehicle_us->id}}"> 
                                          {{@$vehicle_us->licence_plate_number}}
                                        </option>
                                        @endforeach
                                      </select>    
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Invoice Number <span class="text-danger required">*</span></label>
                                        <input   type="text" name="invoice_number" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the Invoice Number" >
                                         
                                      </div>
                                          <div class="form-group col-md-6" id="supplier">
                                            <label for="make" class="col-form-label">Supplier<span class="text-danger required">*</span></label>
                                      <select    name="supplier" placeholder="Staff Drop Down"
                                        class="form-control" id="mySelect"  >
                                       
                                        @foreach($purchase as $purchase_us)
                                        <option value="{{$purchase_us->id}}">{{$purchase_us->name}}<br>
                                          {{@$purchase_us->email}}<br>
                                          {{@$purchase_us->phone}}
                                          </option>
                                        @endforeach
                                          </select> 
                                      </div>
                                       <div class="form-group col-md-6" id="details">
                                            <label for="name">Details<span class="text-danger required">*</span></label>
                                             <textarea class="form-control summernote" hieght="400" width="100%" name="description" id="description" title="error message" maxlength="700" required="required"></textarea>
                                             <!-- <input type="text" class="form-control @error('name') is-invalid @enderror" name="description"  id="name" placeholder="Enter the Details"> -->
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6" id="price">
                                            <label for="name">Price</label>
                                             <input class="radioBtn" type="radio" name="Radio" id="Radio" value="PQR" required >Including VAT
                                            <input type="radio" class="radioBtn" name="Radio" id="Radio" value="ABC" required>Excluding VAT
                                           <input type="number" class="form-control @error('name') is-invalid @enderror" name="price"  id="name" placeholder="Enter the price">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                        </div>
                                          <!-- clone are added to the div  
                                          <div class="col-md-6">
                                             <input type="button"
                                             id="clone_btn" class="btn btn-primary" value="+Add New">
                                        </div>-->
                                          <div class="form-group col-md-6">
                                            <label for="name">Date<span class="text-danger required">*</span></label>
     <input type="date" required="required" class="form-control date_appy"  
  value="<?php echo date('d-m-Y'); ?>" name="dob" autocomplete="off"   placeholder=""
                                             id="booking_start_date">

                                          @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6" id="box">
                                            <label for="name">VAT NUMBER<span class="text-danger required">*</span></label>
                                             <input max="10" min="3" type="text" name="vat_number" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Enter the VAT Number" >
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>

                                          <div class="form-group col-md-6">
                                            <label for="name">Invoice Slip<span class="text-danger required">*</span></label>
                                             <input name="nid_back"  class="form-control form-control-lg" type="file" placeholder="N.I.D(Back)" >
                                          </div>
                                            
                                          
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Add</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>
                                  </div>

                              </form>
                      
                    	</div>
					</div>
            	</div>
        	</div>
     	</div>
	</div>

    


@endsection
@section('scripts')

   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.js"></script>
 
<script>
 



   $('#select2').select2({});
 
// on first focus (bubbles up to document), open the menu
$(document).on('focus', '#select2-selection#select2-selection--single', function (e) {
  $(this).closest("#select2-container").siblings('select:enabled').select2('open');
});

// steal focus during close - only capture once and stop propogation
$('select#select2').on('select2:closing', function (e) {
  $(e.target).data("select2").$selection.one('focus focusin', function (e) {
    e.stopPropagation();
  });
});


    $( function() {

    $( "#booking_start_date" ).datepicker(
      {  
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate: -390,
       maxDate: new Date(),
     });
  });

$('input[type="radio"]').click(function(){

        if($(this).attr("value")=="ABC"){
            $("#box").hide('slow');
        }
        if($(this).attr("value")=="PQR"){
            $("#box").show('slow');

        }        
    });
$('input[type="radio"]').trigger('click');
 </script>

@endsection