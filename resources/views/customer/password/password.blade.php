@extends('layouts.customer')
@section('styles')
@endsection
@section('content')
<script type="text/javascript">

      function viewPassword()
{
  var passwordInput = document.getElementById('password-field');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}
      function viewPassword1()
{
  var passwordInput = document.getElementById('password-field');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}

      function viewPassword3()
{
  var passwordInput = document.getElementById('password-field');
  var passStatus = document.getElementById('pass-status');
 
  if (passwordInput.type == 'password'){
    passwordInput.type='text';
    passStatus.className='fa fa-eye-slash';
    
  }
  else{
    passwordInput.type='password';
    passStatus.className='fa fa-eye';
  }
}

</script>

<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        
                        <div class="card-box">
                            
                            @if($errors->any())
                                {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif

@if(session()->has('message'))
    <div class="alert alert-success"  >
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
    @endif
     @if(session()->has('error'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('error') }}
    </div>
    @endif

                            <form role="form"  action="{{ route('customer_updaet_user_password') }}"  enctype="multipart/form-data" method="post">
                                {{csrf_field()}}

                                <div class="card-body">
                                  

                                    <label for="name">Current<span class="text-danger required">*</span></label>
                                    <div class="form-group col-md-6 input-group ">
                                        
                                      <input type="password" value="" id="password" name="current_password" class="form-control"  required>
                                        <i  type="button" id="pass-status" class="eye fa fa-eye" aria-hidden="true"
                                         onClick="viewPassword3()"  style="background: #ccc; border-top-right-radius: 15px; border-bottom-right-radius: 15px;"></i>
                                        
                                    </div>
                                    <label for="name">Password<span class="text-danger required">*</span></label>
                                    <div class="form-group col-md-6 input-group ">
                                        
                                      <input type="password" id="password" name="password" class="form-control"  required>
                                        <i  type="button" id="pass-status" class="eye fa fa-eye" aria-hidden="true"
                                         onClick="viewPassword()"  style="background: #ccc; border-top-right-radius: 15px; border-bottom-right-radius: 15px;"></i>
                                        
                                    </div>
                                     <label for="phone">Confirm Password<span class="text-danger required">*</span></label>
                                    <div class="form-group col-md-6 input-group">
                                       
                                        
                                       <input type="password" id="password" name="password_confirmation" class="form-control"  required>
                                        <i  type="button" id="pass-status" class="eye fa fa-eye" aria-hidden="true"
                                         onClick="viewPassword1()"  style="background: #ccc; border-top-right-radius: 15px; border-bottom-right-radius: 15px;"></i>
                                    </div>
                                     <div class="col-md-6">
                                    <button type="submit" class="btn btn-info btn-flat">Save</button>
                                    <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                                  </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
        });
    } );



  
</script>
<style type="text/css">
   
   .fa-eye, .fa-eye-slash{
    border: 1px solid #ccc;
    padding: 9px 13px;
    background-color: rgb(255, 255, 255);
  }
  
</style>

@endsection
