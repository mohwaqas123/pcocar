@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
  <div class="content-page">
      <div class="content">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-12">
            <div class="card-box"> 
                    <form role="form" action="{{ route('update_pcn') }}" enctype="multipart/form-data" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                     <input type="hidden" name="id" value="{{$pcn->id}}">
                                   
                                         <div class="row">
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">PCN Ref<span class="text-danger required">*</span></label>
                                    <input max="10" min="3" type="text" name="ref_number" class="form-control" value="{{$pcn->ref_number}}" placeholder="Please Enter Number" required >    
                                      </div>
                                      <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">Vehicle<span class="text-danger required">*</span></label>
                                       <select  name="vehicle" 
                                        class="form-control licence_plate_number"
                                         id="select3" >
                                        @foreach($vehicle as $vehicle_us)
                                        <option  @if($pcn->vrm == $vehicle_us->id) selected @endif value="{{$vehicle_us->id}}"> 
                                          {{$vehicle_us->licence_plate_number}}
                                        </option>
                                        @endforeach
                                       </select>    
                                      </div>
                                      <div class="form-group col-md-6"  >
                                        <label for="make" class="col-form-label">Status <span class="text-danger required">*</span></label>
                                        <input   type="text" value="{{$pcn->status}}" name="status" class="form-control " placeholder="Enter the Status" >
                                         
                                      </div>
                                      <div class="form-group col-md-6"  >
                                        <label for="make" class="col-form-label">Stage <span class="text-danger required">*</span></label>
                                        <input   type="text" name="stage" class="form-control " value="{{$pcn->stage}}" id="" placeholder="Enter the Stage" >
                                    
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">PCN Contravention Date <span class="text-danger required">*</span></label>
                                        <input   type="date" name="pcn_contravention" value="{{$pcn->pcn_contravention}}" class="form-control" id="" placeholder="Enter the Stage" required="required">
                                         
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Date of Notice </label>
                                        <input   type="date" name="notice_date" class="form-control"  value="{{$pcn->notice_date}}" placeholder="Enter the Stage"  >
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Date Received <span class="text-danger required">*</span></label>
                                        <input   type="date" name="received_date" class="form-control" value="{{$pcn->received_date}}" placeholder="Enter the Stage" required="required" >
                                         
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Type of Action<span class="text-danger required">*</span></label>
                                    <input   type="text" name="type_of_action" class="form-control" value="{{$pcn->type_of_action}}" placeholder="Enter the action">    
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Action Date   </label>
                                        <input   type="date" name="action_date" class="form-control" value="{{$pcn->action_date}}" placeholder="Enter the Stage"   >
                                         
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Charging Authority  Name<span class="text-danger required">*</span></label>
                                    <input max="10" min="3" type="text" name="charging_authority_name" value="{{$pcn->charging_authority_name}}" class="form-control" id="" placeholder="Enter the action" required>    
                                      </div>
                                      <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Driver Name<span class="text-danger required">*</span></label>
                                    <input max="10" min="3" type="text" name="driver_name" value="{{$pcn->user->name}}" class="form-control" id="" placeholder="Enter the name"  >    
                                      </div>
                                    <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Other</label>
                                    <input max="10" min="3" type="text" name="driver_email" value="{{$pcn->driver_email}}" class="form-control" id="" placeholder="Enter the Name" required>    
                                      </div>
                                     <!--  <div class="form-group col-md-6" id="vehicle">
                                        <label for="make" class="col-form-label">Driver Phone<span class="text-danger required">*</span></label>
                                    <input type="number" value="{{$pcn->driver_phone}}" name="driver_phone" class="form-control" id="" placeholder="Enter the phone" required>    
                                      </div> -->
                                         <div class="form-group col-md-6">
                                            <label for="name">PCN<span class="text-danger required">*</span></label>
                                             <input name="doc_mot"  class="form-control form-control-lg" type="file" placeholder="N.I.D(Back)" >
                                             <p> @if($pcn->doc_mot)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$pcn->doc_mot}}"> {{$pcn->doc_mot}}</a>  @endif</p>
                                          </div>  
                                       </div>
                                     <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Update</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>
                                  </div>
                                 </form>
                          </div>
                    </div>
              </div>
          </div>
      </div>
  </div>
@endsection
@section('scripts')
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.js"></script>



<script>
   $(document).ready(function () {
       $('#select3').select2({});
    alert('dsaasadssa'); 
// on first focus (bubbles up to document), open the menu
$(document).on('focus', '#select3-selection#select3-selection--single', function (e) {
  $(this).closest("#select3-container").siblings('select:enabled').select2('open');
});

// steal focus during close - only capture once and stop propogation
$('select#select3').on('select3:closing', function (e) {
  $(e.target).data("select3").$selection.one('focus focusin', function (e) {
    e.stopPropagation();
  });
});
     $( function() {
    $( "#booking_start_date" ).datepicker(
      {  
       dateFormat:'yy-m-d', 
       changeMonth: true,
        changeYear: true,
      //  yearRange: "-100:+0", // last hundred years
       minDate: -390,
       maxDate: new Date(),
     });
  });
     $('.licence_plate_number').change(function(event) {
   
    var url_call = "{!! url('/') !!}/admin/pcn/get_customers";
     alert(url_call);
    var vehicle_id = this.value;
    $.ajax({
        type : 'get',
        url  : url_call,
        //  data: {},
        data: { vehicle_id: vehicle_id},
        contentType: 'application/json; charset=utf-8',
        success :  function(data){
 
        console.log(data); 
        $("#customer").empty();
         $("#customer").append(data);
         // $("#driver_email").empty();
         // $("#driver_email").append(data);

 
     }
    });//ajax
});

 </script>

@endsection