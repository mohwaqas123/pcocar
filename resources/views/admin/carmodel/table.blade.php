<div id="custom-search-table">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
        <th style="width: 10px">Sr.#</th>
        <th>Name</th>
        <th>Country Name</th>
        {{-- <th>Status</th> --}}
        <th style="width:90px;">Action</th>
        </tr>
        </thead>
        <tbody>
            @php
                $count = 1;
            @endphp
        @foreach($states as $state)
        <tr>
            <td>{{ $count }}</td>
            <td>{{ $state->name }}</td>
            <td>{{ $state->country->name }}</td>
            {{-- <td style="width:100px;">
                @if ($state->status == 0)
                    In-active
                @else
                    Active
                @endif
            </td> --}}
            <td>
            <a class="btn btn-warning btn-sm" href="{{ route('editState', $state->id) }}" class="mr-5">Edit </a>
        </td>
        </tr>
        @php
            $count++;
        @endphp
        @endforeach
        </tbody>
    </table>
    {{$states->links()}}
</div>
