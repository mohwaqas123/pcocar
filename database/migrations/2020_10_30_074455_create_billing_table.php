<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->integer('booking_id');            
            $table->integer('user_id');
            $table->integer('vehicle_id');
            $table->float('price');
            $table->boolean('payment_mode')->comment('1=bank,2=stripe,3=paypal')->nullable();
            $table->boolean('payment_status')->comment('0=success,1=failure')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing');
    }
}
