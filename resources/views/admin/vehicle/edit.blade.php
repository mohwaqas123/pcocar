@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<style>
.select2-container .select2-selection--single{
height:34px !important;
}
.select2-container--default .select2-selection--single{
border: 1px solid #ccc !important;
border-radius: 0px !important;
}

.bg-dev{border: 1px solid #000;}
.header-title{background: #eee;
padding: 5px;}

</style>
<div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="card-box">
              

  @if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif



                            @if($errors->any())
                            {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif
                               <h4 class="breadcrumb-item active"   > 
                       
                    </h4>
                    <h4 class="m-t-0 "> </h4>
                            <form action="{{ route('vehicleUpdate') }}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" value="{{$vehicle_info->id}}" name="id" />
                                @csrf 
                             <div class="bg-dev">

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Licence Plate Number<span class="text-danger">*</span></label>
                                            <input max="10" min="3" type="text" name="licence_plate_number" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Licence Plate Number" value="{{$vehicle_info->licence_plate_number}}" required>
                                            @if ($errors->has('licence_plate_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('licence_plate_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="make" class="col-form-label">Select Uber(Type)<span class="text-danger">*</span></label>
                                            <select id="uber_type" name="uber_type" class="form-control select2" required>
                                                
                                                <option @if($vehicle_info->uber_type==1) selected @endif value="Uber Lx">Uber Lx</option>
                                                <option  @if($vehicle_info->uber_type==2) selected @endif  value="UberX">UberX</option>
                                                <option  @if($vehicle_info->uber_type==3) selected @endif value="Uber Exec">Uber Exec</option>
                                                <option @if($vehicle_info->uber_type==4) selected @endif  value="Uber Lux">Uber Lux</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                      <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="song name" class="col-form-label">Vehicle Name<span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="" placeholder="Name" value="{{$vehicle_info->name}}" required>
                                        @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">Select Make<span class="text-danger">*</span></label>
                                        <select id="" name="make" class="form-control select2" required>
                                            @foreach($make as $car_make)
                                            <option @if($vehicle_info->make ==$car_make->id) selected @endif  value="{{$car_make->id}}">{{$car_make->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="model" class="col-form-label">Select Model<span class="text-danger">*</span></label>
                                        <select    id="" name="model" class="form-control" required>
                                            @foreach($car_model as $car_mod)
                                            <option @if($vehicle_info->model ==$car_mod->id) selected @endif value="{{$car_mod->id}}">{{$car_mod->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="year" class="col-form-label">Year<span class="text-danger">*</span></label>
                                        <select id="" name="year" class="form-control" required>
                                            <option value="" selected>Select Year</option>

                                            <option @if($vehicle_info->year==2015) selected @endif value="2000">2000</option>
                                            <option @if($vehicle_info->year==2001) selected @endif  value="2001">2001</option>
                                            <option @if($vehicle_info->year==2002) selected @endif  value="2002">2002</option>
                                            <option @if($vehicle_info->year==2003) selected @endif  value="2003">2003</option>
                                            <option @if($vehicle_info->year==2004) selected @endif  value="2004">2004</option>
                                            <option @if($vehicle_info->year==2005) selected @endif  value="2005">2005</option>
                                            <option @if($vehicle_info->year==2006) selected @endif  value="2006">2006</option>
                                            <option @if($vehicle_info->year==2007) selected @endif  value="2007">2007</option>
                                            <option @if($vehicle_info->year==2008) selected @endif  value="2008">2008</option>
                                            <option @if($vehicle_info->year==2009) selected @endif  value="2009">2009</option>
                                            <option @if($vehicle_info->year==2010) selected @endif  value="2010">2010</option>
                                            <option @if($vehicle_info->year==2011) selected @endif  value="2011">2011</option>
                                            <option @if($vehicle_info->year==2012) selected @endif  value="2012">2012</option>
                                            <option @if($vehicle_info->year==2013) selected @endif  value="2013">2013</option>
                                            <option @if($vehicle_info->year==2014) selected @endif  value="2014">2014</option>
                                            


                                            <option  @if($vehicle_info->year==2015) selected @endif  value="2015">2015</option>
                                            <option @if($vehicle_info->year==2016) selected @endif  value="2016">2016</option>
                                            <option @if($vehicle_info->year==2017) selected @endif  value="2017">2017</option>
                                            <option @if($vehicle_info->year==2018) selected @endif  value="2018">2018</option>
                                            <option @if($vehicle_info->year==2019) selected @endif  value="2019">2019</option>
                                            <option @if($vehicle_info->year==2020) selected @endif  value="2020">2020</option>




                                        </select>
                                    </div> </div>
                                </div>


 <br>
                                    <div class="bg-dev">
                                        <h4 class="m-t-0 header-title">CAR DETAILs</h4>
                                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="offer" class="col-form-label">City<span class="text-danger">*</span></label>
                                        <input value="{{$vehicle_info->city}}" type="text" name="city" class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" id="" placeholder="city" required="required">
                                        @if ($errors->has('city'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('city') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="passengers" class="col-form-label">Fuel type<span class="text-danger">*</span></label>
                                        <select id="" name="fuel_type" class="form-control" required>
                                            
                                             
                                 <option  @if($vehicle_info->fuel_type==0) selected @endif value="0">Petrol</option>
                                <option  @if($vehicle_info->fuel_type==3) selected @endif value="3">Electric</option>
                                  <option  @if($vehicle_info->fuel_type==4) selected @endif value="4">Hybrid</option>
                                  <option  @if($vehicle_info->fuel_type==5) selected @endif value="5">Electric Hybrid</option> 
                                        </select>
                                    </div>
                                    
                                </div>



                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="transmission" class="col-form-label">Select Transmission<span class="text-danger">*</span></label>
                                        <select id="" name="transmission" class="form-control" required>
                                            
                                            <option @if($vehicle_info->transmission==0) selected @endif value="0" >Auto</option>
                                            <option @if($vehicle_info->transmission==1) selected @endif value="1" >Manual</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="description" class="col-form-label">Engine Capacity<span class="text-danger">*</span></label>
                                        <input value="{{$vehicle_info->engine_Capacity}}" type="text" name="engine_Capacity" class="form-control" id="" placeholder="Engine Capacity" />
                                    </div>
                                </div>
                                        <div class="form-row">
                                           <div class="form-group col-md-6">
                                        <label for="model" class="col-form-label">Vehicle Category<span class="text-danger">*</span></label>
                                        <select id="vehicle_category" name="vehicle_category" class="form-control" required>
                                            <option @if($vehicle_info->vehicle_category==1) selected @endif  value="1" >PCO</option>
                                            <option @if($vehicle_info->vehicle_category==2) selected @endif  value="2" >Private</option>
                                        </select>
                                    </div>
                                            <div class="form-group col-md-6">
                                        <label for="year" class="col-form-label">Body Type<span class="text-danger">*</span></label>
                                        <select id="body_type" name="body_type" class="form-control" required>
                                            
                                            <option @if($vehicle_info->body_type==1) selected @endif  value="1">MPV</option>
                                            <option @if($vehicle_info->body_type==2) selected @endif  value="2">MPV3</option>
                                            
                                        </select>
                                    </div>
                                            
                                        </div>
                                        <!--<div class="form-row">-->
                                        <!--    <div class="form-group col-md-6">-->
                                        <!--        <label for="transmission" class="col-form-label">Select Transmition<span class="text-danger">*</span></label>-->
                                        <!--        <select id="" name="transmission" class="form-control" required>-->
                                        <!--            <option value="" selected>Select Transmition</option>-->
                                        <!--            <option value="0" >Auto</option>-->
                                        <!--            <option value="1" >Manul</option>-->
                                        <!--        </select>-->
                                        <!--    </div>-->
                                        <!--    <div class="form-group col-md-6">-->
                                        <!--        <label for="description" class="col-form-label">Engine Capacity<span class="text-danger">*</span></label>-->
                                        <!--        <input type="text" name="engine_Capacity" class="form-control" id="" placeholder="Engine Capacity" />-->
                                        <!--    </div>-->
                                        <!--</div>-->
                                        <div class="form-row">
                                           <div class="form-group col-md-6">
                                        <label for="description" class="col-form-label">Description</label>
                                        <textarea  name="description" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" id="" placeholder="description">{{$vehicle_info->description}}</textarea>
                                        @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                        @endif
                                    </div>


                                    <div class="form-group col-md-6">
                                                <label for="description" class="col-form-label">Colour<span class="text-danger">*</span></label>
                                                <input type="text" value="{{$vehicle_info->colour}}" name="colour" class="form-control" id="" placeholder="Colour"  required="required" />
                                            </div>


                                            <div class="form-group col-md-6">
                                          <label for="model" class="col-form-label">Supplier</label>
                                            <select id="" name="purchase" class="form-control" required>
                                                <option value="0">None</option>
                                                @foreach($purchase as $purchease_mod)
                                                <option
                    @if($vehicle_info->purchase_id  == $purchease_mod->id) selected @endif
                                                 value="{{$purchease_mod->id}}">
                                                 {{$purchease_mod->name}}</option>
                                                @endforeach
                                            </select>      
                                        </div>
                                             <div class="form-group col-md-6">
                                          <label for="model" class="col-form-label">Partner</label>
                                            <select id="" name="partner" class="form-control select2" required>
                                                    <option value="0">None</option>
                                                @foreach($partner as $partner_mod)
                        <option @if($vehicle_info->partner_id  == $partner_mod->id) selected @endif
                                                 value="{{$partner_mod->id}}">
                            ID:{{$partner_mod->id}}
                            ,  {{$partner_mod->name}}, <b>{{$partner_mod->email}}</b></option>
                                                @endforeach
                                            </select>      
                                        </div>
                                         <div class="form-group col-md-6">
                                          <label for="model" class="col-form-label">Company  Name<span class="text-danger">*</span></label>
                                              <input type="text" name="company_name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$vehicle_info->company_name}}" id="" placeholder="Company Name" required>      
                                        </div>
                                        <div class="form-group col-md-6">
                                          <label for="model" class="col-form-label">Company Address<span class="text-danger">*</span></label>
                                            <input type="text" name="company_address" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$vehicle_info->company_address}}" placeholder="Company Name" required>        
                                        </div>

                                        </div></div><br>



      
                                        <div class="bg-dev">
                                            <h4 class="m-t-0 header-title">AVAILABILITY AND PRICING</h4>
                                            
                                            <div class="form-row">
                                                 <div class="form-group col-md-6">
                                        <label for="price" class="col-form-label">Price<span class="text-danger">*</span></label>
                                        <input type="text" name="price" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" id="" placeholder="price" value="{{$vehicle_info->price}}" required>
                                        @if ($errors->has('price'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('price') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                                 <div class="form-group col-md-6">
                                        <label for="offer" class="col-form-label">Offer<span class="text-danger">*</span></label>
                                        <input  value="{{$vehicle_info->offer}}"  type="text" name="offer" class="form-control {{ $errors->has('offer') ? ' is-invalid' : '' }}" id="" placeholder="offer">
                                        @if ($errors->has('offer'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('offer') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                                <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Date From<span class="text-danger">*</span></label>
                                                    <input type="text" name="date_from" class="form-control {{ $errors->has('date_from') ? ' is-invalid' : '' }}" id="datepicker1" placeholder="Date format 2020-11-10" value="{{$vehicle_info->date_from}}">
                                                    @if ($errors->has('date_from'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_from') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                
                                                <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Date To<span class="text-danger">*</span></label>
                                                    <input type="text" value="{{$vehicle_info->date_to}}" name="date_to" class="form-control {{ $errors->has('date_to') ? ' is-invalid' : '' }}" id="" placeholder="Date format 2020-11-10">
                                                    @if ($errors->has('date_to'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_to') }}</strong>
                                                    </span>
                                                    @endif
                                                </div >
                                            </div></div> 

<br>
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Pickup Address</h4>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="price" class="col-form-label">Postal pickup dropoff<span class="text-danger">*</span></label>
                                                        <input type="text" value="{{$vehicle_info->postal_pickup_dropoff}}" name="postal_pickup_dropoff" class="form-control {{ $errors->has('postal_pickup_dropoff') ? ' is-invalid' : '' }}" id="" placeholder="Postal pickup dropoff" required>
                                                        @if ($errors->has('postal_pickup_dropoff'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('postal_pickup_dropoff') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="offer" class="col-form-label">Location Description<span class="text-danger">*</span></label>
                                                        <input type="text" value="{{$vehicle_info->location_description}}" name="location_description" class="form-control {{ $errors->has('location_description') ? ' is-invalid' : '' }}" id="" placeholder="Description">
                                                        @if ($errors->has('location_description'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('location_description') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <br>

 
 
                                            
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Documents Upload</h4>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="price" class="col-form-label">Doc Mot</label>
                                                        <input type="file" name="doc_mot"   class="form-control"> 
     <p> @if($vehicle_info->doc_mot)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->doc_mot}}"> {{$vehicle_info->doc_mot}}</a>  @endif</p>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="offer" class="col-form-label">Doc logback</label>
                                                        <input type="file" name="doc_logback"   class="form-control">
                                                        <p> @if($vehicle_info->doc_logback)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->doc_logback}}"> {{$vehicle_info->doc_logback}}</a>  @endif</p>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="offer" class="col-form-label">Doc PHV</label>
                                                        <input type="file" name="doc_phv"   class="form-control">
       <p> @if($vehicle_info->doc_phv)  Document Uploaded: 
        <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->doc_phv}}"> {{$vehicle_info->doc_phv}}</a>  @endif</p>
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Documents Upload</h4>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <label for="price" class="col-form-label">Exterior Front</label>
                                                        <input type="file" name="img_exterior_front"   class="form-control">
                                                        <p> @if($vehicle_info->img_exterior_front)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->img_exterior_front}}"> {{$vehicle_info->img_exterior_front}}</a>  @endif</p>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="offer" class="col-form-label">Exterior Back</label>
                                                        <input type="file" name="img_exterior_back"   class="form-control">
                                                        <p> @if($vehicle_info->img_exterior_back)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->img_exterior_back}}"> {{$vehicle_info->img_exterior_back}}</a>  @endif</p>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="offer" class="col-form-label">Exterior Front2</label>
                                                        <input type="file" name="img_exterior_front2"   class="form-control">
                                                        <p> @if($vehicle_info->img_exterior_front2)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->img_exterior_front2}}"> {{$vehicle_info->img_exterior_front2}}</a>  @endif</p>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="offer" class="col-form-label">Exterior Dashboard</label>
                                                        <input type="file" name="img_exterior_dashboard"   class="form-control">
                                                        <p> @if($vehicle_info->img_exterior_dashboard)  Document Uploaded:  <a target="_blank" href="{{url('/storage/app/').'/'.$vehicle_info->img_exterior_dashboard}}"> {{$vehicle_info->img_exterior_dashboard}}</a>  @endif</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>



 @php
              if($vehicle_info->featurs && $vehicle_info->featurs!='null'){ 
      
               $all_features = json_decode($vehicle_info->featurs); 

 @endphp              

  <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="description" class="col-form-label">Features</label>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Air Conditioning',$all_features)) checked="checked" @endif name="chk[]" value="Traction control" /> Traction control
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Alloy Wheels',$all_features)) checked="checked" @endif name="chk[]" value="Alloy Wheels" /> Alloy Wheels
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Auto Start Stop',$all_features)) checked="checked" @endif name="chk[]" value="Stability control" /> Stability control
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Aux Inp',$all_features)) checked="checked" @endif name="chk[]" value="CVT Automatic" /> CVT Automatic
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Bluetooth',$all_features)) checked="checked" @endif name="chk[]" value="Dual Standard Airbags" /> Dual Standard Airbags
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Cloth Seats',$all_features)) checked="checked" @endif name="chk[]" value="Bluetooth Wireless" /> Bluetooth Wireless
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Cruise Control',$all_features)) checked="checked" @endif name="chk[]" value="Cruise Control"  /> Cruise Control
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Diesel Particulate Filter',$all_features)) checked="checked" @endif name="chk[]" value="Automatic Climate Control"  /> Automatic Climate Control
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Electric Windows',$all_features)) checked="checked" @endif name="chk[]" value="Brake Assist"  /> Brake Assist
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Fm Am Radio',$all_features)) checked="checked" @endif name="chk[]" value="Driver's Information Center"  /> Driver's Information Center
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Parking Sensors',$all_features)) checked="checked" @endif name="chk[]" value=" Parking Sensors"  /> Parking Sensors
                                            </div>
                                            <div class="col-md-3">
                                                <input type="checkbox" @if(in_array('Satellite Navigation',$all_features)) checked="checked" @endif name="chk[]" value="Satellite Navigation"  /> Satellite Navigation
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                

@php
}
else{
    @endphp
          <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Features</h4>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Traction control" /> Traction control
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value=" Alloy Wheels" /> Alloy Wheels
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value=" Stability control" /> Stability control
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="CVT Automatic" /> CVT Automatic
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Dual Standard Airbags" /> Dual Standard Airbags
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Bluetooth Wireless" /> Bluetooth Wireless
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value=" Automatic Climate Control"  /> Automatic Climate Control
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Brake Assist"  /> Brake Assist
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Parking Sensors"  /> Parking Sensors
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value=" Satellite Navigation"  /> Satellite Navigation
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Parking Sensors"  /> Parking Sensors
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Satellite Navigation"  /> Satellite Navigation
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                            </div>
@php
}
@endphp









     
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Image Gallary Pictures</h4>
                                                
                                                <div class="form-row">
                                                              @foreach($vehicle_info->vehicle_images as $img) 
                                            <div class="form-group col-md-3" id="img_{{$img->id}}"> 
                                              <img src="{{ asset('storage/app/'.$img->images) }}" class="img-responsive w-100" alt="..." />
                                               <a onclick="return confirm('Make sure you want remove it?')" href="{{ url('temp_delete_img'.'/'.$img->id) }}" >Remove</a> 
                                            </div>
                                            @endforeach 
                                                </div>


                                                 <div class="form-row"> 
                                                <div class="form-group col-md-6">
                                                    <p class="col-form-label">Upload additional Images(10)<span class="text-danger">*</span></p>
                                                <input type="file" id="images" name="images[]" multiple>
                                                @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>



                                            </div><br>

  
 
 <label for="description" class="col-form-label">Profile Pictures</label> <br>
   <img src="{{ asset('storage/app/'.$vehicle_info->image) }}" class="img-thumbnail" style="width: 100px" alt="..." />
     <div class="form-row"> 
                                        <div class="form-group col-md-6">
                                                    <p class="col-form-label">Replace Profile Pic(JPG, JPEG, PNG)<span class="text-danger">*</span></p>
                                                    <input type="file" name="thumbnail" class="form-control"> 
                                                    @if ($errors->has('thumbnail'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('thumbnail') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
</div>


                            <!--     <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <p class="col-form-label">Upload Thumbnail(JPG, JPEG, PNG 100*100)<span class="text-danger">*</span></p>
                                        <input type="file" name="thumbnail" required class="form-control"></label></span>
                                        @if ($errors->has('thumbnail'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('thumbnail') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <p class="col-form-label">Upload Images(10)<span class="text-danger">*</span></p>
                                    <input type="file" id="images" name="images[]" multiple></span>
                                    @if ($errors->has('images'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('images') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div> -->
                            <button type="submit" class="btn btn-primary">Update</button>
                              <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">



function remove_img(id){

     $.ajax({


                  // url:"http://ip.jsontest.com/",
                  url: '{{ url("temp_delete_img") }}',
                   method:"get",
                   //data: $('#frmConversation').serialize(),
                  data: {id:'4.jpg'},
                   //dataType:'JSON',
                   contentType: 'application/json; charset=utf-8',
                   cache: false,
                   processData: false,
                   success:function(data)
                   {
                       alert(data);
                   },
                   error: function(error)
                        {
                            //console.log(data);
                           // alert(error);
                        }
                  });
}



$('.select2').select2();
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
});
} );
</script>
@endsection