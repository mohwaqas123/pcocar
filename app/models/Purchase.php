<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	protected $table = 'purchase';
	
     protected $fillable = [
        'name','email','phone','address','vat_price','vat_number','date','price','invoice','image','reasons','detail',
      ];

}
