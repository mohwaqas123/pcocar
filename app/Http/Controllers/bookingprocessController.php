<?php

namespace App\Http\Controllers;
use Imagick;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\models\Vehicle;
use App\models\Booking;
use App\models\PaymentHistory;
use App\models\User;
use App\models\Customerinsurance;

use App\models\Swipe; 
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Session;
use PDF; 
use Validator;
use App\Helper\Helper;
use App\models\Coupon;

//use Auth;

class bookingprocessController extends Controller
{ 


  public function get_discount_price($week,$price){
   if($week >= 11 && $week < 24 ){
            $discount = ($price * 5 ) /100;
             
       }

       if($week >= 24 && $week < 48 ){
            $discount = ($price * 10) /100;
             
       }

       if($week >= 48 ){
            $discount = ($price * 10 ) /100;
           
       }
        if($week < 12 ){
           //$discounted_price = $price  * $request->duration;
            $discount =0;

        
      
        }
        return $discount;
}
public function get_discount_percent($week){
   if($week >= 11 && $week < 24 ){ 
            $disc = 5;
       }

       if($week >= 24 && $week < 48 ){  
            $disc = 10;
       }

       if($week >= 48 ){ 
            $disc = 10;
       }
        if($week < 12 ){
           $disc=0;
        }
    return $disc;    
}






    public function index(Request $request)
    {   
       $price_with_disc = $request->price -  $this->get_discount_price( $request->weeks, $request->price);
    

         if($request->weeks){  //if first time booking selected generate its session to use in further
          
         /*if($request->session()->get('permotion_discount')){
            $d = $price_with_disc * $request->session()->get('permotion_discount') / 100;
            $price =$price_with_disc - $d;
         }
         else
         {
            $price = $price_with_disc;
         }


       
       if(Helper::get_flat_discount_amount()[0]->flat_discount_type==2){
            $price = $price - Helper::get_flat_discount_amount()[0]->flat_discount_rate;
       }  */

 $price = $request->price;
  
 session()->forget('price_final_total_book');
 session()->forget('price');

 
   
 if( $request->insurance==1 && $request->deposit==2 ){

  $price_final_total_book = $request->price;
 
  
 }
 else if($request->insurance==1)
 {
 
$price_final_total_book = $request->price_final_total_book - env('INSURANCE_CODE');
 
 }
 else if($request->deposit==2)
 {
  
    $price_final_total_book = $request->price_final_total_book - env('DEPOSIT_SECURITY');
     // dd($price_final_total_book);
 }
 else
 {
   $price_final_total_book = $request->price_final_total_book ;
 }

 if($request->insurance) $ins = 1; else $ins=0;
 if($request->deposit) $dep = 2; else $dep=0;

  
 
          session([
                'months' => $request->weeks,
                'miles' => $request->miles,
                'price' => number_format($price,2),
                'v_id' =>  $request->id,
                'discount' =>$this->get_discount_percent($request->weeks),
                'f_discount' =>$this->get_discount_percent($request->weeks),
                'promotion_type' =>Helper::get_flat_discount_amount()[0]->promotion_type,
                'flat_discount_rate' =>Helper::get_flat_discount_amount()[0]->flat_discount_rate,
                'flat_discount_type'=>$request->flat_discount_type,
                'price_final_total_book' =>$price_final_total_book,
                'booking_start_date' => $request->booking_start_date,
                'booking_drop_off_date' => $request->hid_drop_off,
                'insurance' => $ins,
                'deposit' => $dep,

               
            ]);
        

    }
        //$value = $request->session()->get('key');
        //dd($value);
       // return redirect()->route('usershow');          
        $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 

        //return view('frontend.browse', ['vehicles' => $vehicles]);
  
        return view('frontend.booking', [
  
            'vehicles' => $vehicles, 
            'months'=>   $request->session()->get('months'),
            'miles'=>    $request->session()->get('miles'),
            'price'=>  number_format($price,2),
            'discount' => $request->session()->get('discount'),
            'price_final_total_book' => $request->session()->get('price_final_total_book'),
            'insurance' => $request->session()->get('insurance'),
            'deposit' => $request->session()->get('deposit'),

           // 'promotion' => Helper::check_discount(),

        ]);
    }


    public function step2(Request $request)
    {
   
      $disable_action=1;

      //check same user has already exist booking that is not closed yet
           $check = Booking::where('user_id', $request->session()->get('user_id'))
                          ->where('booking_status',1)
                          ->where('payment_status',1)  
                          ->get();      
            if( count($check) > 0 ){  
                $disable_action = "Not allow!!!! You have already submited booking";
            }


         $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 
        //return view('frontend.browse', ['vehicles' => $vehicles]);
        return view('frontend.step2', [
  
            'vehicles' => $vehicles, 
            'months'=>   $request->session()->get('months'),     
            'miles'=>    $request->session()->get('miles'),
            'price'=>  $request->session()->get('price'),
            'discount' => $request->session()->get('f_discount'),
            'disable_action'=>$disable_action,
             'promotion' => Helper::check_discount(),
              'price_final_total_book' => $request->session()->get('price_final_total_book'),
               'insurance' => $request->session()->get('insurance'),
            'deposit' => $request->session()->get('deposit'),
                


        ]);
    }
 

    public function upload_information(Request $request){   
 
       /* $this->validate($request, [
                'name' => 'required',
                'email'=>'required|email',
           ]);
*/   
 
  //check pco_licence_no is already booking exist that is not closed.
           $c = Booking::where('pco_licence_no', $request->pco_licence_no)
                          ->where('booking_status',1)
                          ->orWhere('national_insu_numb',$request->national_insu_numb)
                          ->orWhere('dvla',$request->dvla)
                          ->get();  
           /*if( count($c) > 0 ){
           return redirect()->back()->with('message', 'Booking is already exist against this Licence No or National Insurance Number or DVLA Licence Number')->with([
            'pco_licence_no'=>$request->pco_licence_no,
            'national_insu_numb'=>$request->national_insu_numb,
            'dob'=>$request->dob,
            'phone'=>$request->phone,
            'dvla'=>$request->dvla,
            'address'=>$request->address,

           ]);

       }*/


        if($request->hasFile('file_dvla'))
          { 
                $allowedfileExtension=['pdf','jpg','png','docx'];
               // $files = $request->file('file');
          $vehicleImagesFolder = 'documents_uploads'; 
            if (!Storage::exists($vehicleImagesFolder)) {
                Storage::makeDirectory($vehicleImagesFolder);
            }

             
$file1 =Storage::putFile($vehicleImagesFolder, new File($request->file_dvla));
$file2 =Storage::putFile($vehicleImagesFolder, new File($request->file_cnic));


                 // $file3 =Storage::putFile($vehicleImagesFolder, new File($request->file_utility));
           if($request->doc_other) { 
            $file4 =Storage::putFile($vehicleImagesFolder, new File($request->doc_other)); 
           $pco_paper_licence =Storage::putFile($vehicleImagesFolder, new File($request->pco_paper_licence)); 
      //     $pco_driver_back =Storage::putFile($vehicleImagesFolder, new File($request->pco_driver_back)); 
         } 
         else{
          $file4='';
           $pco_paper_licence ='';
        //   $pco_driver_back ='';

         } 
          
         if($request->session()->get('insurance') == 0) 
         { 
        
           $file3 = '';

         
        } 
        else
        {
       
         $file3 = Storage::putFile($vehicleImagesFolder, new File($request->customer_private_insurance));

        
          // $file_n = $_SERVER['DOCUMENT_ROOT']."/storage/app/".$file3;
          // dd($file_n);
          // C:\\xampp\htdocs\pcocar2\storage\app\documents_uploads/1.pdf

        $im = new Imagick();

        $im->readImage(base_path()."/storage/app/".$file3);
        $filename = rand().'.png';
 

        $im->writeImages(base_path()."/storage/app/documents_uploads/pdf/" .$filename, true);
         } 

$booking_price =$request->session()->get('price');
/*if($request->session()->get('permotion_discount')){
            $price = $request->session()->get('price') * $request->session()->get('permotion_discount') / 100;
            $booking_price =$request->session()->get('price') - $price;
}
*/
if($request->duration==1) $duration=4; else $duration=$request->duration;
if($request->session()->get('permotion_discount')) $promo = $request->session()->get('permotion_discount'); else $promo=0;


$expiry_date = date(Helper::get_expire_date($request->duration).' H:i:s'); 
$booking_start_date = date($request->session()->get('booking_start_date').' H:i:s');
$booking_drop_off_date = date($request->session()->get('booking_drop_off_date').' H:i:s'); 
$dataBooking = [
    'name' => $request->name,
    'user_id' => session()->get('user_id'),//$request->name,
    'vehicle_id' => $request->id,
    'duration' => $duration,
    'miles' => $request->mile,
    'address' => $request->address,
    'expiredate' => $request->expiredate,
    'dob' => $request->dob,
    'dvla' => $request->dvla,
    'national_insu_numb' => $request->national_insu_numb,
    'pco_licence_no' => $request->pco_licence_no,
    'doc_dvla' => $file1,
    'doc_cnic' => $file2,
    'expiry_date' =>   $expiry_date,
    'price'=>  $booking_price,
    'discount'=>  $request->session()->get('f_discount'),
    'booking_status'=>1,   
    'pco_paper_licence'=>$pco_paper_licence,
    'promo' => $promo,
    'doc_other' => $file4,
    'payment_status'=>1,   //later we change it 0, because if we now send it 0 user can do multiple time booking with same id. 0 will be changed when we confirm payment.
    'first_payment'=>  $request->session()->get('price_final_total_book'),
     'booking_start_date' => $booking_start_date,  
     'booking_drop_off_date' => $booking_drop_off_date, 
     'agreement_status'=>1,
     'insurance_type' => $request->session()->get('insurance'),
      'deposit' => $request->session()->get('deposit'),



];
 

//print_r($dataBooking); dd();


        $booking = Booking::create($dataBooking);
       
$booking_id = $booking->id;

         session([
                'booking_id' => $booking_id, 
            ]);


 
 if($booking->insurance_type == 1) 
         { 
 
// $validator = Validator::make($request->all(), [
//     'private_cartificate' => 'mimes:jpg,png, pdf'
// ]); 
 

    $sh =  $request->hr;
    $sm =  $request->min;
     $esh =  $request->ehr;
    $esm =  $request->emin;
 
     $Insurance_started_date = date("$request->Insurance_started_date $sh:$sm:s");

 $Insurance_end_date = date("$request->Insurance_end_date $esh:$esm:s");
     
    

$datacustomerinsurance = [
   'booking_id' => $booking->id,
    'user_id' => session()->get('user_id'),
    'policy_number' => $request->policy_number,
    'cover' => $request->cover,
    'company_name' => $request->Insurance_company_name,
    'email' => $request->Insurance_company_email,
    'contact_number' => $request->Insurance_company_contact_number,
    'start_date' => $Insurance_started_date,
    'end_date' => $Insurance_end_date,
     'customer_private_insurance' => $filename,
     // 'file3' => $file3,


];
 
 
        $data = Customerinsurance::create($datacustomerinsurance);
           
          $customer_insurance_id = $data->id;

         session([ 
                'customer_insurance_id' => $customer_insurance_id, 
            ]);
 
//update status of booking at vehicle table 
        $veh = Vehicle::findOrFail($request->id);     
        
        $veh->bookingStatus =0; 
        $veh->save(); 
//=======================================================

 

       /* return redirect()->route('step3',
            [
                'id'=>$request->id, 
                'step'=>3,
                'mile'=>$request->mile,
                'after_month'=>$request->duration, 
                'booking_id'=>$data->id,
                'price' => $request->price, 
                'booking_id' => $booking_id,


            ]
        )->with('success','Documents Added Successfully');*/


 $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 
  
 return view('frontend.step3', [
  
            'vehicles' => $vehicles, 
            'booking_id'=>   $booking_id,
            'customer_insurance_id'=>$customer_insurance_id,
            'csutomer_insurance_record'=> $data,
            'name' =>$request->name,
            'months'=>   $request->session()->get('months'),
            'price'=>  $request->session()->get('price'),
            'discount' => $request->session()->get('f_discount'),
            'signature_name' => $data->user->name ,
            'signature'=> $data->user->name ,        
            'vehicle' => $veh,
            'booking' => $booking,
            'promotion' => Helper::check_discount(),
            'price_final_total_book'=> $request->session()->get('price_final_total_book'), 
             'insurance' => $request->session()->get('insurance'),
             'deposit' => $request->session()->get('deposit'),


        ]);

}
 
//update status of booking at vehicle table 
        $veh = Vehicle::findOrFail($request->id);     
        
        $veh->bookingStatus =0; 
        $veh->save(); 
//=======================================================
    /* return redirect()->route('step3',
            [
                'id'=>$request->id, 
                'step'=>3,
                'mile'=>$request->mile,
                'after_month'=>$request->duration, 
                'booking_id'=>$data->id,
                'price' => $request->price, 
                'booking_id' => $booking_id,


            ]
        )->with('success','Documents Added Successfully');*/


 $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 

 return view('frontend.step3', [
  
            'vehicles' => $vehicles, 
            'booking_id'=>   $booking_id,
          
            'name' =>$request->name,
            'months'=>   $request->session()->get('months'),
            'price'=>  $request->session()->get('price'),
            'discount' => $request->session()->get('f_discount'),
            'signature_name' => $booking->user->name ,
            'signature'=> $booking->user->name ,        
            'vehicle' => $veh,
            'booking' => $booking,
            'promotion' => Helper::check_discount(),
            'price_final_total_book'=> $request->session()->get('price_final_total_book'), 
             'insurance' => $request->session()->get('insurance'),
             'deposit' => $request->session()->get('deposit'),


        ]);



/*
        foreach($request->file as $documents)
            {
                $imageUrl = Storage::putFile($vehicleImagesFolder, new File($documents));
                 VehicleImage::create(
                    [
                        'vehicle_id'=> $vehicle->id,
                        'images' => $imageUrl
                    ]
                ); 
            }//foreach */




          } //end if





    }//end function


public function generate_pdf(Request $request)
    {
      
      
       $booking_info = Booking::where('id', $request->session()->get('booking_id'))->with('user')->first(); 
      $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 
       $swipes_records = Swipe::where('booking_id',$booking_info->id)->orderBy('id','desc')->first();
      if(count($swipes_records)>0){
        $start_date = $swipes_records->swipe_date_time;
       }
       else{
        $start_date = $booking_info->start_date;
       }  
       $data = [
          'title' => 'PCOCAR',
          'heading' => 'SUBSCRIPTION BOOKING FORM PCOCAR',
          'content' => '',
          'signature_name' => $booking_info->user->name ,
          'signature'=> $booking_info->user->name ,        
          'vehicle' => $vehicles,
          'booking' => $booking_info,
          'start_date'=>$start_date,
            ];
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        
        $pdf = PDF::loadView('frontend.pdf_view', $data);  
        $sheet = $pdf->setPaper('a4', 'portrait');
        $filename = rand().'.pdf';
        

         
    //return $sheet->download('download.pdf');


        //print "<script>window.location='www.google.com'</script>";
        return $sheet->download($filename);

         $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 

return view('frontend.step4', [
  
            'vehicles' => $vehicles, 
       //     'booking_id'=>   $booking_id,
       //     'name' =>$request->name,
             'promotion' => Helper::check_discount(),
              'price_final_total_book' => $request->session()->get('price_final_total_book'),

        ]);

 
    }



    public function generate_pdf_email(Request $request)
    {

 $booking_info = Booking::where('id', $request->id)->with('user')->first();

$vehicles = Vehicle::where('id', $booking_info->vehicle_id)->with('car_make')->with('car_model')->first(); 
       $user = User::where('id', $request->id)->first(); 

     $customer_insurance_id = Customerinsurance::where('booking_id',  $booking_info->id)->with('user')->with('booking')->first(); 
   
     

         

        $swipes_records = Swipe::where('booking_id',$booking_info->id)->orderBy('id','desc')->first();
        $checker =Swipe::where('booking_id',$booking_info->id)->orderBy('id','desc')->exists();

       if($checker){
        $start_date = $swipes_records->swipe_date_time;
        
       }
       else{  
        $start_date = $booking_info->booking_start_date;
   
      
       }
 

         // This  $data array will be passed to our PDF blade
       $data = [
          'title' => 'PCOCAR',
          'heading' => 'SUBSCRIPTION BOOKING FORM PCOCAR',
          'content' => '',
          'signature_name' => $booking_info->user->name ,
          'signature'=> $booking_info->user->name ,   
          'booking' => $booking_info, 
          'customer_insurance_id' => $customer_insurance_id, 
           'vehicle' => $vehicles,    
             'insurance' => $request->session()->get('insurance'),
             'start_date'=>$start_date,

            ];
       
        $pdf = PDF::loadView('frontend.pdf_view', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);
 
    }


//==========swipe vehicle agreements=========================================
public function generate_pdf_email_swipe_old(Request $request){  
  $swipe_id = Swipe::where('id', $request->id)->first();
  $booking_info = Booking::where('id', $swipe_id->booking_id)->with('user')->first();
$vehicles = Vehicle::where('id',  $swipe_id->old_vehicle_id)->with('car_make')->with('car_model')->first(); 
       $user = User::where('id', $swipe_id->booking_id)->first(); 
         // This  $data array will be passed to our PDF blade
       $data = [
          'title' => 'PCOCAR',
          'heading' => 'SUBSCRIPTION BOOKING FORM PCOCAR - CANCELLED',
          'content' => '',
          'signature_name' => $booking_info->user->name ,
          'signature'=> $booking_info->user->name ,   
          'booking' => $booking_info, 
           'vehicle' => $vehicles, 
           'swipe'=>$swipe_id,   
           'cancelled'=>1,
           'start_date' =>$booking_info->booking_start_date,
             'insurance' => $request->session()->get('insurance'),

            ];
        
        $pdf = PDF::loadView('frontend.pdf_view_swipe', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);

 }
 public function generate_pdf_email_swipe_new(Request $request){
 $swipe_id = Swipe::where('id', $request->id)->first();
  $booking_info = Booking::where('id', $swipe_id->booking_id)->with('user')->first();
$vehicles = Vehicle::where('id', $swipe_id->new_vehicle_id)->with('car_make')->with('car_model')->first(); 
       $user = User::where('id', $swipe_id->booking_id)->first(); 
         // This  $data array will be passed to our PDF blade
       $data = [
          'title' => 'PCOCAR',
          'heading' => 'SUBSCRIPTION BOOKING FORM PCOCAR',
          'content' => '',
          'signature_name' => $booking_info->user->name ,
          'signature'=> $booking_info->user->name ,   
          'booking' => $booking_info, 
           'vehicle' => $vehicles, 
           'start_date' =>$swipe_id->swipe_date_time,
           'swipe'=>$swipe_id,   
             'insurance' => $request->session()->get('insurance'),

            ];
        
        $pdf = PDF::loadView('frontend.pdf_view_swipe', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);
 }


 //===========================================================================



  public function step4(Request $request)
    {      



         $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first();
      
         $booking_detail = Booking::where('id',$request->session()->get('booking_id') )->with('user')->first();

     $date = $booking_detail->booking_start_date;//date('Y-m-d'); 
          $date_time = Carbon::parse($date)->addHour();
          $ldate = date('Y-m-d H:i:s');



         if ($ldate > $date_time)
          {
            
        $veh = Vehicle::findOrFail($vehicles->id);  
        $booking = Booking::find($booking_detail->id);
        $booking->booking_status =0; 
        $veh->bookingStatus =0; 
        $veh->save(); 
        $booking->save(); 

return redirect()->route('contactus')->with('message','Booking Time  End.');
        
         
          
          } 
          else
          {
           
           $booking_detail = Booking::where('id',$request->session()->get('booking_id') )->with('user')->first();
          
         $plate_no = preg_replace('/\s+/', '', $vehicles->licence_plate_number);

         $generate_ref_booking = $plate_no.'-'.date('dmy');
         session(['generate_ref_booking' => $generate_ref_booking,  ]);

         return view('frontend.step4', [
             'vehicles' => $vehicles,
            
             'months'=>   $request->session()->get('months'),
             'price'=>  $request->session()->get('price'),
             'discount' => $request->session()->get('f_discount'),
             'booking_detail' => $booking_detail,
             'generate_ref_booking'=> $generate_ref_booking, 
              'promotion' => Helper::check_discount(),
               'price_final_total_book' => $request->session()->get('price_final_total_book'),
               'booking_start_date' => $request->session()->get('booking_start_date'), 
               'insurance' => $request->session()->get('insurance'),
             'deposit' => $request->session()->get('deposit'),
               
        ]);
          }
         

        


        //return view('frontend.browse', ['vehicles' => $vehicles]);
         
         
    }



public function confirm_booking(Request $request){

        // $user = User::where('id',$request->session()->get('user_id'))->first();
        $name =  'test';
        $title = "Booking Status"; 
        $email_to =  'payments@pcocar.com';//$user->email;//'waqas.ger@gmail.com';
        $booking = Booking::where('id', $request->session()->get('booking_id'))->with('vehicle')->with('user')->first();
        $vehicle = Vehicle::where('id', $booking->vehicle_id)->with('car_make')->with('car_model')->first();
        $ses_values = $request->session();

    
        $booking->generate_ref_booking =$request->session()->get('generate_ref_booking'); 
        $booking->save(); 

        $months=  $request->session()->get('months');
      //  $price=   $request->session()->get('price');
        $discount = $request->session()->get('f_discount');
        $price_per_week = $request->session()->get('price');
        $price_total = $request->session()->get('price_final_total_book');
        $booking_start_date =  $request->session()->get('booking_start_date');
       //dd($months);
     

       Mail::to([$email_to,'bookings@pcocar.com'])->cc($request->session()->get('email'))->send(new \App\Mail\Booking(
          $payment_mode=1,
          $name, 
          $title, 
          $booking , 
          $vehicle , 
          $months,
          $discount,
          $price_per_week,
          $price_total,
          $booking_start_date
           )); 
      //  Mail::to($request->session()->get('email'))->send(new \App\Mail\Booking($name, $title, $booking , $vehicle , $months,$discount ));  //cc to customer
        $this->payment_history_generate($booking);        
        return redirect('login');


}



public function payment_history_generate($booking){
      for($i=1;$i<=$booking->duration; $i++){ 
         
         if($i==1) $price = $booking->price + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY') ;  else $price = $booking->price + env('INSURANCE_CODE') ;
          $data = [
        'booking_id' => $booking->id,
        'user_id' => $booking->user_id,
        'amount' => $price,
        'vehicle_id' =>$booking->vehicle_id,
        'payment_status'=>0,
        'payment_mode' =>1,
        
    ];
     PaymentHistory::create($data);

      }
}





public function done(Request $request){

       $booking = Booking::where('id', $request->booking_id)->with('vehicle')->first();  
       $vehicles = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 
       return view('frontend.step4', [
             'vehicles' => $vehicles,
             'months'=>   $request->session()->get('months'),
             'price'=>  $request->session()->get('price'),
             'discount' => $request->session()->get('f_discount'),
             'booking_detail' => $booking_detail,
          //   'generate_ref_booking'=> $generate_ref_booking, 
              'price_final_total_book' => $request->session()->get('price_final_total_book'),
        ]);
       return view('frontend.booking_done',['booking'=>$booking]);
} 



public function booking_list_admin(){
    $bookings = Booking::all();
    return view('admin.booking.index',['bookings'=>$bookings]);

}


public function payment_update_status_cron_job(){
     //$pay_history = PaymentHistory::where('payment_status'.'')->where('payment_mode',2)->whereDate('expire_date', date('Y-m-d'))->get();
/*     $pay_history =  PaymentHistory::where(['payment_mode'=>2, 'expire_date'=>date('Y-m-d')])->update(['payment_status'=>1]);

dd($pay_history);
*/


$updateData = [
    'payment_status' => 1,
    //'datetime' => $datetime->format('Y-m-d H:i:s'),
];

$data = PaymentHistory::WhereNull('payment_mode')->where('expire_date',date('Y-m-d') )->update($updateData);
 

}

 public function upload_information_second(Request $request){   


       $booking = Booking::where('id', $request->id)->with('vehicle')->first();   
      $vehicles = Vehicle::where('id', $booking->vehicle_id)->with('car_make')->with('car_model')->first(); 
      $csutomer_insurance_record = Customerinsurance::where('id', $request->session()->get('customer_insurance_id'))->with('user')->first(); 
               
        session([
               /* 'months' => $request->weeks,
                'miles' => $request->miles,
                'price' => number_format($price,2),
                'v_id' =>  $request->id,
                'discount' =>$this->get_discount_percent($request->weeks),
                'f_discount' =>$this->get_discount_percent($request->weeks),
                'promotion_type' =>Helper::get_flat_discount_amount()[0]->promotion_type,
                'flat_discount_rate' =>Helper::get_flat_discount_amount()[0]->flat_discount_rate,
                'flat_discount_type'=>$request->flat_discount_type,
                'price_final_total_book' =>$price_final_total_book,
                'booking_start_date' => $request->booking_start_date,
                'booking_drop_off_date' => $request->hid_drop_off,*/
                'booking_id' => $booking->id,

                'v_id' =>  $vehicles->id,
                 'price_final_total_book' =>$booking->first_payment,
                 'price' =>  $booking->price,
                'insurance' => $booking->insurance_type,
               
            ]);

           $signature =  $booking->user->name;
           $booking_id =  $booking->id;
$vehicle = Vehicle::where('id', $request->session()->get('v_id'))->with('car_make')->with('car_model')->first(); 
         $data = [
          'insurance'=> $booking->insurance_type,
        'vehicles' => $vehicles, 
        'booking' => $booking,
        'vehicle' => $vehicle,
        'booking_id' => $booking_id,
        'csutomer_insurance_record' => $csutomer_insurance_record,
        'name' =>$request->name,
        'months'=>   $request->session()->get('months'),
        'price'=>  $request->session()->get('price'),
        'discount' => $request->session()->get('f_discount'),
        'signature_name' => $signature ,
        'signature'=> $signature ,     
            'deposit' => $request->session()->get('deposit'),
           
        'promotion' => Helper::check_discount(),
          'price_final_total_book'=> $request->session()->get('price_final_total_book'), 
         ]; 
        
 return view('frontend.step3')->with($data);

 



}



}

