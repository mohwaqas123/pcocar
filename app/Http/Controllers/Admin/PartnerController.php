<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\models\Partner;
use App\models\Booking;
use App\models\Vehicle;
use App\models\Make;
use App\models\CarModel;
use Illuminate\Support\Facades\Mail;


use Session;

class PartnerController extends Controller
{


    public function signup(Request $request){
      request()->validate([ 
            'email' => 'required|unique:users',   
            'password' => 'required',
            'name' => 'required|max:20',
             
            ]);

 
         $data = [
        'name' => $request->name,
        'email' => $request->email,
        'password' => Hash::make($request->password),
        'phone' =>$request->phone,

        
    ];
  
     User::create($data);
     Mail::to($request->email)->send(new \App\Mail\Parntersignup( $request->name, $request->password,  $request->email)); 
     return redirect()->back()->with('message','User Registered Successfully');


    }
 
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
           $car_model = CarModel::all(); 
            $url = "/admin/dashboard";
              $title = 'Add Partner';

          $data = [
             
                'title' =>$title,
                 'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Partner'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Add Partner' ,
           ];  
        return view('admin.partners.create')->with($data); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:partners', 
            'phone' => 'required',
            'business_address' => 'required',
        ]);
           $img_exterior_dashboard='';
           if ($request->hasFile('img_exterior_dashboard')) 
         {
            $vehicleDocFolder = 'accidentalImages';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }
            if ($request->hasFile('img_exterior_dashboard'))
               $img_exterior_dashboard = Storage::putFile($vehicleDocFolder, new File($request->img_exterior_dashboard));
       }
        $generated_pass = rand();   
        $data = [
        'name' => $request->name,
        'business_address' => $request->business_address,
        'pickup_address' => $request->pickup_address,
        'dropoff_address' => $request->dropoff_address,
        'email' => $request->email,
        'password' => Hash::make($generated_pass),
        'phone' =>$request->phone,
         'commission' => $request->commission,
         'address' => $request->address,
         'bank_name' => $request->bank_name,
         'account_number' => $request->account_number,
         'account_holder_name' => $request->account_holder_name,
         'sort_code' => $request->sort_code,
        'profile_pic'=> $img_exterior_dashboard,
    ];
     
      
     Partner::create($data);
     Mail::to($request->email)->send(new \App\Mail\Partnersignup( $request->name, $generated_pass,  $request->email)); 
     return redirect()->back()->with('message','Partner Registered Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




public function adminpartnerdetail(Request $request){
      $partner = Partner::where('id',  $request->id)->first();   
      $vehicle = Vehicle::where('partner_id',  $request->id)->get();   
      
      return view('admin.partners.partnerdetail',['partner'=>$partner, 'vehicles'=>$vehicle]);
} 


 
}
