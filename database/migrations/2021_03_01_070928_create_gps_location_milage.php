<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGpsLocationMilage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gps_location_milage', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('imei')->nullable();
            $table->string('device')->nullable();  
            $table->string('odometer')->nullable(); 
            $table->date('c_date')->nullable(); 
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gps_location_milage');
    }
}
