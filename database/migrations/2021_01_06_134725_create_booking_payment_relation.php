<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingPaymentRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_payment_relation', function (Blueprint $table) {
           $table->bigIncrements('id');
            $table->integer('booking_id')->nullable();
            $table->integer('weekly_payment')->nullable();
            $table->float('price')->nullable();
            $table->boolean('payment_status')->comment('0=not,1=yes')->nullable();
            $table->date('due_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_payment_relation');
    }
}
