 
@extends('layouts.admin')
@section('styles')
@endsection
@section('content')

    <style>
          .swipe p{    margin-bottom: 1px;}
          .swipe{border-bottom: 1px solid #eee;}
        </style>
<div class="content-page">
    <!-- Top Bar Start -->
    <div class="topbar">
        <nav class="navbar-custom">
            <!-- <ul class="list-unstyled topbar-right-menu float-right mb-0">
                <li class="hide-phone app-search">
      


                    <form>
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </li>
             
            </ul> -->
            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left disable-btn">
                    <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    
                </li>
            </ul>
        </nav>
    </div>
    <!-- Top Bar End -->
    <!-- Start Page content -->
    <div class="content">
        <div class="container-fluid">
          @if(session()->has('message'))
    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true">&times;</span>
</button>
        {{ session()->get('message') }}
    </div>
@endif 
 @if($booking->booking_status == 2 )
    <div class="alert alert-danger">
    <h1> {{$booking->detail}}</h1>
  </div>
 
  @else
   
  @endif

            <div class="row">
                <div class="col-md-3">
                    <div class="card-box">
                       <h5>Customer Details</h5>
                       <p>Name: {{$booking->user->name}}</p>
                       <p> Email: {{$booking->user->email}}</p>
                       <p>Phone: {{$booking->user->phone}}</p>
                       <p>DVLA Licence Number: {{$booking->dvla}}</p>
                       <p>DOB: {{date('d-m-Y', strtotime($booking->dob))}}</p>
                       <p>Address: {{$booking->address}}</p>
                       <p> Insurance Number: {{$booking->national_insu_numb}}</p>
                       <p> @if($booking->pco_licence_no) PCO License Number: {{$booking->pco_licence_no}}  @endif</p>
                       
                        <!-- end row -->
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class="card-box">
                        <h5>Vehicle Details</h5>
                     
                        
                       <p>Make: {{$vehicle->car_make->name}} </p>
                       <p>Model: {{$vehicle->car_model->name}}</p>
                       <p>Colour: {{$vehicle->car_model->colour}}</p>
                       <p>Engine Capacity: {{$vehicle->engine_Capacity}}</p>
                       <p>Year: {{$vehicle->year }}</p>
                       <p>Registration Number: {{$vehicle->licence_plate_number }}</p>
                       <p>Postal pickup dropoff: {{$vehicle->postal_pickup_dropoff }}</p>
                        
                        <!-- end row -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-box">
                         <h5>Attachment</h5>
                        <p>  

                           <a href="{{url('/')}}/storage/app/{{$booking->doc_cnic}}">DVLA Licence(Back)</a>  
                        </p>
                        <p>   
                           <a href="{{url('/')}}/storage/app/{{$booking->doc_dvla}}">DVLA Licence(Front)</a>  
                        </p>
                        @if($vehicle->vehicle_category==1) 
                       <p> <a href="{{url('/')}}/storage/app/{{$booking->doc_other}}">PCO Driver Licence Front</a> </p>
                        <p><a href="{{url('/')}}/storage/app/{{$booking->pco_paper_licence}}">PCO Paper Licence</a> </p>
                        @endif
                      <p>  <a href="{{url('/')}}/generate_pdf_email/{{$booking->id}}">Hire Agreement </a>  </p>
                      <p>  <a href="{{url('/')}}/generate_pdf_permission/{{$booking->id}}">Permission Letter</a>  </p>
                      <p>  <a href="{{url('/')}}/generate_pdf_pcn_hire_agreement/{{$booking->id}}">PCN Hire Agreement</a>  </p>




                       @if($booking->insurance_type == 1)
                       <!-- <p>  <a href="{{url('/')}}/generate_pdf_permission/{{$booking->id}}">Customer insurance Certificate</a>  </p> -->
                       @endif
                      <div class="h-100 d-inline-block" style="min-height: 60px">&nbsp;</div>
                     </div>
                </div>

                <div class="col-md-3">
                    <div class="card-box">
                        @php
         $discount_amount = $vehicle->price * ($booking->discount / 100);
        $total = $vehicle->price - $booking->discount;  



       if($booking->reference_booking > 0)

        $grand_tota = $booking->price + env('INSURANCE_CODE'); 
       else
        if($booking->insurance_type == 1)
         $grand_tota = $booking->price +  env('DEPOSIT_SECURITY');
          else


        $grand_tota = $booking->price + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');







          //   $total + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY');
 if($payment_mode=1) $p = "Credit Card Stripe"; else $p = "Bank Deposit option";
if($booking->duration==1)
   $m = '4 weeks';
else
  $m = $booking->duration.' weeks';
@endphp
                      <h5>Booking Details</h5>
         
  <p> Booking Started Date: {{date('d-m-Y',strtotime($booking->booking_start_date))}}</p>
   <p>Expire Date: {{date('d-m-Y', strtotime($booking->booking_drop_off_date))}}</p>
                       <p> Price per week: £{{$booking->price}}</p>
                       <p> Duration: {{$booking->duration}} /Weeks</p>
                       <p> @if ($booking->discount == 0)
                            @else
                            Discount: {{$booking->discount}}%  
                           @endif</p>
                        <p>
                        @if ($booking->insurance_type == 0)
                        Insurance: £{{env('INSURANCE_CODE')}}                        @else
                        @endif
                        </p>
                       @if($booking->reference_booking == 0)
                      <p> Security Deposit: £ {{env('DEPOSIT_SECURITY')}}</p>
                       @else
                       @endif

                        @if ($booking->insurance_type == 1)
                        @php
                        $grand_tota_without_insurance  =  $booking->price + env('DEPOSIT_SECURITY');
                        @endphp
                        <p>Total:£{{$grand_tota_without_insurance}}</p>
                      Payment Mode:  {{$p}} 

                       @else
                     <p>Total:  £{{$grand_tota}}</p>
               <!--        Payment Mode:  {{$p}}  -->
                        @endif



                       <p>  
                        <!-- end row -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card-box">
                          <table class="table table-striped table-bordered"  style="width:100%">
          <thead>
            <tr>
                <th>Date</th>
                 
                <th>Amount</th>
              
                <th>Status</th>
            </tr>
            <tr>
                <td>
                    {{$booking->created_at}}
                     
                </td>    
                <td>
                    £{{$grand_tota}}
                  
                </td>
                <td>
                 
                        <a href="{{ route('/customerpaymenthistory', $booking->id) }}" class="btn btn-success"> Payment Received </a>


                
                </td>
            </tr>
            </thead>
            

  <?php
  $months = $booking->duration;
  if($months==1) $months=4;
$total_week = $months - 1;
  if($booking->insurance_type == 1) $price=0; else $price=70; 

$price =  $booking->price + $price ;
for($i=1;$i<=$total_week;$i++){
 
  $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 

  $date = strtotime( $booking->booking_start_date );

  $date = strtotime($days_add, $date);
  $show_date = date('d/m/Y', $date);
  echo "
  <tr>
  <td>$show_date</td>

          <td>£$price</td>
          </tr>
  ";
     
}
?>   


      <tbody>
  <!--<tr> 
          <td>  
           <br>
           <a class="btn btn-warning btn-sm" onclick='return confirm("Do you want to clear this payment?")' href="{{ route('clearpayment', $booking->id) }}" class="mr-5">
            
          clearpayment </a> </td>
        </tr>-->
      </tbody>
 

     

        </table>

                    </div>
                </div>
               @if($booking->insurance_type == 1)

                <div class="col-md-3">
                    <div class="card-box">
      <h5>Customer's Insurance Details</h5>
 
    <p>policy Number:{{@$customer_insurance->policy_number}}</p>
    <p>Insurance :{{@$customer_insurance->company_name}}</p>
    <p>Cover :{{@$customer_insurance->cover}}</p><!-- 
    <p>Email  :{{@$customer_insurance->email}}</p>
    <p>Contact Number :{{@$customer_insurance->contact_number}}</p> -->
<p>Start :{{date('d-m-Y', strtotime(@$customer_insurance->start_date))}}</p>
    <p>End :{{date('d-m-Y', strtotime(@$customer_insurance->end_date))}}</p>
     </div>
                </div>

@else
    @endif

                <div class="col-md-3">
                    <div class="card-box">
                      @if($swipe)
                         <h5>Swap History</h5>
                        @foreach($swipe as $swipe_history)  
                         <div class="swipe mb-3">
                              <p><b>{{$swipe_history->swipe_date_time}}</b></p>
                              <p><span> {{$swipe_history->old_vehicles->year}} {{$swipe_history->old_vehicles->licence_plate_number}}  </span> <i class="fa fa-arrow-right" aria-hidden="true"></i> <span>{{$swipe_history->new_vehicles->name}} {{$swipe_history->new_vehicles->year}} {{$swipe_history->new_vehicles->licence_plate_number}}</span></p>  
                              <p>{{$swipe_history->description}}</p>
                         </div>
                        @endforeach 
                    @endif    
                     </div>
                </div>


                 
                
            </div>
            </div> 
            <!-- end row -->
            </div> <!-- container -->
            </div> <!-- content -->
            
        </div>
        @endsection
        @section('scripts')
     
        @endsection

    