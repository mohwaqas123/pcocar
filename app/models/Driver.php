<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'name', 'email', 'phone' , 'created_from', 'driver_status','partner_id',
    ];

 /*   public function make()
    {
        return $this->belongsTo('App\models\Make', 'make_id', 'id');
    }
*/
}
