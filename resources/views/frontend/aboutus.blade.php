@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')




      <section class="slice py-7">
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-12 col-md-5 col-lg-6 order-md-2 text-center">
                    <!-- Image -->
                    <figure class="w-100">
                        <img alt="Image placeholder" src="{{ url('/')}}/public/theme3/assets/img/header-car.png" class="img-fluid" />
                    </figure>
                </div>
                <div class="col-12 col-md-7 col-lg-6 order-md-1 pr-md-5">
                    <!-- Heading -->
                    <h1 class="display-4 text-center text-md-left mb-3">
                         
                       About &nbsp<strong class="text-primary">Us</strong>
                    </h1>
                    <!-- Text -->
                    <p class="lead text-center text-md-left text-muted">
                        Business is serving high quality services for more than 10 years and in return we have received great appreciation and satisfaction from all our customers till today.</p>
                </div>
            </div>
        </div>
    </section>






    <section class="slice slice-lg bg-section-secondary">
        <div class="container text-center">
           
            <!-- Pricing -->
                     
            <div class="row justify-content-center">

                               <div class="col-md-8 col-md">
                    <div class="card card-pricing  border border-dark  text-center delimiter-bottom ">
                       
                            <a class="navbar-brand" href="{{ url('/')}}">
                         <img src="{{ asset('public/theme3/assets/img/logo1.png')}}" alt="logo" style="height:60px" />
                         </a>
                        </div>
                    <div class="card card-pricing bg-dark text-center px-3  hover-scale-110">

                        <div class="card-body">
                            <ul class="list-unstyled text-white text-sm opacity-8 mb-4">
                              <p class="h6 text-white">Intercity Hire with Trade Name PCOCAR  is based at heart of East London and specialized in popular & high quality Hybrid & Electric vehicles.</p>


                                <li class="py-2">We  offer short term hire for Mini Cab & Delivery vehicles including Cars & Vans at various pick up and drop off points to ease off customers commutation.</li>
                                <li class="py-2">All our vehicles are fully serviced and well maintained to fulfil regulations and customers demand. </li>
                                <li class="py-2">Our  friendly & trained staff is always there to assist you from booking till the end of hire period.</li>
                                <li class="py-2">Our support lines are open to ensure best quality customer service for any problem caused during or even after the expiry of booking.</li>
                                <li class="py-2">Collection/Drop Off Points:</li>
                                  <p class="py-2 float-left " ><b>London:</b> Walthamstow , Ilford, <br> Stratford, Leyton, Leytonstone, Seven kings, Romford, </p> 
                                    <!-- <li class="py-2">Rainham</li> -->
                                 <li class="py-2 float-right"><b>Manchester:</b> Shudehill<br> Interchange, Manchester. M4 4AA</li>
                                 
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--<div class="mt-5 text-center">-->
            <!--    <p class="mb-2">-->
            <!--        Both pricings contains all 6 months free support. Need more?-->
            <!--    </p>-->
            <!--    <a href="#" class="text-primary text-underline--dashed">Contact us<i data-feather="arrow-right" class="ml-2"></i></a>-->
            <!--</div>-->
        </div>
    </section> 







@include('layouts.footer')
@endsection
@section('scripts')
@endsection
<style >
    .text-primary
    {
        margin-left: -11px !important;
    }
</style>


