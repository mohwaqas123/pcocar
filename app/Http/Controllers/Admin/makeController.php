<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Make;
use App\models\CarModel;
use Validator;
use Redirect;
use Input;

class makeController extends Controller
{
    //list all country
    public function index()
    {
        $makes = Make::select('id', 'name')->orderBy('name', 'ASC')->paginate(10);
        $make = Make::all(); 
        $car_model = CarModel::all(); 
         $url = "/admin/dashboard";
             $title = 'Car Make list';


          $data = [
               'makes'=> $makes,
               'title' =>$title,
               'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Make'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Make List' , 
     
                
        ];  
        return view('admin.make.index')->with($data);
    }

    //show add country form
    public function showAddmake()
    {
             $url = "/admin/dashboard";
             $title = 'Add Car Make';
             $data = [
                'title' =>$title,
             'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Make'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Add New' , 
                
                ]; 
        return view('admin.make.create')->with($data);
    }

    //store new country
    public function store(Request $request)
    {


          $validator = $request->validate([
            'name' => 'required|max:191|unique:makes',
            
        ]);


          $vehicleData = [
            'name' => $request->name, 
        ];
 
        $vehicle = Make::create($vehicleData);

/*




        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191|unique:makes',
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }

        $data = [
                'name' => $request->name,
            ];

        Make::create($data);*/
        return redirect()->route('adminmakeList')->with('success', 'Record Created Successfully');
    }

    //show edit form
    public function showEditmake($id)
    {
        $make = Make::find($id);
        if ($make == null) {
            return redirect()->back()->with('error', 'No Recor Found');
        }



        $url = "/admin/dashboard";
        $title = 'Edit Make';
          $data = [
            'make' => $make,
            'title'=>   $title,
            'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Make'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Edit Make' , 
          

           ]; 

        return view('admin.make.edit')->with($data);
    }
     public function destory($id)
    {  
       

  $makes = Make::findOrFail($id);
      $makes->delete();


     
     return redirect()->route('adminmakeList')->with('success', 'Record delete Successfully');
           
           
    }

    // update country

    public function update(Request $request)
    {
        $make = Make::find($request->id);

        if ($make == null) {
            return redirect()->back()->with('error', 'No Record Found');
        }

        // if($request->name != $country->name){
        $rules['name'] = 'required|max:150|unique:makes';
        // }
        // else{
        //     return redirect()->back()->with('failure', 'Cannot update same name of country');
        // }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput($request->all());
        }

        $makeData = [
            'name' => $request->name,
        ];

        $make->update($makeData);

        return redirect()->route('adminmakeList')->with('success', 'Record Updated Successfully');
    }

    public function geStates($id)
    {
        $states = State::where("country_id",$id)->pluck("name","id");
        return response()->json($states);
    }


}
