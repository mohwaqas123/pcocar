<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
	
      protected $fillable = [
        'name', 'email', 'phone' , 'address', 'description','nid_front','nid_back',
        'profile_img','thumbnail',
		    ];

}
