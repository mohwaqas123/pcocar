<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Booking;
use App\models\Vehicle;
use Illuminate\Support\Facades\Mail;

class bookingvehicleController extends Controller
{
      public function p_customerview(Request $request)
    {
    	
  
		$booking = Booking::where('id', $request->id)->with('vehicle')->first();
		$vehicles = Vehicle::where('id',  $booking->vehicle_id)->first();
		
			return view('partner.booking.booking_detail',[
			 'vehicle' => $vehicles, 
			'booking' => $booking,

			]) ;
    }
}
