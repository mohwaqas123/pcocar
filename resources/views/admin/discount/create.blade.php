@extends('layouts.admin')

@section('styles')

@endsection

@section('content')
<div class="content-page"
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                       
                        <div class="card-box">
                                 
                            <form role="form" action="{{ route('code/create') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="row"> 
                                           <div class="form-group col-md-6">
                                            <label for="country name">Percent Discount<span class="text-danger required">*</span></label>
                                           <input type="text" class="form-control @error('name') is-invalid @enderror" name="percentdiscount"   id="name" placeholder="Enter Percent Discount" required="required">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                           </div>
                                    </div>
                                    <div class="row"> 
                                           <div class="form-group col-md-6">
                                            <label for="country name">Title<span class="text-danger required">*</span></label>
                                           <input type="text" class="form-control @error('name') is-invalid @enderror" name="Title"   placeholder="Enter Title" required="required">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                           </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="status">Discount Code<span class="text-danger required">*</span></label>
                                           <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                           value="{{$randomString}}" name="randomString"    placeholder="Enter Discount Code" required="required">
                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                       <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Date From<span class="text-danger">*</span></label>
    <input type="date" name="date_from" class="form-control {{ $errors->has('date_from') ? ' is-invalid' : '' }}" id="datepicker1" placeholder="Date format 2020-11-10" required="required">
                                                    @if ($errors->has('date_from'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_from') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                   </div>
                                          <div class="row">
                                       <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Date To<span class="text-danger">*</span></label>
                                                    <input type="date" name="date_to" class="form-control {{ $errors->has('date_to') ? ' is-invalid' : '' }}" id="datepicker2" placeholder="Date format 2020-11-10" required="required">
                                                    @if ($errors->has('date_to'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_to') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                   </div>
                                        <input type="radio" id="active" name="status" value="1">
                                      <label for="active">Active</label><br>
                                      <input type="radio" id="inactive"   name="status" value="2">
                                      <label for="inactive">In Active</label>
                                      <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Add New</button>
                                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>


                              </form>


                        </div>
                    </div>

                      <script>
        $('#datepicker1').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });

         $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });
    </script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection

 
