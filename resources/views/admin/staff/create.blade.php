@extends('layouts.admin')
@section('styles')
@endsection
@section('content')


 	<div class="content-page"
    	<div class="content">
        	<div class="container-fluid">
            	<div class="row">
                	<div class="col-md-12">
						<div class="card-box"> 
  							    <form enctype="multipart/form-data" role="form" action="{{ route('store_staff') }}" method="post">
                                {{csrf_field()}}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">Name<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"  id="name" placeholder="Enter staff Name">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Email<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="email"  id="name" placeholder="Enter staff Email">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Phone<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="phone"  id="name" placeholder="Enter staff phone">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Address<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="address"  id="name" placeholder="Enter staff Address">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Job Description<span class="text-danger required">*</span></label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="description"  id="name" placeholder="Enter staff Description">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">N.I.D(Front)<span class="text-danger required">*</span></label>
                                             <input type="file" name="nid_front"  class="form-control form-control-lg" >
                                           </div>
                                          
                                          <div class="form-group col-md-6">
                                            <label for="name">N.I.D(Back)<span class="text-danger required">*</span></label>
                                             <input type="file" name="nid_back"  class="form-control form-control-lg"  placeholder="N.I.D(Back)" >
                                            </div>
                                          <div class="form-group col-md-6">
                                            <label for="name">Profile Image<span class="text-danger required">*</span></label>
                                             <input name="profile_img" id="file_dvla" class="form-control form-control-lg" type="file" placeholder="Profile Imag">
                                          </div>
                                          
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="submit" class="btn btn-info btn-flat">Add</button>
                                             <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                        </div>
                                    </div>
                                  </div>

                              </form>
                      
                    	</div>
					</div>
            	</div>
        	</div>
     	</div>
	</div>

    


@endsection
@section('scripts')
@endsection