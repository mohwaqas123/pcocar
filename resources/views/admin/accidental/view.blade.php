@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-page"><h3> </h3>
  @if(session()->has('message'))
  <div class="alert alert-success">
    {{ session()->get('message') }}
  </div>
  @endif

  @if($booking->int_status == 4 )
   
  <div class="alert alert-danger">
    <h1>This conversation is Disputed.</h1>
  </div>
 
  @else
   
  @endif
   
 
  <div class="content">
    @if($booking->int_status == 3 )
   
  <div class="alert alert-success">
    <h1>This conversation is Settled.</h1>
  </div>
 
  @else
   
  @endif
    <div class="container-fluid" style="position: absolute;width:98%">
     <input type="hidden"  name="id" value="{{$booking->id}}">
   

   
                <div class="col-md-8">
 <div class="card-box">
            
              <h3>Title: {{$title}}</h3>
            
 </div>
 </div>

<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Customer Details</b></p>
                        @if (@$booking->user->name == '')
                        @if (@$booking->staff->name == '')
                        Name: {{@$booking->Purchase->name}} <br>
                        Email: {{@$booking->Purchase->email}} <br>
                        Phone: {{@$booking->Purchase->phone}} <br>
              
                       DOB:{{date('d-m-Y', strtotime(@$booking->Purchase->date))}} <br>
                        @else
                        Name: Staff:{{@$booking->staff->name}} <br>
                        Email: {{@$booking->staff->email}} <br>
                        Phone: {{@$booking->staff->phone}} <br>
              
                       DOB:{{date('d-m-Y', strtotime(@$booking->staff->created_at))}} <br>

                        @endif           
                        @else
                         Name: {{@$booking->user->name}} <br>
                        Email: {{@$booking->user->email}} <br>
                        Phone: {{@$booking->user->phone}} <br>
              
                       DOB:{{date('d-m-Y', strtotime(@$booking->user->dob))}} 
                        @endif
                                                 
                        <!-- end row -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Vehicle Details</b></p>
                        Vehicle: {{$booking->vehicle->name}} <br>
                        Model: {{$booking->vehicle->year}} <br>
                        Price: {{$booking->vehicle->price}} <br>
                        Registration: {{$booking->vehicle->licence_plate_number}} <br>
                         
                
                    </div>
                </div>
                  </div>
                   <div class="col-md-4">
                    <div class="card-box">
                       <p><b>Damage Details</b></p>
                         {{$booking->detail}}
                         
                
                    </div>
                </div>
                </div>
              </div>
      <thead>
        <tr>
          <th>
            @if($booking->doc_mot=='')
            @else
            <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$booking->doc_mot}}">
            <img src="{{ asset('storage/app/'.$booking->doc_mot) }}"  height="250" width="250">
             </a>
            @endif
            </th>
            <th>
            @if($booking->doc_logback=='')
            @else
            <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$booking->doc_logback}}">
            <img src="{{ asset('storage/app/'.$booking->doc_logback) }}"  height="250" width="250">
            </a>
            @endif </th>
            <th>
            @if($booking->img_exterior_front=='')
            @else
            <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$booking->img_exterior_front}}">
            <img src="{{ asset('storage/app/'.$booking->img_exterior_front) }}"  height="250" width="250">
            </a>
              @endif
              </th>
            </tr>
          </thead>
          <br>
          <thead>
            <tr>
              <th>
                @if($booking->img_exterior_front2=='')
            @else
            <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$booking->img_exterior_front2}}">
             <img src="{{ asset('storage/app/'.$booking->img_exterior_front2) }}"  height="250" width="250">
            </a>
            @endif
                 </th>
             <th>
            @if($booking->img_exterior_back=='')
            @else
            <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$booking->img_exterior_back}}">
            <img src="{{ asset('storage/app/'.$booking->img_exterior_back) }}"  height="290" width="290">
            </a>
            @endif
             </th>
             <th>
              @if($booking->img_exterior_dashboard=='')
              @else
              <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$booking->img_exterior_dashboard}}">
              <img src="{{ asset('storage/app/'.$booking->img_exterior_dashboard) }}"  height="250" width="250">
              </a>
                @endif
                  </th>
                </tr>
                
              </thead>
              
              <div class="card card-white post">
                <form method="POST" enctype="multipart/form-data"
                  action="{{ route('admin_accidental_comment') }}" >
                  <input type="hidden" value="{{$booking->id}}" name="id" />
                  {{csrf_field()}}
                  
                  <div class="row">
                    <div class="col-md-12 p-5" >
                      @if($booking->int_status == 3 || $booking->int_status == 4) 
                        @else
                      <label for="make" class="col-form-label">Please Enter Comments<span class="text-danger">*</span></label> 
                      <textarea  type="text" class="form-control w-50" name="detail" id="detail" placeholder="Enter Description"  required> </textarea>
                      <br>
                      <label for="file-input">
                      <img src="https://icon-library.net/images/upload-photo-icon/upload-photo-icon-21.jpg" style="width: 20px;" />
                        </label>
                         <input id="file-input"  name="doc_logback" type="file" />
                         <br> 
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input type="button" onclick="history.back()" class="btn btn-primary" value="Back">
                        
                        <a href="{{ route('/adminsettelmentrecords',$booking->id) }}" onclick="return confirm('You want to Settled this report?')" class="btn btn-success">Settled </a>
                         
                       <a href="{{ route('/admindisputerecords',$booking->id) }}" onclick="return confirm('You want to Disputed this report?')" class="btn btn-danger">Disputed </a>   
                       

                         @endif
                      </div>
                  </div>
         
                </form>
              
              @foreach($comments as $accidental_rec_comments) 
              <div class="col-md-9">
                <div class="card card-white post">
                  <div class="post-heading">
                    <div class="float-left image"> 
                      <i class="fa fa-user user-icon"></i>
                    </div>
                    <div class="float-left meta">
                      <div class="title h5">
                           <div class="card-box">
                        <b>@if($accidental_rec_comments->user_id==0) Admin @else {{$accidental_rec_comments->user->name}} @endif</b>  
                        made a post.
                        <a    href="{{ route('admincommentdestory', $accidental_rec_comments->id) }}" onclick="return confirm('You want to remove your selected Comment?')" ><i class="fa fa-remove"></i> </a>
                        <a    onclick="commentedit({{$accidental_rec_comments->id}})"     data-toggle="modal" data-target="#commit"
                        ><i class="fa fa-edit"></i> </a>

                      <h6 class="text-muted time">{{$accidental_rec_comments->created_at}}</h6>
                    </div>
                  </div>
                    </div>
                  </div>
                  <div class="post-description">
                       
                    <p>{{$accidental_rec_comments->comments}}</p>
                      @if($accidental_rec_comments->attachment=='')
                      @else
                      <a class="navbar-brand" href="{{url('/')}}/storage/app/{{$accidental_rec_comments->attachment}}"><p>View Attachment</p>
                      </a>
                      @endif
                   

                  </div>
                </div>
              </div>
              @endforeach
       </div>    
               
              
            </div>
          </div>
        </div>



        <div class="modal fade"  id="commit"  tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit a Comment</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body"> 
          @foreach($comments as $accidental_rec_comments) 
      <form action="{{ route('admincommentedit',$accidental_rec_comments->id) }}" method="post" enctype="multipart/form-data">
         @csrf
      <input type="hidden" value="{{$accidental_rec_comments->id}}" id="comment_id" name="id"/>
              
              <div class="cum"></div>

               @if($accidental_rec_comments->attachment=='')
                      @else
                      <div class="img22"></div>
                      
                      @endif
             
              <center><input type="submit" name="submit" value="Submit" class="btn btn-primary"></center>
               
          </form>
           </div>

     
        </div>
       
      </div>
    </div>
  </div>
 
        @endsection
        @section('scripts')
        <script>
        function commentedit(id){  
  var result = confirm("Want to Edit?");
   var url_call = "{!! url('/') !!}/edit/comments/"+id ;   
   $('#comment_id').val(id);
            var cid = '#commit'+id;
            $( cid ).hide(); 
            
            $.ajax({ 
                type : 'get',
                url  : url_call,  
                data: { id:id  },
                contentType: 'application/json; charset=utf-8',  
                success :  function(data){  
               var form = "<textarea name='description' id='$accidental_rec_comments->comments'  rows='10' cols='70' class='form-control' placeholder='Enter Description' required>"
                +data.comments+
               " </textarea>"
              var form2 ="<a   href='{{url("/")}}/storage/app/{{$accidental_rec_comments->attachment}}'>  " +data.attachment+ "</a>"
                 $(".cum").append(form,form2);
 } 
        });//ajax
}
</script>
 @endforeach
        @endsection
        <style>
          .fa-edit 
          {
            margin-left: -0px !important;
          }
           .fa-remove   
          {
            margin-left: 350px !important;
          }
          input {
  display: none;
}
        .card-white  .card-heading {
        color: #333;
        background-color: #fff;
        border-color: #ddd;
        border: 1px solid #dddddd;
        width:80%
        }
        .card-white  .card-footer {
        background-color: #fff;
        border-color: #ddd;
        }
        .card-white .h5 {
        font-size:14px;
        //font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        }
        .card-white .time {
        font-size:12px;
        //font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        }
        .post .post-heading {
        height: 95px;
        padding: 20px 15px;
        }
        .post .post-heading .avatar {
        width: 60px;
        height: 60px;
        display: block;
        margin-right: 15px;
        }
        .user-icon{font-size: 60px!important; padding: 6px;
    
    border-radius: 50%;
    border: 1px solid #eee;}

        .post .post-heading .meta .title {
        margin-bottom: 0;
        }
        .post .post-heading .meta .title a {
        color: black;
        }
        .post .post-heading .meta .title a:hover {
        color: #aaaaaa;
        }
        .post .post-heading .meta .time {
        margin-top: 8px;
        color: #999;
        }
        .post .post-image .image {
        width: 100%;
        height: auto;
        }
        .post .post-description {
        padding: 15px;
        }
        .post .post-description p {
        font-size: 14px;
        width: 50%;
        }
        .post .post-description .stats {
        margin-top: 20px;
        }
        .post .post-description .stats .stat-item {
        display: inline-block;
        margin-right: 15px;
        }
        .post .post-description .stats .stat-item .icon {
        margin-right: 8px;
        }
        .post .post-footer {
        border-top: 1px solid #ddd;
        padding: 15px;
        }
        .post .post-footer .input-group-addon a {
        color: #454545;
        }
        .post .post-footer .comments-list {
        padding: 0;
        margin-top: 20px;
        list-style-type: none;
        }
        .post .post-footer .comments-list .comment {
        display: block;
        width: 100%;
        margin: 20px 0;
        }
        .post .post-footer .comments-list .comment .avatar {
        width: 35px;
        height: 35px;
        }
        .post .post-footer .comments-list .comment .comment-heading {
        display: block;
        width: 100%;
        }
        .post .post-footer .comments-list .comment .comment-heading .user {
        font-size: 14px;
        font-weight: bold;
        display: inline;
        margin-top: 0;
        margin-right: 10px;
        }
        .post .post-footer .comments-list .comment .comment-heading .time {
        font-size: 12px;
        color: #aaa;
        margin-top: 0;
        display: inline;
        }
        .post .post-footer .comments-list .comment .comment-body {
        margin-left: 50px;
        }
        .post .post-footer .comments-list .comment > .comments-list {
        margin-left: 50px;
        }
        .bold_font{font-weight: bold;}
        
        /* Create three equal columns that floats next to each other */
        .column {
        float: left;
        width: 28.33%;
        padding: 10px;
        height: 300px; /* Should be removed. Only for demonstration */
        }
        /* Clear floats after the columns */
        .row:after {
        content: "";
        display: table;
        clear: both;
        }
        </style>