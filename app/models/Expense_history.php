<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Expense_history extends Model
{
    protected $table = 'expense_history';
    protected $fillable = [
        'expence_id','images','details','int_status',
    ];
}
