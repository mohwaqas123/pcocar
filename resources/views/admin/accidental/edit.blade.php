@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
 <div class="content-page">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box"> 
                        <div class="card-box">  
                            <form method="POST" enctype="multipart/form-data" action="{{ route('adminaccidentalUpdate') }}" >
						<input type="hidden" value="{{$make->id}}" name="id" />
                            @csrf 
                            <div class="form-row col-md-6 mb-3">
                              <label for="make" class="col-form-label">Title<span class="text-danger">*</span></label>
                              <input type="text" value="{{$make->title}}" name="title" class="form-control" placeholder="Please enter title of Damage Record">
                            </div>
                                <div class="form-row col-md-6">
                                        <label for="make" class="col-form-label">Customer<span class="text-danger">*</span></label>
                                        <select name="name" 
                                        placeholder="Customer Drop Down" 
                                        class="form-control select2" required>
                                        @foreach($user as $car_mod)
                                        <option @if($make->$user ==$car_mod->id) selected @endif  value="{{$car_mod->id}}">{{$car_mod->name}}</option>
                                        @endforeach
                                        </select>
                                </div>
                                <div class="form-row col-md-6 mb-3" id="customer">
                  <label for="make" id="mySelect" class="col-form-label">Supplier</label>
                  <select    name="purchase" placeholder="Purchase Drop Down"
                    class="form-control select2"   >
                    <option value="0">None</option>
                    @foreach($purchase as $purchase_us)
                    <option value="{{$purchase_us->id}}">{{$purchase_us->name}}
                    </option>
                    @endforeach
                  </select>
                </div>
                <div class="form-row col-md-6 mb-3" id="customer">
                  <label for="make" class="col-form-label">Staff</label>
                  <select    name="staff" placeholder="Staff Drop Down"
                    class="form-control select2" id="mySelect"  >
                    <option value="0">None</option>
                    @foreach($staff as $staff_us)
                    <option value="{{$staff_us->id}}">{{$staff_us->name}}
                    </option>
                    @endforeach
                  </select>
                </div>
                                @php
                                  if($make->part && $make->part!='null'){ 
                                   $parts = json_decode($make->part); 
                                    @endphp
                                        <div class="form-group col-md-6">
                                        <label for="make" class="col-form-label">Vehicle Parts<span class="text-danger">*</span></label>
                                        <div class="row">
                                                <div class="col-md-3">
                                                    <input type="checkbox" 
                                                    @if(in_array('Bumper',$parts)) checked="checked" @endif name="chk[]" value="Bumper">Bumper
                                                </div>
                                        <div class="col-md-3">
                                                    <input type="checkbox"
                                                     @if(in_array('Tyer',$parts)) checked="checked" @endif name="chk[]" value="Tyer">Tyer
                                                </div>
                                                <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Decklid',$parts)) checked="checked" @endif name="chk[]" value="Decklid">Decklid
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Front clip',$parts)) checked="checked" @endif name="chk[]" value="Front clip">Front clip
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Radiator',$parts)) checked="checked" @endif name="chk[]" value="Radiator">Radiator
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Brakes',$parts)) checked="checked" @endif  name="chk[]" value="Brakes">Brakes
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Front Mirror',$parts)) checked="checked" @endif name="chk[]" value="Front Mirror">Front Mirror
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Battery',$parts)) checked="checked" @endif name="chk[]" value="Battery">Battery
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Front Axle',$parts)) checked="checked" @endif name="chk[]" value="Front Axle">Front Axle
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('roof box',$parts)) checked="checked" @endif name="chk[]" value="roof box ">Roof Box 
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"  @if(in_array('Radio',$parts)) checked="checked" @endif name="chk[]" value="Radio">Radio
                                                </div>
                                                 <div class="col-md-3">
                                                <input type="checkbox"
                                                 @if(in_array('Mirror',$parts)) checked="checked" @endif  name="chk[]"value="Mirror">Mirror
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" @if(in_array('Head Light',$parts)) checked="checked" @endif name="chk[]" value="Head Light">Head Light
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"  @if(in_array('Handle',$parts)) checked="checked" @endif  name="chk[]" value="Handle">Handle
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" 
                                                    @if(in_array('Door',$parts)) checked="checked" @endif name="chk[]" value="Door">Door
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"  @if(in_array('Turn Signal',$parts)) checked="checked" @endif  name="chk[]" value="Turn Signal">Turn Signal
                                                </div>
                                            </div>
                                             
                                        </div>
@php
}
else{
    @endphp

                                        <div class="form-group col-md-6">

                                            <label for="make" class="col-form-label">Vehicle Parts<span class="text-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="checkbox" 
                                                      name="chk[]" value="Bumper">Bumper
                                                </div>
                                            <div class="col-md-3">
                                                    <input type="checkbox"
                                                      name="chk[]" value="Tyer">Tyer
                                                </div>

                                                 <div class="col-md-3">
                                                    <input type="checkbox" name="chk[]" value="Decklid">Decklid
                                                </div>

                                                 <div class="col-md-3">
                                                    <input type="checkbox"   name="chk[]" value="Front clip">Front clip
                                                </div>

                                                 <div class="col-md-3">
                                                    <input type="checkbox"  name="chk[]" value="Radiator">Radiator
                                                </div>

                                                 <div class="col-md-3">
                                                    <input type="checkbox"   name="chk[]" value="Brakes">Brakes
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"   name="chk[]" value="Front Mirror">Front Mirror
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" name="chk[]" value="Battery">Battery
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"  name="chk[]" value="Front Axle">Front Axle
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" name="chk[]" value="roof box ">Roof Box 
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"   name="chk[]" value="Radio">Radio
                                                </div>
                                                 <div class="col-md-3">
                                                <input type="checkbox"
                                                   name="chk[]"value="Mirror">Mirror
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"  name="chk[]" value="Head Light">Head Light
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"   name="chk[]" value="Handle">Handle
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox" 
                                                     name="chk[]" value="Door">Door
                                                </div>
                                                 <div class="col-md-3">
                                                    <input type="checkbox"     name="chk[]" value="Turn Signal">Turn Signal
                                                </div>
                                            </div>
                                        </div>
                                            @php
                                            }
                                            @endphp
                                            <div class="form-row col-md-6">
                                            <label for="make" class="col-form-label">Vehicle<span class="text-danger">*</span></label>
                                            <select   name="licence_plate_number" placeholder="Customer Drop Down" 
                                            class="form-control select2" required>
                                            @foreach($vehicle as $car_mod)
                                            <option @if($make->model ==$car_mod->id) selected @endif value="{{$car_mod->id}}">{{$car_mod->licence_plate_number}}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                            <br>
                                     <div class="form-row col-md-6">
                                     <label for="make" class="col-form-label">Price<span class="text-danger">*</span></label>
                                         <input type="text" class="form-control @error('name') is-invalid @enderror"  value="{{ $make->price }}" name="price"   placeholder="Enter the Amount"  required>
                                        </div>
                                        <br>
                                     <div class="form-row col-md-6">

                                        <label for="make" class="col-form-label">Detail<span class="text-danger">*</span></label>

                                 	 <textarea  type="text" class="form-control @error('name') is-invalid @enderror" name="detail" id="detail" placeholder="Enter Detail"  required>
                                 	 	{{$make->detail	}}
                                 	 </textarea>
                                    </div>
                                 <br>
                                 <p>Please select your Status:</p>
                                      <input type="radio" id="male" name="int_status" required="required" value="{{$make->int_status}}" >
                                      <label for="male">Settle </label><br>
                                      <input type="radio" id="female" name="int_status" value="{{$make->int_status}}">
                                      <label for="female">Dispute</label><br>
                                       <div class="form-row">
                                        <p class="col-form-label">Upload  Images
                                               <br>
                                                <input type="file" id="images" name="images[]" multiple>
                                                </p> 
                                                @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                                @endif
                                              </div>

                              <!--  <div class="bg-dev">

                                                 

                                                

                                                <div class="form-row">

                                                    <div class="form-group col-md-4">

                                                        <label for="price" class="col-form-label">Image 1</label>
  <input type="file" name="doc_mot"   class="form-control">
<p> @if($make->doc_mot)
    <a target="_blank" href="{{url('/storage/app/').'/'.$make->doc_mot}}"> {{$make->doc_mot}}</a>  @endif</p>
                                                    </div>

                                                    <div class="form-group col-md-4">

                                                        <label for="offer" class="col-form-label">Image 2</label>

                                                        <input type="file" name="doc_logback"   class="form-control">
<p> @if($make->doc_logback)   
    <a target="_blank" href="{{url('/storage/app/').'/'.$make->doc_logback}}"> {{$make->doc_logback}}</a>  @endif</p>

                                                    </div>

                                                    <div class="form-group col-md-4">

                                                        <label for="offer" class="col-form-label">Image 3</label>

                                                        <input type="file" name="doc_phv"   class="form-control">
                         <p> @if($make->img_exterior_front)   
    <a target="_blank" href="{{url('/storage/app/').'/'.$make->img_exterior_front}}"> {{$make->img_exterior_front}}</a>  @endif</p>

                                                    </div>

                                                    <div class="form-group col-md-4">

                                                        <label for="offer" class="col-form-label">Image 4</label>

                                                        <input type="file" name="img_exterior_back"   class="form-control">
                                                        <p> 
                                                @if($make->img_exterior_front2)   
    <a target="_blank" href="{{url('/storage/app/').'/'.$make->img_exterior_front2}}"> {{$make->img_exterior_front2}}</a>  @endif</p>

                                                    </div>

                                               

                                                <div class="form-group col-md-4">

                                                        <label for="offer" class="col-form-label">Image 5</label>

                                                        <input type="file" name="img_exterior_front2"   class="form-control">
                                                        <p> 
                                                @if($make->img_exterior_back)   
                                                 <a target="_blank" href="{{url('/storage/app/').'/'.$make->img_exterior_back}}"> {{$make->img_exterior_back}}</a>  @endif</p>

                                                    </div>

                                             

                                                <div class="form-group col-md-4">

                                                        <label for="offer" class="col-form-label">Image 6</label>

                                                        <input type="file" name="img_exterior_dashboard"   class="form-control">
                                                        <p> 
                                                     @if($make->img_exterior_dashboard)   
                                                    <a target="_blank" href="{{url('/storage/app/').'/'.$make->img_exterior_dashboard}}"> {{$make->img_exterior_dashboard}}</a>  @endif</p>

                                                    </div>
       
                                               <div class="form-group col-md-4">
                                                    <p class="col-form-label">Upload   Pic(JPG, JPEG, PNG)<span class="text-danger">*</span></p>
                                                    <input type="file" name="thumbnail"  class="form-control"> 
                                                <img src="{{ asset('storage/app/'.$make->image) }}" class="img-thumbnail" style="width: 100px" alt="..." />
                                                
                                                    @if ($errors->has('thumbnail'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('thumbnail') }}</strong>
                                                    </span>
                                                    @endif
                                                </div> 
                                               

                                            </div>

                                                </div>-->
                                                 </div>
                                         </div>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
              </div>
                </div>
            </div>
            <style>
.select2-container .select2-selection--single{

height:34px !important;

}

.select2-container--default .select2-selection--single{

border: 1px solid #ccc !important;

border-radius: 0px !important;

}

.bg-dev{border: 1px solid #000;}

.header-title{background: #eee;

padding: 5px;}

</style>

@endsection

@section('scripts')

@endsection