@extends('layouts.app')

@section('styles')

@endsection

@section('content')
 


  @include('layouts.header')


    <section class="slice slice-lg pt-lg-12 pb-0 pb-lg-12 bg-section-secondary">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
               
                   Booking duration
                   <form action="{{ route('browse') }}" method="get" ifor 1 Monthd="frm_duration">
                     {{csrf_field()}}
                   <select name="duration" id="duration" onchange="this.form.submit()" class="form-group select_custom">
                        
                        <option @if($duration && $duration==4) selected="selected" @endif value="4">4 Weeks</option>
                        <option @if($duration && $duration==8) selected="selected" @endif value="8">8 Weeks</option>
                        <option @if($duration && $duration==12) selected="selected" @endif value="12">12 Weeks</option>
                        <option @if($duration && $duration==16) selected="selected" @endif value="16">16 Weeks</option>
                        <option  @if($duration && $duration==20) selected="selected" @endif value="20">20 Weeks</option>
                        <option @if($duration && $duration==24) selected="selected" @endif value="24">24 Weeks</option>
                        <option @if($duration && $duration==26) selected="selected" @endif value="28">28 Weeks</option>
                        <option @if($duration && $duration==28) selected="selected" @endif value="32">32 Weeks</option>
                        <option @if($duration && $duration==32) selected="selected" @endif value="36">36 Weeks</option> 
                        <option @if($duration && $duration==40) selected="selected" @endif value="40">40 Weeks</option> 
                        <option @if($duration && $duration==44) selected="selected" @endif value="44">44 Weeks</option> 
                        <option @if($duration && $duration==48) selected="selected" @endif value="48">48 Weeks</option> 
                        <option @if($duration && $duration==52) selected="selected" @endif value="52">52 Weeks</option>  

                   </select></form> <br >
   <form action="{{ route('browse') }}" method="get" id="frm_duration">  
   {{csrf_field()}}
    
 
    
                   Monthly Budget 
 <input type="range" step="1000" class="border-0 range" min="1000" max="20000">
 <div class="code">
 £<code></code>  
</div>
<div>
  Vehicle Make  
                   <select name="car_make" id="" class="form-group select_custom">
                       <option value="">Any</option>
                       @foreach($car_make as $make)
                        <option value="{{$make->id}}">{{$make->name}}</option>
                       @endforeach  
                   </select>  
</div>



<div>
  Vehicle Model
                   <select name="car_model" id="" class="form-group select_custom">
                     <option value="">Any</option>
                            @foreach($car_model as $model)
                        <option value="{{$model->id}}">{{$model->name}}</option>
                       @endforeach
                   </select>  
</div>

<div>
  Vehicle Year
                   <select name="" id="" class="form-group select_custom">
                       <option value="">Any</option>
                       <option value="">2020</option>
                       <option value="">2019</option>
                       <option value="">2018</option> 
                   </select>  
                   
</div>

<div>
  Fuel Type
                   <select name="" id="" class="form-group select_custom">
                       <option value="">Any</option>
                       <option value="Petrol">Petrol</option>
                       <option value="Diesel">Diesel</option>
                       <option value="Gasoline">Gasoline</option> 
                   </select>  
                   
</div>




<input name="btn_submit" type="submit" class="btn btn-primary btn-sm" value="Find" />
<p><a href="">Rest</a></p>
<!-- <input name="btn_submit" type="button" onclick="window.location='{{ url('/')}}/browse'" class="btn btn-primary btn-sm center" value="Reset" />  -->

</form>

                </div>
                <div class="col-md-10">
                    <div class="row">
                      <div class="col-md-6"> <h2>{{count($vehicles)}} Models Available</h2></div>
                      <div class="col-md-6" align="right"> 
                        <select name="" id="" class="form-group">
                             <option value="">Filter</option>
                             <option value="1">Price High to Low</option>
                             <option value="2">Price Low to High</option>
                             <option value="3">Best Value</option> 
                        </select>  
                      </div>
                    </div>
                   


  <div class="row">                     
@foreach($vehicles as $cab)
           
                    <div class="col-md-4">
                        <div class="card" >
                         <a href="{{ route('car_detail', ['id' => $cab->id]) }}"> <img src="{{ asset('storage/app/'.$cab->image) }}" class="card-img-top img-thumbnail" alt="..."> </a>
                          <div class="card-body">
                            <p class="card-text">
                                 <b>{{$cab->year}} </b><br>
                                <span>{{$cab->car_make->name}}, {{$cab->car_model->name}}</span>
                               
@php
if($duration && $duration!='') 
$price = $duration * $cab->price;
else

$price = $cab->price ;

@endphp

 <div><span class="bold">£ {{$price}}/week</span> </div>
                                   
 
                            </p>
                          </div>
                     </div>
                 </div> 

@endforeach
 </div>


 
 {!! $vehicles->links() !!}



 
                
            

 
                       
                </div>
            </div>    
           
        </div>
    </section>
   
     

     @include('layouts.footer')
   
 

</body>


@endsection

@section('scripts')

@endsection

<style>
  .img-thumbnail{height: 230px!important;}
</style>