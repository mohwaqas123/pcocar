<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PCOCAR Vehicle Mileage</title>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
  </head>
  <body>
    
    <div class="container">
      <div class="row" style="margin-top:4px;">
        <div class="col-md-9" align="center"><img src="https://pcocar.com/public/theme3/assets/img/logo1.png" alt="logo" style="height:60px"></div>
        <div class="col-md-2"><span class=""><a href="{{ route('vehicle_mileage_logout') }}"  class="mt-3">Log Out</a></span></div>
      </div>
      @if(session()->has('message'))
      <div class="alert alert-danger"  >
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        {{ session()->get('message') }}
      </div>
      @endif
      @if(session()->has('error'))
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        {{ session()->get('error') }}
      </div>
      @endif
      <div class="row">
        <div><h3>PCOCAR  Vehicles Mileage</h3></div><br><br>
        <form method='post' action="{{ route('search_customer_records') }}"> {{csrf_field()}}
          
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <div class='input-group date' id='datetimepicker1'>
                  <input value="{{@$date_to}}" type='text' name="date_to" class="form-control" placeholder="Date From"  id='id'/>
                  <input type="hidden" value="{{@$date_to}}" name="date_to" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                  <input value="{{@$date_from}}" type='text' name="date_from" class="form-control" placeholder="Date To" />
                  <input type="hidden" value="{{@$date_from}}" name="date_from" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-md-4"><input type='submit' class="btn" name='but_search' value='Search'>
              <!--  <a  href="{{url('/')}}/generate_report_mileage"  type='submit' class="btn btn-success"> Download in PDF </a> --></div>
            </div>
          </form>
          
          <form method='get' action="{{ route('generate_report_mileage') }}">
            <input type="hidden" value="{{$date_to}}" name="date_to" />
            <input type="hidden" value="{{$date_from}}" name="date_from" />
            <input  type='submit' class="btn btn-success" value='Download in PDF'>
          </form>
          <br>
          
          <table class="table" width='28%'><tr>
            <!--  <th>Fleet Name</th> -->
            <th>Registration Number</th>
            <th>Make</th>
            <th>Model</th>
            <!--  <th>Imei</th> -->
            <th>Mileage(mi)</th>
          </tr>
          @foreach ($get_gps_location_rec as $rec)
          <?php
          $insurance_rec_diff_reading=0;
          $registration = explode('-',$rec->name);
          $vehicle = App\models\Vehicle::with('car_make')->with('car_model')->where('licence_plate_number', $registration[0])->first();
          //$vehicle = DB::table('vehicles')->with('car_make')->where('licence_plate_number', $registration[0])->first();
          //$get_milage = GpsLocationMilage::where('c_date', '2021-03-03')->where('imei',$rec->imei)->first();
          $get_milage = DB::table('gps_location_milage')->where('c_date', $date_from)->where('imei',$rec->imei)->first();
          $milage_calculate =   $get_milage->odometer - $rec->odometer;
          
 


//================= insurance milage deduct reading=======================
  $get_milage_insurance_first = DB::table('gps_location_milage')->whereBetween('c_date', [$date_to, $date_from])->where('insurance_type',1)->where('imei',$rec->imei)->first();
  $get_milage_insurance_last = DB::table('gps_location_milage')->whereBetween('c_date', [$date_to, $date_from])->where('insurance_type',1)->where('imei',$rec->imei)->orderBy('id', 'desc')->first();
  //print_r($get_milage_insurance_last->odometer);
   //dd();
  /*
 $insurance_rec_first_reading = $get_milage_insurance_first->odometer; 
 $insurance_rec_last_reading = $get_milage_insurance_last->odometer;
*/
 $insurance_rec_diff_reading =   @$get_milage_insurance_last->odometer - @$get_milage_insurance_first->odometer ;

//===============================================================================================
//dd( $insurance_rec_diff_reading);
//total
 $milage_calculate = $milage_calculate - @$insurance_rec_diff_reading;
 

          if($vehicle && $vehicle->insurance_type==0){
          echo  "<tr>
            <td>".$registration[0]."</td>
            <td>".@$vehicle->car_make->name."</td>
            <td>".@$vehicle->car_model->name."</td>
            
            <td>".round($milage_calculate * $factor,2)."</td>
          </tr>";
          }
          ?>
          @endforeach

@foreach ($get_vehicles_partner as $vehicles_partner)

<tr>
  <td>
    {{$vehicles_partner->licence_plate_number}}
  </td>
  <td>
    {{$vehicles_partner->car_make->name}}
  </td>
  <td>
    {{$vehicles_partner->car_model->name}}
  </td>
  <td>
    0.00
  </td>
  
</tr>

           @endforeach

        </table>
      </div></div>
    </body></html>
    
    <script>
    $(function() {
    var d = new Date();
    d.setDate(d.getDate()-1)
    // alert(d.toLocaleString());
    $('#datetimepicker1').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate:d.toLocaleString(),
    // minDate:-61,
    
    });
    $('#datetimepicker2').datetimepicker({
    format: 'YYYY-MM-DD',
    maxDate:d.toLocaleString(),
    
    });
    });
    
    
    </script>

