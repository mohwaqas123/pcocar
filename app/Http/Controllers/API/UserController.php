<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Device;
use App\models\LikeVideo;
use App\models\CommentsVideo;
use App\models\Follower;
use App\models\Following;
use App\models\Video;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Config;
use Str;
use App\Mail\OTP;
use Storage;
use Illuminate\Http\File;

class UserController extends Controller
{
    private $responseConstants;
    private $userConstants;

    public function __construct()
    {
        $this->responseConstants = Config::get('constants.RESPONSE_CONSTANTS');
        $this->userConstants = Config::get('constants.USER_CONSTANTS');
    }

    public function viewProfile(Request $request)
    {
        $rules = [
            $this->userConstants['KEY_USER_ID'] => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user_id = JWTAuth::toUser($request->token)->id;
        $user_profile = User::find($request->get($this->userConstants['KEY_USER_ID']));
        $videos = [];
        $followers = 0;
        $followings = 0;
        $is_follow = 0;
        $likes = 0;

        $follower = Follower::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->where('follower_id', $user_id)->first();
        if($follower == null)
        {
            $is_follow = 0;
        }
        else
        {
            $is_follow = 1;
        }
        
        
        if(empty($user_profile)){
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => "No User Found",
            ]);
        }

        if($user_profile != null)
        {
            $user_profile->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at'
            ]);
        }

        $followers = Follower::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->count();
        $followings = Following::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->count();
        $likes = LikeVideo::where('liked_by', $request->get($this->userConstants['KEY_USER_ID']))->where('type', 1)->count();
        $videos = Video::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->get();

        $user_profile['total_likes'] = $likes;
        $user_profile['total_followers'] = $followers;
        $user_profile['total_following'] = $followings;
        $user_profile['is_follow'] = $is_follow;
        $user_profile['videos'] = $videos;       

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => $this->responseConstants['MSG_LOGGED_IN'],
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $user_profile,
        ]);
    }

    public function viewProfileWithoutAuth(Request $request)
    {
        $rules = [
            $this->userConstants['KEY_USER_ID'] => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user_profile = User::find($request->get($this->userConstants['KEY_USER_ID']));
        $videos = [];
        $followers = 0;
        $followings = 0;
        $is_follow = 0;
        $likes = 0;
        
        if(empty($user_profile)){
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => "No User Found",
            ]);
        }

        if($user_profile != null)
        {
            $user_profile->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at'
            ]);
        }

        $followers = Follower::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->count();
        $followings = Following::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->count();
        $likes = LikeVideo::where('liked_by', $request->get($this->userConstants['KEY_USER_ID']))->where('type', 1)->count();
        $videos = Video::where('user_id', $request->get($this->userConstants['KEY_USER_ID']))->get();

        $user_profile['total_likes'] = $likes;
        $user_profile['total_followers'] = $followers;
        $user_profile['total_following'] = $followings;
        $user_profile['is_follow'] = $is_follow;
        $user_profile['videos'] = $videos;       

        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'User profile view',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $user_profile,
        ]);
    }

    public function checkUserName(Request $request)
    {
        $rules = [
            $this->userConstants['KEY_USER_USERNAME'] => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $username = User::where('username', $request->get($this->userConstants['KEY_USER_USERNAME']))->first();

        if(empty($username))
        {
            // dd($request->get($this->userConstants['KEY_USER_DEVICE_ID']));
            User::create([
                'username' => $request->get($this->userConstants['KEY_USER_USERNAME']),
                'device_id' => $request->get($this->userConstants['KEY_USER_DEVICE_ID']),
                'device_type' => $request->get($this->userConstants['KEY_USER_DEVICE_TYPE']),
                'user_type' => 2,
            ]);

            $data = User::where('username', $request->get($this->userConstants['KEY_USER_USERNAME']))->first();

            if($data != null)
            {
                $data->makeHidden([
                    'created_at', 'updated_at', 'user_type', 'email_verified_at', 'name', 'email', 'phone', 'profile_image', 'cover_video', 'date_of_birth', 'OTP', 
                    'is_verified', 'is_registered', 'device_id', 'device_type', 'description', 'website', 'facebook', 'youtube', 'twitter', 'instagram', 'user_firebase_id'
                ]);
            }

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'This username is available.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $data,
            ]);
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'This username is already taken.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }

    }

    public function checkEmail(Request $request)
    {
        $rules = [
            $this->userConstants['KEY_USER_ID'] => 'required',
            $this->userConstants['KEY_USER_EMAIL'] => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $UserbyId = User::find($request->get($this->userConstants['KEY_USER_ID']));

        if(empty($UserbyId))
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'This user does not exists',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }

        $emailUser = User::where('email', $request->get($this->userConstants['KEY_USER_EMAIL']))->first();

        $otp = Str::random(4);

        
        if(empty($emailUser))
        {  

            $userData = [
                'OTP' => $otp,
                'email' => $request->get($this->userConstants['KEY_USER_EMAIL'])
            ];

            $UserbyId->update($userData);

            \Mail::to($request->get('email'))->send(new \App\Mail\OTP($UserbyId));

            $finalUser = User::where('email', $request->get($this->userConstants['KEY_USER_EMAIL']))->first();

            $finalUser->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at', 'name', 'phone', 'profile_image', 'cover_video', 'date_of_birth', 'OTP', 
                'is_verified', 'is_registered', 'device_id', 'device_type', 'description', 'website', 'facebook', 'youtube', 'twitter', 'instagram'
            ]);

            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'OTP has been send to this email address.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $finalUser,
            ]);
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'This user email already exists',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
    }

    public function checkPhone(Request $request)
    {
        $rules = [
            // $this->userConstants['KEY_USER_ID'] => 'required',
            $this->userConstants['KEY_USER_PHONE'] => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $validator->messages()->first(),
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        // $UserbyId = User::find($request->get($this->userConstants['KEY_USER_ID']));

        // if(empty($UserbyId))
        // {
        //     return response()->json([
        //         'status' => $this->responseConstants['STATUS_ERROR'],
        //         'message' => 'This user does not exists',
        //         'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
        //     ]);
        // }

        $phoneUser = User::where('phone', $request->get($this->userConstants['KEY_USER_PHONE']))->first();

        if(empty($phoneUser))
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Phone Number is available.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'Phone number is not Available.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
    }

    public function saveDevice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'device_id' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        Device::create([
            'device_number' => $request->get('device_id'),
        ]);
        
        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'Device has been saved.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
        ]);
    }

    public function checkOTP(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',   
            'otp' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user = User::find($request->get('user_id'));
        
        if($request->get('otp') == $user->OTP)
        {

            $user->makeHidden([
                'created_at', 'updated_at', 'user_type', 'email_verified_at', 'profile_image', 'cover_video', 'OTP'
                , 'device_id', 'device_type', 'description', 'website', 'facebook', 'youtube', 'twitter', 'instagram', 'is_verified', 'is_registered'
            ]);
            
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'OTP has been matched.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                'data' => $user,
                
            ]);
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'OTP does not match.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
    }

    public function updateProfile(Request $request)
    {
        
        $userID = JWTAuth::toUser($request->token)->id;
        
        $user = User::find($userID);
        
        if(empty($user))
        {
            return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user does not exists',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
        }

        $userData = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'description' => $request->get('description'),
            'website' => $request->get('website'),
            'facebook' => $request->get('facebook'),
            'youtube' => $request->get('youtube'),
            'twitter' => $request->get('twitter'),
            'instagram' => $request->get('instagram'),
        ];
        $user->update($userData);

        if(!empty($request->file('profile_image')))
        {
            $userProfileImage = 'userprofileimage';

            if(!Storage::exists($userProfileImage)){
                Storage::makeDirectory($userProfileImage);
            }
            // dd($request->file('profile_image'));
            $userProfileImageURL = Storage::putFile($userProfileImage, new File($request->file('profile_image')));
            $user->update([
                'profile_image' => $userProfileImageURL
            ]);
        }

        if(!empty($request->file('cover_video')))
        {
            $userProfileCoverVideo = 'userprofilecoverVideo';

            if(!Storage::exists($userProfileCoverVideo)){
                Storage::makeDirectory($userProfileCoverVideo);
            }

            $userProfileCoverVideoURL = Storage::putFile($userProfileCoverVideo, new File($request->file('cover_video')));
            $user->update([
                'cover_video' => $userProfileCoverVideoURL
            ]);
        }

        $user->makeHidden([
            'created_at', 'updated_at', 'user_type', 'email_verified_at', 'OTP'
            , 'device_id', 'device_type', 'is_verified', 'is_registered'
        ]);
        
        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'This user data has been updated.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            'data' => $user
        ]);

    }

    public function deletePhone(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',   
        ]);

        $message = $validator->errors()->first();

        if ($validator->fails()) {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => $message,
                'response_code' => $this->responseConstants['INVALID_PARAMETERS_CODE'],
            ]);
        }

        $user = User::where('phone', $request->get('phone'))->first(); 

        if(empty($user))
        {
            return response()->json([
                    'status' => $this->responseConstants['STATUS_ERROR'],
                    'message' => 'This user does not exists',
                    'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
                ]);
        }

        $user->delete();
        
        return response()->json([
            'status' => $this->responseConstants['STATUS_SUCCESS'],
            'message' => 'This user data has been deleted.',
            'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
        ]);
    }

    public function logout()
    {
        if(JWTAuth::invalidate(JWTAuth::getToken()))
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_SUCCESS'],
                'message' => 'User has been logged out.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
        else
        {
            return response()->json([
                'status' => $this->responseConstants['STATUS_ERROR'],
                'message' => 'This user cannot be logged out.',
                'response_code' => $this->responseConstants['RESPONSE_CODE_SUCCESS'],
            ]);
        }
    }

}
