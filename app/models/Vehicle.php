<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'name',
        'hide_reason',
        'make', 
        'model',
         'year', 
         'purchase_id',
        'fuel', 
        'price',
         'offer',
     //    'no_of_Passengers',
         'engine_Capacity',
        'toll_Charges',
         'description',
        'vehicle_category',
        'vehicle_type',
         'image',
         'city','colour',
         'fuel_type',
         'featurs',
         'postal_pickup_dropoff',
        'date_from',
        'date_to',
        'doc_mot',
        'doc_logback',
        'img_exterior_front',
        'img_exterior_front2',
        'img_exterior_back',
        'img_exterior_dashboard', 
        'doc_phv',
        'licence_plate_number',
        'partner_id',
        'company_name',
        'company_address',
 

    ];

  public function partners()
    {
        return $this->belongsTo('App\models\Partner','partner_id', 'id');
    }


    public function car_model()
    {
        return $this->belongsTo('App\models\CarModel','model', 'id');
    }

    public function car_make()
    {
        //return $this->hasMany('App\models\Make');
        return $this->belongsTo('App\models\Make','make','id');
    }
     public function car_purchase()
    {

        return $this->belongsTo('App\models\Purchase','purchase','id');
    }


    public function vehicle_images()
    {
        return $this->hasMany('App\models\VehicleImage');
        //return $this->belongsTo('App\models\Make','make','id');
    }


}
