@extends('layouts.partner')
@section('styles')
@endsection
@section('content')

<!-- date picker -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>   
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />



<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<style>
.select2-container .select2-selection--single{
height:34px !important;
}
.select2-container--default .select2-selection--single{
border: 1px solid #ccc !important;
border-radius: 0px !important;
}
.bg-dev{border: 1px solid #000;}
.header-title{background: #eee;
padding: 5px;}
</style>
<div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div class="card-box">
                            
                            @if($errors->any())
                            {!! implode('', $errors->all('<div class="danger">:message</div>')) !!}
                            @endif
                            <form action="{{ route('p_vehile_add') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="bg-dev">
                                    <h4 class="m-t-0 header-title">Add NEW CAR</h4>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Licence Plate Number<span class="text-danger">*</span></label>
                                            <input max="10" min="3" type="text" name="licence_plate_number" class="form-control {{ $errors->has('licence_plate_number') ? ' is-invalid' : '' }}" id="" placeholder="Licence Plate Number" required>
                                            @if ($errors->has('licence_plate_number'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('licence_plate_number') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="make" class="col-form-label">Select Uber(Type)<span class="text-danger">*</span></label>
                                            <select id="uber_type" name="uber_type" class="form-control select2" required>
                                                
                                                <option value=1>Uber Lx</option>
                                                <option value=2>UberX</option>
                                                <option value=3>Uber Exec</option>
                                                <option value=4>Uber Lux</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="song name" class="col-form-label">Vehicle Name<span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="" placeholder="Name" required>
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="make" class="col-form-label">Select Make<span class="text-danger">*</span></label>
                                            <select id="" name="make" class="form-control select2" required>
                                                @foreach($make as $car_make)
                                                <option value="{{$car_make->id}}">{{$car_make->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="model" class="col-form-label">Select Model<span class="text-danger">*</span></label>
                                            <select id="" name="model" class="form-control" required>
                                                @foreach($car_model as $car_mod)
                                                <option value="{{$car_mod->id}}">{{$car_mod->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="year" class="col-form-label">Year<span class="text-danger">*</span></label>
                                            <select id="" name="year" class="form-control" required>
                                                <option value="" selected>Select Year</option>
                                                
                                                <option value="2000">2000</option>
                                                <option value="2001">2001</option>
                                                <option value="2002">2002</option>
                                                <option value="2003">2003</option>
                                                <option value="2004">2004</option>
                                                <option value="2005">2005</option>
                                                <option value="2006">2006</option>
                                                <option value="2007">2007</option>
                                                <option value="2008">2008</option>
                                                <option value="2009">2009</option>
                                                <option value="2010">2010</option>
                                                <option value="2011">2011</option>
                                                <option value="2012">2012</option>
                                                <option value="2013">2013</option>
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                            </select>
                                        </div>
                                        
                                    </div></div>
                                    <br>
                                    <div class="bg-dev">
                                        <h4 class="m-t-0 header-title">CAR DETAIL</h4>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="offer" class="col-form-label">City<span class="text-danger">*</span></label>
                                                <input type="text" name="city" class="form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" id="" placeholder="city" required="required">
                                                @if ($errors->has('city'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="passengers" class="col-form-label">Fuel type</label>
                                                <select id="" name="fuel_type" class="form-control" required>
                                                    
                                                  
                                                    <option value="0" >Petrol</option>
                                                    
                                                    <option value="3">Electric</option> 
                                                     <option value="4">Hybrid</option>
                                                    <option value="5">Electric Hybrid</option> 
                                                </select>
                                            </div>
                                            
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="model" class="col-form-label">Vehicle Category<span class="text-danger">*</span></label>
                                                <select id="vehicle_category" name="vehicle_category" class="form-control" required>
                                                    <option value="1" >PCO</option>
                                                    <option value="2" >Private</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="year" class="col-form-label">Body Type<span class="text-danger">*</span></label>
                                                <select id="body_type" name="body_type" class="form-control" required>
                                                    
                                                    <option value="1">MPV</option>
                                                    <option value="2">MPV3</option>
                                                    
                                                </select>
                                            </div>
                                            
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="transmission" class="col-form-label">Select Transmition<span class="text-danger">*</span></label>
                                                <select id="" name="transmission" class="form-control" required>
                                                    <option value="" selected>Select Transmition</option>
                                                    <option value="0" >Auto</option>
                                                    <option value="1" >Manul</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="description" class="col-form-label">Engine Capacity<span class="text-danger">*</span></label>
                                                <input type="text" name="engine_Capacity" class="form-control" id="" required placeholder="Engine Capacity" />
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="description" class="col-form-label">Description<span class="text-danger">*</span></label>
                                                <input type="text" name="description" class="form-control" id="" placeholder="description" />
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="description" class="col-form-label">Colour<span class="text-danger">*</span></label>
                                                <input type="text" name="colour" class="form-control" id="" placeholder="Colour"  required="required" />
                                            </div>
                                        </div></div><br>
                                        <div class="bg-dev">
                                            <h4 class="m-t-0 header-title">AVAILABILITY AND PRICING</h4>
                                            
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="price" class="col-form-label">Price<span class="text-danger">*</span></label>
                                                    <input type="text" name="price" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }}" id="" placeholder="price" required>
                                                    @if ($errors->has('price'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('price') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Offer<span class="text-danger">*</span></label>
                                                    <input type="text" name="offer" class="form-control {{ $errors->has('offer') ? ' is-invalid' : '' }}" id="" placeholder="offer">
                                                    @if ($errors->has('offer'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('offer') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Date From<span class="text-danger">*</span></label>
    <input type="text" name="date_from" class="form-control {{ $errors->has('date_from') ? ' is-invalid' : '' }}" id="datepicker1" placeholder="Date format 2020-11-10">
                                                    @if ($errors->has('date_from'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_from') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                
                                                <div class="form-group col-md-6">
                                                    <label for="offer" class="col-form-label">Date To<span class="text-danger">*</span></label>
                                                    <input type="text" name="date_to" class="form-control {{ $errors->has('date_to') ? ' is-invalid' : '' }}" id="datepicker2" placeholder="Date format 2020-11-10">
                                                    @if ($errors->has('date_to'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('date_to') }}</strong>
                                                    </span>
                                                    @endif
                                                </div >
                                            </div></div><br>
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Pickup Address</h4>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label for="price" class="col-form-label">Postal pickup dropoff<span class="text-danger">*</span></label>
                                                        <input type="text" name="postal_pickup_dropoff" class="form-control {{ $errors->has('postal_pickup_dropoff') ? ' is-invalid' : '' }}" id="" placeholder="Postal pickup dropoff" required>
                                                        @if ($errors->has('postal_pickup_dropoff'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('postal_pickup_dropoff') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="offer" class="col-form-label">Location Description<span class="text-danger">*</span></label>
                                                        <input type="text" name="location_description" class="form-control {{ $errors->has('location_description') ? ' is-invalid' : '' }}" id="" placeholder="Description">
                                                        @if ($errors->has('location_description'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('location_description') }}</strong>
                                                        </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Documents Upload</h4>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="price" class="col-form-label">Doc Mot</label>
                                                        <input type="file" name="doc_mot"   class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="offer" class="col-form-label">Doc logback</label>
                                                        <input type="file" name="doc_logback"   class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="offer" class="col-form-label">Doc PHV</label>
                                                        <input type="file" name="doc_phv"   class="form-control">
                                                    </div>
                                                </div>
                                            </div><br>
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Documents Upload</h4>
                                                
                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <label for="price" class="col-form-label">Exterior Front</label>
                                                        <input type="file" name="img_exterior_front"   class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="offer" class="col-form-label">Exterior Back</label>
                                                        <input type="file" name="img_exterior_back"   class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="offer" class="col-form-label">Exterior Front2</label>
                                                        <input type="file" name="img_exterior_front2"   class="form-control">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label for="offer" class="col-form-label">Exterior Dashboard</label>
                                                        <input type="file" name="img_exterior_dashboard"   class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="bg-dev">
                                                <h4 class="m-t-0 header-title">Features</h4>
                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Air Conditioning" /> Air Conditioning
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Alloy Wheels" /> Alloy Wheels
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Auto Start Stop" /> Auto Start Stop
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Aux Input" /> Aux Input
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Bluetooth" /> Bluetooth
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Cloth Seats" /> Cloth Seats
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Cruise Control"  /> Cruise Control
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Diesel Particulate Filter"  /> Diesel Particulate Filter
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Electric Windows"  /> Electric Windows
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Fm Am Radio"  /> Fm Am Radio
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Parking Sensors"  /> Parking Sensors
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="checkbox" name="chk[]" value="Satellite Navigation"  /> Satellite Navigation
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <p class="col-form-label">Upload Profile Pic(JPG, JPEG, PNG)<span class="text-danger">*</span></p>
                                                    <input type="file" name="thumbnail" required class="form-control"></label></span>
                                                    @if ($errors->has('thumbnail'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('thumbnail') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <p class="col-form-label">Upload additional Images(10)<span class="text-danger">*</span></p>
                                                <input type="file" id="images" name="images[]" multiple></span>
                                                @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                                    </form>






                                </div>
                            </div>


 
    <script>
        $('#datepicker1').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });

         $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });
    </script>
 

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
        @section('scripts')

        <script type="text/javascript">
        $('.select2').select2();
        $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
        });
        } );


 





        </script>
        @endsection

 