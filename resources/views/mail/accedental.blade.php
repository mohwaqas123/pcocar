<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>PCOCAR </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body style="margin: 0; padding: 0; background: #cccccc; font-family: arial; ">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="padding: 10px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                        
                        
                        
                        <tr>
                            <td bgcolor="#fff" style="padding: 0px 25px 0px 25px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    
                                    
                                    <tr>
                                        <td style="padding: 0px 0 10px 0; font-weight: bold; color:#153643;">
                                            Hi  {{$name}},
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td style="padding: 0px 0 10px 0; color: #153643;   font-size: 14px; line-height: 20px;">
                                           Thankyou!! Your AccidentalRecorded is submited, Please when you are free to contact the customer center.  <br><br><br>
                                            

                                          
                                            <br><br><br>
                                        </td>
                                    </tr>
                                    
                                    
                                </table>
                               
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#0b5578" style="padding: 30px 30px 30px 30px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="color: #ffffff;   font-size: 12px; margin-left: 17px; " width="50%">
                                            Copyright © 2020 PCOCAR. All rights reserved.<br/>
                                        </td>
                                        <td align="right" width="25%">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="font-family: Arial, sans-serif; font-size: 12px;  ">
                                                        <a href="https://twitter.com/pcocar"><i class="fab fa-twitter-square"></i></a>
                                                        
                                                    </td>
                                                    <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                                    <td style="  font-size: 10px;  ">
                                                        <a href="https://www.facebook.com/pcocar/"><i class="fab fa-facebook"></i></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>