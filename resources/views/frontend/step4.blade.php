@extends('layouts.app')
@section('styles')
@endsection
@section('content')
@include('layouts.header')
<div class="container">
  
  <div class="row">
    <div class="col-md-3 text-center">
      
      
      @include ('frontend.bookingleft')
    </div>
    <div class="col-md-9">
      <ul class="progress">
        <li class="completed">
          <span>
            <span class="order">1 </span>Step 1 <span class="inner hidden-sm">(Login)</span>
          </span>
          <div class="diagonal"></div>
        </li>
        <li class="completed">
          <span>
            <span class="order">2 </span>Step 2 <span class="inner hidden-sm">(Information)</span>
          </span>
          <div class="diagonal"></div>
        </li>
        <li class="completed">
          <span>
            <span class="order">3 </span>Step 3 <span class="inner hidden-sm">(Agreement)</span>
          </span>
        </li>
        <li class="completed">
          <span>
            <span class="order">4 </span>Step 4 <span class="inner hidden-sm">(Payment)</span>
          </span>
        </li>
      </ul>
      
      
      <div class="container py-5">
        <!-- For demo purpose -->
        <div class="row mb-4">
          <div class="col-lg-8 mx-auto text-center">
            <h1 class="display-4">Payment Option</h1>
          </div>
          </div> <!-- End -->
          <div class="row">
            <div class="col-lg-12 mx-auto">
              <div class="card ">
                <div class="card-header">
                  <div class="bg-white shadow-sm cust">
                    
                    <ul role="tablist" class="nav bg-light nav-pills rounded nav-fill mb-3">
                      <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active"> <i class="fas fa-credit-card mr-2"></i> Credit Card </a> </li> 
                 <!--      <li class="nav-item"> <a data-toggle="pill" href="#net-banking" class="nav-link"> <i class="fas fa-mobile-alt mr-2"></i> Net Banking </a> </li> -->
                    </ul>
                  </div>
                  <div class="tab-content">
                    <!-- credit card info-->
                    <div id="credit-card" class="tab-pane fadeIn active pt-3">
                         <form
                        role="form"
                        action="{{ route('stripe.post') }}"
                        method="post"
                        class="require-validation"
                        data-cc-on-file="false"
                        data-stripe-publishable-key="{{env('STRIPE_KEY')}}"
                        id="payment-form">

                   <!-- LIVE    <form
                        role="form"
                        action="{{ route('stripe.post') }}"
                        method="post"
                        class="require-validation"
                        data-cc-on-file="false"
                        data-stripe-publishable-key="pk_live_51HhcciCs5DUo8X5JfQZayxhBeoK4QdHiNRaqQwzR0S22MxNYWbwOuTPFbgm4vo8gTReg5JnQOfRUfaqFv1xBrj0m00cwKP3nKh"
                        id="payment-form"> -->
                        @csrf
                        @php $price_total = ($price + env('INSURANCE_CODE') + env('DEPOSIT_SECURITY'))*100; @endphp 
                        <input type="hidden" name="amount" value="{{$price_total}}" />
                        <input type="hidden" name="customer_name" value="{{$booking_detail->user->name}}" />
                        <input type="hidden" name="customer_email" value="{{$booking_detail->user->email}}" />
                        <input type="hidden" name="expire_month_after" value="4" />
                        <input type="hidden" name="v_id" value="" />
                        <input type="hidden" name="booking_id" value="{{$vehicles->id}}" />
                         <input type="hidden" name="duration" value="{{$months}}" />
                        

                        <div>
                          <div class='col-xs-12 form-group required'>
                            <label class='control-label'>Name on Card</label> <input class='form-control' placeholder='Master'  size='4' type='text' required="required" />
                          </div>
                        </div>
                        <div>
                          <div class='col-xs-12 form-group  required'>
                            <label class='control-label'>Card Number</label> <input
                            autocomplete='off' class='form-control card-number' placeholder='4111111111111' size='20'
                            type='text' maxlength='16' onkeypress="return isNumber(event)"  required="required" />
                          </div>
                        </div>
                        
                        <div class='form-row row'>
                          <div class='col-xs-12 col-md-4 form-group cvc required'>
                            <label class='control-label'>CVC</label> <input maxlength="3" autocomplete='off'
                            class='form-control card-cvc' onkeypress="return isNumber(event)"  placeholder='ex. 311' size='4'
                            type='text'>
                          </div>
                          <div class='col-xs-12 col-md-4 form-group expiration required'>
                            <label class='control-label'>Expiration Month</label> <input
                            class='form-control card-expiry-month' placeholder='MM' size='2'
                            type='text' onkeypress="return isNumber(event)"  maxlength="2" />
                          </div>
                          <div class='col-xs-12 col-md-4 form-group expiration required'>
                            <label class='control-label'>Expiration Year</label> <input
                            class='form-control card-expiry-year' placeholder='YYYY' size='4'
                            type='number' onkeypress="return isNumber(event)"  maxlength="4" />
                          </div>
                        </div>
                        
                        <div class='form-row row'>
                          <div class='col-md-12 error form-group hide'>
                            <div class='alert-danger alert'>Please correct the errors and try
                            again.</div>
                          </div>
                        </div>   
                        <div class='form-row row'>
                          <div class="col-xs-12 form-group text-center">                            
                            <button class="btn btn-primary" type="submit"  style="color:#fff" >PAY NOW  £{{$price_final_total_book}}</button>                            
                          </div>
                        </div>



       
          <div class="tab-example-result">
            <div id="accordion-1" class="accordion accordion-stacked">
              <!-- Accordion card 1 -->
              <div class="card">
                <div class="card-header py-4" id="heading-1-1" data-toggle="collapse" role="button" data-target="#collapse-1-1" aria-expanded="true" aria-controls="collapse-1-1">
                <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file mr-3"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>Check Recurring Billing</h6>
              </div>
              <div id="collapse-1-1" class="collapse" aria-labelledby="heading-1-1" data-parent="#accordion-1" style="">
                <div class="card-body">
                  <div id="checkout">
                     <table class="table">
    <thead>
      <tr>
        <th>Date</th>
        <th>Amount</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
  <?php
if($months==1) $months=4;
$total_week = $months - 1;

 if( $insurance==1){
     $price =  $price ;
 }
 else{
     $price =  $price + env('INSURANCE_CODE') ;
 }


for($i=1;$i<=$total_week;$i++){
 
  $ff = $i * 7;
  $days_add = '+'.$ff.' day'; 
    $date = strtotime( $booking_detail->booking_start_date);
 
  $date = strtotime($days_add, $date);
  $show_date = date('d/m/Y', $date);

  echo "<tr>
          <td>".$show_date."</td>
          <td>£".$price."</td>
          <td>Unpaid</td>
  </tr>";


    //  echo "£".$price.' @ '. $show_date.'<br>';
}

?>   
    </tbody>
  </table>
                    
                  </div>
                </div>
              </div>
            </div>
            
            
          </div>
        </div>


 

                       
                      </form>
                    </div>
                   
                  
                  <div id="paypal" class="tab-pane fade pt-3">
                    <h6 class="pb-2"> <div class="coming"> Option not available (Coming Soon) </div></h6>
                    
                    </div> <!-- End -->
                    <!-- bank transfer info -->
                    <div id="net-banking" class="tab-pane fade show pt-3">
                      <form
                        role="form"
                        action="{{ route('confirm_booking') }}"
                        method="post" 
                        id="payment-form">
                        @csrf
                        
                        <div class="row"> 

                          <div class="col-md-6"><span>Ref#:</span> {{$generate_ref_booking}}  <br>
                              Bank Detail:
                              <p>Barclays Bank<br> Intercity Hire<br>Account number: 23057496<br>Sort Code:  20-76-90</p>
                          </div>
                        </div>                        <br>
                        <div class="form-group" align="center">
                          <p>   <input type="submit" class="btn btn-primary"  value="Confirm Submit Booking" name="" id="" /> </p>
                        </div>
                        <p class="text-muted">Note: After clicking on the button, your booking will be submitted and redirect to customer login page to review your booking. </p></form>
                        </div> <!-- End -->
                        <!-- End -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              
              
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('layouts.footer')
</body>
@endsection
@section('scripts')
@endsection
 
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
<style>
#v2{display: none;}
.rounded {
border-radius: 1rem
}
.nav-pills .nav-link {
color: #555
}
.nav-pills .nav-link.active {
color: white
}
input[type="radio"] {
margin-right: 5px
}
.bold {
font-weight: bold
}
.nav-fill .nav-item{float: left}
.cust{border: 2px solid #eee;
padding-bottom: 20px;}
.hide{display: none;}
</style>
 




<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
var $form         = $(".require-validation");
$('form.require-validation').bind('submit', function(e) {
var $form         = $(".require-validation"),
inputSelector = ['input[type=email]', 'input[type=password]',
'input[type=text]', 'input[type=file]',
'textarea'].join(', '),
$inputs       = $form.find('.required').find(inputSelector),
$errorMessage = $form.find('div.error'),
valid         = true;
$errorMessage.addClass('hide');
$('.has-error').removeClass('has-error');
$inputs.each(function(i, el) {
var $input = $(el);
if ($input.val() === '') {
$input.parent().addClass('has-error');
$errorMessage.removeClass('hide');
e.preventDefault();
}
});
if (!$form.data('cc-on-file')) {
e.preventDefault();
Stripe.setPublishableKey($form.data('stripe-publishable-key'));
Stripe.createToken({
number: $('.card-number').val(),
cvc: $('.card-cvc').val(),
exp_month: $('.card-expiry-month').val(),
exp_year: $('.card-expiry-year').val()
}, stripeResponseHandler);
}
});
function stripeResponseHandler(status, response) {
if (response.error) {
$('.error')
.removeClass('hide')
.find('.alert')
.text(response.error.message);
} else {
/* token contains id, last4, and card type */
var token = response['id'];
$form.find('input[type=text]').empty();
$form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
$form.get(0).submit();
}
}
});
</script>