<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8" />
    <title>PCOCAR Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />


    







    
    <script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

    <link href={{ asset('public/theme/plugins/datatables/dataTables.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/plugins/datatables/buttons.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href={{ asset('public/theme/plugins/datatables/responsive.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />

    <!-- Multi Item Selection examples -->
    <link href={{ asset('public/theme/plugins/datatables/select.bootstrap4.min.css') }} rel="stylesheet" type="text/css" />

    <!-- App favicon -->
    <link rel="shortcut icon" href={{ asset('theme/assets/images/favicon.ico') }}>

    <!-- App css -->
    <link href={{ asset('public/theme/assets/css/bootstrap.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/icons.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/metismenu.min.css') }} rel="stylesheet" type="text/css" />
    <link href={{ asset('public/theme/assets/css/style.css') }} rel="stylesheet" type="text/css" />

    

    @yield('styles')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div id="wrapper">
        {{--  <div class="content-page">  --}}

            <!-- Top Bar Start -->
            <div class="topbar">

                <nav class="navbar-custom">

                    <ul class="list-unstyled topbar-right-menu float-right mb-0">

                        <li class="dropdown notification-list">
                            <!-- <a     class="nav-link dropdown-toggle arrow-none custom_css" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false"> <i class="fi-bell noti-icon"></i> <span class="badge badge-danger badge-pill noti-icon-badge"> </span> </a> -->
                            <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="{{ asset('public/theme/assets/images/admin.png') }}" class="rounded-circle"> <span class="ml-1">{{ Auth::guard('admin')->user()->name }} <i class="mdi mdi-chevron-down"></i> </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h6 class="text-overflow m-0">Welcome !</h6>
                                </div>

                                <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>My Account</span>
                                </a>
                            <a href="{{ route('admin_setting') }}" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>Setting</span>
                                </a>
                                 <a href="{{ route('admin_notification') }}" class="dropdown-item notify-item">
                                    <i class="fi-head"></i> <span>Swap Req</span>
                                </a>

                                <!-- item-->
                                <a href="{{ route('adminLogout') }}" class="dropdown-item notify-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('adminLogout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-right ">
                        <li class="float-right">
                            <button class="button-menu-mobile open-right disable-btn">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        
                        
                        <li>
                            <div class="page-title-box">
                                

                                    <div class="card-box"  >
                                <h4 class="mt-5" style="margin-left: 250px; "  > 

                                {!! html_entity_decode($breadcrum ?? '') ?? '' !!}</h4>
                           
                                   </div>
                              
                            </div>
                        </li>
                        
                        
                        
                        
                        
                        
                        
                        

                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->
 

        @yield('content')

        @include('layouts.adminSidebar')
    </div>
</body>

<!-- jQuery  -->
    <script src={{ asset('public/theme/assets/js/jquery.min.js') }}></script>
  <!--   <script src={{ asset('public/theme/assets/js/bootstrap.bundle.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/metisMenu.min.js') }}></script>
    <script src={{ asset('public/theme/assets/js/waves.js') }}></script>
    <script src={{ asset('public/theme/assets/js/jquery.slimscroll.js') }}></script> -->

    <!-- App js -->
    <script src={{ asset('public/theme/assets/js/jquery.core.js') }}></script>
    <script src={{ asset('public/theme/assets/js/jquery.app.js') }}></script>

    <script src={{ asset('public/theme/assets/js/modernizr.min.js') }}></script>

    <!-- Required datatable js -->
    <script src={{ asset('public/theme/plugins/datatables/jquery.dataTables.min.js') }}></script>
    <script src={{ asset('public/theme/plugins/datatables/dataTables.bootstrap4.min.js') }}></script>






<!-- Datatable -->
<!--  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
 <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
 <script>
     $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
 <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

 -->


    <!-- jQuery  -->
      <!--   <script src="{{ asset('public/admin/assets/js/jquery.min.js')}}"></script> -->
        <script src="{{ asset('public/admin/assets/js/popper.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/metisMenu.min.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/waves.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.slimscroll.js')}}"></script>

        <!-- Flot chart -->
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.min.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.time.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.tooltip.min.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.resize.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.pie.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.crosshair.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/curvedLines.js')}}"></script>
        <script src="{{ asset('public/admin/plugins/flot-chart/jquery.flot.axislabels.js')}}"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="../plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="{{ asset('public/admin/plugins/jquery-knob/jquery.knob.js')}}"></script>

        <!-- Dashboard Init -->
        <script src="{{ asset('public/admin/assets/pages/jquery.dashboard.init.js')}}"></script>

        <!-- App js -->
        <script src="{{ asset('public/admin/assets/js/jquery.core.js')}}"></script>
        <script src="{{ asset('public/admin/assets/js/jquery.app.js')}}"></script>

    @yield('scripts')

</html>


 