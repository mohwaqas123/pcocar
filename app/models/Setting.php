<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';

    protected $fillable = [
        'stripe_mode',
        'stripe_live_published_key',
        'stripe_live_secret_key',
        'stripe_live_prod_key',
        'stripe_test_published_key',
        'stripe_test_secret_key',
        'stripe_test_prod_key',
        'promotion_type',
        'flat_discount_rate',
        'discount_rate',
        'flat_discount_type',
    ];
}
