<div class="left side-menu" >

    <div class="slimscroll-menu" id="remove-scroll">

        <!-- LOGO -->
        <div class="topbar-left">
            <h5>{{ Auth::guard('admin')->user()->name }}</h5>
             <a href="{{ route('adminLogout') }}"  
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
            </a>
            <!-- <a href="#" class="logo">
                <span>
                    <img src="{{ asset('public/admin/assets/images/logo.png') }}" alt="" height="22">  
                </span>
                <i>
                    <img src="{{ asset('public/admin/assets/images/logo.png') }}" alt="" height="28">
                </i>
            </a> -->
        </div>
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->

        <!-- User box -->
        <div class="user-box">
            <!-- <div class="user-img">
                <img src="{{ asset('public/theme/assets/images/admin.png') }}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
            </div> --><!-- 
            <h5><a href="#">{{ Auth::guard('admin')->user()->name }}</a> </h5>
            <p class="text-muted">PCOCAR</p>

              <a href="{{ route('adminLogout') }}" class="dropdown-item notify-item"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a> -->



        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu metismenu without-tooltip" id="side-menu">

                <!--<li class="menu-title">Navigation</li>-->

                <li>
                    <a href="{{ route('adminDashboard') }}">
                        <i class="fi-air-play"></i><span> Dashboard </span>
                    </a>
                </li>

                

                <li>
                    <a href="javascript: void(0);"><i class="fa fa-automobile"></i> <span> Vehicles  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('vehicleList') }}">Vehicle List </a></li>
                        <li><a href="{{ route('vehicleCreate') }}">Add Vehicle</a></li>
                        <li><a href="{{ route('vehiclehide_list') }}">Hidden Vehicles</a></li>

                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Make  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('adminmakeList') }}">Makes List </a></li>
                        <li><a href="{{ route('adminmakeCreate') }}">Add New</a></li>
                    </ul>
                </li>

                <li> 
                    <a href="javascript: void(0);"><i class="fi-layers"></i> <span> Car Models  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('listcarmodels') }}">Model List </a></li>
                        <li><a href="{{ route('createcarmodel') }}">Add Model</a></li>
                    </ul>
                </li>
                

                  <li>
                    <a href="javascript: void(0);"><i class="fa fa-book"></i> <span> Bookings  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('vehicle_booking') }}">Active </a></li>
                        <li><a href="{{ route('vehicle_booking_pending') }}">Pending</a></li>
                        <li><a href="{{ route('vehicle_booking_completed') }}">Completed </a></li>
                        <li><a href="{{ route('/vehicle_booking_cancel') }}">Cancelled </a></li>
                        <li><a href="{{ route('/vehicle_booking_all') }}">All</a></li>
                       

                         
                        
                    </ul>
                </li>
                  <li>
                    <a href="javascript: void(0);"><i class="fa fa-handshake-o"></i> <span> Partner's Accounts  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('partner_list') }}">Accounts List</a></li>
                        <li><a href="{{ route('partner_add') }}">Add New</a></li>
                        
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);">
                         <i class="mdi mdi-account-switch"></i> <span> Customer's Accounts  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('customer_list') }}">All Accounts</a>
                   
                    </ul>
                </li>

                   <li>
                    <a href="javascript: void(0);"><i class="fa fa-id-card"></i> <span> Driver's Accounts  </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('admindriver') }}">Accounts List</a></li>
                        <li><a href="{{ route('admindriveradd') }}">Add New</a></li>
                        
                    </ul>
                </li> 
                  <li>
                    <a href="javascript: void(0);"><i class="fa fa-tag fa-lg"></i> <span>Promo Codes</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('code/generate') }}">Create New Promo</a></li>
                        <li><a href="{{ route('code/generate/list') }}">Promo Codes List</a></li>

                       
                    </ul>
                </li>
                  <li> 
                    <a href="javascript: void(0);"><i class="dripicons-bookmarks"></i> <span>Damages </span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('admin/accidental_recorded') }}">Record Damage</a></li>
                         <li><a href="{{ route('admin/accidental/recorded/list') }}"> All Damages</a></li>
                       </ul>
                </li>
                <li> 
                    <a href="javascript: void(0);"><i class="dripicons-wallet"></i> <span>Expenses</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                          <li><a href="{{ route('admin/invoice/add') }}"> Add invoice</a></li>
                         <li><a href="{{ route('admin/invoice/show') }}"> All invoices</a></li>
                          <li><a href="{{ route('admin/expence/list') }}"> Customer's Expense Invoices</a></li>
                       </ul>
                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-heartbeat"></i> <span>Insurance</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                          <li><a href="{{ route('admin/insurancecompany/add') }}">Add Company</a></li>
                          <li><a href="{{ route('admin/insurancecompany/list') }}"> Companies List</a></li>
                         
                       </ul>
                </li>
                <li> 
                    <a href="javascript: void(0);"><i class="fa fa-id-card-o"></i> <span>Staff</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                          <li><a href="{{ route('admin/staff/add') }}"> Add new</a></li>
                          <li><a href="{{ route('admin/staff/list') }}">  Staff List</a></li>
                         
                       </ul>
                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-industry"></i> <span>Supplier</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                          <li><a href="{{ route('admin/purcheas/add') }}"> Add new</a></li>
                          <li><a href="{{ route('admin/purcheas/list') }}">  Supplier List</a></li>
                         
                       </ul>
                </li>
                <li>
                    <a href="javascript: void(0);"><i class="fa fa-industry"></i> <span>PCN Management</span> <span class="menu-arrow"></span></a>
                    <ul class="nav-second-level" aria-expanded="false">
                          <li><a href="{{ route('admin/pcn_management/add') }}"> Add new</a></li>
                          <li><a href="{{ route('admin/pcn_management/list') }}">  PCN List</a></li>
                         
                       </ul>
                </li>

            </ul>


        </div>
        <!-- Sidebar -->

  

    </div>
    <!-- Sidebar -left -->

</div>


