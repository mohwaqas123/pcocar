@extends('layouts.customer')
@section('styles')
@endsection
@section('content')

<div class="content-page">
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        
                        
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">

<table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">

                                <thead>
                                <tr>
                                <th style="width: 10px">ID</th>
                                <th>Date</th>
                                <th>Detail</th>
                                <th>Price</th>     
                                <th>Payment Reference</th>
                                <th>Status</th>
                            	</tr>
                                </thead>
                                <tbody>
                                @foreach($payment_history as $payment_history_customer)

                                 <tr>
                                    <td>{{$payment_history_customer->id}}</td>
@if($payment_history_customer->payment_type==0)
 <td>{{$payment_history_customer->expire_date}}</td>
@else
@endif
@if($payment_history_customer->payment_type==2)
<td>{{$payment_history_customer->created_at}}</td>
@else
@endif
@if($payment_history_customer->payment_type==1)
<td>{{$payment_history_customer->created_at}}</td>
@else
@endif
                   
                                    <td>
                              @if($payment_history_customer->payment_type==2)
                             Payment receive against booking# {{@$payment_history_customer->booking->id}}
                                        @else
                                        Payment receive against booking# {{$payment_history_customer->booking_id}}
                                    @endif
                                    </td>






                                    <td>{{$payment_history_customer->amount}}GBP</td>
                                    <td>
      @if($payment_history_customer->payment_type==1)
        <span class="badge badge-info">Weekly booking payment</span>
        @elseif($payment_history_customer->payment_type==2)
      <span class="badge badge-primary">{{$payment_history_customer->description}}</span>
       @endif
       </td>
		<td>
	@if($payment_history_customer->payment_status == 1)
                                            Paid
                                        @else
                                            Not Paid 
                                        @endif
									</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





@endsection
@section('scripts')
<script>
 $(document).ready(function() {
    $('#datatable_tbl').DataTable();
} );
 </script>
<script type="text/javascript">
    $(document).ready(function() {
        // Default Datatable
        $('#songsListTable').DataTable({
            "columnDefs": [
            { "orderable": false, "targets": [4,5,6] },
            ],
            "bPaginate": false,
        });
    } );
</script>
@endsection
