<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Purchase;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchase = Purchase::get(); 
        $url = "/admin/dashboard";
          $data = [
               'purchase'=> $purchase,
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Supplier'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Supplier List' ,

        ];

        return view('admin.purchase.list')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $url = "/admin/dashboard";
            $data = [
                'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Supplier'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Supplier Add' ,
            ];  
        return view('admin.purchase.create')->with($data);
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'dob' => 'required',

        ]);

        $nid_back='';
        $profile_img='';
        
        if ($request->hasFile('nid_back') || $request->hasFile('profile_img')) 
         {
            $vehicleDocFolder = 'purchasedocs';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }
            if ($request->hasFile('nid_back'))
               $nid_back = Storage::putFile($vehicleDocFolder, new File($request->nid_back));
            
            if ($request->hasFile('profile_img'))
            $profile_img = Storage::putFile($vehicleDocFolder, new File($request->profile_img));
            
        }

        if ($request->vat_number == '') 
        {
            $price2 =  0 ;
                        
        } 
        else
         {
            $price = $request->price;
            $vat_number = $price / 1.2;
            $price2 =  $price -  $vat_number ;
            

        }
            $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'detail' => $request->description,
            'price' => $request->price,
            'vat_price' => $price2,
            'invoice' => $nid_back,
            'date' => $request->dob,
            'image' => $profile_img,
            'vat_number' => $request->vat_number,
        ];
       
        $purchase = Purchase::create($data);
            
         return redirect()->route('admin/purcheas/list')->with('message','Supplier Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

            $purchase = Purchase::where('id', $request->id)->first();  
            
             $url = "/admin/dashboard";
             $data = [
                  'purchase' => $purchase,

                   'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Supplier'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Supplier View' ,


                    
                ]; 
                
            return view('admin.purchase.view')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purchase = Purchase::find($id);

       $url = "/admin/dashboard";
          $data = [
            'purchase' => $purchase,
            'breadcrum' =>
         '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'Supplier'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Supplier Edit' ,

             

           ]; 

        return view('admin.purchase.edit')->with($data);
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $purchase = Purchase::where('id', $request->id)->first();

        $nid_back='';
        $profile_img='';
        
        if ($request->hasFile('nid_back') || $request->hasFile('profile_img')) 
         {
            $vehicleDocFolder = 'vehicleDocs';

            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }
            if ($request->hasFile('nid_back'))
               $nid_back = Storage::putFile($vehicleDocFolder, new File($request->nid_back));
            
            if ($request->hasFile('profile_img'))
            $profile_img = Storage::putFile($vehicleDocFolder, new File($request->profile_img));
            
        }

        if ($request->vat_number == '') 
        {
            $price2 =  0 ;
                        
        } 
        else
         {
            $price = $request->price;
            $vat_number = $price / 1.2;
            $price2 =  $price -  $vat_number ;
            

        }
            $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'detail' => $request->description,
            'price' => $request->price,
            'vat_price' => $price2,
            'nid_back' => $nid_back,
            'date' => $request->dob,
            'profile_img' => $profile_img,
            'vat_number' => $request->vat_number,
        ];

        $purchase->update($data); 
        $purchase->save();
         return redirect()->route('admin/purcheas/list')->with('message','Supplier Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
