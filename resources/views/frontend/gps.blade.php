<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">
    <title>PCOCAR  Vehicle Mileage</title>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
  </head>
  <body>
    
    <div class="container">
       
  <div class="row">

        <br>
        <table class="table" width='28%'><tr>
          
          <th>Registration Number</th>
          <th>Make</th>
          <th>Model</th>
          <!--  <th>Imei</th> -->
          <th>Mileage(mi)</th>
        </tr>
        @foreach ($get_gps_location_rec as $rec)
        <?php
        $registration = explode('-',$rec->name);
            $vehicle = App\models\Vehicle::with('car_make')->with('car_model')->where('licence_plate_number', $registration[0])->first();
                //$vehicle = DB::table('vehicles')->with('car_make')->where('licence_plate_number', $registration[0])->first();
            //$get_milage = GpsLocationMilage::where('c_date', '2021-03-03')->where('imei',$rec->imei)->first();
            $get_milage = DB::table('gps_location_milage')->where('c_date', $date_from)->where('imei',$rec->imei)->first();
            $milage_calculate =   $get_milage->odometer - $rec->odometer;
           if($vehicle && $vehicle->insurance_type==0){ 
            echo  "<tr>
         
              <td>".$registration[0]."</td>
              <td>".@$vehicle->car_make->name."</td>
              <td>".@$vehicle->car_model->name."</td>
                
                    <td>".round($milage_calculate * $factor,2)."</td>
              </tr>";
           }
        ?>
        @endforeach
      </table>
    </div></div>
  </body></html>
  
  <script>
  $(function() {
  $('#datetimepicker1').datetimepicker({
  format: 'YYYY-MM-DD',
    maxDate:new Date(),
   // minDate:-61, 
      
  });
  $('#datetimepicker2').datetimepicker({
  format: 'YYYY-MM-DD',
    maxDate:new Date(), 
        
  });
  });
  
 
  </script>