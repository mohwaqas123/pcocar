<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Vehicle;
use App\models\User;
use App\models\PCN;
use PDF; 
use Carbon\Carbon;
use App\models\Booking;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;


class PCNManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function generate_pdf_pcn_letter(Request $request)
     {
       $pcn = PCN::where('id', $request->id)->with('vehicle')->with('user')->first();

       $date =Carbon::now()->format('d-m-Y'); 
       $records = Booking::with('user')->where('vehicle_id',$pcn->vrm)->orderby('id', 'Desc')->first();

        $data = [
          'title' => 'Intercity Hire',
          'heading' => 'PCOCAR Permission Letter',
          'pcn'=>$pcn,
          'date'=>$date,
          'records'=>$records,
            ];
      
        $pdf = PDF::loadView('frontend.pdf_pcn_letter', $data);  
        $filename = rand().'.pdf';
        
        //print "<script>window.location='www.google.com'</script>";
        return $pdf->download($filename);


}
 public function get_customers(Request $request){ 
         $records = Booking::with('user')->where('vehicle_id',$request->vehicle_id)->orderby('id', 'Desc')->get();


         
         // dd( $records->user->name);
            // return json_encode($records);


$customer = ' <label for="make" class="col-form-label">Customer</label>
                  <select    name="driver_name" placeholder="Customer Drop Down"
                    class="form-control select2">
                     ';
                 $customer .= '<option value="0">None</option>';
            foreach($records as $rec){
               // if($rec->user->name)
                $customer .= 
                "<option value='".@$rec->user->id ."'>".@$rec->user->name ."
                ".@$rec->user->phone ."
                 (".@$rec->user->email .")</option>";



            }

 $customer .= "</select>";
 return $customer;


    }
    public function index()
    {

            // $pcn = PCN::get()->orderBy('name', 'ASC')->paginate(10);
            $pcn = PCN::get();

            $url = "/admin/dashboard";
             $title = 'Car Make list';
             $data = [
               'pcn'=> $pcn,
               'title' =>$title,
               'breadcrum' =>
                 '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'PCN Management'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'PCN List' , 
                    ];
                    
            return view('admin.pcnmanagement.list')->with($data);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        $vehicle = Vehicle::all(); 
        $user = User::all();  
         $url = "/admin/dashboard";
        $title = 'Add Vehicle';
          $data = [
             
               'title' =>$title,
               'vehicle' =>$vehicle,
             'user' =>$user,
                'breadcrum' =>
                 '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'PCN Management'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'Add PCN  ' ,    
        ];  
       
        return view('admin.pcnmanagement.create')->with($data); 
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $doc_mot='';
         if ($request->hasFile('doc_mot')) 
         {
            $vehicleDocFolder = 'vehicleDocs';    
            if (!Storage::exists($vehicleDocFolder))
             {
                Storage::makeDirectory($vehicleDocFolder);
            }
            if ($request->hasFile('doc_mot'))
               $doc_mot = Storage::putFile($vehicleDocFolder, new File($request->doc_mot));
            }
         
             $data = [
            'ref_number' => $request->ref_number,
            'vrm' => $request->vehicle,
            'status' => $request->status,
            'stage' => $request->stage,
            'pcn_contravention' => $request->pcn_contravention,
            'notice_date' => $request->notice_date,
            'received_date' => $request->received_date,
            'type_of_action' => $request->type_of_action,
            'action_date' => $request->action_date,
            'charging_authority_name' => $request->charging_authority_name,
            'driver_name' => $request->driver_name,
            'driver_email' => $request->driver_email,
            'driver_phone' => $request->driver_phone,
            'doc_mot'=> $doc_mot,
            ];  
        
            $pcn = PCN::create($data);


            if($pcn->driver_name == 0 )
            {
               
            }
            else
            {
                

                $user_email =  $pcn->user->email;
        $user_name =  $pcn->user->name;
        $vehicle =  $pcn->vehicle->licence_plate_number;
        $pcn_image =  $pcn->doc_mot;
        $ref_number =  $pcn->ref_number;



        // $vehicle_name =  $pcn->user->name;
        
        // $vehicle_year =  $expense_view->vehicle->year;
        // $user_name =     $expense_view->booking->user->name;
        // $user_Email =    $expense_view->booking->user->email;
        // $user_Phone =    $expense_view->booking->user->phone;
        // $Expense_price = $expense_view->price;
        // $detail        =    $expense_view->detail;
        // $shop_name =    $expense_view->name;
        
         Mail::to($user_email)->send(new \App\Mail\PCN(
          $pcn_image,$user_name,$vehicle,$ref_number)); 
            }
      

      
         return redirect()->back()->with('success','PCN Added Successfully');
 
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {   
        // dd('show');s
        $pcn = PCN::find($request->id);
        // dd($pcn->ref_number);
        $pcn_ref = PCN::get();
        // dd($pcn_ref);
        $data = [
            'pcn_ref' => $pcn_ref,
            'pcn' => $pcn,
            ];
        return view('admin.pcnmanagement.view')->with($data); 

       
 
    }
    public function pcn_letter_edit(Request $request)
    {   
        $pcn = PCN::find($request->id);
        $data = [
            'letter_txt' => $request->detail,
        ];
        $pcn->update($data); 
        $pcn->save();
        return redirect()->back()->with('success','information Added Successfully');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $pcn = PCN::find($request->id);
        $vehicle = Vehicle::all(); 
        $user = User::all();  
        $url = "/admin/dashboard";
        $title = 'Edit Vehicle';
          $data = [
               'pcn' =>$pcn,
               'vehicle' =>$vehicle,
               'user' =>$user,
               'title'=>   $title,
               'breadcrum' =>
                '<a style="color:black; &nbsp" href="'. url( $url ).'"  >
                  Dashboard</a>   '.'&nbsp<br> '.'&nbsp'.'<span  
                  style="font-size: 12px;"</span>' .'PCN Management'.'&nbsp'.'&nbsp'.'&nbsp'.'>'
                .'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'&nbsp'.'<span  style="font-size: 12px;"</span>'.'PCN Edit' ,
           ];  
         return view('admin.pcnmanagement.edit')->with($data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $pcn = PCN::where('id', $request->id)->first();
         

      $doc_mot=$pcn->doc_mot;
          

        //documents upload process
         if ($request->hasFile('doc_mot')  ) 
         {
            $vehicleDocFolder = 'vehicleDocs';

            if (!Storage::exists($vehicleDocFolder)) {
                Storage::makeDirectory($vehicleDocFolder);
            }

            if ($request->hasFile('doc_mot'))
               $doc_mot = Storage::putFile($vehicleDocFolder, new File($request->doc_mot));
           }



        $data = [
            'ref_number' => $request->ref_number,
            'vrm' => $request->vehicle,
            'status' => $request->status,
            'stage' => $request->stage,
            'pcn_contravention' => $request->pcn_contravention,
            'notice_date' => $request->notice_date,
            'received_date' => $request->received_date,
            'type_of_action' => $request->type_of_action,
            'action_date' => $request->action_date,
            'charging_authority_name' => $request->charging_authority_name,
            'driver_name' => $request->driver_name,
            'driver_email' => $request->driver_email,
            'driver_phone' => $request->driver_phone,
            'doc_mot'=> $doc_mot,
            ];
    
        $pcn->update($data); 
        $pcn->save();
        return redirect()->route('admin/pcn_management/list')->with('message','PCN Updated Successfully');


}
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
