@extends('layouts.partner')
@section('styles')
@endsection
@section('content')
<div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <div id="datatable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
                            
                            
                            
                            <!-- Datatable -->
                            
                            <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
                            <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
                            <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet" type="text/css" />
                            <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            <table id="datatable_tbl" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <!--  <th>Customer Detail</th> -->
                                        <th>Vehicle</th>
                                        <th>Booking</th>
                                        <th>Price</th>
                                        
                                        <!-- <th>Action</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($booking as $bookings)
                                    @if(@$bookings->vehicle->partner_id==$partner_id)
                                    <tr>
                                        <td>{{$bookings->id}}</td>
                                        <td>{{@$bookings->user->name}} <br />
                                            {{@$bookings->user->email}} <br />
                                            {{@$bookings->user->phone}}
                                        </td>
                                        <td>{{@$bookings->vehicle->car_make->name}}<br>
                                            {{@$bookings->vehicle->year}} <br>
                                            {{@$bookings->vehicle->licence_plate_number}}
                                            {{@$bookings->vehicle->model->name}}
                                        </td>
                                        <td>
                                            Created: {{$bookings->created_at}}<br>
                                            Expire:  {{$bookings->expiry_date}}
                                        </td>
                                        <td>{{$bookings->price}} Per week</td>
                                        
                                        <td>  <a href="{{route('p_customerview', $bookings->id)}}" class="btn btn-sm btn-icon waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>  </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <input type="button" onclick="history.back()" class="btn btn-primary" value="C A N C E L">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$(document).ready(function() {
$('#datatable_tbl').DataTable();
} );
</script>
<script type="text/javascript">
$(document).ready(function() {
// Default Datatable
$('#songsListTable').DataTable({
"columnDefs": [
{ "orderable": false, "targets": [4,5,6] },
],
"bPaginate": false,
});
} );
</script>
@endsection