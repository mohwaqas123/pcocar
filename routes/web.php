<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
return view('frontend.main');
});

Route::get('/Forgot_password', function () {
    return view('customer.forgot');
});

Route::get('/partner_inquiry', function () {
    return view('partner.partner_inquiry');
});

Route::post('/forgot_password', 'UserController@forgot_password')->name('forgot_password');


Route::post('/partner_inquiry_send', 'ContactController@index')->name('partner_inquiry_send');

 


Route::get('admin/vehicle/adminAproveBookingNew/{id}', 'Admin\bookingvehicleController@adminAproveBooking')->name('adminAproveBookingNew');
Route::get('/generate_pdf_email_swipe_old/{id}', 'bookingprocessController@generate_pdf_email_swipe_old');
Route::get('/generate_pdf_email_swipe_new/{id}', 'bookingprocessController@generate_pdf_email_swipe_new');

 
Route::get('customer/verify/',"UserController@customer_verification")->name('customerlogin');
Route::post('/set_forgot', 'UserController@set_forgot')->name('set_forgot');



Route::get('admin/vehicle/swipevecicle/{id}', 'Admin\bookingvehicleController@swipevecicle')->name('swipevecicle');
Route::post('admin/vehicle/swipebooking_add/', 'Admin\bookingvehicleController@swipebooking_add')->name('swipebooking_add');




Route::get('admin/vehicle/adminCancelBookingNew/{id}', 'Admin\bookingvehicleController@adminCancelBookingNew')->name('adminCancelBookingNew');



Route::get('admin/vehicle/adminCompleteBookingNew/{id}', 'Admin\bookingvehicleController@adminAproveBooking')->name('adminCompleteBookingNew');

Route::post('browse', 'searchController@index')->name('browse');   //when click at buttons link this rout execute

Route::get('update_payment_status', 'StripePaymentController@update_payment_status');
//cron job
Route::get('payment_update_status_cron_job', 'bookingprocessController@payment_update_status_cron_job');


//Route::post('promotion_apply', 'bookingprocessController@promotion_apply')->name('promotion_apply');
Route::get('promotion_apply', 'browsesController@promotion_apply');

//permiossion letter download
Route::get('/generate_pdf_permission/{id}', 'Admin\CustomerController@generate_pdf_permission')->name('generate_pdf_permission');
//PCN_Hire_Agreement

Route::get('/generate_pdf_pcn_hire_agreement/{id}', 'Admin\CustomerController@generate_pdf_pcn_hire_agreement')->name('generate_pdf_pcn_hire_agreement');











//staff login
Route::get('/stafflogin', 'UserController@stafflogin')->name('stafflogin');
Route::get('/stafflogout', 'UserController@stafflogout')->name('stafflogout');
Route::get('/browse_staff', function () {
return view('frontend.browse');
});
//user signup and login for customer etc.
Route::post('/userlogin', 'UserController@login')->name('userlogin');
Route::post('/usersignup', 'UserController@signup')->name('usersignup');
//user login for partner etc.
Route::get('/login_partner', function () {
return view('partner.login');
});
//Route::post('/login_partner', 'Partner\PartnerController@login_partner')->name('login_partner');
Route::post('/partnerlogin', 'Partner\PartnerController@partnerlogin')->name('partnerlogin');
// Route::get('partner/partnerdashboard', 'Partner\PartnerController@partnerdashboard')->name('partnerdashboard');
//dashbaord

//Route::post('/usersignup', 'UserController@signup')->name('usersignup');
Route::post('partnerLogout', 'Partner\PartnerController@partnerLogout')->name('partnerLogout');
Route::get('partner/p_vehiclesList', 'Partner\VehicleController@index')->name('p_vehicleList');
Route::get('partner/p_vehicleCreate', 'Partner\VehicleController@create')->name('p_vehicleCreate');
Route::post('partner/p_vehile_add', 'Partner\VehicleController@store')->name('p_vehile_add');
Route::get('partner/edit_vehicle/{id}', 'Partner\VehicleController@edit')->name('p_vehicleEdit'); //show edit blade

Route::get('partner/p_vehicledestory/{id}', 'Partner\VehicleController@destory')->name('p_vehicledestory'); 



Route::post('partner/update_vehicle', 'Partner\VehicleController@update')->name('p_edit_vehicle');
Route::get('partner/destory_vehicle/{id}', 'Partner\VehicleController@destory')->name('p_vehicleDestory');

//customer login and signup
Route::get('/login', function () {
return view('customer.login');
});
//Route::post('/usersignup', 'UserController@signup')->name('usersignup');
Route::post('/customerlogin', 'UserController@customerlogin')->name('customerlogin');
Route::get('/customerdashboard', 'UserController@customerdashboard')->name('customerdashboard'); //dashbaord
Route::post('/customerLogout', 'UserController@customerLogout')->name('customerLogout');
//===customer login routs end
/*Route::post('/browse', function () {   //not 
return view('frontend.browse');
});
*/



Route::get('/temp_delete_img/{id}', 'Admin\vehicleController@temp_delete_img')->name('temp_delete_img');  // image gallary remove




Route::get('/browse_all/{id}', 'searchController@index')->name('browse_all');   //when click at browser link this rout execute
Route::get('/browse_vehicle_category/{id}', 'searchController@browse_vehicle_category')->name('browse_vehicle_category');   //when click at browser link this rout execute

Route::get('/get_models', 'searchController@get_models');         //ajax call for get moted at selection of make
Route::get('/get_all_vehicles', 'searchController@get_all_vehicles');
Route::get('/calculate_booking', 'searchController@calculate_booking');

Route::get('/calculate_renewbooking', 'searchController@calculate_renewbooking');



Route::get('/contactus', 'searchController@contactus')->name('contactus');
Route::get('/aboutus', 'searchController@aboutus')->name('aboutus');
Route::get('/howitswork', 'searchController@howitswork')->name('howitswork');




Route::get('/generate_pdf', 'bookingprocessController@generate_pdf')->name('generate_pdf');




Route::get('/booking_done', 'bookingprocessController@done')->name('booking_done');
//Route::get('/car_detail', 'browseController@car_detail')->name('car_detail');
Route::get('/car_detail', 'browsesController@car_detail')->name('car_detail');
Route::get('/booking', 'bookingprocessController@index')->name('booking');   //first step
Route::get('/booking2', 'bookingprocessController@step2')->name('booking2');   //second step
//Route::get('/booking3', 'bookingprocessController@step3')->name('booking3');   //third step document download agreement
Route::post('/booking4', 'bookingprocessController@step4')->name('booking4');   //payment

Route::get('/booking4', 'bookingprocessController@step4')->name('booking4'); 
  //if user close borwse without complete payment, next time he will redirect
Route::post('/upload_information', 'bookingprocessController@upload_information')->name('upload_information');
 


Route::get('/upload_information_second/{id}', 'bookingprocessController@upload_information_second')->name('upload_information_second');



Route::get('/generate_pdf_email/{id}', 'bookingprocessController@generate_pdf_email')->name('generate_pdf_email');
Route::get('done', 'bookingController@done')->name('done');
//Route::post('booking/upload_information', 'bookingController@upload_information')->name('upload_information');
//Route::post('booking/temp_upload_img', 'bookingController@temp_upload_img')->name('temp_upload_img');
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
Route::post('confirm_booking', 'bookingprocessController@confirm_booking')->name('confirm_booking');


// Route::get('/AdminLogin', function () {
//     return view('admin.login');
// });
Route::get('/adminlogin', function () {
if(Auth::guard('admin')->check()){
return redirect()->route('adminDashboard');
}
return view('admin.login');
})->name('showAdminLogin');

Route::post('/adminlogin', 'Admin\HomeController@login')->name('AdminLogin');
Route::post('/contactus', 'ContactController@contact')->name('contactus');
Route::get('customer_booking_list', 'UserController@customer_booking_list')->name('customer_booking_list');


Route::get('customer_paymenthistory_list', 'UserController@customer_paymenthistory_list')->name('customer_paymenthistory_list');



//drivers crud for admin
Route::get('admin/driver', 'driverController@index')->name('admindriver');
Route::get('admin/admindriveradd', 'driverController@create')->name('admindriveradd');
Route::post('admin/admindriverstore', 'driverController@store')->name('admindriverstore');
Route::post('admin/admindriverstore', 'driverController@store')->name('admindriverstore');
Route::post('admin/updatedriver', 'driverController@updatedriver')->name('updatedriver');

 


 


Route::get('admin/driverupdate/{id}', 'driverController@edit')->name('driverupdate');
Route::get('admin/driverupdate/{id}', 'driverController@edit')->name('driverupdate');
Route::get('admin/driverdestory/{id}', 'driverController@driverdestory')->name('driverdestory');



// Route::post('admin/driverupdate/{id}', 'driverController@update')->name('driverupdate');


Route::get('/pco_car', 'UserController@pco_car')->name('pco_car');
Route::get('/private_car', 'UserController@private_car')->name('private_car');
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
Route::get('/dashboard', 'Admin\dashboardController@index')->name('adminDashboard');
Route::get('/vehiclesList', 'Admin\vehicleController@index')->name('vehicleList');
Route::get('/vehiclesListPartnersOnly/{id}', 'Admin\vehicleController@index')->name('vehiclesListPartnersOnly');
Route::get('/customer_list', 'Admin\CustomerController@index')->name('customer_list');

Route::get('/vehicleListAdmin', 'Admin\vehicleController@index')->name('vehicleListAdmin');
Route::get('/vehicleListPartner', 'Admin\vehicleController@index')->name('vehicleListPartner');

Route::get('/add_vehicle', 'Admin\vehicleController@create')->name('vehicleCreate');
Route::post('/store_vehicle', 'Admin\vehicleController@store')->name('vehicleStore');
Route::get('/edit_vehicle/{id}', 'Admin\vehicleController@edit')->name('vehicleEdit');
Route::post('/vehiclehide', 'Admin\vehicleController@hide')->name('vehiclehide'); //show edit blade

Route::get('/vehiclehide_list', 'Admin\vehicleController@hide_vehicle')->name('vehiclehide_list');



Route::get('/vehiclevisible/{id}', 'Admin\vehicleController@vehiclevisible')->name('vehiclevisible');





 //show edit blade
Route::post('/update_vehicle', 'Admin\vehicleController@update')->name('vehicleUpdate');

Route::get('/destory_vehicle/{id}', 'Admin\vehicleController@destory')->name('vehicleDestory');
// Route::get('/booking', 'bookingController@booking_list_admin')->name('booking');
//partners routs for admin
Route::post('partner/partnerStore', 'Admin\PartnerController@store')->name('partnerStore');
Route::get('/partner_list', 'Partner\PartnerController@list')->name('partner_list');
 
Route::get('/chargeamount/{id}', 'Admin\CustomerController@chargeamount')->name('chargeamount');


Route::get('/customer_history/{id}', 'Admin\CustomerController@customer_history')->name('customer_history');

Route::post('/chargeamountStripe/{id}', 'Admin\CustomerController@chargeamountStripe')->name('chargeamountStripe');

Route::get('/addamount/{id}', 'Admin\CustomerController@addamount')->name('addamount');
Route::post('/admin/addcharges_payment_history', 'Admin\CustomerController@addcharges_payment_history')->name('addcharges_payment_history');
 

/* Route::post('/admin/addcharges_payment_history', function () {
            dd('feefefe');
})->name('addcharges_payment_history');
*/

Route::get('/partner_add', 'Admin\PartnerController@create')->name('partner_add');


Route::get('adminpartnerdetail/{id}', 'Admin\PartnerController@adminpartnerdetail')->name('adminpartnerdetail');

 

 
Route::get('admin/edit/{id}', 'Partner\PartnerController@adminpartnerEdit')->name('adminpartnerEdit');
Route::post('partnerUpdate', 'Partner\PartnerController@update')->name('partnerUpdate');
Route::get('/delete/{id}', 'Partner\PartnerController@adminpartnerdestory')->name('adminpartnerdestory');

//booking routs
Route::get('/vehicle_booking', 'Admin\bookingvehicleController@index')->name('vehicle_booking');
Route::get('/vehicle_booking_pending','Admin\bookingvehicleController@pending_vehicle_booking_here')->name('vehicle_booking_pending');
Route::get('/vehicle_booking_completed', 'Admin\bookingvehicleController@vehicle_booking_old')->name('vehicle_booking_completed');
Route::get('/vehicle_booking_cancel','Admin\bookingvehicleController@vehicle_booking_cancel')->name('/vehicle_booking_cancel');
Route::get('/vehicle_booking_all','Admin\bookingvehicleController@vehicle_booking_all')->name('/vehicle_booking_all');
//Route::get('/vehicle_booking_spam', 'Admin\bookingvehicleController@index')->name('vehicle_booking_spam');
Route::get('/booking_vehicle_id/{id}', 'Admin\bookingvehicleController@customerview')->name('customerview');
Route::post('/swipevehicle/{id}', 'Admin\bookingvehicleController@swipevehicle')->name('swipevehicle');
Route::post('/adminLogout', 'Admin\HomeController@logout')->name('adminLogout');
Route::group(['prefix' => 'make'], function () {
Route::get('/', 'Admin\makeController@index')->name('adminmakeList');
Route::get('/create', 'Admin\makeController@showAddmake')->name('adminmakeCreate');
Route::get('/edit/{id}', 'Admin\makeController@showEditmake')->name('adminmakeEdit');
Route::get('/adminmakedestory/{id}', 'Admin\makeController@destory')->name('adminmakedestory');
Route::get('/adminRemoveBooking/{id}', 'Admin\bookingvehicleController@destroy')->name('adminRemoveBooking');



Route::post('/adminremovebooking/{id}', 'Admin\bookingvehicleController@destroy')->name('adminremovebooking');

/*For the All booking blade reasons for cancel */
Route::post('/adminremovebooking', 'Admin\bookingvehicleController@destroy')->name('adminremovebooking');



Route::get('/delete/{id}', 'Admin\makeController@destory')->name('destory');
Route::post('/store', 'Admin\makeController@store')->name('adminmakeStore');
Route::post('/update', 'Admin\makeController@update')->name('adminmakeUpdate');
Route::get('/getStates/{id}','Admin\makeController@geStates');
});
Route::group(['prefix' => 'car-models'], function () {
Route::get('/','Admin\carmodelController@index')->name("listcarmodels");
Route::get('/create', 'Admin\carmodelController@create')->name('createcarmodel');
Route::post('/store', 'Admin\carmodelController@store')->name('storecarmodel');
Route::get('/edit/{id}', 'Admin\carmodelController@edit')->name('editcarmodel');
Route::get('/delete/{id}', 'Admin\carmodelController@destory')->name('deletecarmodel');
Route::post('/update', 'Admin\carmodelController@update')->name('updatecarmodel');
});
Route::group(['prefix' => 'year'], function () {
Route::get('/', 'Admin\yearController@index')->name('adminyearList');
Route::get('/create', 'Admin\yearController@showAddyear')->name('adminyearCreate');
Route::get('/edit/{id}', 'Admin\yearController@showEdityear')->name('adminyearEdit');
Route::post('/store', 'Admin\yearController@store')->name('adminyearStore');
Route::post('/update', 'Admin\yearController@update')->name('adminyearUpdate');
});
});
// Route::get('/adminlogin', 'Admin\AdminController@ViewLogin')->name('AdminLoginView');
// Route::post('/adminloginattempt', 'Admin\AdminController@Adminlogin')->name('AdminLogin');
// Route::get('/dashboard', function () {
//     return view('admin.dashboard.dashboard');
// });






// ==================================================== Customer route============
Route::get('/customer_change_password','UserController@change_password')->name('customer_change_password');
Route::get('/customer_profiile', 'UserController@customer_profiile')->name('customer_profiile');
Route::POST('/customer_profile_update', 'UserController@customer_profile_update')->name('customer_profile_update'); 
 
 Route::POST('/password', 'UserController@customerown_change_password')->name('password');
// ==================================================== Partner  route============
Route::get('/partner_profiile', 'Partner\ProfileController@partner_profile')->name('partner_profiile');



Route::POST('/partner_profile_update', 'Partner\ProfileController@partner_profile_update')->name('partner_profile_update');




Route::get('/partner_driver_add', 'Partner\driverController@create')->name('partner_driver_add');

Route::POST('/p_create_driver', 'Partner\driverController@store')->name('p_create_driver');
Route::POST('/partner_add_driver_update', 'Partner\driverController@update')->name('partner_add_driver_update');


Route::get('/partner_driver_list', 'Partner\driverController@list')->name('partner_driver_list');
Route::get('partner/partnerdriverupdate/{id}','Partner\driverController@edit')->name('partnerdriverupdate');
Route::get('partner/partnerdriverdestory/{id}','Partner\driverController@delete')->name('partnerdriverdestory');

 
Route::get('/partner_vehicle_booking','Partner\PartnerController@partner_booking_vehicle')->name('partner_vehicle_booking');
 
Route::get('/partner_change_password','Partner\PartnerController@partner_change_password')->name('partner_change_password');
 
Route::post('/change_password','partner\PartnerController@change_password')->name('change_password');


// Route::get('/booking_vehicle_id/{id}', 'Partner\PartnerController@customerview')->name('customerview');


Route::get('partnerdashboard', 'Partner\dashboardController@partnerdashboard')->name('partnerdashboard');


Route::get('code/generate', 'Admin\DiscountController@generateRandomString')->name('code/generate');
 
Route::post('code/create','Admin\DiscountController@store')->name('code/create');
 
Route::get('code/generate/list','Admin\DiscountController@index')->name('code/generate/list');

Route::get('/delete/{id}','Admin\DiscountController@destory')->name('destory');
Route::get('/admin/edit/{id}', 'Admin\DiscountController@showEditdiscount')->name('admin/discount/edit');

Route::post('codeupdate','Admin\DiscountController@discount_update')->name('codeupdate');

Route::get('bookingView/{id}', 'UserController@bookingView')->name('bookingView');


Route::get('/user_id/{id}', 'Admin\CustomerController@customerview')->name('bookingview');



Route::get('/user_edit/{id}', 'Admin\CustomerController@user_edit')->name('user_edit');
Route::post('/updaet_user', 'Admin\CustomerController@updaet_user')->name('updaet_user');
Route::get('bookingedit/{id}', 'Admin\CustomerController@bookingedit')->name('bookingedit');


/*==============Csutomer=============*/

Route::get('/customer_account','UserController@customer_account')->name('customer_account');
Route::get('/customer_account_password','UserController@customer_account_password')->name('customer_account_password');


Route::post('/customer_updaet_user', 'UserController@user_update')->name('customer_updaet_user');

 Route::post('customer_updaet_user_password','UserController@user_update_password')->name('customer_updaet_user_password');
/*===============admin==============================*/
Route::get('/admin/admin_setting','Admin\SettingController@admin_setting')->name('admin_setting');
Route::post('setting_admin_update','Admin\SettingController@setting_admin_update')->name('setting_admin_update');
Route::get('clearpayment/{id}', 'Admin\bookingvehicleController@clearpayment')->name('clearpayment');



/*======partner p_customerview ====*/
 
Route::get('partner/p_customerview/{id}','Partner\bookingvehicleController@p_customerview')->name('p_customerview');
 
Route::get('admin/accidental_recorded','Admin\AccidentalRecordedController@accidental')->name('admin/accidental_recorded');

Route::post('/store_accidental', 'Admin\AccidentalRecordedController@store')->name('accidentalStore');

Route::get('admin/accidental/recorded/list','Admin\AccidentalRecordedController@index')->name('admin/accidental/recorded/list');

Route::get('admin/accidental/recorded/pratices','Admin\AccidentalRecordedController@pratices')->name('admin/accidental/recorded/pratices');
Route::get('/edit/{id}', 'Admin\AccidentalRecordedController@edit')->name('adminrecordsEdit');
Route::get('admin/get_customers', 'Admin\AccidentalRecordedController@get_customers');



Route::get('/adminrecordsdestory/{id}', 'Admin\AccidentalRecordedController@destroy')->name('adminrecordsdestory');
 
 
Route::post('/update', 'Admin\AccidentalRecordedController@update')->name('adminaccidentalUpdate');


Route::get('/adminrecordsview/{id}', 'Admin\AccidentalRecordedController@customerview')->name('adminrecordsview');


Route::get('/adminrecordsview/{id}', 'Admin\AccidentalRecordedController@customerview')->name('adminrecordsview');

Route::post('/admin_accidental_comment', 'Admin\AccidentalRecordedController@admin_accidental_comment')->name('admin_accidental_comment');
 Route::get('customer_accidental_list', 'UserController@customer_accidental_list')->name('customer_accidental_list');
Route::get('/customerrecordsview/{id}', 'UserController@customerview')->name('customerrecordsview');
Route::post('/customer_accidental_comment', 'UserController@customer_accidental_comment')->name('customer_accidental_comment');

Route::get('/adminsettelmentrecords/{id}','Admin\AccidentalRecordedController@settelment_records')->name('/adminsettelmentrecords');

Route::get('/admindisputerecords/{id}','Admin\AccidentalRecordedController@unsettelment_records')
->name('/admindisputerecords');
Route::get('/vehicle_booking_renew', 'Admin\bookingvehicleController@renew')->name('vehicle_booking_renew');

Route::get('/adminrenewbooking/{id}', 'Admin\bookingvehicleController@renew')->name('adminrenewbooking');

Route::get('/Renew/booking/admin/{id}', 'Admin\RenewBookingController@index')->name('Renew/booking/admin');
 Route::get('/admincommentdestory/{id}', 'Admin\AccidentalRecordedController@commentdestory')->name('admincommentdestory');


 Route::post('/admincommentedit/{id}', 'Admin\AccidentalRecordedController@commentedit')->name('admincommentedit');


 Route::get('/edit/comments/{id}', 'Admin\AccidentalRecordedController@edit_comments')->name('edit/comments');



 Route::get('/edit/comments/customer/{id}', 'UserController@edit_comments')->name('edit/comments/customer');


 
Route::post('/admin/booking/renew', 'Admin\RenewBookingController@index')->name('renewbooking'); // renew booking for admin
Route::get('get_gps_location_milage', 'GpsLocationMilageController@index');  //gps milage calculation
Route::get('get_gps_customer_records', 'GpsLocationMilageController@get_gps_location');  //get gps milage calculation

Route::post('search_customer_records', 'GpsLocationMilageController@search_customer_records')->name('search_customer_records');  //get gps milage calculation

Route::get('vehicle_mileage_login', 'GpsLocationMilageController@customer_vehicle_mileage_login');  //login

















Route::post('/customer/vehicle/mileage', 'GpsLocationMilageController@gps_post_login')->name('gps_post_login'); // renew booking for admin
Route::get('customer_vehicle_mileage_logout', 'GpsLocationMilageController@logout')->name('customer_vehicle_mileage_logout');



 Route::get('customer_expense', 'UserController@customer_expense')->name('customer_expense');



Route::get('customer_swape_req', 'UserController@customer_swape_req')->name('customer_swape_req');


 Route::post('/customer_expense_add','UserController@customer_expense_add')->name('customer_expense_add');

 Route::get('admin/expence/list','Admin\ExpenceController@index')->name('admin/expence/list');


  Route::get('admin/expence/view','Admin\ExpenceController@create')->name('admin/expence/view');
  Route::get('/adminexpensesview/{id}', 'Admin\ExpenceController@show')->name('adminexpensesview');


   Route::get('/adminexpensesprove/{id}', 'Admin\ExpenceController@prove')->name('adminexpensesprove');

// Route::post('/adminexpensesdiscard', 'Admin\ExpenceController@discard')->name('adminexpensesdiscard');
















Route::post('/admin_expence_remark', 'Admin\ExpenceController@discard')->name('admin_expence_remark');
Route::get('/adminrenewbookingpermission/{id}', 'Admin\bookingvehicleController@renew_permission')->name('adminrenewbookingpermission');
Route::get('/adminbookingexpire/{id}', 'Admin\bookingvehicleController@expire')->name('adminbookingexpire');
Route::get('/renewbooking/customer/{id}', 'UserController@renewbooking_customer')->name('renewbooking/customer');

Route::get('customer_expense_list', 'UserController@customer_expense_list')->name('customer_expense_list');




Route::post('/adminexpensesreject', 'Admin\ExpenceController@rejected')->name('adminexpensesreject');






Route::post('/customerexpensesfurtherevidence', 'UserController@customerexpensesfurtherevidence')->name('customerexpensesfurtherevidence');



Route::post('/adminexpensesfurtherevidence', 'Admin\ExpenceController@recorded_further_evidence')->name('adminexpensesfurtherevidence');



Route::get('/adminexpensesprogress/{id}', 'Admin\ExpenceController@inprogress')->name('adminexpensesprogress');
Route::get('/customerexpensesview/{id}', 'UserController@expensesview')->name('customerexpensesview');




Route::get('/customerexpensesedit/{id}', 'UserController@expensesedit')->name('customerexpensesedit');
Route::post('/customer_expense_edit','UserController@customer_expense_update')->name('customer_expense_edit');
Route::get('/customerpaymenthistory/{id}','Admin\PaymentController@index')->name('/customerpaymenthistory');
Route::get('/admin/insurancecompany/add','Admin\InsuranceCompanyController@index')->name('admin/insurancecompany/add');
Route::get('/admin/insurancecompany/list','Admin\InsuranceCompanyController@list')->name('admin/insurancecompany/list');

Route::post('/storeinsurancecompany','Admin\InsuranceCompanyController@store')->name('storeinsurancecompany');


Route::get('admin/insurance_company_edit/{id}','Admin\InsuranceCompanyController@edit')->name('admin/insurance_company_edit');


Route::post('/updateinsurancecompany','Admin\InsuranceCompanyController@update')->name('updateinsurancecompany');


Route::get('admin/staff/add','Admin\StaffController@create')->name('admin/staff/add');
Route::get('admin/staff/list','Admin\StaffController@index')->name('admin/staff/list');
Route::post('/store_staff','Admin\StaffController@store')->name('store_staff');
Route::get('adminstaffview/{id}','Admin\StaffController@show')->name('adminstaffview');
Route::get('adminstaffedit/{id}','Admin\StaffController@edit')->name('adminstaffedit');




Route::get('admin/purcheas/add','Admin\PurchaseController@create')->name('admin/purcheas/add');
Route::get('admin/purcheas/list','Admin\PurchaseController@index')->name('admin/purcheas/list');
Route::post('/store_purchase','Admin\PurchaseController@store')->name('store_purchase');
Route::get('adminpurchaseedit/{id}','Admin\PurchaseController@edit')->name('adminpurchaseedit');
Route::get('adminpurchaseshow/{id}','Admin\PurchaseController@show')->name('adminpurchaseshow');

Route::post('admin/update_purchase', 'Admin\PurchaseController@update')->name('update_purchase');
Route::post('admin/updatestaff', 'Admin\StaffController@update')->name('updatestaff');
Route::post('customer_swap_req_send', 'UserController@customer_swap_req_send')->name('customer_swap_req_send');
Route::get('customer_swape_show','UserController@customer_swap_req_show')->name('customer_swape_show');
Route::get('admin/invoice/add','Admin\ExpenceController@invoice_add')->name('admin/invoice/add');
Route::get('admin/invoice/show','Admin\ExpenceController@invoice_show')->name('admin/invoice/show');
Route::post('add_invoice', 'Admin\ExpenceController@add_invoice')->name('add_invoice');
Route::get('admininvoiceshow/{id}','Admin\ExpenceController@invoice_show_detail')->name('admininvoiceshow');
Route::get('/admin/admin_notification','Admin\NotificationController@index')->name('admin_notification');
Route::get('/notificationview/{id} ','Admin\NotificationController@show')->name('notificationview');
Route::get('admin/get_vehicle', 'Admin\bookingvehicleController@vehicle_details');
Route::get('total_vehicle_pco_available','Admin\vehicleController@available_car')->name('total_vehicle_pco_available');
Route::get('total_vehicle_private_available','Admin\vehicleController@total_vehicle_private_available')->name('total_vehicle_private_available');




Route::get('admin/pcn_management/add','Admin\PCNManagementController@create')
->name('admin/pcn_management/add');

 Route::get('admin/pcn_management/list','Admin\PCNManagementController@index')->name('admin/pcn_management/list');
// Route::get('admin/get_customers/pcn','Admin\PCNManagementController@get_customers');
Route::post('add_pcn', 'Admin\PCNManagementController@store')->name('add_pcn');
Route::get('adminpcnedit/{id}', 'Admin\PCNManagementController@edit')->name('adminpcnedit');
Route::post('update_pcn', 'Admin\PCNManagementController@update')->name('update_pcn');
Route::get('adminpcnview/{id}', 'Admin\PCNManagementController@show')->name('adminpcnview');
 



Route::get('admin/pcn/get_customers','Admin\PCNManagementController@get_customers');
//Download the PCN Hire intercity Letter 

Route::get('/generate_pdf_pcn_letter/{id}', 'Admin\PCNManagementController@generate_pdf_pcn_letter')->name('generate_pdf_pcn_letter');


Route::post('/admineditpcnletter', 'Admin\PCNManagementController@pcn_letter_edit')->name('admineditpcnletter');




Route::get('/generate_report_mileage', 'GpsLocationMilageController@generate_report_pcn')->name('generate_report_mileage');




<<<<<<< HEAD
Route::get('partner_profiile_view', 'Partner\ProfileController@partner_profile_view')->name('partner_profiile_view');
=======
  Route::get('partner/p_recordsEdit/{id}', 'Partner\DamageController@edit')->name('p_recordsEdit');
  Route::get('/p_recordsdestory/{id}', 'Partner\DamageController@destroy')->name('p_recordsdestory');

Route::get('/p_recordsview/{id}', 'Partner\DamageController@show')->name('p_recordsview');
>>>>>>> 9c370ab2e9863d7207f81ef8fe16cd608985da92
