<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('frontend.main');
});



//customer login and signup 
Route::get('/login', function () {
    return view('customer.login');
});
//Route::post('/usersignup', 'UserController@signup')->name('usersignup');
Route::post('/customerlogin', 'UserController@customerlogin')->name('customerlogin');
Route::get('/customerdashboard', 'UserController@customerdashboard')->name('customerdashboard'); //dashbaord
Route::post('/customerLogout', 'UserController@customerLogout')->name('customerLogout');
//===customer login routs end



Route::get('/browse', function () {
    return view('frontend.browse');
});






Route::get('/temp_delete_img/{id}', 'Admin\vehicleController@temp_delete_img')->name('temp_delete_img');  // image gallary remove


Route::post('/upload_information', 'bookingprocessController@upload_information')->name('upload_information');


Route::get('/browse', 'searchController@index')->name('browse');   //when click at browser link this rout execute
Route::get('/get_models', 'searchController@get_models');         //ajax call for get moted at selection of make
Route::get('/get_all_vehicles', 'searchController@get_all_vehicles'); 

Route::get('/calculate_booking', 'searchController@calculate_booking'); 

 

Route::get('/contactus', 'searchController@contactus')->name('contactus');
Route::get('/generate_pdf', 'bookingprocessController@generate_pdf')->name('generate_pdf');

Route::get('/booking_done', 'bookingprocessController@done')->name('booking_done');

Route::get('/car_detail', 'browseController@car_detail')->name('car_detail'); 
Route::get('/booking', 'bookingprocessController@index')->name('booking');   //first step
Route::get('/booking2', 'bookingprocessController@step2')->name('booking2');   //second step
//Route::get('/booking3', 'bookingprocessController@step3')->name('booking3');   //third step document download agreement
Route::post('/booking4', 'bookingprocessController@step4')->name('booking4');   //payment

Route::get('/generate_pdf_email/{id}', 'bookingprocessController@generate_pdf_email')->name('generate_pdf_email');


Route::get('done', 'bookingController@done')->name('done');
//Route::post('booking/upload_information', 'bookingController@upload_information')->name('upload_information');
//Route::post('booking/temp_upload_img', 'bookingController@temp_upload_img')->name('temp_upload_img');



Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');


Route::post('confirm_booking', 'bookingprocessController@confirm_booking')->name('confirm_booking');


//user signup and login etc.

Route::post('/usersignup', 'UserController@signup')->name('usersignup');
Route::post('/userlogin', 'UserController@login')->name('userlogin');


// Route::get('/AdminLogin', function () {
//     return view('admin.login');
// });

Route::get('/adminlogin', function () {
    if(Auth::guard('admin')->check()){
        return redirect()->route('adminDashboard');
    }
    return view('admin.login');
})->name('showAdminLogin');

Route::post('/adminlogin', 'Admin\HomeController@login')->name('AdminLogin');
Route::post('/contactus', 'ContactController@contact')->name('contactus');

Route::get('/customer_vehicleList', 'UserController@customer_vehicle')->name('customer_vehicleList');
 
Route::get('/pco_car', 'UserController@pco_car')->name('pco_car');
Route::get('/private_car', 'UserController@private_car')->name('private_car');






Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/dashboard', 'Admin\dashboardController@index')->name('adminDashboard');

    Route::get('/vehiclesList', 'Admin\vehicleController@index')->name('vehicleList');
    Route::get('/add_vehicle', 'Admin\vehicleController@create')->name('vehicleCreate');
    Route::post('/store_vehicle', 'Admin\vehicleController@store')->name('vehicleStore');
    Route::get('/edit_vehicle/{id}', 'Admin\vehicleController@edit')->name('vehicleEdit'); //show edit blade 
    Route::post('/update_vehicle', 'Admin\vehicleController@update')->name('vehicleUpdate');
    
    Route::get('/destory_vehicle/{id}', 'Admin\vehicleController@destory')->name('vehicleDestory');
   // Route::get('/booking', 'bookingController@booking_list_admin')->name('booking');





    Route::post('/adminLogout', 'Admin\HomeController@logout')->name('adminLogout');


    Route::group(['prefix' => 'make'], function () {
        Route::get('/', 'Admin\makeController@index')->name('adminmakeList');
        Route::get('/create', 'Admin\makeController@showAddmake')->name('adminmakeCreate');
        Route::get('/edit/{id}', 'Admin\makeController@showEditmake')->name('adminmakeEdit');
        Route::post('/store', 'Admin\makeController@store')->name('adminmakeStore');
        Route::post('/update', 'Admin\makeController@update')->name('adminmakeUpdate');
        Route::get('/getStates/{id}','Admin\makeController@geStates');

    });

    Route::group(['prefix' => 'car-models'], function () {
        Route::get('/','Admin\carmodelController@index')->name("listcarmodels");
        Route::get('/create', 'Admin\carmodelController@create')->name('createcarmodel');
        Route::post('/store', 'Admin\carmodelController@store')->name('storecarmodel');
        Route::get('/edit/{id}', 'Admin\carmodelController@edit')->name('editcarmodel');
        Route::post('/update', 'Admin\carmodelController@update')->name('updatecarmodel');

    });

    Route::group(['prefix' => 'year'], function () {
        Route::get('/', 'Admin\yearController@index')->name('adminyearList');
        Route::get('/create', 'Admin\yearController@showAddyear')->name('adminyearCreate');
        Route::get('/edit/{id}', 'Admin\yearController@showEdityear')->name('adminyearEdit');
        Route::post('/store', 'Admin\yearController@store')->name('adminyearStore');
        Route::post('/update', 'Admin\yearController@update')->name('adminyearUpdate');

    });

});

// Route::get('/adminlogin', 'Admin\AdminController@ViewLogin')->name('AdminLoginView');
// Route::post('/adminloginattempt', 'Admin\AdminController@Adminlogin')->name('AdminLogin');

// Route::get('/dashboard', function () {
//     return view('admin.dashboard.dashboard');
// }); 